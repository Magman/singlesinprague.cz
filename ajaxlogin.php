<?php
session_start();

require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

require_once './include/parameter.php';

$sent = 0;

if (isset($_POST['login'])) {

  switch ($lang) {
    case 1: $phFill = 'Fill log in, please!'; 
    $phNoUser = 'No users in system.'; 
    $phNoAccount = 'No active account for this e-mail!'; 
    $phPass = 'Password is incorrect or you did not confirm your registration. See registration e-mail.'; 
    $phSuccess = 'OK::Your log in was succesfull.::'; 
    $phEmail = 'The format of the e-mail is incorrect!'; 
    break;
    case 2: $phFill = 'Vyplňte přihlašovací údaje!'; 
    $phNoUser = 'V systému nejsou žádní uživatelé!'; 
    $phNoAccount = 'K e-mailu se neváže aktivní uživatelský účet!'; 
    $phPass = 'Heslo není správné nebo jste nepotvrdili Vaší registraci! Podívejte se do registračního e-mailu.'; 
    $phSuccess = 'OK::Vaše přihlášení bylo úspěšné.::'; 
    $phEmail = 'E-mail není ve správném tvaru!'; 
  }
    
  if (isset($_POST['login'])) $email = $_POST['login']; else $email = null;
  if (isset($_POST['personaly'])) $personaly = $_POST['personaly']; else $personaly = null;
 
  if (empty($email) or empty($personaly)) {
    
    echo $phFill;
  
  } else if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $email)) {
    
    echo $phEmail; 

  } else {
       
    $result = mysqli_query($db_connect, "select email from registration where active = 1");
    
    while ($row = mysqli_fetch_array($result)) {
      
      $mailArr[] = $row['email'];
    
    }    
    
    if ($mailArr != null) {  
      
      if (in_array($email, $mailArr)) 
      {
        
        foreach ($mailArr as $m) {
        
          if ($m == $email) {
          
            $result = mysqli_query($db_connect, "select uid, personaly from registration where email = '".mysqli_real_escape_string($db_connect, $email)."' and active = 1 and confirm = 1 limit 0,1");
            
            $row = mysqli_fetch_array($result); 
            
            if ($row['personaly'] == md5($personaly)) {           
                                            
              $_SESSION['user'] = $row['uid'];
                            
              echo $phSuccess;
              
              $sent = 1;
            
            } else {
            
              echo $phPass;
            
            }
          
          } 
        
        }
        
      } else {
      
        echo $phNoAccount;
      
      }
    
    } else {
      
      echo $phNoUser;
      
    }
  
  }

}

if ($sent == 1) { ?>
  <script type="text/javascript">
    $('.formOrdLog').clearForm();   
    location.reload(); 
  </script>
<?php } ?>