<?php
require_once './function/function_connect.php';  
connect();
$id = 0;
$result = mysqli_query($db_connect, "select id from email");
while ($row = mysqli_fetch_array($result)) {
  if ($_GET['mov'] == md5($row['id'])) {
    $id = $row['id'];
  }
}
if ($id != 0) {
  $del = mysqli_query($db_connect, "delete from email where id = $id limit 1");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $xmlLang; ?>" lang="<?php echo $xmlLang; ?>">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo $description; ?>" />
    <meta name="keywords" content="Singles in Prague" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/main.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/color.css" />
    <title><?php echo $title; ?></title>
    <style type="text/css">
      body {background:none !important; }
    </style>      
  </head>
  <body>       
    <div class="centerer">              
      <div class="heaDer">          
        <a href="index.php" title="home"><img class="logo" src="./design/logo.png" alt="logo" /></a>                                              
      </div>
      <div class="confirm">
        <?php 
        if ($del) {
          if ($_GET['lang'] == '2') {
            echo 'Byli jste úspěšně odhlášeni z odběru newsletterů.';
          } else {
            echo 'You have successfully unsubscribed from our database.';
          }
        } else {
          echo 'Error!';
        }
        ?>
      </div>
      <div class="footer smallFont">
        <div class="footerIn">        
          <div class="copy">
            &copy; Singles in Prague 2016 | powered by <a class="newTab" href="http://www.easyweb4u.cz" title="easyWeb4U">easyWeb4U.cz</a>
          </div>  
        </div>
      </div>       
    </div>     
  </body>
</html>