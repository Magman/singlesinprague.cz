<?php
session_start();

// require_once './facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php';
// https://developers.facebook.com/docs/php/gettingstarted

// phpinfo();

require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

require_once './include/parameter.php';

$uid = 0;

if (isset($_SESSION['user'])) {
  
  $uid = $_SESSION['user'];

}

if (isset($_GET['id'])) { // id menu
  
  $id = $_GET['id']; 

} else {
  
  $id = 1; 

}
require_once './lib/Text/Text.php';
$text = new Text ($db_connect, $id, $lang);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $xmlLang; ?>" lang="<?php echo $xmlLang; ?>">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo $text->showDescription(); ?>" />
    <meta name="keywords" content="<?php echo $text->showKeywords(); ?>" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/main_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/color_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/css/lib.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/SwitcherLanguage/css/switcher_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Menu/css/menu_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Message/css/message.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/jquery.showmore.css" />
    <link rel="stylesheet" type="text/css" href="./css/jquery.lightbox-0.5.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin-ext" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script&amp;subset=latin-ext" rel="stylesheet" />
    <title><?php echo $text->showTitle(); ?></title>          
    <script type="text/javascript">
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script> 
    <script type="text/javascript" src="./js/jquery1.10.2.js"></script>
    <script type="text/javascript" src="./js/cookies.js"></script>
    <script type="text/javascript" src="./js/ajaxform.js"></script>
    <script type="text/javascript" src="./js/ajaxform_order.js"></script> 
    <script type="text/javascript" src="./js/ajaxform_registration.js"></script>
    <script type="text/javascript" src="./js/ajaxform_newpass.js"></script>
    <script type="text/javascript" src="./js/ajaxform_ownpass.js"></script>
    <script type="text/javascript" src="./js/ajaxform_login.js"></script> 
    <script type="text/javascript" src="./js/ajaxform_logout.js"></script>  
    <script type="text/javascript" src="./js/jquery.clearform.js"></script>
    <script type="text/javascript" src="./js/jquery.accordion.js"></script>
    <script type="text/javascript" src="./js/jquery.eventlink.js"></script>
    <script type="text/javascript" src="./js/jquery.formorder.js"></script>
    <script type="text/javascript" src="./js/jquery.formreg.js"></script>
    <script type="text/javascript" src="./js/jquery.newtab.js"></script>
    <script type="text/javascript" src="./js/jquery.adver.js"></script>
    <script type="text/javascript" src="./js/jquery.window.js"></script>
    <script type="text/javascript" src="./js/jquery.browser.js"></script> 
    <script type="text/javascript" src="./js/jquery.showmore.js"></script> 
    <script type="text/javascript" src="./js/jquery.singlepage_new.js"></script>
    <script type="text/javascript" src="./js/jquery.slideparagraph.js"></script>
    <script type="text/javascript" src="./js/menu.js"></script> 
    <script type="text/javascript" src="./js/jquery.lightbox-0.5.js"></script>
    <script type="text/javascript">
      $(function() {
      	$('#gallery a').lightBox({fixedNavigation:true});
      });
    </script> 
    <?php 
    if (isset($_GET['it'])) { 
      $item = intval($_GET['it']); ?>
      <script type="text/javascript">
        $(document).ready(function(){ // from galleries
          $blockFrom = '#item' + <?php echo $item; ?>;
          $hId = parseInt($($blockFrom).position().top);
          window.scrollTo(0,$hId);
        });
      </script>
    <?php } ?> 
    <?php 
    if ($lang == 1) { ?>
        <script type="text/javascript">
                //$(document).ready(function(){
                    var moretext = "Show more &gt;";
                    var lesstext = "Hide";
                //});
        </script>
    <?php } ?>
    <?php 
    if ($lang == 2) { ?>
        <script type="text/javascript">
                //$(document).ready(function(){
                    var moretext = "Ukázat více &gt;";
                    var lesstext = "Skrýt";
                //});
        </script>
    <?php } ?>
    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');    
      ga('create', 'UA-75996534-1', 'auto');
      ga('send', 'pageview');    
    </script>    
  </head>
  <body>       
    <div class="centerer">              
      <div class="heaDer">          
        <a href="index.php" title="home"><img class="logo" src="./design/logo_sip.png" alt="logo" /></a>                                        
        <div class="coverMenuJs">  
          <div class="menuJs2">
            <?php 
            require_once './lib/Menu/MenuTop.php';                          
            $menu = new MenuTop ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu; $xmlLang, $parHotel kvůli booking, $path ../ or ./ menu mutace 
            echo $menu->render (); 
            ?>      
          </div> <!-- .menu --> 
        </div>       
        <div id="navButton"><img src="./design/nav_button_new.gif" alt="menu" title="menu" /></div>
        <div id="navigation">        
          <div id="menuVer">
            <?php
            require_once './lib/Menu/MenuTopVer.php'; 
            $menu = new MenuTopVer ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu 
            echo $menu->render(); 
            ?>
          </div>  
        </div>
        <div class="coverUser"> <!-- + main.css, jquery.formreg.js -->
          <div class="userReg buttonSubmit">
            <?php
            switch ($lang) {
              case 1: $phReg = 'Sign up';
              break;
              case 2: $phReg = 'Registrace'; 
            }
            echo $phReg;
            ?>
          </div> 
          <div class="userLog buttonSubmit">
            <?php
            switch ($lang) {
              case 1: $phLog = 'Log in';
              break;
              case 2: $phLog = 'Přihlášení'; 
            }
            echo $phLog;
            ?>
          </div>            
        </div> 
        <div class="switcher">
          <?php         
          //$parHotelBu = $parHotel;
          require_once './lib/SwitcherLanguage/SwitcherLanguage.php';
          if (isset($_GET['album'])) $idA = $_GET['album']; else $idA = 0;
          $switcher = new SwitcherLanguage ($db_connect, $lang, $id, $link = 'index.php', $idA, $path = './');
          $switcher->showLanguages ($parHotel = null);
          //$parHotel = $parHotelBu;
          ?> 
        </div> <!-- .switcher -->       
      </div>                          
      <div class="slogan">
        <?php
        switch ($lang) {
          case 1: $sloganMain = 'Events for Singles in Prague,<br />';
          $sloganSub = '<br />Our Networking Events are oriented towards Singles who are 30 and older. We have events for both Czech and English speaking Singles, 30+ and 45+. Join us and we will find you a partner - Matchmaking and Consultancy.';
          break;
          case 2: $sloganMain = 'Singles in Prague,<br />seznamka pro nezadané v Praze.';
          $sloganSub = '<br />Organizujeme seznamovací večírky v českém a anglickém jazyce pro nezadané ve věku 30+ a 45+. Najdeme Vám partnera dle Vašich požadavků - seznámení na míru a poradenství.';
        }
        ?>       
        <h1><?php echo $sloganMain; ?></h1>
        <h2><?php echo $sloganSub; ?></h2>
      </div>
      <img class="slider" src="./design/slider1.gif" alt="slider" />
      <div class="coverContent">      
        <div class="contentIn">         
            <?php        
            if ($id == 1) {  
              // banners     
              require_once('./lib/Event/ShowEvent.php');
              $event = new ShowEvent($db_connect);

              $now = date('Y-m-d');

              $result = mysqli_query($db_connect, "select * from events where dateEvent >= '$now' order by dateEvent asc");

              if (mysqli_num_rows($result) > 0) {

                echo '<div class="forEn">';

                  $event->event($limit = 1, $lang);          

                echo '</div>';

              }

              if (mysqli_num_rows($result) > 1) {

                echo '<div class="forCs">'; 

                  $event->event($limit = 2, $lang);

                echo '</div>'; 

              }

            } 

            switch ($lang) {          
              case 1: $phBack = 'back'; $phFb = 'You can receive a 10% discount on your entry if you follow us on the social media.';
              break;
              case 2: $phBack = 'nahoru'; $phFb = 'Získejte 10% slevu na Vašem vstupu, když nás následujete na sociálních médiích.';       
            }

            echo '<div class="content" id="item1">';  

              $text = new Text ($db_connect, $id = 1, $lang);
              echo '<div class="more">' . $text->showText() . '</div>'; 

              echo '<div class="video">';
              
                echo '<iframe src="https://www.youtube.com/embed/uXBVjkYYgBw" frameborder="0" allowfullscreen></iframe>';
                echo '<iframe src="https://www.youtube.com/embed/VgsWk8OvYJc" frameborder="0" allowfullscreen></iframe>';
              
              echo '</div>';
              
              echo '<p class="back"><a href="index.php" title="back"><button class="buttonSubmit">'.$phBack.'</button></a></p>';

              echo '<h3>'.$phFb.'</h3>'; 

              echo '<table class="socMedia">';

                echo '<tr>';

                  echo '<td>';

                    echo '<div class="fb-like" data-href="https://www.facebook.com/singlesinpraguecz" data-send="false" data-layout="button_count" data-width="70" data-show-faces="false"></div>';  

                  echo '</td>';

                  echo '<td>';

                    echo '<a class="newTab" href="https://www.instagram.com/singlesinprague/" title="Instagram"><img src="./design/instagram.png" alt="Instagram" /></a>';

                  echo '</td>';

                  echo '<td>';

                    echo '<a class="newTab" href="https://twitter.com/SinglesInPrague" title="Twitter"><img src="./design/twitter.png" alt="Twitter" /></a>';

                  echo '</td>';

                  echo '<td>';

                    echo '<a class="newTab" href="https://www.linkedin.com/company/9188560?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9188560%2Cidx%3A2-1-2%2CtarId%3A1468259458960%2Ctas%3Asingles%20in%20prague" title="LinkedIn"><img src="./design/linkedin.png" alt="LinkedIn" /></a>';

                  echo '</td>';

                echo '</tr>';

              echo '</table>';          

            echo '</div>';

            // Events        
            echo '<div class="content" id="item3">';  

              require_once('./lib/Event/DetailEvent.php');
              $detailEvent = new DetailEvent($db_connect, $lang);

              $detailEvent->detEvent($uid);

              echo '<p class="back"><a href="index.php" title="back"><button class="buttonSubmit">'.$phBack.'</button></a></a></p>';

            echo '</div>';

            $result = mysqli_query($db_connect, "select distinct text_1.idMenu from text_1, menu_top_1 where menu_top_1.active != 0 and text_1.idMenu = menu_top_1.id and (menu_top_1.id > 1 and menu_top_1.id != 3) order by menu_top_1.ord asc");

            while ($row = mysqli_fetch_array($result)) {

              echo '<div class="content" id="item'.$row['idMenu'].'">';  

                if ($row['idMenu'] == 2) {

                  switch ($lang) {
                    case 1: $phGallery = 'Galleries';
                    break;
                    case 2: $phGallery = 'Galerie';
                  }

                  echo '<h1>'.$phGallery.'</h1>';

                  echo '<div class="albums">';

                    $albums = mysqli_query($db_connect, "select gallery_album_1.id, gallery_album_1.idEvent from gallery_album_1, events where gallery_album_1.idEvent = events.id order by events.dateEvent desc");

                    while ($abs = mysqli_fetch_array($albums)) {

                      $images = mysqli_query($db_connect, "select nameImage from gallery where idAlbum = {$abs['id']} order by dateInsert desc limit 0,1");
                      $img = mysqli_fetch_array($images);

                      echo '<div class="album">';

                        $tb = 'gallery_album_'.$lang;

                        $title = mysqli_query($db_connect, "select item from `$tb` where id = {$abs['id']}");
                        $tt = mysqli_fetch_array($title);

                        $file = './gallery/' .$abs['id'] . '/thumb_' . $img['nameImage'];
                        $imgclass = 'landscape';
                        
                        if (substr($file, -3) == 'peg') {
												
													$image = imagecreatefromjpeg($file);
													
												} else if (substr($file, -3) == 'png') {
													
													$image = imagecreatefrompng($file);
												} else if (substr($file, -3) == 'gif') {
													
													$image = imagecreatefromgif($file);
												}   
                        
                        $marginLeft = intval(((imagesx($image) / imagesy($image) * 170) - 170) / 2);
                        
                        if (imagesx($image) <= imagesy($image)) {
                           
                           $file = './gallery/' .$abs['id'] . '/' . $img['nameImage'];
                           $imgclass = 'portrait';
                           $marginLeft = 0;
                        }
                        
                        echo '<div class="coverImg">'; 
                        
                            echo '<a href="gallery.php?album='.$abs['id'].'&amp;id=2" title="'.$tt['item']. '"><img style="margin-left:-'.$marginLeft.'px" class="' .  $imgclass . '" src="' . $file . '" alt="'.$tt['item'].'" /></a>';

                        echo '</div>'; 
                        
                        echo '<div class="albumName">'; 

                          echo  $tt['item'];

                        echo '</div>'; 

                      echo '</div>';

                    }

                  echo '</div>';

                } else {

                  
									if ($row['idMenu'] == 102) {
									
										$text = new Text ($db_connect, $id = $row['idMenu'], $lang);
										echo '<table class="tbRef">';
											echo '<tr>';
												echo '<td class="prev"><br />&lt;</td>';
												echo '<td class="references"><em>' . $text->showText() . '</em></td>';
												echo '<td class="next"><br />&gt;</td>';
											echo '</tr>';
										echo '</table>';										
									} else {
									
										$text = new Text ($db_connect, $id = $row['idMenu'], $lang);
										echo '<div class="more">' . $text->showText() . '</div>';

									}
									
								}            
                
                echo '<p class="back"><a href="index.php" title="back"><button class="buttonSubmit">'.$phBack.'</button></a></p>';

              echo '</div>';

            }      
            ?>
        </div>
      </div>
      <div class="footer smallFont">
        <div class="footerIn">        
          <div class="copy">
            <?php 
            switch ($lang) {
              case 1: $phDiscount = '<h2>50%off if you bring a friend</h2>'; $cond = 'Terms and Conditions';
              break;
              case 2: $phDiscount = '<h2>50% sleva, když přivedete kamaráda</h2>'; $cond = 'Obchodní podmínky';
            }
            ?>
            &copy; Singles in Prague 2016 | powered by <a class="newTab" href="http://www.easyweb4u.cz" title="easyWeb4U">easyWeb4U.cz</a> | <span class="cond"><?php echo $cond ?></span>
          </div> 
          <div class="links">
            <a class="newTab" href="http://hitfit.cz" title="hitfit.cz">hitfit.cz</a> | <a class="newTab" href="http://partysaurus.cz" title="partysaurus.cz">partysaurus.cz</a> |
            <a class="newTab" href="http://djviktor.cz" title="djviktor.cz">djviktor.cz</a> | <a class="newTab" href="http://graphicproduction.cz" title="graphicproduction.cz">dgraphicproduction.cz</a> |
          </div> 
        </div>
      </div> 
      <div class="conditionFoo">
        <img id="delWin" src="./design/close_form_new.png" alt="hide" />
        <?php 
        $text = new Text ($db_connect, $id = 105, $lang);
        echo '<div class="more">' . $text->showText() . '</div>'; 
        ?>
      </div>  
      
      <div class="formLogin">
      
          <div class="coverClose"><div class="formTitle"><h3><?php echo $phLog; ?></h3></div><img class="closeFormLog" src="./design/close_form_new.png" alt="close" /></div>
        
        <?php if (!isset($_SESSION['user'])) { ?>  
        
          <form class="formOrdLog" method="post" action="">
          
            <table> 
                                    
              <?php
              switch ($lang) {
                case 1: $phEmail = 'E-mail'; $phPass = 'Password'; $phSend = 'Send'; $phForgot = 'Forgot your password?'; $phNew = 'New password will be send on Your e-mail.'; $phLogout = 'Log out.';
                break;
                case 2: $phEmail = 'E-mail'; $phPass = 'Heslo'; $phSend = 'Odeslat'; $phForgot = 'Zapomněli jste heslo?'; $phNew = 'Nové heslo bude odesláno na Váš e-mail.'; $phLogout = 'Odhlásit.';
              }      
              ?>  
              
              <tr><td colspan="2"><p class="ajaxLog"></p></td></tr> 
              
              <tr><td class="tdFormReg"><strong><?php echo $phEmail; ?>:</strong> </td><td><input class="inpReserve" type="text" name="login" value="" /></td></tr>
                              
              <tr><td><strong><?php echo $phPass; ?>:</strong> </td><td><input class="inpReserve" type="password" name="personaly" value="" /></td></tr> 
              
              <tr><td colspan="2"><input class="sbmReserveLog buttonSubmit" type="submit" value="<?php echo $phSend; ?>" /></td></tr>
              
              <tr><td colspan="2" class="cursorP"><span id="changeForgot"><?php echo $phForgot; ?></span></td></tr>
              
              <tr><td colspan="2" class="cursorP"><span id="changeForm"><?php echo $phReg; ?></span></td></tr>
                                    
            </table>
                                          
          </form>
          
          <div class="formForgot">  
            
            <form class="formOrdForgot" method="post" action="">
            
              <input type="hidden" name="newpass" value="1" />
              
              <table>
              
                <tr><td colspan="2"><p class="ajaxForgot"></p></td></tr>
                
                <tr><td colspan="2"><?php echo $phNew; ?></td></tr>
                
                <tr><td class="tdFormReg"><strong><?php echo $phEmail; ?>:</strong> </td><td><input class="inpReserve" type="text" name="forgot" value="" /></td></tr>
                
                <tr><td colspan="2"><input class="sbmReserveForgot buttonSubmit" type="submit" value="<?php echo $phSend; ?>" /></td></tr>
                                 
              </table>
              
            </form>
            
          </div>
          
        <?php } else {
      
          switch ($lang) {
            case 1: $phLogout = 'log out'; $phUser = 'User'; $phOther = 'New events'; $phOwn = 'Change password'; $phSend = 'Send'; $phNewPass = 'New password'; $phNewPassAgain = 'New password again'; 
            break;
            case 2: $phLogout = 'odhlásit'; $phUser = 'Uživatel'; $phOther = 'Nové akce'; $phOwn = 'Změnit heslo';  $phSend = 'Odeslat'; $phNewPass = 'Nové heslo'; $phNewPassAgain = 'Nové heslo znovu'; 
          }    
          
          $result = mysqli_query($db_connect, "select * from registration where uid = $uid limit 0,1");
          
          $row = mysqli_fetch_array($result);
          
          echo '<div class="logged">';
            
            echo '<p class="ajaxForgot"></p>';
            
            echo '<p>'.$phUser.': '.$row['name'].' '.$row['surname'].'</p>';
            
            echo '<form class="formOrdLogout" method="post" action="">';
            
              echo '<input type="hidden" name="logout" value="1" />';
              
              echo '<input class="sbmReserveLogout buttonSubmit" type="submit" value="'.$phLogout.'" />';
                 
            echo '</form>';      
      
            echo '<p class="pChangePass">'.$phOwn.'</p>'; 
            
            echo '<p class="ajaxOwnPass"></p>';          
                       
            echo '<form class="formOwnPass" method="post" action="">';
              
              echo '<input type="hidden" name="uid" value="noveheslo" />';
              
              echo '<table>';
                
                echo '<tr><td>'.$phNewPass.'</td><td><input type="password" name="new" value="noveheslo" size="10" /></td></tr>';
                
                echo '<tr><td>'.$phNewPassAgain.'&nbsp;&nbsp;</td><td><input type="password" name="valid" size="10" /></td></tr>';
                
                echo '<tr><td><input class="sbmOwnPass buttonSubmit" type="submit" value="'.$phSend.'" /></td><td></td></tr>';
              
              echo '</table>';
            
            echo '</form>';      
              
          echo '</div>';          
          
          // EVENTS
              
          $now = date('Y-m-d');
          
          $result = mysqli_query($db_connect, "select * from orders where dateEvent >= '$now' and uid = $uid order by dateEvent asc");
          
          if (mysqli_num_rows($result) > 0) {
          
            switch ($lang) {
              case 1: $phEvent = 'Your reservations';
              break;
              case 2: $phEvent = 'Vaše rezervace';    
            }
          
            $i = 0;
            
            echo '<table>';
              
              echo '<tr><td><h3>'.$phEvent.'</h3></td></tr>'; 
              
            echo '</table>';     
            
            while ($row = mysqli_fetch_array($result)) {  
              
              $i++;
                
              echo '<table>';
                
                $date = date("j. n. Y", mktime(0,0,0,substr($row['dateEvent'], 5, 2), substr($row['dateEvent'], 8, 2), substr($row['dateEvent'], 0, 4)));
                  
                $title = mysqli_query($db_connect, "select id from event_1 where idEvent = {$row['idEvent']}");
                  
                $tt = mysqli_fetch_array($title);
                  
                $idE = $tt['id'];
                  
                $tb = 'event_'.$lang;
                  
                $title = mysqli_query($db_connect, "select title from `$tb` where id = $idE");
                  
                $tt = mysqli_fetch_array($title);
                              
                $categ = mysqli_query($db_connect, "select category from events where id = {$row['idEvent']}");
                  
                $ctg = mysqli_fetch_array($categ);
                  
                $tb = 'category_'.$lang;
                  
                $categ = mysqli_query($db_connect, "select category from `$tb` where id = {$ctg['category']}");
                  
                $ctg = mysqli_fetch_array($categ);
                  
                echo '<tr><td>'.$date.', '.$row['hour'].':'.$row['minute'].'</td></tr>';
                  
                echo '<tr><td><h4>'.$tt['title'].'</h4></td></tr>';
                  
                echo '<tr><td>'.$ctg['category'].'</td></tr>';
                  
                echo '<tr><td>'.$row['place'].'</td></tr>';
                  
                echo '<tr><td></td></tr>';
                
              echo '</table>';
            
            }
          
          } 
          
          echo '<p class="other"><a href="index.php?id=1&amp;it=3" title="'.$phOther.'">'.$phOther.'</a></p>';   
        
        } ?>
      
      </div>

      <div class="formRegistration">
        
        <div class="coverClose"><div class="formTitle"><h3><?php echo $phReg; ?></h3></div><img class="closeFormReg" src="./design/close_form_new.png" alt="close" /></div>
        
        <form class="formOrdReg" method="post" action="">
      
          <table> 
                            
            <?php
            switch ($lang) {
              case 1: $phName = 'Name'; $phPhone = 'Phone'; $phSurname = 'Surname'; $phDateBirth = 'Year of Birth'; $phMale = 'Male';  $phFemale = 'Female';  $phAgree = 'I agree with terms and conditions'; $phCat = 'Category'; $phCode = 'Rewrite the control code'; $phSend = 'Send';
              break;
              case 2: $phName = 'Jméno'; $phPhone = 'Telefon'; $phSurname = 'Příjmení';  $phDateBirth = 'Rok narození'; $phMale = 'Muž'; $phFemale = 'Žena'; $phAgree = 'Souhlasím s obchodními podmínkami'; $phCat = 'Kategorie';  $phCode = 'Opište kontrolní kód'; $phSend = 'Odeslat';   
            }      
            ?>            
                    
            <tr><td colspan="2"><p class="ajaxReg"></p></td></tr>
                              
            <tr><td class="tdFormReg"><label><strong><?php echo $phName; ?>:</strong></label> </td><td><input class="inpReserve" type="text" name="name" value="" /></td></tr>
                      
            <tr><td><label><strong><?php echo $phSurname; ?>: </strong></label></td><td><input class="inpReserve" type="text" name="surname" value="" /></td></tr>
                                
            <tr><td><label><strong>E-mail: </strong></label></td><td><input class="inpReserve" type="text" name="email" value="" /></td></tr>
                                
            <tr><td><label><strong><?php echo $phPhone; ?>: </strong></label></td><td><input class="inpReserve" type="text" name="phone" value="" /></td></tr>
                      
            <tr><td><label><strong><?php echo $phDateBirth; ?>: </strong></label></td><td><input class="inpYear" type="text" name="birth" value="" /></td></tr>
                      
            <tr><td><label><strong><?php echo $phFemale.'/'.$phMale; ?>:</strong></label></td><td>
                      
              <select name="sex">
                          
                <option value="1"><?php echo $phFemale; ?></option>
                            
                <option value="2"><?php echo $phMale; ?></option>
                          
              </select>
                        
            </td></tr>
            
            <tr><td><label><strong><?php echo $phCat; ?>:</strong></label></td><td>
                      
              <select name="category">
                          
                <?php
                $result = mysqli_query($db_connect, "select * from category_1 order by id");
                
                while ($row = mysqli_fetch_array($result)) {
                          
                  $tb = 'category_'.$lang;
                  
                  $ctg = mysqli_query($db_connect, "select category from `$tb` where id = {$row['id']}");
                  
                  $cg = mysqli_fetch_array($ctg);
                  
                  if ($row['id'] == 4) {
                  
                    switch ($lang) { // moc dlouhé
                    
                      case 1: $category = 'Lesbian singles event';
                      break;
                      case 2: $category = 'Seznamovací večer pro lesbičky';             
                    }
                  
                  } else {
                  
                    $category = $cg['category'];           
                  
                  }
                  
                  echo '<option value="'.$row['id'].'">'.$category.'</option>';
                  
                }
                
                ?>
                                      
              </select>
                        
            </td></tr>
                                   
            <?php 
            $now = date('Y-m-d');
            
            $result = mysqli_query($db_connect, "select * from events where dateEvent >= '$now' order by dateEvent asc");
            
            if (mysqli_num_rows($result) > 0) {
            
              switch ($lang) {
                case 1: $phEvent = 'Attend';
                break;
                case 2: $phEvent = 'Zúčastnit se';    
              }
                        
              echo '<tr><td colspan="2"><h3>'.$phEvent.'</h3></td></tr>';
            
              $i = 0;
              
              while ($row = mysqli_fetch_array($result)) {  
                
                $i++;
                
                echo '<tr><td colspan="2">';
                  
                  echo '<table>';
                
                    $date = date("j. n. Y", mktime(0,0,0,substr($row['dateEvent'], 5, 2), substr($row['dateEvent'], 8, 2), substr($row['dateEvent'], 0, 4)));
                    
                    $title = mysqli_query($db_connect, "select id from event_1 where idEvent = {$row['id']}");
                    
                    $tt = mysqli_fetch_array($title);
                    
                    $idE = $tt['id'];
                    
                    $tb = 'event_'.$lang;
                    
                    $title = mysqli_query($db_connect, "select title from `$tb` where id = $idE");
                    
                    $tt = mysqli_fetch_array($title);
                    
                    $tb = 'category_'.$lang;
                    
                    $categ = mysqli_query($db_connect, "select category from `$tb` where id = {$row['category']}");
                    
                    $ctg = mysqli_fetch_array($categ);
                    
                    echo '<tr><td colspan="2"><strong>'.$date.'</strong></td></tr>';
                    
                    echo '<tr><td><strong>'.$tt['title'].'</strong></td><td class="tdRegEvent"><input class="checkRegEvent" type="checkbox" name="event'.$row['id'].'" /></td></tr>';
                    
                    echo '<tr><td colspan="2">'.$ctg['category'].'</td></tr>';
                    
                    echo '<tr><td colspan="2">'.$row['place'].'</td></tr>';
                    
                    echo '<tr><td colspan="2"></td></tr>';
                  
                  echo '</table>';
                  
                echo '</td></tr>';
              
              }
            
            }
            ?>
            
            <tr>
                    
              <td colspan="2">
                      
                <div class="conditionForm" id="cfReg">                   
      
                  <img class="delWinForm" src="./design/close_form_new.png" alt="hide" />
                          
                  <?php
                  $text = new Text ($db_connect, $id = 105, $lang);
                  echo '<div class="more">' . $text->showText() . '</div>';
                  ?>
                        
                </div>
                        
                <div class="coverPhAgree"><span class="phAgree"><strong><?php echo $phAgree; ?></strong></span> <input type="checkbox" name="agree" /></div>
                        
              </td>
                      
            </tr>                    
            
            <tr>
            
              <td colspan="2">
              
                <div class="control">
                    
                  <div class="pict">
                  
                    <img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" />
                      
                    <div class="refresh"><a href="#" title="Nový kód" onclick="document.getElementById('captcha').src = './securimage/securimage_show.php?' + Math.random(); return false"><img src="./securimage/images/refresh.png" alt="refresh" /></a></div>
                    
                  </div>
                    
                  <div class="field">
                  
                    <input type="text" name="captcha_code" value="<?php echo $phCode ?>" onclick="if(!this.__click) {this.__click = true;this.value = ''}" />
                  
                  </div>
                  
                </div>
               
              </td>
              
            </tr>     
            
            <tr><td colspan="2"><input class="sbmReserveReg buttonSubmit" type="submit" value="<?php echo $phSend; ?>" /></td></tr>
                            
         </table>
                                
        </form>          
      
      </div>    
    </div> 
    <div class="adver"> <!-- hide() v jquery.adver.js -->
      <img id="delAdv" src="./design/close_form_new.png" alt="hide" />
      <?php 
      // echo $phDiscount; 
      switch ($lang) {
        case 1: $phCorrect = '<p>New Matchmaking for Your.<br /><a class="newTab" href="https://www.singlesin.eu" title="SinglesIn.eu">Register on SinglesIn.eu</a></p>';
        break;
        case 2: $phCorrect = '<p>Připravili jsme pro vás novou seznamku<br><a class="newTab" href="https://www.singlesin.eu" title="SinglesIn.eu">Registrujte se na SinglesIn.eu</a></p>';
      } 
      echo $phCorrect; 
      ?>      
    </div>     
  </body>
</html>