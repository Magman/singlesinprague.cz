<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        <?php
        require_once '../function/function_languages.php';
        languages($db_connect);          
         echo '<div class="editBlockList">';   
          
          require_once '../lib/Setting/SettingAdmin.php';         
          
          $setting = new SettingAdmin ($db_connect);  
          
          if (isset($_POST['order'])) {
            
            require_once '../lib/ChangeOrder/ChangeOrder.php';
            
            $changeOrder = new ChangeOrder ($db_connect, $_POST['order'], 'language'); 
            
            $changeOrder->render();         
                    
          } else if (isset($_POST['new'])) {
            
            $setting->personaly ($_POST['new'], $_POST['valid']);
          
          } else if (isset($_GET['checked'])) {
            
            $setting->activity ($_GET['checked'], $_GET['id']);          
          
          }
                          
          echo $setting->render();                  
         echo '</div>';                                         
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>
