<?php
session_start();
if (!isset($_SESSION['authuser_1'])) $_SESSION['authuser_1'] = 0;
require_once '../include/top_login.php';
require_once '../function/function_connect.php';
connect();

$result = mysqli_query($db_connect, "select personaly from shop");
while ($row =  mysqli_fetch_array($result)) {
  $personaly = $row['personaly'];
}

//if (isset($_POST['heslo'])) $pass = md5($_POST['heslo']); else $pass = null;

if (isset($_POST['heslo'])) $pass = $_POST['heslo']; else $pass = null;

if (($personaly == $pass) or ($_SESSION['authuser_1'] == 1)) {
  $_SESSION['authuser_1'] = 1;
}

if ($_SESSION['authuser_1'] != 1) {
  echo '<div class="contentAdmin">'; 
		echo '<form class="formLogin" method="post" action="index_admin.php">';           
      echo '<H3 class="warAdmin">Neplatné heslo!</H3>';
      echo '<div class="row_reg"><div class="form_header">Heslo:</div><input type="password" name="heslo" value="mojeheslo" size="25" onclick="if(!this.__click) {this.__click = true;this.value = \'\'}" /></div>';            
      echo '<div class="row_reg"><input type="submit" value="přihlásit" /></div>';
    echo '</form>';           
  echo '</div> <!-- .contentAdmin -->';       
  require_once '../include/footer_admin.php';       
  exit();
} else {
  require_once '../include/top_login.php'; 
  require_once '../include/menuadmin.php';
  ?>
      <div class="contentAdmin">
        <div class="editBlockList">
          Administrační část umožňuje vkládat, editovat a mazat obsah těchto www stránek. Administrace je zaheslovaná, <strong>heslo pro vstup udržujte <br />v tajnosti.</strong><br /><br />
          <h3>NASTAVENÍ</h3>
          <ul>
            <li><strong>nastavení hesla</strong> pro přístup do administrace</li>          
            <li><strong>aktivace a deaktivace</strong> jazyků</li>
            <li><strong>změna pořadí</strong> jazyků</li>
          </ul>
          <h3>MENU</h3>
          <ul>
            <li><strong>přidávání a mazání</strong> položek menu</li>          
            <li><strong>editace</strong> položek menu v <strong>aktivních jazycích</strong></li>
            <li>změna <strong>pořadí</strong> položek menu</li>
          </ul>
          <h3>OBSAH</h3>
          <ul>
            <li><strong>vkládání a editace</strong> obsahu textových sekcí</li>
            <li><strong>editace</strong> překladů textových sekcí v <strong>aktivních jazycích</strong></li>            
          </ul>
          <h3>EVENTS</h3>
          <ul>
            <li><strong>vkládání a editace</strong> akcí v <strong>aktivních jazycích</strong></li>            
          </ul>
          <h3>CENY</h3>
          <ul>
            <li>Nastavení cen (objednávací formulář, odpovědní e-mail)</li>            
          </ul>
          <h3>GALERIE</h3>
          <ul>
            <li><strong>vkládání a mazání fotografií a obrázků</strong></li>
            <li><strong>přidávání, editace a mazání komentářů</strong> k fotografiím v aktivních jazycích</li>
          </ul>
        </div> <!-- .edit_block_list -->
      </div> <!-- .contentAdmin -->       
      <?php require_once '../include/footer_admin.php'; 
}
?>