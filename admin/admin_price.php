<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        <?php
        echo '<div class="editBlockList">';
          require_once '../lib/Price/PriceAdmin.php';         
          $priceAdmin = new PriceAdmin ($db_connect);
                                 
          if(isset($_POST['price'])) {
            
            $priceAdmin->updatePrice($_POST['id'], $_POST['price']);    
          
          }
          
          echo $priceAdmin->render();                 
       
        echo '</div>';                                         
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>
