<?php require_once '../include/top_admin.php'; ?>
      
      <div class="contentAdmin">                     
        
        <?php 
        require_once '../function/function_languages.php';
        languages($db_connect);  
        
        if (isset($_GET['lang'])) $lang = $_GET['lang']; else $lang = 1;          
        
        echo '<div class="editBlockList">';
          
          require_once '../lib/Newsletter/NewsletterAdmin.php';         
          
          $newsletterAdmin = new NewsletterAdmin ($db_connect, $languageActive, $lang); 
                                            
          if (isset($_POST['update'])) {
          
            $newsletterAdmin->updateNewsletter($_POST['lang'], $_POST['subject'], $_POST['title'], $_POST['text']);    
                    
          } else if (isset($_POST['email'])) {
          
            require_once '../lib/Newsletter/Newsletter.php';
            $newsletter = new Newsletter($db_connect, $_POST['lang'], $addBcc = 'mailing@prima-e-shop.com', $mailServer = 'bulk.smtp.cz', $path = '../');
            $newsletter->mailNewsletter($mailArr = array($_POST['email']));
            
            $newsletterAdmin = new NewsletterAdmin ($db_connect, $languageActive, $_POST['lang']); 
          
          } else if (isset($_POST['sendGroup'])) {

            require_once '../lib/Newsletter/Newsletter.php';
            $newsletter = new Newsletter($db_connect, $_POST['lang'], $addBcc = 'mailing@prima-e-shop.com', $mailServer = 'bulk.smtp.cz', $path = '../');
            
            if (isset($_SESSION['emailArr'])) {
            
              if ($_SESSION['emailArr'] != null) {  
                
                $newsletter->mailNewsletter($mailArr = $_SESSION['emailArr']);                        
                $newsletterAdmin = new NewsletterAdmin ($db_connect, $languageActive, $_POST['lang']); 

              }
            
            }
          
          } else if (isset($_POST['describe'])) {
          
            $newsletterAdmin->showDescribe ($_POST['id']);
          
          } else if (isset($_POST['showEmail'])) {
          
            $newsletterAdmin->showEmail ($_POST['id']);
          
          }
          
          echo $newsletterAdmin->render();                 
        
        echo '</div>';                                                 
        ?>
      
      </div> <!-- .contentAdmin -->       

<?php require_once '../include/footer_admin.php'; ?>
