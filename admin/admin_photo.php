<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">
        <?php
        if (isset($_POST['setLA'])) { // update $langAdmin   
          $result = mysqli_query($db_connect, "update language set admin = 0 where id > 0");
          $result = mysqli_query($db_connect, "update language set admin = 1 where id = {$_POST['setLA']}");
        }
        
        // languages
        require_once '../function/function_languages.php';
        languages($db_connect); // Returns $language, $langAdmin, $languageActive, $langJs.
    
        $langAdmin = 1;
        
        echo '<div class="showGalAdmin">';
          require_once '../function/menu_gal.php';
          // translation
          $phArr = array();
          $result = mysqli_query($db_connect, "select phrase from trans_admin where category = 'gallad1' and lang = $langAdmin order by ord");
          while ($row = mysqli_fetch_array($result)) {
            $phArr[] = $row['phrase'];
          }
          echo '<h1>'.$phArr[0].'</h1>';
            
          require_once '../lib/Gallery/PhotoAdmin.php';
          $photo = new PhotoAdmin ($db_connect, $langAdmin, $language, $languageActive);
              
          if (isset($_POST['insertComment'])) {
                    
            $photo->insertComment ($_POST['image'], $_POST['comment']);
                                                        
          }
                                 
          if (isset($_POST['editComment'])) {
                    
            $photo->editComment ($_POST['id'], $_POST['lang'], $_POST['comment']);
                                                             
          }
                  
          if (isset($_GET['image'])) {
    
            $photo->showPhotoDetail ($_GET['album'], $_GET['image']);
                                
          }          
    
        echo '</div> <!-- .showGalAdmin -->';   
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>