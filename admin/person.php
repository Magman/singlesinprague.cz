<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        
        <script type="text/javascript">
          $(document).ready(function() 
            { 
              $("#myTable").tablesorter( {sortList: [[0,0]]} ); 
            } 
          ); 
    	  </script>
        <script type="text/javascript">
          // pagination setting
          $(document).ready(function(){
            $.pagMag.startPagination(
              perPage = 15, // items per page
              reload = 1, // default 0, value 1 keeps actual page when webpage is reloaded (need modul cookies.js) 
              item = 'itemPerson', // css class of item
              pager = 'pagerPerson' // css id of pagination       
            );      
          });
        </script>          
        
        <?php
        require_once '../function/function_languages.php';
        languages($db_connect);          
         
        echo '<div class="editBlockList">'; 
      
          require_once '../lib/Account/AccountPerson.php';
          
          $account = new AccountPerson($db_connect, $uid = 0, 1);
                
          if (isset($_POST['dokAccount'])) {
                
            $account->deleteUser($_POST['id']);
            
            $account->insertButton();
              
          } else if (isset($_POST['delAccount'])) {
                
            $account->queryDeleteUser($_POST['id']);
              
          } else if (isset($_POST['checkAccount'])) {
                
            $account->changeActive($_POST['id'], $_POST['checked']);
              
          } else if (isset($_POST['changePass'])) {
                
            $account->changePass($_POST['id']);
              
          } else if (isset($_POST['insPass'])) {
                
            $account->insPass($_POST['id'], $_POST['person'], $_POST['again']);
              
          } else if (isset($_POST['insEditAccount'])) {
                
            $account->editUser($_POST['id'], $_POST['phone'], $_POST['birth'], $_POST['sex'], $_POST['category'], $_POST['active'], $_POST['confirm']);
              
          } else if (isset($_POST['editAccount'])) {
            
            $account->editForm($_POST['id']);   
              
          } else if (isset($_POST['insAccount'])) {
                
            $account->insAccount($_POST['name'], $_POST['surname'], $_POST['email'], $_POST['phone'], $_POST['birth'], $_POST['sex'], $_POST['category']);       
                     
          } else if (isset($_POST['addAccount'])) {
                
            $account->registrationForm();       
              
          } else if (!isset($_POST['insAccount'])) { // second level
                                 
            $account->insertButton();
  
          } 
                                
        echo '</div>';
          
        $account->showUser();                  
        ?>                    
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>