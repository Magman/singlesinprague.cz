<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        <?php
        require_once '../function/function_languages.php';
        languages($db_connect);          
         echo '<div class="editBlockList">';   
          require_once '../lib/Menu/MenuAdmin.php';         
          $menuAdmin = new MenuAdmin ($db_connect); 
          
          if (isset($_GET['checked'])) {
            $menuAdmin->activity($_GET['id'], $_GET['checked']);
          }
          
          if (isset($_POST['insert'])) {
            $menuAdmin->insertItem($_POST['item'], $_POST['idParent'], $language, $ftpServer, $ftpLogin, $ftpPass, $root);
          }
          
          if (isset($_POST['sequence'])) {
            if (isset($_POST['order'])) $order = $_POST['order']; else $order = array();
            $menuAdmin->changeOrder($order);
          }
          
          if (isset($_GET['del'])) {
            if (isset($_POST['del'])) $del = $_POST['del']; else $del = 1;
            if ($del != 0) $menuAdmin->deleteQuery($_GET['del'], $_GET['id']);
          }
          
          if (isset($_POST['dok'])) {
            $menuAdmin->deleteOk($_GET['id']);
          }
          
          if (isset($_POST['insedit'])) {
            $menuAdmin->insedit($_POST['id'], $_POST['change'], $_POST['lg']);
          }
          
          if (isset($_GET['edit'])) {
            echo '<div class="method">';
              if (isset($_POST['edit'])) $edit = $_POST['edit']; else $edit = 1;
              if ($edit != 0) $menuAdmin->edit($_GET['id'], $languageActive);          
            echo '</div>';
          }
                    
          echo $menuAdmin->render();              
         echo '</div>';                                         
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>
