<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">
        <?php                
        if (isset($_POST['setLA'])) { // update $langAdmin   
          $result = mysqli_query($db_connect, "update language set admin = 0 where id > 0");
          $result = mysqli_query($db_connect, "update language set admin = 1 where id = {$_POST['setLA']}");
        }
        
        // languages
        require_once '../function/function_languages.php';
        languages($db_connect); // Returns $language, $langAdmin, $languageActive, $langJs.
        
        $langAdmin = 1;
        
        // Parametry nastavení galerie (argumenty metod).
        $result = mysqli_query($db_connect, "select id, value from parameter order by id");
        while ($row = mysqli_fetch_array($result)) {
          switch ($row['id']) {
            case 1: $largeViewX = $row['value'];
            case 2: $maxFileSize = $row['value'];
            case 3: $fileSize = $row['value'];
            case 9: $largeViewY = $row['value'];
          }    
        }
        
          // translation
          $result = mysqli_query($db_connect, "select phrase from trans_admin where category = 'delchecked' and lang = $langAdmin order by ord limit 0,1");
          $row = mysqli_fetch_array($result);
          echo '<script type="text/javascript">';
            echo 'var warr = \''.$row['phrase'].'\'';
          echo '</script>';
      
        echo '<div class="showGalAdmin">';
          require_once '../function/menu_gal.php';
          // translation
          $phArr = array();
          $result = mysqli_query($db_connect, "select phrase from trans_admin where category = 'gallad1' and lang = $langAdmin order by ord");
          while ($row = mysqli_fetch_array($result)) {
            $phArr[] = $row['phrase'];
          }
          echo '<h1>'.$phArr[0].'</h1>';
              
          require_once '../lib/Gallery/GalleryAdmin.php';
          $gallery = new GalleryAdmin ($db_connect, $langAdmin, $language);               
                         
          if (isset($_GET['album'])) { // idAlbum
                
            if (isset($_POST['ins'])) { // upload na server a načtení do DB
                  
              $gallery->insertPhoto ($_POST['ins'], $_POST['album'], $largeViewX, $largeViewY);
                          
            } 
            
            if (isset($_POST['delChecked'])) { // hromadné smazání
              
              if (isset($_POST['check'])) $checked = $_POST['check']; else $checked = array();
              $gallery->deleteChecked ($checked);  
              
            }
    
            $gallery->showPhoto ($_GET['album'], $maxFileSize, $fileSize); 
                
          } else {
                         
            $gallery->showGallery ();
                  
          }
        echo '</div> <!-- .showGalAdmin -->';   
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>
