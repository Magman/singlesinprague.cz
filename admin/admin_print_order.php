<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../favicon.ico" />
    <meta name="description" content="administrace systému SinglesInPrague" />
    <meta name="keywords" content="SinglesInPrague" />
    <style>
      body {
        margin:0; 
        padding:0;  	
        /*font-family:Vollkorn;*/  
        /*font-family: "Times New Roman", Georgia, Serif;*/
        font-family: "Arial", Serif;
      	font-size:0.7em;
      	font-style:normal;
      	font-variant:normal;
      	/*font-weight:500;*/
      	line-height:1.0;  
      }
      
      h2 {
        font-size:1.3em; 
        margin:0; 
      }
      
      .printPage {
        float:left;
        width:194mm;
        padding:8mm;
        height:auto;
      }
      
      .printOrders {
        table-layout:fixed;
        border-collapse:collapse;
        width:190mm;
      }
      
      .printOrders td {
        padding:3px 5px;
        border:1px solid black;
        overflow:hidden;
      }
      
      .printOrders th {
        padding:2px 5px;
        border:1px solid black;
        text-align:left;
      }
      
      .tdName {
        width:140px;
      }
      
      .tdEmail {
        width:160px;
      }
      
      .tdPhone {
        width:100px;
      }
      
      .tdBirth {
        width:40px;
      }
      
      .tdVou {
        width:100px;
      }
    </style>
  </head>
  <body>
    <div class="printPage">
      <?php
        require_once('../function/function_connect.php');
        connect();
        $result = mysqli_query($db_connect, "select * from orders where idEvent = {$_POST['id']} limit 0,1");
        $row = mysqli_fetch_array($result);
        echo '<h2>Akce: ' . $row['dateEvent'] . ', ' . $row['place'] . '</h2>';
        if (isset($_POST['id'])) {
          $result = mysqli_query($db_connect, "select * from orders where idEvent = {$_POST['id']} and del = 0 order by sex, surname");
          echo '<table class="printOrders">';
            echo '<tr><th>č.</th><th class="tdName">Jméno</th><th class="tdEmail">email</th><th class="tdPhone">tel.</th><th class="tdBirth">rok nar.</th><th>sex</th><th>pay</th><th class="tdVou">voucher</th></tr>';  
            $i = 0;
            while($row = mysqli_fetch_array($result)) {
              $i++;
              switch ($row['payment']) {
                case 1: $pay = 'pp';
                break;
                case 2: $pay = 'cash';
                break;
                case 3: $pay = 'mem';
              }
              switch ($row['sex']) {
                case 1: $sex = 'Ž';
                break;
                case 2: $sex = 'M';
              }
              echo '<tr><td>' . $i. '</td><td>' . $row['surname'] . '&nbsp;' . $row['name'] . '</td><td>' . $row['email'] . '</td><td>' . $row['phone'] . '</td><td>' . $row['birth'] . '</td><td>' . $sex . '</td><td>' . $pay . '</td><td>' . $row['voucher'] . '</td></tr>';
            }
          echo '</table>';
        } else {
          echo 'Nebyla vybraná žádná akce.';
        }
      ?>
    </div>  
  </body>
</html>