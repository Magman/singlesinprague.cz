<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">  
        <?php       
        echo '<div class="editBlockList">'; 
        
          require_once('../function/function_languages.php');
          languages($db_connect);
          
          if (isset($_POST['id'])) $id = $_POST['id']; else $id = 1; // id product       
          if (isset($_POST['lang'])) $lang = $_POST['lang']; else $lang = 1;
  
          require_once '../lib/Order/OrderAdmin.php';         
          $orderAdmin = new OrderAdmin ($db_connect, $language, $languageActive);                             
                    
          if(isset($_POST['dok'])) {      
            
            $orderAdmin->dokOrder ($id);
          
          } else if(isset($_POST['del'])) {      
            
            $orderAdmin->deleteOrder ($id);
          
          } else if(isset($_POST['detail'])) {      
            
            $orderAdmin->detailOrder ($id);
          
          } 
          /*
          echo '<div id="formPrint">';       
         
            echo '<form method="post" action="admin_print_order.php">';
            
              echo '<img id="print" src="../design/printer.png" alt="print" />&nbsp;';
            
              echo '<input id="datepicker" type="text" name="date" size="7" />&nbsp;';
              
              echo '<input type="submit" value="Vyberte datum"/>';
            
            echo '</form>';
            
          echo '</div>';
          */            
          echo $orderAdmin->render(); 
        
        echo '</div>';                
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>