<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        <?php
        require_once '../function/function_languages.php';
        languages($db_connect);            
        if (isset($_GET['id'])) $id = $_GET['id']; else $id = 1; // id menu         
        if (isset($_GET['lang'])) $lang = $_GET['lang']; else $lang = 1;
            
        require_once '../lib/Menu/MenuTop.php';
        $menu = new MenuTop ($db_connect, $lang, $id, null, 0, null); // null: $xmlLang; 0: $parHotel; null: $path;
        
        echo '<div class="menu">';
          echo $menu->renderAdmin();
        echo '</div>';
        echo '<div class="editBlockList">';
          require_once '../lib/Text/TextAdmin.php';         
          $textAdmin = new TextAdmin ($db_connect, $languageActive, $id, $lang); // id menu
                                 
          if(isset($_POST['id'])) { // id text
            if (isset($_POST['text'])) $text = $_POST['text']; else $text = null;
            $textAdmin->updateText($_POST['id'], $_POST['lang'], $_POST['keywords'], $_POST['description'], $_POST['title'], $text);    
          }
          
          echo $textAdmin->render();                 
        echo '</div>';                                         
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>
