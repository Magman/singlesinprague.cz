<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">
        <?php                
        if (isset($_POST['setLA'])) { // update $langAdmin   
          $result = mysqli_query($db_connect, "update language set admin = 0 where id > 0");
          $result = mysqli_query($db_connect, "update language set admin = 1 where id = {$_POST['setLA']}");
        }
        
        // languages
        require_once '../function/function_languages.php';
        languages($db_connect); // Returns $language, $langAdmin, $languageActive, $langJs.
        
        $langAdmin = 1;
        
        echo '<div class="showGalAdmin">';      
          require_once '../function/menu_gal.php';
          
          // translation
          $phArr = array();
          $result = mysqli_query($db_connect, "select phrase from trans_admin where category = 'album1' and lang = $langAdmin order by ord");
          while ($row = mysqli_fetch_array($result)) {
            $phArr[] = $row['phrase'];
          }
          echo '<h1>'.$phArr[0].'</h1>'; 
          
          require_once '../lib/Gallery/AlbumAdmin.php';
          $album = new AlbumAdmin ($db_connect, $langAdmin, $language, $languageActive); 
                  
          if (isset($_POST['item'])) {                               
      
            $album->insertAlbum ($_POST['item'], $_POST['idParent'], $ftpServer, $ftpLogin, $ftpPass, $root);
    
          } else if (isset($_POST['change'])) {
            
            if (isset($_POST['order'])) { 
                        
              $album->changeOrderAlbum ($_POST['order']);
            
            }        
          
          } else if (isset($_POST['insedit'])) {
                    
            $album->editAlbum ($_POST['album'], trim($_POST['new']), $_POST['lang']); 
            $album->editFormAlbum ($_POST['album']);      
                     
          } else if (isset($_POST['dok'])) {
                    
            $album->deleteAlbum ($_POST['dok'], $_POST['album']); 
                                    
          } else if (isset($_GET['del'])) {
                    
            $del = $_GET['del'];
            if (isset($_POST['del'])) $del = $_POST['del'];
            $album->deleteQueryAlbum($del, $_GET['album']);
                                                                           
          } else if (isset($_GET['edit'])) {
                                     
            $album->editFormAlbum ($_GET['album']);
                    
          }                                                           
                                        
          $album->showAlbum ();
        echo '</div> <!-- .showGalAdmin -->'; 
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>