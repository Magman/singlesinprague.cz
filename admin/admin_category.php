<?php require_once '../include/top_admin.php';
      
      echo '<div class="contentAdmin">';                     

        require_once '../function/function_languages.php';
        languages($db_connect);                      
        
        echo '<div class="editBlockList">';  
          
          echo '<h1>Správa e-mailů</h1>';          

          require_once '../lib/Category/Category.php';           
          $adjust = new Category($db_connect, $language = array(1), $langAdmin = 1, $entita = 'cat_email');

          require_once '../lib/Newsletter/EmailNewsletterAdmin.php';  
          
          if (isset($_POST['idCategory'])) {
            
            $idCategory =  $_POST['idCategory'];
            
            $email = new EmailNewsletterAdmin($db_connect, $idCategory);         
          
          } else if (isset($_GET['idCategory'])) {
            
            $idCategory =  $_GET['idCategory'];
            
            $email = new EmailNewsletterAdmin($db_connect, $idCategory);         
          
          }
          
          if (isset($_POST['change'])) {
            
            $adjust->orderCategory($_POST['ord']);
          
          } else if (isset($_POST['insert'])) {
            
            $adjust->insertCategory($_POST['new']);
          
          } else if (isset($_POST['insedit'])) {            
            
            $adjust->inseditCategory($_POST['lang'], $_POST['id'], $_POST['item']); 
            
            echo '<div class="method">';
              
              $adjust->editCategory($_GET['id'], $_GET['entita']);
            
            echo '</div>'; 
          
          } else if (isset($_POST['dok'])) {
            
            $adjust->deleteCategory($_POST['id'], $_POST['entita']);
          
          } else if (isset($_POST['insertEmail'])) {
            
            $email->insertEmail($_POST['idCategory'], $_POST['email'], $_POST['name'], $_POST['surname']);
            
            $email->inputEmail();
            
            $email->render();
          
          } else if (isset($_POST['editEmail'])) {
            
            $email->editEmail($_POST['id'], $idCategory);
            
            $email->render();
                      
          } else if (isset($_POST['inseditEmail'])) {
            
            $email->inseditEmail($_POST['id'], $_POST['email'], $_POST['name'], $_POST['surname']);
            
            $email->editEmail($_POST['id'], $idCategory);
            
            $email->render();
                      
          } else if (isset($_POST['delEmail'])) {
            
            $email->deleteQuery($_POST['delEmail'], $_POST['id']);
            
            $email->render();
            
          } else if (isset($_POST['dokEmail'])) {
            
            $email->deleteEmail($_POST['id']);
            
            $email->inputEmail();
            
            $email->render();
          
          } else if (isset($_POST['showEmail'])) {
            
            $email->inputEmail();
            
            $email->render();
          
          } else if (isset($_GET['edit'])) {
            
            echo '<div class="method">';
              
              $adjust->editCategory($_GET['id'], $_GET['entita']);
            
            echo '</div>';
          
          } else if (isset($_GET['del'])) {
            
            $del = $_GET['del'];
            if (isset($_POST['del'])) $del = $_POST['del'];
            if ($del != 0) $adjust->deleteQuery($_GET['del'], $_GET['id'], $_GET['entita']);
          
          } else if (isset($_GET['showEmail'])) {
                     
            $email->inputEmail();
            
            $email->render();
          
          }
          
          $adjust->render();
 
          echo '<div class="method">';
            
            $adjust->inputCategory ();                   
          
          echo '</div>';
        
        echo '</div>';         

      echo '</div> <!-- .contentAdmin -->';       

require_once '../include/footer_admin.php';
?>