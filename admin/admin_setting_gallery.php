<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">
        <?php                    
        if (isset($_POST['setLA'])) { // update $langAdmin   
          $result = mysqli_query($db_connect, "update language set admin = 0 where id > 0");
          $result = mysqli_query($db_connect, "update language set admin = 1 where id = {$_POST['setLA']}");
        }
        
        // languages
        require_once '../function/function_languages.php';
        languages($db_connect); // Returns $language, $langAdmin, $languageActive, $langJs.
                
        $langAdmin = 1;
        
        echo '<div class="showGalAdmin">';
          require_once '../function/menu_gal.php';      
          echo '<h1>'.$phArr[0].'</h1>'; 
          
          require_once '../lib/Gallery/SettingAdmin.php';
          $setting = new SettingAdmin ($db_connect, $langAdmin);
            
          if (isset($_POST['largeViewX'])) {
          
            $setting->updateSetting ($_POST['largeViewX'], $_POST['largeViewY'], $_POST['maxFileSize'], $_POST['fileSize'], $thumb = 1, $limit = 10, $width = 800, $switcher = 1, $_POST['sort']); //$_POST['perPage']
          
          }
          
          $setting->formSetting ();        
                                      
          /*  zrušen autonomní systém pro jazyk
          if (isset($_GET['selected'])) {
                       
            $setting->updateLanguage ($_GET['selected'], $_GET['idLanguage']);
            
          }
                  
          if (isset($_POST['change'])) {
            
            $setting->changeOrderLanguage ($_POST['order']);
                                                   
          }
                
          $setting->formLanguageAdmin (array(1, 2)); // cs = 1, en = 2
          
          $setting->formLanguage ();
          */                                                                                                              
        echo '</div>'; 
        ?>
      </div> <!-- .contentAdmin -->       
<?php require_once '../include/footer_admin.php'; ?>