<?php require_once '../include/top_admin.php'; ?>
      <div class="contentAdmin">                     
        <?php
        
        require_once '../function/function_languages.php';
        languages($db_connect);          
         
         echo '<div class="editBlockList">';   
          
          require_once '../lib/Event/EventAdmin.php';         
          $event = new EventAdmin ($db_connect); 
         
          if (isset($_POST['input'])) {
            
            $event->input();
          
          } else if (isset($_POST['insert'])) {
            
            $event->insert($_POST['category'], $_POST['dateEvent'], $_POST['hour'], $_POST['minute'], $_POST['place'], $_POST['address'], $_POST['attend'], $_POST['map']);
            
            $event->insertButton();
          
          } else if (isset($_POST['insedit'])) {
            
            $event->insedit($_POST['id'], $_POST['category'], $_POST['dateEvent'], $_POST['hour'], $_POST['minute'], $_POST['place'], $_POST['address'], $_POST['attend'], $_POST['map']);
          
            $event->edit($_POST['id']); 
          
          } else if (isset($_POST['del'])) {
            
            $event->deleteQuery($_POST['id']);
          
          } else if (isset($_POST['dok'])) {
            
            $event->delete($_POST['id']);
          
          } else if (isset($_POST['edit'])) {
              
            $event->edit($_POST['id']);          
          
          } else if (isset($_POST['describe'])) {
              
            if (isset($_POST['lang'])) $lang = $_POST['lang']; else $lang = 1;
            
            $event->describe($_POST['id'], $languageActive, $lang);          
          
          } else if (isset($_POST['insdescribe'])) {
            
            $event->insDescribe($_POST['id'], $_POST['lang'], $_POST['title'], $_POST['event']); 
            
            $result = mysqli_query($db_connect, "select idEvent from event_1 where id = {$_POST['id']}");
            $row = mysqli_fetch_array($result);
            $event->describe($row['idEvent'], $languageActive, $_POST['lang']);          
          
          } else if (isset($_POST['newsletter'])) {
                          
            $event->newsletterQuery($_POST['id']);          
          
          } else if (isset($_POST['nlok'])) {
                          
            $event->sendEventReg ($_POST['id']);          
          
          } else if (isset($_POST['sent'])) {
                          
            $event->emailSent ($_POST['id']);          
          
          } else if (!isset($_POST['insert'])) {
          
            $event->insertButton();
          
          }
        
          echo $event->render();              
         
         echo '</div>';                                         
        
        ?>
      
      </div> <!-- .contentAdmin -->       

<?php require_once '../include/footer_admin.php'; ?>
