<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

$sent = 0;

switch ($lang) {
  case 1: $phEmail = 'The format of the e-mail is incorrect!';  
  $phSent = 'OK::New password has been sent to your e-mail address.::'; 
  $phFailData = 'Error! Data transfer failed.';  
  $phNoExist = 'Registration on this email does not exist.';
  break;
  case 2: $phEmail = 'The format of the e-mail is incorrect!';  
  $phSent = 'OK::New password has been sent to your e-mail address.::'; 
  $phFailData = 'Error! Data transfer failed.';  
  $phNoExist = 'Registration on this email does not exist.';
}  

if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $_POST['forgot'])) {
      
  echo $phEmail;
         
} else {            
                
  $result = mysqli_query($db_connect, "select email from registration");
      
  while ($row = mysqli_fetch_array($result)) {
        
    $emailArr[] = $row['email'];
      
  }
      
  $exist = 0;
      
  if ($emailArr != null) {
      
    if (in_array($_POST['forgot'], $emailArr)) $exist = 1;
      
  } else { // tabulka je prazdna
      
    $exist = 0;
      
  }
      
  if ($exist == 1) {        
        
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = null;
        
    for ($i = 0; $i < 7; $i++) {
          
      $randomString .= $characters[rand(0, $charactersLength - 1)];
        
    }
    
    $personaly = md5($randomString);
        
    $forgot = $_POST['forgot'];
    
    $update = mysqli_query($db_connect, "update registration set personaly = '$personaly' where email = '".mysqli_real_escape_string($db_connect, $forgot)."'"); 
                   
    if (!$update) {
            
      echo $phFailData;  
        
    } else {  
      
      require_once ('./lib/Message/NewPassword.php');
            
      $newpass = new NewPassword ($db_connect, $lang, $mailArr = array('lecornu.klara@gmail.com', 'singles@singlesinprague.cz', $_POST['forgot']), $addBcc = 'mailing@prima-e-shop.com', $mailServer = 'out.smtp.cz', $path = './');        
            
      $newpass->mailPassword ($randomString);     
        
      $sent = 1; 
        
      echo $phSent; 
            
    } 
      
  } else {
      
    echo $phNoExist;
      
  }
          
}
                                   
if ($sent == 1) { ?>
  <script type="text/javascript">
    $('.formOrdForgot').clearForm();
    location.reload();
  </script>
<?php } ?>
