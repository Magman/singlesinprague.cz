<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

$sent = 0;

if (isset($_SESSION['user'])) {
  
  $uid = intval($_SESSION['user']);

}

switch ($lang) {
  case 1: $phSent = 'OK::Your password has been changed.::'; 
  $phFailData = 'Error! Data transfer failed.'; 
  $phMinChar = 'Password must have minimum 5 characters.'; 
  $phConfirm = 'Confirmation of password failed. Try it again.';
  break;
  case 2: $phSent = 'OK::Vaše heslo bylo změněno.::'; 
  $phFailData = 'Chyba! Přenos dat se nezdařil.'; 
  $phMinChar = 'Heslo musí mít nejméně 5 znaků.'; 
  $phConfirm = 'Ověření hesla se nezdařilo. Zkuste to prosím znovu.';
}  

if (isset($_POST['new'])) {
              
  $new = trim($_POST['new']);
        
  $valid = trim($_POST['valid']);
        
  if (strlen($new) < 5) {
          
    echo $phMinChar;
        
  } else if ($new == $valid) {
          
    $new = md5($new);
          
    $result = mysqli_query($db_connect, "update registration set personaly = '$new' where uid = $uid");
          
    if (!$result) {        
            
      echo $phFailData;     
          
    } else {
            
      echo $phSent;
      
      $sent = 1;
          
    }    
        
  } else {
          
    echo 'Ověření hesla se nezdařilo. Zkus to znovu.';
        
  } 
            
} else {

  echo $phConfirm;

}
                                   
if ($sent == 1) { ?>
  <script type="text/javascript">
    $('.formOwnPass').clearForm();
    location.reload();
  </script>
<?php } ?>
