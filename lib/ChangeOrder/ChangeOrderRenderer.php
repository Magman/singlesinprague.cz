<?php

class ChangeOrderRenderer
{
  private $change;
  
  public function __construct (ChangeOrder $change)
  {
    $this->change = $change;  
  }

  public function render()	
	{ 		
		return $this->renderDefault();
	}

  public function renderDefault()
  {
    $order = $this->change->order;
    $table = $this->change->table;
    
    if ($order != null) {                           
      //Kontrola zda jsou prvky poradi unikátní
      if (count($order) != count(array_unique($order))) {
        echo '<div class="warrCover"><div class="warr">Vložená čísla pořadí musí být unikátní!</div></div>';
      } else {
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                     
          $sql = "update `$table` set ord = '"
          .mysqli_real_escape_string($this->change->db_connect, $i)."' where id = $id";                        
          $result = mysqli_query($this->change->db_connect, $sql);
          $i++;      
        }
        if (!$result) {
          echo '<div class="warrCover"><div class="warr">Chyba. Změna pořadí se nezdařila!</div></div>';
        } else {
          echo '<div class="warrCover"><div class="warr">Změna pořadí proběhla úspěšně.</div></div>';
        }                                                                             
      }
    }     
  }

}
?>