<?php
/* změna pořadí prvků v tabulce */

require_once (dirname(__FILE__) . '/ChangeOrderRenderer.php');

class ChangeOrder
{
	    /**
	 * Připojení k db.
	 *
	 * @var array
	 */
	public $db_connect = null;
  
	    /**
	 * Pole s pořadím.
	 *
	 * @var array
	 */
	public $order = null;
	
		    /**
	 * Tabulka.
	 *
	 * @var string
	 */
	public $table = null;
  
  
  public function __construct ($db_connect, $order, $table)
  {
    $this->db_connect = $db_connect;
    $this->order = $order;
    $this->table = (string) $table;  
  }

  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new ChangeOrderRenderer($this);
		}
		return $this->renderer->render();
	}

}
?>