<?php
class IncreaseDate {

	  /**
	* lang.
	*
	* @var int
	*/
	public $lang = 0;
		
	public function __construct($lang) {
    
    $this->lang = (int) $lang;

  }

  public function incDate ($date)
  {

    $dayD = date("D", mktime(0,0,0,substr($date, 5, 2), substr($date, 8, 2), substr($date, 0, 4)));
        
    if ($this->lang == 2) {
                      
      switch ($dayD) {
                        
        case 'Mon': $dayD = 'po';
        break;
        case 'Tue': $dayD = 'út';
        break;
        case 'Wed': $dayD = 'st';
        break;
        case 'Thu': $dayD = 'čt';
        break;
        case 'Fri': $dayD = 'pá';
        break;
        case 'Sat': $dayD = 'so';
        break;
        case 'Sun': $dayD = 'ne';        
                     
      }
                    
    }
        
    $day = date("j", mktime(0,0,0,substr($date, 5, 2), substr($date, 8, 2), substr($date, 0, 4)));
                    
    if ($this->lang == 2) $day = $day.'.';
                    
    $month = date("M", mktime(0,0,0,substr($date, 5, 2), substr($date, 8, 2), substr($date, 0, 4)));
                    
    if ($this->lang == 2) {
                      
      switch ($month) {
                        
        case 'Jan': $month = 'ledna';
        break;
        case 'Feb': $month = 'února';
        break;
        case 'Mar': $month = 'března';
        break;
        case 'Apr': $month = 'dubna';
        break;
        case 'May': $month = 'května';
        break;
        case 'Jun': $month = 'června';
        break;
        case 'Jul': $month = 'července';
        break;
        case 'Aug': $month = 'srpna'; 
        break;
        case 'Sep': $month = 'září'; 
        break;
        case 'Okt': $month = 'října'; 
        break;
        case 'Nov': $month = 'listopadu'; 
        break; 
        case 'Dec': $month = 'prosince';              
      }
                    
    }
          
    $when = $dayD.' '.$day.' '.$month;
          
    return $when;
          
  }

}
?>
