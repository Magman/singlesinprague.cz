<?php
class Text {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	      /**
	 * Aktivní jazyky.
	 *
	 * @var array
	 */

	public $id = 0;
	
			/**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
		
	public function __construct($db_connect, $id, $lang) {
    $this->db_connect = $db_connect;
    $this->id = $id;
    $this->lang = $lang;
  }
  
  public function showDescription ()
	{    
    $tbText = $this->tbText();  
    $result = mysqli_query($this->db_connect, "select `description` from `$tbText` where id = {$this->setId()}");
    $row = mysqli_fetch_array($result); 
    return $row['description'];      
  }
  
  public function showKeywords ()
	{    
    $tbText = $this->tbText();  
    $result = mysqli_query($this->db_connect, "select `keywords` from `$tbText` where id = {$this->setId()}");
    $row = mysqli_fetch_array($result); 
    return $row['keywords'];    
  }
  
  public function showTitle ()
	{    
    $tbText = $this->tbText();  
    $result = mysqli_query($this->db_connect, "select `title` from `$tbText` where id = {$this->setId()}");
    $row = mysqli_fetch_array($result); 
    return $row['title'];      
  }
  
  public function showText ()
	{   
    $tbText = $this->tbText();
    $result = mysqli_query($this->db_connect, "select `text` from `$tbText` where id = {$this->setId()}");
    $row = mysqli_fetch_array($result); 
    return $row['text'];      
  }
  
  private function setId ()
  {  
    $result = mysqli_query($this->db_connect, "select id from text_1 where idMenu = {$this->id}");
    $row = mysqli_fetch_array($result);    
    return $row['id'];
  }
  
  private function tbText ()
  {
    return 'text_'.$this->lang;
  }

}

?>