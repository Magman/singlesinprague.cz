<?php

require_once (dirname(__FILE__) . '/TextAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Pager) 

class TextAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	      /**
	 * Aktivní jazyky.
	 *
	 * @var array
	 */
	public $languageActive = null;
	
		  /**
	 * id menu.
	 *
	 * @var int
	 */
	public $id = 0;
	
			/**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
		
	public function __construct($db_connect, $languageActive, $id, $lang) {
    $this->db_connect = $db_connect;
    $this->languageActive = $languageActive;
    $this->id = $id;
    $this->lang = $lang;
  }
      
  public function updateText ($id, $lang, $keywords, $description, $title, $text)
  {
    $text = strtr($text, array("'"=>""));
                                                        
    $tbText = 'text_'.$lang;
              
    $sql = "update `$tbText` set `keywords` = '"
    .mysqli_real_escape_string($this->db_connect, $keywords)."', `description` = '"
    .mysqli_real_escape_string($this->db_connect, $description)."', `title` = '"
    .mysqli_real_escape_string($this->db_connect, $title)."', `text` = '"
    .mysqli_real_escape_string($this->db_connect, $text)."' where id = $id";
              
    $result = mysqli_query($this->db_connect, $sql);
                          
    if (!$result) {
      echo '<div class="warrCover"><div class="warr">Chyba! Aktualizace se nezdařila.</div></div>';
    } else {
      echo '<div class="warrCover"><div class="warr">Aktualizace textu proběhla úspěšně.</div></div>';
    }   
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new TextAdminRenderer($this);
		}
		return $this->renderer->render();
	}


}

?>