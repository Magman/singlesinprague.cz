<?php
require_once '../lib/Help/HelpAdmin.php';

class TextAdminRenderer {

  private $textAdmin;

	public function __construct(TextAdmin $textAdmin)
	{
		$this->textAdmin = $textAdmin;
	}

	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    
    $help = new HelpAdmin ('cs'); // nápověda
      
    $help->showHelp ('text', 1);
                  
    //nacteni vlajek
    echo '<table><tr>';
              
      foreach ($this->textAdmin->languageActive as $co => $lg) {                        
        echo '<td>';                 
          echo '<a href="admin_text.php?lang='.$lg.'&amp;id='.$this->textAdmin->id.'" title="'.$co.'">
          <img src="../lib/design/'.$co.'.png" alt="'.$co.'" /></a>';
        echo '</td>';                 
      }
    echo '</tr></table>';
         
    foreach ($this->textAdmin->languageActive as $co => $lg) {                                                              
      if ($lg == $this->textAdmin->lang) echo '<div class="menuLang"><img src="../lib/design/'.$co.'.png" alt="'.$co.'" /></div>';
    }        
             
    $tbMenu = 'menu_top_'.$this->textAdmin->lang;
                
    $result = mysqli_query($this->textAdmin->db_connect, "select item from `$tbMenu` where id = {$this->textAdmin->id}");              
    $row = mysqli_fetch_array($result);                                           
    echo '<h3>'.$row['item'].'</h3>';
                          
    $tbText = 'text_'.$this->textAdmin->lang;
                  
    if ($this->textAdmin->lang == 1) {
      $result = mysqli_query($this->textAdmin->db_connect, "select id, `keywords`, `description`, `title`, `text` from `$tbText` where idMenu = {$this->textAdmin->id}");
    } else {
      $result = mysqli_query($this->textAdmin->db_connect, "select `$tbText`.id, `$tbText`.`keywords`, `$tbText`.`description`, `$tbText`.`title`, `$tbText`.`text` from `$tbText`, text_1 where text_1.idMenu = {$this->textAdmin->id} and text_1.id = `$tbText`.id");
    }
                  
    $row = mysqli_fetch_array($result);
    $id = $row['id'];
    $keywords = $row['keywords'];
    $description = $row['description'];
    $title = $row['title'];
    $text = $row['text'];
                                                                
    echo '<form method="post" action="">';
      echo '<table class="textEdit">';
        echo '<input type="hidden" name="lang" value="'.$this->textAdmin->lang.'">';
        echo '<input type="hidden" name="id" value="'.$id.'">';
        echo '<tr><td><strong>Title</strong></td><td><input type="text" name="title" value="'.$title.'" size="59" /></td></tr>';
        echo '<tr><td><strong>Keywords</strong></td><td><input type="text" name="keywords" value="'.$keywords.'" size="130" /></td></tr>';
        echo '<tr><td><strong>Description</strong></td><td><input type="text" name="description" value="'.$description.'" size="130" /></td></tr>';
      echo '</table>';
      if ($this->textAdmin->id == 2 or $this->textAdmin->id == 3 or $this->textAdmin->id == 5 or $this->textAdmin->id == 7 or $this->textAdmin->id == 8 or $this->textAdmin->id == 9) {
        echo '<div class="warrCover"><div class="warr">Sekce nemá textový obsah.</div></div>'; 
      } else { 
        echo '<textarea name="text">'.$text.'</textarea>'; 
      }
      echo '<input class="textSubmit" type="submit" value="editovat">';     
    echo '</form>';                                                                                                                     
      
    echo '<script type="text/javascript">';
    	echo 'CKEDITOR.replace( \'text\' );';
    echo '</script>';  
  }

}

?>