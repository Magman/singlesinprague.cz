<?php

require_once '../lib/Help/HelpAdmin.php';

class SettingAdminRenderer {

  private $setting;

	public function __construct(SettingAdmin $setting)
	{
		$this->setting = $setting;
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    $help = new HelpAdmin ('cs'); // nápověda
    
    //echo '<div class="editBlockList">';      

      echo '<h3>Heslo pro přístup do administrace</h3>';
      
      $help->showHelp ('setting', 1);

      echo '<form method="post" action="">';
        echo '<table>';
          echo '<tr><td>Nové heslo</td><td><input type="password" name="new" value="noveheslo" /></td></tr>';
          echo '<tr><td>Nové heslo znovu&nbsp;&nbsp;</td><td><input type="password" name="valid" /></td></tr>';
          echo '<tr><td><input type="submit" value="potvrdit" /></td><td></td></tr>';
        echo '</table>';
      echo '</form>';                     
  
      echo '<h3>Jazyky</h3>';
      $help->showHelp ('setting', 2);
      echo '<form method="post" action="">';
        echo '<table class="tabAdmin">';      
          echo '<tr><th>jazyk</th><th>pořadí</th><th>aktivní</th></tr>';
          $i = 1;
          $result = mysqli_query($this->setting->db_connect, "select id, lang, active from language order by ord");
          while ($row = mysqli_fetch_array($result)) {
            echo '<tr><td><img src="../lib/design/'.$row['lang'].'.png" alt="'.$row['lang'].'" /></td><td><input type="text" name="order['.$row['id'].']" value="'.$i.'" size="1"/></td>';
              echo '<td>';
                if ($row['active'] == 1) {
                  echo '<a href="admin_setting.php?checked=0&amp;id='.$row['id'].'" title="aktivní"><img src="../lib/design/checked_yes.gif" alt="yes" /></a>';
                } else {
                  echo '<a href="admin_setting.php?checked=1&amp;id='.$row['id'].'" title="neaktivní"><img src="../lib/design/checked_no.gif" alt="no" /></a>';
                }
              echo '</td>';            
            echo '</tr>';
            $i++;        
          }
        echo '</table>';
        echo '<input type="submit" name="change" value="změnit pořadí" />';
      echo '</form>'; 
      /*
      echo '<h3>Pozadí</h3>';
      $help->showHelp ('setting', 3);
      $result = mysqli_query($this->setting->db_connect, "select * from background order by id desc");
      echo '<div class="multipart">';
        echo '<form action="" method="post" enctype="multipart/form-data">';
          echo '<input type="hidden" name="insert" value="1" />';
          echo '<input type="hidden" name="max_file_size" value="500000" />';
          echo '<strong>Vyberte obrázek</strong>&nbsp;<input type="file" name="upload" />'; 
          echo '<input type="submit" value="uložit" />';
        echo '</form>'; 
      echo '</div>';
      echo '<table>';
        echo '<tr>';
          while ($row = mysqli_fetch_array($result)) {
            echo '<td>';
              if ($row['active'] == 1) $style = 'id="active"'; else $style = null;
              echo '<a href="./admin_setting.php?bg='.$row['id'].'" title="přepnout aktivitu" />';
                echo '<img class="thumb2" '.$style.' src="../background/'.$row['image'].'" alt="'.$row['image'].'" />';
              echo '</a>';
              echo '<form method="post" action="">';
                echo '<input type="hidden" name="del" value="1" />';
                echo '<input type="hidden" name="file" value="'.$row['id'].'" />';
                echo '<input class="delBg" type="image" src="../lib/design/delete.gif" />';
              echo '</form>'; 
            echo '</td>';
          }
        echo '</tr>';            
      echo '</table>';
      */
    //echo '</div> <!-- .editBlockList -->';      
  }

}

?>