<?php

require_once (dirname(__FILE__) . '/SettingAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Pager) 

class SettingAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
		
	public function __construct($db_connect) {
    $this->db_connect = $db_connect;
  }
	  
  public function personaly ($new, $valid) {
       
    if (strlen($new) < 5) {
      echo '<div class="warrCover"><div class="warr">Heslo musí mít nejméně 5 znaků!</div></div>';
    } else if ($new == $valid) {
      //$new = md5($new);
      $result = mysqli_query($this->db_connect, "update shop set personaly = '$new'");
      if (!$result) {        
        echo 'Chyba! Poslani SQL prikazu se nepodarilo.';
        echo '<br />';
        echo 'Popis chyby: ';
        mysqli_error($this->db_connect);      
      } else {
        echo '<div class="warrCover"><div class="warr">Heslo pro přístup do administrace bylo změněno.</div></div>';
      }    
    } else {
      echo '<div class="warrCover"><div class="warr">Ověření hesla se nezdařilo. Zkus to znovu.</div></div>';
    }    
  }
  
  public function activity ($checked, $id) 
  {
    $result = mysqli_query($this->db_connect, "update language set active = $checked where id = $id");  
  }
  
  public function sequence ($order) 
  {
    if ($order != null) {                           
      //Kontrola zda jsou prvky poradi unikátní
      if (count($order) != count(array_unique($order))) {
        echo '<div class="warrCover"><div class="warr">Vložená čísla pořadí musí být unikátní!</div></div>';
      } else {
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                     
          $sql = "update language set ord = $i where id = $id";                        
          $result = mysqli_query($this->db_connect, $sql);
          $i++;      
        }
        if (!$result) {
          echo '<div class="warrCover"><div class="warr">Chyba! Změna pořadí se nezdařila.</div></div>';
        } else {
          echo '<div class="warrCover"><div class="warr">Změna pořadí proběhla úspěšně.</div></div>';
        }                                                                             
      }
    }  
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new SettingAdminRenderer($this);
		}

		return $this->renderer->render();
	}


}

?>