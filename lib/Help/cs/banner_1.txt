<strong>Správa bannerů</strong> umožňuje přidávat, editovat a mazat bannery pro bannerový systém. Obrázky pro bannery načítejte do galerie - <strong>album banner</strong>. 
Obrázky pro bannery musí mít rozměr <span style="color:red"><strong>100 x 100 px</strong></span>.<br /><br />
<strong>Link:</strong> příklad odkazu pro slevu <strong>Bookassist</strong>:<br /><br />
<span style="color:red">booking.php?price_group=XXX</span><br /><br /> 
nebo<br /><br /> 
<span style="color:red">booking.php?price_group=XXX&action=cal</span>
<br /><br />
XX - číslo slevy<br /> 
action=cal - pokud je ve slevě nastaven kalendář (Bookassist)