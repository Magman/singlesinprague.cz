<?php

class HelpAdmin {
	
	    /**
	 * Jazyky.
	 *
	 * @var string
	 */
	public $lang = null;
		
	public function __construct($lang) {
    $this->lang = $lang;
  }
  
  
  public function showHelp ($category, $section)
  {
    $path = '../lib/Help/'.$this->lang.'/'.$category.'_'.$section.'.txt';
    $file = fopen($path, 'r');        
    $filesize = filesize($path);
    $showHelp = fread($file, $filesize);   
    fclose($file); 
    echo '<div class="queryString" id="'.$category.$section.'">';
      echo '<ul>';
        echo '<li>';
          echo '<a href="#" title="i"><img src="../lib/Help/design/bg_help.png" alt="?" /></a>';
          echo '<ul>';
            echo '<li class="helpText">'.$showHelp.'</li>';
          echo '</ul>';
        echo '</li>';
      echo '</ul>';    
    echo '</div>';
  }

  
}