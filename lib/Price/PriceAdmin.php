<?php

require_once (dirname(__FILE__) . '/PriceAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Pager) 

class PriceAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
		
	public function __construct($db_connect) {
    
    $this->db_connect = $db_connect;

  }
      
  public function updatePrice ($id, $price)
  {
              
    $sql = "update price set `price` = '".mysqli_real_escape_string($this->db_connect, $price)."' where id = $id";
              
    $result = mysqli_query($this->db_connect, $sql);
                          
    if (!$result) {
      echo '<div class="warrCover"><div class="warr">Chyba! Aktualizace se nezdařila.</div></div>';
    } else {
      echo '<div class="warrCover"><div class="warr">Aktualizace proběhla úspěšně.</div></div>';
    }   
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new PriceAdminRenderer($this);
		}
		return $this->renderer->render();
	}


}

?>