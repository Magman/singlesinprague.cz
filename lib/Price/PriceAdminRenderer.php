<?php
require_once '../lib/Help/HelpAdmin.php';

class PriceAdminRenderer {

  private $priceAdmin;

	public function __construct(PriceAdmin $priceAdmin)
	{
		$this->priceAdmin = $priceAdmin;
	}

	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
	
	  $help = new HelpAdmin ('cs'); // nápověda
      
    $help->showHelp ('price', 1);
                  
    echo '<h1>Ceny</h1>';
    
    $result = mysqli_query($this->priceAdmin->db_connect, "select * from price order by id");
                                                                
    while ($row = mysqli_fetch_array($result)) { 
      
      echo '<form method="post" action="">';
        
        echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
        
        echo '<table>';
  
          echo '<tr><td class="tdPayment">'.$row['payment'].'</td><td><input class="tdPrice" type="text" name="price" value="'.$row['price'].'" /></td><td><input type="submit" value="ok"></td></tr>';
  
        echo '</table>';
      
      echo '</form>'; 
    
    }                                                                                                                    
 
  }

}

?>