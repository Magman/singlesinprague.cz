<?php

class Newsletter {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	    /**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
	
		  /**
	 * Pole e-mailových adres
	 *
	 * @var array
	 */
	//public $mailArr = null;
	
		  /**
	 * e-mail pro skrytou kopii
	 *
	 * @var string
	 */
	public $addBcc = null;
	
			  /**
	 * mail server
	 *
	 * @var string
	 */
	public $mailServer = null;
	
		  /**
	 * path to securimage
	 *
	 * @var string
	 */
	public $path = null;
	
	public function __construct($db_connect, $lang, $addBcc, $mailServer, $path) {
    $this->db_connect = $db_connect;
    $this->lang = (int) $lang;
    //$this->mailArr = $mailArr;
    $this->addBcc = (string) $addBcc;
    $this->mailServer = (string) $mailServer;
    $this->path = (string) $path;
  }

  public function mailNewsletter($mailArr)
  {

    // $mailArr = array($email); 
    
    $tb = 'newsletter_'.$this->lang;

    $result = mysqli_query($this->db_connect, "select * from `$tb` where id = 1");
    $row = mysqli_fetch_array($result);
    $subject = $row['subject'];
    $title = $row['title'];
    $text = $row['text'];       
       
    require ($this->path.'phpmailer/class.phpmailer.php');
    
    if ($mailArr != null) {
      $mov = null;
      $movNewsletter = null;
      foreach ($mailArr as $k => $em) { 
        if ($k != 0) {
          $mov = md5($k);
          if ($this->lang == 1) {
            $movNewsletter = 'If you do not wish to receive any more emails from Singles in Prague, please <a href="http://singlesinprague.cz/mov.php?mov='.$mov.'&amp;lang='.$this->lang.'" title="Unsubscribe">click here</a> to opt out.';
          } else {
            $movNewsletter = 'Odhlásit z odběru newsletterů se můžete <a href="http://singlesinprague.cz/mov.php?mov='.$mov.'&amp;lang='.$this->lang.'" title="Odhlásit z odběru newsletterů">zde</a>';
          }
        }  
        $mail = new PHPMailer();
        $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
        $mail->Host = $this->mailServer;  // zadáme adresu SMTP serveru
        $mail->SMTPAuth = true;               // nastavíme true v případě, že server vyžaduje SMTP autentizaci
        $mail->Username = "singles@singlesinprague.cz";   // uživatelské jméno pro SMTP autentizaci
        $mail->Password = "Iamsuccesful123";            // heslo pro SMTP autentizaci  57ubyXWUXR
        $mail->From = 'singles@singlesinprague.cz';   // adresa odesílatele skriptu
        $mail->FromName = 'Singles in Prague'; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
        $mail->AddAddress($em);  // přidáme příjemce a klidně i druhého včetně jména
        if ($k == 0) $mail->AddBcc($this->addBcc); 
        $mail->Subject = $subject;    // nastavíme předmět e-mailu
        $mail->IsHTML(true);
        $mail->Body = "<html>
          <head>
            <style type=\"text/css\">
              p {margin:10px 0; }
              h2 {color:#992f4a; text-align:left; font-size:18px; margin:10px 0px; }
            </style>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
          </head>
          <body style=\"font-family: 'Times New Roman', serif; font-size:14px; line-height:1.3;\">
            <div style=\"float:left; width:670px; height:auto;\">
              <img style=\"width:135px; height:50px; margin:10px;\" src=\"http://singlesinprague.cz/design/logo.png\" alt=\"Logo\">
              <h2 style=\"color:#992f4a; text-align:left; font-size:18px; margin:10px;\">$title</h2>           
              <div style=\"float:left; width:540px; height:auto; padding:15px 15px;\">            
                $text<br /><br />
                $movNewsletter                
              </div>                                         
            </div>
          </body>
        </html>
        ";       
        $mail->AltBody = "$title\n\n
        $text\n
        ";  // nastavíme tělo e-mailu
        $mail->WordWrap = 100;   // je vhodné taky nastavit zalomení (po 50 znacích)
        $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail
        
        if(!$mail->Send()) {  // odešleme e-mail
          echo '<div class="warrCover"><div class="warr">';
            echo 'Error. E-mail failed.';
            echo 'Notice: ' . $mail->ErrorInfo;
          echo '</div></div>';
        } else {
          echo '<div class="warrCover"><div class="warr">Newsletter byl odeslán na adresu '.$em.'.</div></div>';
          $dateNewsletter = date('Y-m-d');
          $result = mysqli_query($this->db_connect, "insert into newsletter (dateNewsletter, `subject`, title, `text`, lang) values ('$dateNewsletter', '"
          .mysqli_real_escape_string($this->db_connect, $subject)."','"
          .mysqli_real_escape_string($this->db_connect, $title)."','"
          .mysqli_real_escape_string($this->db_connect, $text)."', {$this->lang}
          )");
          $result = mysqli_query($this->db_connect, "select id from newsletter where lang = {$this->lang} order by id desc limit 0,1");
          $row = mysqli_fetch_array($result);
          $idNewsletter = $row['id'];
          $result = mysqli_query($this->db_connect, "insert into newsletter_email (idNewsletter, email) values ($idNewsletter, '"
          .mysqli_real_escape_string($this->db_connect, $em)."'
          )");
          unset ($mail);          
        }
      }
    }
  }

}
?>