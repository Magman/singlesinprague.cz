<?php

class NewsletterEventReg {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
		  /**
	 * e-mail pro skrytou kopii
	 *
	 * @var string
	 */
	public $addBcc = null;
	
			  /**
	 * mail server
	 *
	 * @var string
	 */
	public $mailServer = null;
	
		  /**
	 * path to securimage
	 *
	 * @var string
	 */
	public $path = null;
	
	public function __construct($db_connect, $addBcc, $mailServer, $path) {
    $this->db_connect = $db_connect;
    $this->addBcc = (string) $addBcc;
    $this->mailServer = (string) $mailServer;
    $this->path = (string) $path;
  }

  public function eventNewsletter($id)
  {

    $result = mysqli_query($this->db_connect, "select * from events where id = $id");
    
    $row = mysqli_fetch_array($result);
    
    $category = $row['category'];
    $dateEvent = $row['dateEvent']; 
    $hour = $row['hour']; 
    $minute = $row['minute']; 
    $place = $row['place']; 
    $address = $row['address'];  
    
    $result = mysqli_query($this->db_connect, "select * from category_1 where id = $category"); 
    
    $row = mysqli_fetch_array($result);
    
    $idC = $row['id'];
    
    $category1 = $row['category'];
    
    $result = mysqli_query($this->db_connect, "select category from category_2 where id = $idC"); 
    
    $row = mysqli_fetch_array($result);
    
    $category2 = $row['category'];
    
    $result = mysqli_query($this->db_connect, "select * from event_1 where idEvent = $id"); 
    
    $row = mysqli_fetch_array($result);
    
    $idE = $row['id'];
    $title1 = $row['title'];
    $event1 = $row['event'];
    
    $result = mysqli_query($this->db_connect, "select * from event_2 where id = $idE"); 
    
    $row = mysqli_fetch_array($result);

    $title2 = $row['title'];
    $event2 = $row['event'];
    
    $subject = 'New event (další večírek)';
    
    $result = mysqli_query($this->db_connect, "select email from registration where active = 1"); 
    
    while ($row = mysqli_fetch_array($result)) {
    
      $mailArr[] = $row['email']; 
    
    } 
          
    require ($this->path.'phpmailer/class.phpmailer.php');
    
    if ($mailArr != null) {
      foreach ($mailArr as $k => $em) { 
        $mail = new PHPMailer();
        $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
        $mail->Host = $this->mailServer;  // zadáme adresu SMTP serveru
        $mail->SMTPAuth = true;               // nastavíme true v případě, že server vyžaduje SMTP autentizaci
        $mail->Username = "singles@singlesinprague.cz";   // uživatelské jméno pro SMTP autentizaci        
        $mail->Password = "Iamsuccesful123";  // heslo pro SMTP autentizaci
        $mail->From = 'singles@singlesinprague.cz';   // adresa odesílatele skriptu        
        $mail->FromName = 'Singles in Prague'; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
        $mail->AddAddress($em);  // přidáme příjemce a klidně i druhého včetně jména
        if ($k == 0) $mail->AddBcc($this->addBcc);
        if ($k == 0) $mail->AddBcc('singles@singlesinprague.cz'); 
        $mail->Subject = $subject;    // nastavíme předmět e-mailu
        $mail->IsHTML(true);
        $mail->Body = "<html>
          <head>
            <style type=\"text/css\">
              p {margin:10px 0; }
              h2 {color:#992f4a; text-align:left; font-size:18px; margin:10px 0px; }
            </style>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
          </head>
          <body style=\"font-family: 'Times New Roman', serif; font-size:14px; line-height:1.3;\">
            <div style=\"float:left; width:670px; height:auto;\">
              <img style=\"width:135px; height:50px; margin:10px;\" src=\"http://singlesinprague.cz/design/logo.png\" alt=\"Logo\">                         
              <div style=\"float:left; width:540px; height:auto; padding:15px 15px 0 15px;\">
                <h2 style=\"color:#992f4a; text-align:left; font-size:18px; margin:10px 0;\">$title1</h2>            
                <strong>$category1</strong><br />
                <strong>When:</strong> $dateEvent, $hour:$minute<br />
                <strong>Where:</strong> $place, $address<br />
                <p>$event1</p> 
                <p>Sign in at <a href=\"http://www.singlesinprague.cz\" title=\"singlesinprague.cz\">www.singlesinprague.cz</a></p> 
              </div>           
              <div style=\"float:left; width:540px; height:auto; padding:15px 15px 0 15px;\">
                <h2 style=\"color:#992f4a; text-align:left; font-size:18px; margin:10px 0;\">$title2</h2>            
                <strong>$category2</strong><br />
                <strong>Kdy:</strong> $dateEvent, $hour:$minute<br />
                <strong>Kde:</strong> $place, $address<br />
                <p>$event2</p>
                <p>Zarezervujte si místo na <a href=\"http://www.singlesinprague.cz\" title=\"singlesinprague.cz\">www.singlesinprague.cz</a></p>  
              </div>                                                      
            </div>
          </body>
        </html>
        ";       
        $mail->AltBody = "$title1\n\n
        $category1\n
        When: $dateEvent, $hour:$minute\n
        Where: $place, $address\n\n
        $event1\n\n
        $title2\n\n
        $category2\n
        Kdy: $dateEvent, $hour:$minute\n
        Kde: $place, $address\n\n
        $event2\n\n  
        Sign in at www.singlesinprague.cz\n
        Zarezervujte si místo na www.singlesinprague.cz\n\n  
        ";  // nastavíme tělo e-mailu
        $mail->WordWrap = 100;   // je vhodné taky nastavit zalomení (po 50 znacích)
        $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail
        
        if(!$mail->Send()) {  // odešleme e-mail
          echo '<div class="warrCover"><div class="warr">';
            echo 'Error. E-mail failed.';
            echo 'Notice: ' . $mail->ErrorInfo;
          echo '</div></div>';
        } else {
          echo '<div class="warrCover"><div class="warr">Newsletter byl odeslán na adresu '.$em.'.</div></div>';
          $result = mysqli_query($this->db_connect, "insert into event_email (idEvent, email) values ($id, '"
          .mysqli_real_escape_string($this->db_connect, $em)."'
          )");
          unset ($mail);          
        }
      }
    }
  }

}
?>