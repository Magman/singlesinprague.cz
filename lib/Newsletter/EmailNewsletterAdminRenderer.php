<?php 
require_once '../lib/Help/HelpAdmin.php';

class EmailNewsletterAdminRenderer
{
	private $email;

	public function __construct(EmailNewsletterAdmin $email)
	{
		$this->email = $email;	
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{                                                                                                                                                                              

    echo '<script type="text/javascript">
      // pagination setting
      $(document).ready(function(){
        $.pagMag.startPagination(
          perPage = 10, // items per page
          reload = 1, // default 0, value 1 keeps actual page when webpage is reloaded (need modul cookies.js) 
          item = \'itemEmailAdmin\', // css class of item
          pager = \'pagerEmailAdmin\' // css id of pagination    
        );      
      }); 
    </script>';    
    
    $result = mysqli_query($this->email->db_connect, "select id, cat_email from cat_email_1 where id = {$this->email->idCategory}");
    $row = mysqli_fetch_array($result);
    $idCategory = $row['id'];
    
    echo '<div class="method">';                                  
             
      echo '<h3>'.$row['cat_email'].'</h3>';
        
      $help = new HelpAdmin ('cs'); // nápověda
      $help->showHelp ('category', 1);    
        
      $result = mysqli_query($this->email->db_connect, "select * from email where idCategory = {$this->email->idCategory} order by email");
        
      echo '<div class="pager" id="pagerEmailAdmin"></div>';
      
      echo '<table class="tablesorter" id="tabEmail">';
        
        echo '<thead>';
          
          echo '<tr><th id="tabShowEmail">E-mail</th><th>Jméno</th><th class="tdIcon"></th><th class="tdIcon"></th></tr>';      
              
        echo '</thead>';
              
        echo '<tbody>';    
              
          while ($row = mysqli_fetch_array($result)) {        
                
            echo '<tr class="itemEmailAdmin">';
                    
              echo '<td>'.$row['email'].'</td>';                    
                    
              echo '<td>'.$row['surname'].' '.$row['name'].'</td>'; 
                
              echo '<td>';
              
                echo '<form method="post" action="">';
                
                  echo '<input type="hidden" name="editEmail" value="1" />'; 
                  
                  echo '<input type="hidden" name="idCategory" value="'.$idCategory.'" />'; // kvuli konstruktoru 
                  
                  echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                  
                  echo '<input class="formEdit" type="submit" value="" />'; 
                
                echo '</form>';              
              
              echo '</td>';
                    
              echo '<td>';
              
                echo '<form method="post" action="">';
                
                  echo '<input type="hidden" name="delEmail" value="1" />'; 
                  
                  echo '<input type="hidden" name="idCategory" value="'.$idCategory.'" />'; // kvuli konstruktoru 
                  
                  echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                  
                  echo '<input class="formDelete" type="submit" value="" />'; 
                
                echo '</form>';              
              
              echo '</td>';    
            
            echo '</tr>';
                
          }            
          
        echo '</tbody>';  
            
      echo '</table>';
            
    echo '</div> <!-- .method -->';

  }
}
?> 