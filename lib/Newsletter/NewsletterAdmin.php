<?php

require_once (dirname(__FILE__) . '/NewsletterAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Pager) 

class NewsletterAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
		  /**
	 * Aktivní jazyky.
	 *
	 * @var array
	 */
	public $languageActive = null;
	
				/**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
		
	public function __construct($db_connect, $languageActive, $lang) 
  {
    
    $this->db_connect = $db_connect;
    
    $this->languageActive = $languageActive;
    
    $this->lang = $lang;
  
  }
	  
  public function updateNewsletter ($lang, $subject, $title, $text) 
  {
    
    $tb = 'newsletter_'.$lang;
        
    $sql = "update `$tb` set `subject` = '"
    .mysqli_real_escape_string($this->db_connect, $subject)."', `title` = '"
    .mysqli_real_escape_string($this->db_connect, $title)."', `text` = '"
    .mysqli_real_escape_string($this->db_connect, $text)."'";
              
    $result = mysqli_query($this->db_connect, $sql);
                          
    if (!$result) {
      
      echo '<div class="warrCover"><div class="warr">Chyba! Aktualizace se nezdařila.</div></div>';
    
    } else {
      
      echo '<div class="warrCover"><div class="warr">Aktualizace textu proběhla úspěšně.</div></div>';
    
      //return 1;
    
    }   
  
  }
  
  public function showDescribe ($id) 
  {
            
    $result = mysqli_query($this->db_connect, "select * from newsletter where id = $id");
    
    $row = mysqli_fetch_array($result);
    
    echo '<div class="method">';
    
      echo '<table class="tabAdmin describeNewsletter">';
      
        echo '<tr><td><strong>'.$row['subject'].'</strong></td>';
        
          echo '<td class="tdDel">';
        
            echo '<form class="back" method="post" action="">';
              echo '<input type="image" src="../lib/design/delete.gif" value="" />';
            echo '</form>';        
        
          echo '</td>';
        
        echo '</tr>';
        
        echo '<tr><td colspan="2">'.$row['title'].'</td></tr>';
        
        echo '<tr><td colspan="2">'.$row['text'].'</td></tr>';
      
      echo '</table>';
      
    echo '</div>';  
  
  }
  
  public function showEmail ($id) 
  {
            
    $result = mysqli_query($this->db_connect, "select email from newsletter_email where idNewsletter = $id");
    
    echo '<div class="method">';
    
      echo '<form class="back" id="backCorrect" method="post" action="">';
        echo '<input type="image" src="../lib/design/delete.gif" value="" />';
      echo '</form>';            
            
      echo '<table class="tabAdmin tablesorter" id="tabShowEmail">';
      
        echo '<thead>';
        
          echo '<tr><th>Email</th></tr>';
        
        echo '</thead>';
        
        echo '<tbody>';
      
          while ($row = mysqli_fetch_array($result)) {
          
            echo '<tr><td>'.$row['email'].'</td</tr>';  
          
          }
          
        echo '</tbody>';
        
      echo '</table>';
      
    echo '</div>';  
  
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new NewsletterAdminRenderer($this);
		}

		return $this->renderer->render();
	}


}

?>