<?php
require_once '../lib/Help/HelpAdmin.php';

class NewsletterAdminRenderer {

  private $newsletter;

	public function __construct(NewsletterAdmin $newsletter)
	{
		$this->newsletterAdmin = $newsletter;
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
	
    $help = new HelpAdmin ('cs'); // nápověda
      
    $help->showHelp ('text', 1);
                  

    
    //nacteni vlajek
    echo '<table>';
      
      echo '<tr>';  
      
        foreach ($this->newsletterAdmin->languageActive as $co => $lg) { // vlajky                       
          
          echo '<td>';                 
            
            echo '<a href="admin_newsletter.php?lang='.$lg.'" title="'.$co.'"><img src="../lib/design/'.$co.'.png" alt="'.$co.'" /></a>';
          
          echo '</td>';                 
        
        }
    
      echo '</tr>';
      
    echo '</table>';
         
    echo '<div class="method">';

      echo '<form method="post" action="">';
      
        echo '<table>';
        
          echo '<tr>';
          
            echo '<td>';
              
              foreach ($this->newsletterAdmin->languageActive as $co => $lg) { // aktivní vlajka                                                              
                    
                if ($lg == $this->newsletterAdmin->lang) echo '<img src="../lib/design/'.$co.'.png" alt="'.$co.'" />';
                  
              }              
            
            echo '</td>';
            
            echo '<td>';
            
              echo '<select name="group">';
              
                $result = mysqli_query($this->newsletterAdmin->db_connect, "select * from cat_email_1 order by ord");
                
                while ($row = mysqli_fetch_array($result)) {
                
                  $selected[$row['id']] = null;
                  
                  if (isset($_POST['group'])) {
                  
                    if ($row['id'] == $_POST['group']) $selected[$row['id']] = ' selected="selected"'; 
                  
                  }
                  
                  echo '<option value="'.$row['id'].'"'.$selected[$row['id']].'>'.$row['cat_email'].'</option>';
                
                }
              
              echo '</select>';
            
            echo '</td>'; 
            
            echo '<td>'; 
            
              echo '<input type="submit" value="potvrdit skupinu" />';
            
            echo '</td>';        
          
          echo '</tr>';
        
        echo '</table>';
            
      echo '</form>';
            
      if (isset($_POST['group'])) {
      
        echo '<div class="coverGroup">';  
        
          echo '<a href="admin_newsletter.php" title="zpět"><img class="back" src="../lib/design/delete.gif" alt="zpět" /></a>';
          
          if (isset($_POST['moveEmail'])) {
            
            if ($_SESSION['emailArr'] != null) {
              
              $mailArr = null;
              
              foreach ($_SESSION['emailArr'] as $iM => $eM) {
                          
                if ($_POST['id'] != $iM) {
                
                  echo '<div class="cellEmail">';
                  
                    echo $eM;
                    
                    echo '<form class="moveEmail" method="post" action="">';
                    
                      echo '<input type="hidden" name="moveEmail" value="1" />';
                      
                      echo '<input type="hidden" name="group" value="'.$_POST['group'].'" />';
                      
                      echo '<input type="hidden" name="id" value="'.$iM.'" />';
                      
                      echo '<input class="moveEmailButton" type="submit" value="x" />';
                      
                    echo '</form>';
                    
                  echo '</div>'; 
              
                  $mailArr[$iM] = $eM;
                
                }
              
              }
              
              $_SESSION['emailArr'] = $mailArr;       
          
            }  
          
          } else {
                    
            $result = mysqli_query($this->newsletterAdmin->db_connect, "select * from email where idCategory = {$_POST['group']} order by email");
            
            while ($row = mysqli_fetch_array($result)) {
            
              $emailArr[$row['id']] = $row['email'];
              
              echo '<div class="cellEmail">';
              
                echo $row['email'];
                
                echo '<form class="moveEmail" method="post" action="">';
                
                  echo '<input type="hidden" name="moveEmail" value="1" />';
                  
                  echo '<input type="hidden" name="group" value="'.$_POST['group'].'" />';
                  
                  echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
                  
                  echo '<input class="moveEmailButton" type="submit" value="x" />';
                  
                echo '</form>';
                
              echo '</div>'; 
            
            }
            
            $_SESSION['emailArr'] = $emailArr;
          
          }

          echo '<form class="sendGroup" method="post" action="admin_newsletter.php">';
        
            echo '<input type="hidden" name="sendGroup" value="1" />';
            
            echo '<input type="hidden" name="lang" value="'.$this->newsletterAdmin->lang.'" />';
            
            echo '<input type="hidden" name="group" value="'.$_POST['group'].'" value="1" />';
            
            echo '<input class="sendGroupButton" type="submit" value="odeslat" />';
          
          echo '</form>';
        
        echo '</div>';
      
      }
      
      echo '<form method="post" action="admin_newsletter.php">';
      
        echo '<input type="hidden" name="lang" value="'.$this->newsletterAdmin->lang.'" />';
        
        echo '<table>';
        
          echo '<tr>';
          
            echo '<td>';
              
              foreach ($this->newsletterAdmin->languageActive as $co => $lg) { // aktivní vlajka                                                              
                    
                if ($lg == $this->newsletterAdmin->lang) echo '<img src="../lib/design/'.$co.'.png" alt="'.$co.'" />';
                  
              }              
            
            echo '</td>';
            
            echo '<td><strong>Vlož e-mail</strong></td><td><input type="text" name="email" value="" size="40" /></td>';
            
            echo '<td><input type="submit" value="odeslat newsletter" /></td>'; 
            
          echo '</tr>';
        
        echo '</table>';
      
      echo '</form>';
    
    echo '</div>';
    
    foreach ($this->newsletterAdmin->languageActive as $co => $lg) { // aktivní vlajka                                                              
      
      if ($lg == $this->newsletterAdmin->lang) echo '<div class="menuLang"><img src="../lib/design/'.$co.'.png" alt="'.$co.'" /></div>';
    
    }            

    $tb = 'newsletter_'.$this->newsletterAdmin->lang;
    
    $result = mysqli_query($this->newsletterAdmin->db_connect, "select * from `$tb`");
    
    $row = mysqli_fetch_array($result);
    
    echo '<form method="post" action="">';
      echo '<table class="textEdit">';
        echo '<input type="hidden" name="update" value="1">';
        echo '<input type="hidden" name="lang" value="'.$this->newsletterAdmin->lang.'">';
        echo '<tr><td><strong>Předmět</strong></td><td><input type="text" name="subject" value="'.$row['subject'].'" size="95" /></td></tr>';
        echo '<tr><td><strong>Titulek</strong></td><td><input type="text" name="title" value="'.$row['title'].'" size="95" /></td></tr>';
      echo '</table>';
      echo '<textarea name="text">'.$row['text'].'</textarea>'; 
      echo '<input class="textSubmit" type="submit" value="editovat">';     
    echo '</form>';                                                                                                                     
      
    echo '<script type="text/javascript">';
    	echo 'CKEDITOR.replace( \'text\' );';
    echo '</script>'; 
    
    echo '<div class="method">';
    
      echo '<h2>Odeslané newslettery</h2>';
      /*
      if (isset($_POST['describe'])) {
          
        $this->newsletterAdmin->showDescribe ($_POST['id']);
          
      }
      */
      $result = mysqli_query($this->newsletterAdmin->db_connect, "select * from newsletter order by id desc");
      
      echo '<script type="text/javascript">
        // pagination setting
        $(document).ready(function(){
          $.pagMag.startPagination(
            perPage = 10, // items per page
            reload = 1, // default 0, value 1 keeps actual page when webpage is reloaded (need modul cookies.js) 
            item = \'itemNewsletterAdmin\', // css class of item
            pager = \'pagerNewsletterAdmin\' // css id of pagination    
          );      
        }); 
      </script>';   
      
      echo '<div class="pager" id="pagerNewsletterAdmin"></div>';   
      
      echo '<table class="tabAdmin tablesorter" id="tabNewsletter">';
      
        echo '<thead>';
        
          echo '<tr><th>Datum</th><th>Titulek</th><th>Text</th><th>Email</th></tr>';
        
        echo '</thead>';
        
        echo '<tbody>';
        
          while ($row = mysqli_fetch_array($result)) {
        
            echo '<tr class="item itemNewsletterAdmin">';
            
              echo '<td class="tdDate">';
              
                echo $row['dateNewsletter'];
              
              echo '</td>'; 
              
              echo '<td>';
              
                echo $row['title'];
              
              echo '</td>';  
              
              echo '<td class="tdIconLong">';
              
                echo '<form method="post" action="">';
                  echo '<input type="hidden" name="describe" value="1" />';
                  echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                  echo '<input type="image" src="../lib/design/ikona_download.gif" value="" />';
                echo '</form>';  
              
              echo '</td>';  
              
              echo '<td class="tdIconLong">';
              
                echo '<form method="post" action="">';
                  echo '<input type="hidden" name="showEmail" value="1" />';
                  echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                  echo '<input type="image" src="../lib/design/ikona_envelope.png" value="" />';
                echo '</form>';  
              
              echo '</td>';             
              
            echo '</tr>';
        
          }
        
        echo '</tbody>';
        
      echo '</table>';
      
    echo '</div>';

	}

}
?>