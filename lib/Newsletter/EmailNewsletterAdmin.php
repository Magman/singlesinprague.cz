<?php
require_once (dirname(__FILE__) . '/EmailNewsletterAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru 

/**
 * Ovládání kategorií.
 */
class EmailNewsletterAdmin
{
  	/**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
  	/**
	 * Categorie
	 *
	 * @var int
	 */
	public $idCategory = null;

	/**
	 *
	 * @var CategoryRenderer
	 */
	private $renderer = null;

	/**
	 * Konstruktor nastavuje hlavní parametry.
	 *
	 * @param $db_connect	 
	 * @param array $language
	 * @param int $langAdmin	 
	 * @param string $entita      	 
	 */
	public function __construct ($db_connect, $idCategory)
	{
		$this->db_connect = $db_connect;
		$this->idCategory = (int) $idCategory;
	}


  public function inputEmail ()
  {
    echo '<a href="admin_category.php" title="zpět"><img class="back" src="../lib/design/delete.gif" alt="zpět" /></a>';
            
    //PŘIDÁNÍ POLOŽKY
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="insertEmail" value="1" />'; 
      echo '<input type="hidden" name="idCategory" value="'.$this->idCategory.'" />'; 
      echo '<table>';
        echo '<tr><td><strong>E-mail</strong></td><td><input type="text" name="email" value="" size="35" /></td></tr>';
        echo '<tr><td><strong>Jméno</strong></td><td><input type="text" name="name" value="" size="35" /></td></tr>';
        echo '<tr><td><strong>Příjmení</strong></td><td><input type="text" name="surname" value="" size="35" /></td></tr>';        
        echo '<tr><td><input type="submit" value="Přidat e-mail" /></td><td></td></tr>';
      echo '</table>';   
    echo '</form>';     
  }
  
  public function editEmail ($id, $idCategory)
  {
    
    $result = mysqli_query($this->db_connect, "select email, name, surname from email where id = $id");
    $row = mysqli_fetch_array($result);                 
                                  
    echo '<a href="admin_category.php?showEmail=1&amp;idCategory='.$idCategory.'" title="zpět"><img class="back" src="../lib/design/delete.gif" alt="zpět" /></a>';
       
    echo '<div class="formCat">';
      echo '<form method="post" action="">';          
        echo '<input type="hidden" name="inseditEmail" value="1">';
        echo '<input type="hidden" name="edit" value="1">';
        echo '<input type="hidden" name="id" value="'.$id.'">';                   
        echo '<table>';
          echo '<tr><td><strong>Email</strong></td><td><input type="text" name="email" size="35" value="'.$row['email'].'"></td></tr>';
          echo '<tr><td><strong>Jméno</strong></td><td><input type="text" name="name" size="35" value="'.$row['name'].'"></td></tr>';
          echo '<tr><td><strong>Příjmení</strong></td><td><input type="text" name="surname" size="35" value="'.$row['surname'].'"></td></tr>';
          echo '<tr><td><input type="submit" value="Změnit položku"></td><td></td></tr></table>';
        echo '</table>';      
      echo '</form>';
    echo '</div> <!-- .formCat -->';                                                                                                                                                             
  
  }
  
  public function inseditEmail($id, $email, $name, $surname)
  {
    
    if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $email)) {
      
      echo '<div class="warrCover"><div class="warr">Email nemá správný tvar.</div></div>';
    
    } else { 
        
      $result = mysqli_query($this->db_connect, "select email from email where id = $id");
      $row = mysqli_fetch_array($result);
      $emailThis = $row['email'];
      
      $emailDbArr = array();          
      // Parametry pro zjištění shodných položek
      $result = mysqli_query($this->db_connect, "select email from email");
      while ($row = mysqli_fetch_array($result)) {                                     
        
        if ($emailThis != $email) { // novy email neni stejny pak probehne kontrola
        
          $emailDbArr[] = $row['email'];                                 
      
        }
      
      }  
      $emailDbArr = array_unique($emailDbArr);              
            
      if (in_array($email, $emailDbArr) == true) { //ochrana před vložením stejných položek
        
        echo '<div class="warrCover"><div class="warr">Email <strong>'.$email.'</strong> už je v databázi uložen.</div></div>';
      
      } else {                                                                                                      
        
        $sql = "update email set email = '"
        .mysqli_real_escape_string($this->db_connect, $email)."', name = '" 
        .mysqli_real_escape_string($this->db_connect, $name)."', surname = '" 
        .mysqli_real_escape_string($this->db_connect, $surname)."' 
        where id = $id";                                    
        
        $result = mysqli_query($this->db_connect, $sql);
        
        if (!$result) {
          
          echo '<div class="warrCover"><div class="warr">Chyba. Uložení změny se nezdařilo!</div></div>';
        
        }            
      
      }
    
    }
  
  }
                                                                                                         
  
  public function insertEmail ($idCategory, $email, $name, $surname)
  {      
    $email = trim($email);
   
    $emailDbArr = array();
   
    // Parametry pro zjištění shodných položek
    $result = mysqli_query($this->db_connect, "select email from email where idCategory = $idCategory");
    while ($row = mysqli_fetch_array($result)) {                                     
      $emailDbArr[] = $row['email'];                  
      $emailDbArr = array_unique($emailDbArr);                
    }              

    if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $email)) {
      
      echo '<div class="warrCover"><div class="warr">Email není ve správném tvaru!</div></div>';
   
    } else if (in_array($email, $emailDbArr) == true) { //ochrana před vložením stejných položek
      
      echo '<div class="warrCover"><div class="warr">Email <strong>'.$email.'</strong> už je v databázi uložen.</div></div>';
    
    } else {
                                                                                       
      $email = strtr($email, array('"'=>'',"'"=>""));
                              
      // zapis do databaze pro hlavni jazyk administrace                                                          
      $sql = "insert into email (idCategory, email, name, surname) values ($idCategory, '"
      .mysqli_real_escape_string($this->db_connect, $email)."','"
      .mysqli_real_escape_string($this->db_connect, $name)."','"
      .mysqli_real_escape_string($this->db_connect, $surname)."')";
                                                
      $result = mysqli_query($this->db_connect, $sql);
                                                                                                     
      if (!$result) {
        
        echo '<div class="warrCover"><div class="warr">Chyba. Položku <strong>'.$email.'</strong> se nepodařilo zapsat do databáze.</div></div>';
      
      }                                                                                                         
      
    }
                
  }  
  
  public function deleteQuery ($del, $id)
	{
    
    $result = mysqli_query($this->db_connect, "select idCategory, email from email where id = $id");
    $row = mysqli_fetch_array($result);
    if ($del != 0) {
      echo '<div class="warrCover"><div class="warr">';
      echo 'Položka <strong>'.$row['email'].'</strong> bude smazána. Opravdu chcete položku <strong>'.$row['email'].'</strong> smazat?'; 
      echo '<table><tr>';
      echo '<td>';
        echo '<form method="post" action="">';
          echo '<input type="hidden" name="dokEmail" value="1" />';
          echo '<input type="hidden" name="idCategory" value="'.$row['idCategory'].'" />'; // parametr objektu
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="smazat" />';
        echo '</form>';
      echo '</td>';
      echo '<td>';
        echo '<form method="get" action="">';
          echo '<input type="hidden" name="showEmail" value="1" />';
          echo '<input type="hidden" name="idCategory" value="'.$row['idCategory'].'" />'; // parametr objektu
          echo '<input type="submit" value="ne" />';
        echo '</form>';
      echo '</td>';
      echo '</tr></table>';
      echo '</div></div>';
    
    }
	
  }
	
	public function deleteEmail ($id)
	{ 
     
    $result = mysqli_query($this->db_connect, "delete from email where id = $id");
    
    if (!$result) {
      
      echo '<div class="warrCover"><div class="warr">Chyba! Smazání položky se nezdařilo.</div></div>';
    
    } else {
    
      echo '<div class="warrCover"><div class="warr">Položka byla úspěšně smazána.</div></div>';
    
    }     
  
  }  

  
  public function render()
	{		
    if (!isset($this->renderer)) {			
      $this->renderer = new EmailNewsletterAdminRenderer($this);		
    }
		return $this->renderer->render();	
  }

}