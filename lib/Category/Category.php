<?php
require_once (dirname(__FILE__) . '/CategoryRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru 

/**
 * Ovládání kategorií.
 */
class Category
{
  	/**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;

  	/**
	 * Jazyky.
	 *
	 * @var array
	 */
	public $language = null;
	
  	/**
	 * Jazyk administrace.
	 *
	 * @var int
	 */
	public $langAdmin = 0;
	
  	/**
	 * Entita.
	 *
	 * @var string
	 */
	public $entita = null;

	/**
	 *
	 * @var CategoryRenderer
	 */
	private $renderer = null;

	/**
	 * Konstruktor nastavuje hlavní parametry.
	 *
	 * @param $db_connect	 
	 * @param array $language
	 * @param int $langAdmin	 
	 * @param string $entita      	 
	 */
	public function __construct($db_connect, $language, $langAdmin, $entita)
	{
		$this->db_connect = $db_connect;
    $this->language = $language;
		$this->langAdmin = $langAdmin;
		$this->entita = (string) $entita;
	}

	            
  public function orderCategory ($order) {
                
    if ($order != null) {                           
      //Kontrola zda jsou prvky poradi unikátní
      if (count($order) != count(array_unique($order))) {
        echo '<div class="warrCover"><div class="warr">Vložená čísla pořadí musí být unikátní!</div></div>';
      } else {
        // ZMENY
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                    
          $tbItemAdmin = $this->entita.'_'.$this->langAdmin;
                          
          $sql = "update `$tbItemAdmin` set `ord`= '"
          .mysqli_real_escape_string($this->db_connect, $i)."' where id = $id";  
                          
          $result = mysqli_query($this->db_connect, $sql);
          $i++;      
        }
   
        if (!$result) {
          echo '<div class="warrCover"><div class="warr">Chyba! Změna pořadí se nezdařila.</div></div>';
        } else {
          echo '<div class="warrCover"><div class="warr">Změna pořadí proběhla úspěšně.</div></div>';
        }                                                                             
      }
    }                                                 
  }
  
  public function inputCategory ()
  {
    //PŘIDÁNÍ POLOŽKY
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="insert" value="1" />'; 
      echo '<input type="hidden" name="entita" value="'.$this->entita.'" />'; 
      echo '<table>';
        echo '<tr>';
          echo '<td><strong>Nová skupina</strong> <input type="text" name="new" value="" size="35" /></td>';
          echo '<td><input type="submit" value="Přidat položku" /></td>';
        echo '</tr>';
      echo '</table>';   
    echo '</form>';     
  }
  
  public function editCategory ($id, $ent)
  {
    
    $tb = $ent.'_'.$this->langAdmin;
                                              
    foreach ($this->language as $lg) {              
      $tbItem = $ent.'_'.$lg;
                    
      $result = mysqli_query($this->db_connect, "select `$ent` from `$tbItem` where id = $id limit 0,1");
      while ($row = mysqli_fetch_array($result)) {
        $item = $row[''.$ent.''];
      }
                    
      $result = mysqli_query($this->db_connect, "select lang from language where id = $lg");
              
      while ($row = mysqli_fetch_array($result)) {
        $file = $row['lang'];
      }                       
                                  
      echo '<div class="formCat">';
        echo '<form method="post" action="">';          
          echo '<input type="hidden" name="insedit" value="1">';
          echo '<input type="hidden" name="edit" value="1">';
          echo '<input type="hidden" name="id" value="'.$id.'">';
          echo '<input type="hidden" name="lang" value="'.$lg.'">';                    
          echo '<table><tr><td><strong>Skupina</strong></td> 
          <td><input type="text" name="item" size="35" value="'.$item.'"></td>
          <td><input type="submit" value="Změnit položku"></td><td><a href="admin_category.php" title="zpět"><img src="../lib/design/delete.gif" alt="zpět" /></a></td></tr></table>';
        echo '</form>';
      echo '</div> <!-- .formCat -->';                                                             
    }                                                                                                
  }
  
  public function inseditCategory($lang, $id, $item)
  {
    if (empty($item)) {
      echo '<div class="warrCover"><div class="warr">Vyplňte název položky! Položka musí obsahovat alespoň 1 znak.</div></div>';
    } else { 
              
      $tb = $this->entita.'_'.$lang;
        
      $itemDbArr = array();          
      // Parametry pro zjištění shodných položek
      $result = mysqli_query($this->db_connect, "select `{$this->entita}` from `$tb`");
      while ($row = mysqli_fetch_array($result)) {                                     
        $itemDbArr[] = $row[''.$this->entita.''];                                 
      }  
      $itemDbArr = array_unique($itemDbArr);              
            
      if (in_array($item, $itemDbArr) == true) { //ochrana před vložením stejných položek
        echo '<div class="warrCover"><div class="warr">Položka menu <strong>'.$item.'</strong> už existuje. Zvolte jiný název.</div></div>';
      } else {                                                                                                      
        $sql = "update `$tb` set `{$this->entita}` = '"
        .mysqli_real_escape_string($this->db_connect, $item)."' where id = $id";                                    
        $result = mysqli_query($this->db_connect, $sql);
        if (!$result) {
          echo '<div class="warrCover"><div class="warr">Chyba. Uložení změny se nezdařilo!</div></div>';
        }            
      }
    }
  }
                                                                                                         
  
  public function insertCategory ($new)
  {      
    $new = trim($new);
    $tb = $this->entita.'_'.$this->langAdmin;
   
    $newDbArr = array();
   
    // Parametry pro zjištění shodných položek
    $result = mysqli_query($this->db_connect, "select `{$this->entita}` from `$tb`");
    while ($row = mysqli_fetch_array($result)) {                                     
      $newDbArr[] = $row[''.$this->entita.''];                  
      $newDbArr = array_unique($newDbArr);                
    }              

    if (empty($new)) {
      echo '<div class="warrCover"><div class="warr">Nezadali jste název položky!</div></div>';
    } else if (in_array($new, $newDbArr) == true) { //ochrana před vložením stejných položek
      echo '<div class="warrCover"><div class="warr">Položka menu <strong>'.$new.'</strong> už existuje. Zvolte jiný název</div></div>';
    } else {
                                                                                                                                                                                                     
      $order = 1;
      // poslední položka
      $result = mysqli_query($this->db_connect, "select `ord` from `$tb` order by `ord` desc limit 0,1");
      if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) {
          $order = $row['ord'] + 1;
        }
      } else {
        $order = 1;     
      }                                                                                       
      $new = strtr($new, array('"'=>'',"'"=>""));
                              
      // zapis do databaze pro hlavni jazyk administrace                                                          
      $sql = "insert into `$tb` (`{$this->entita}`, `ord`) values ('"
      .mysqli_real_escape_string($this->db_connect, $new)."','"
      .mysqli_real_escape_string($this->db_connect, $order)."')";
                                                
      $result = mysqli_query($this->db_connect, $sql);
                                                                                                     
      if (!$result) {
        echo '<div class="warrCover"><div class="warr">Chyba. Položku kategorie <strong>'.$new.'</strong> se nepodařilo zapsat do databáze '.$entita.' pro jazyk <strong>'.$this->langAdmin.'</strong>.</div></div>';
      }  
                
      $result = mysqli_query($this->db_connect, "select id from `$tb` order by id desc limit 0,1");
      $row = mysqli_fetch_array($result);
      $id = $row['id'];           
                            
      $languageAdmin = array_shift($this->language); // odstraní 1. hodnotu a vrací pole $language 
      // var_dump($language);      
      if ($this->language != null) {
        foreach ($this->language as $lg) {
          $tb = $this->entita.'_'.$lg;
          //echo $tbItem.'<br />'; 
                                                                                    
          $sql = "insert into `$tb` (id, `{$this->entita}`) values ($id,'"
          .mysqli_real_escape_string($this->db_connect, $new)."')";
                                                  
          $result = mysqli_query($this->db_connect, $sql);
                                                                                  
          if (!$result) {
            echo '<div class="warrCover"><div class="warr">Chyba! Položku kategorie <strong>'.$new.'</strong> se nepodařilo zapsat do databáze pro jazyk <strong>'.$lg.'</strong>.</div></div>';
          }          
        
        }                                                                                       
      
      }
                
    }  
  
  }
  
  public function deleteQuery($del, $id, $ent)
	{
    $tb = $ent.'_'.$this->langAdmin;
    $result = mysqli_query($this->db_connect, "select `$ent` from `$tb` where id = $id");
    $row = mysqli_fetch_array($result);
    if ($del != 0) {
      echo '<div class="warrCover"><div class="warr">';
      echo 'Položka <strong>'.$row[''.$ent.''].'</strong> bude smazána včetně všech navázaných položek. Opravdu chcete položku <strong>'.$row[''.$ent.''].'</strong> smazat?'; 
      echo '<table><tr>';
      echo '<td>';
        echo '<form method="post" action="">';
          echo '<input type="hidden" name="dok" value="1" />';
          echo '<input type="hidden" name="del" value="0" />';
          echo '<input type="hidden" name="entita" value="'.$ent.'" />'; // pro udržení entity ve formuláři
          echo '<input type="hidden" name="category" value="'.$ent.'" />'; // parametr objektu
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="smazat" />';
        echo '</form>';
      echo '</td>';
      echo '<td>';
        echo '<form method="get" action="">';
          echo '<input type="hidden" name="del" value="0" />';
          echo '<input type="hidden" name="entita" value="'.$ent.'" />'; // pro udržení entity ve formuláři
          echo '<input type="hidden" name="category" value="'.$ent.'" />';  // parametr objektu
          echo '<input type="submit" value="ne" />';
        echo '</form>';
      echo '</td>';
      echo '</tr></table>';
      echo '</div></div>';
    }
	}
	
	public function deleteCategory ($id, $ent)
	{ 
    $tb = $ent.'_'.$this->langAdmin;      
    $delCat = mysqli_query($this->db_connect, "delete from `$tb` where id = $id");
    if (!$delCat) {
      echo '<div class="warrCover"><div class="warr">Chyba! Smazání kategorie se nezdařilo.</div></div>';
    } 
    $delCat = mysqli_query($this->db_connect, "delete from dailymenu_1 where `group` = $id");      
  }  

  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new CategoryRenderer($this);
		}

		return $this->renderer->render();
	}
}