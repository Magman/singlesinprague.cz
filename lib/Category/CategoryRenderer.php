<?php 
require_once '../lib/Help/HelpAdmin.php';

class CategoryRenderer
{
	private $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{                                                                                                                                                                              
    $tb = $this->category->entita.'_'.$this->category->langAdmin;
    $result = mysqli_query($this->category->db_connect, "select * from `$tb` order by `ord`");
    $orderArr = array();
    while ($row = mysqli_fetch_array($result)) {
      $orderArr[$row['id']][$row[''.$this->category->entita.'']] = $row['ord'];
    }
    if ($orderArr != null) {
      echo '<div class="method">';                                  
        
        echo '<h3>Skupiny e-mailů</h3>';
        
        $help = new HelpAdmin ('cs'); // nápověda
        $help->showHelp ('category', 1);    
        echo '<form method="post" action="">';
          if ($orderArr != null) { 
            echo '<table>';
              foreach ($orderArr as $id => $arr) {                
                //var_dump($arr);
                foreach ($arr as $item => $or) {        
                  echo '<tr>';
                    echo '<td><strong>'.$item.'</strong></td>'; // link na subcategory                     
                    echo '<td>pořadí&nbsp;&nbsp;<input type="text" name="ord['.$id.']" value="'.$or.'" size="1"></td>
                    <td><a href = "admin_category.php?id='.$id.'&amp;entita='.$this->category->entita.'&amp;edit=1"><img src="../lib/design/tuzka.gif" alt="editovat" /></a></td>
                    <td><a href = "admin_category.php?idCategory='.$id.'&amp;showEmail=1"><img class="env" src="../lib/design/ikona_envelope.png" alt="e-maily" /></a></td>
                    <td><a href = "admin_category.php?id='.$id.'&amp;entita='.$this->category->entita.'&amp;del=1"><img src="../lib/design/delete.gif" alt="smazat" /></a></td>';
                  echo '</tr>';
                }            
              }
            echo '</table>';
          }
                    
          $orderArr = array();
                    
          echo '<input type="hidden" name="change" value="1">';
          echo '<input type="hidden" name="entita" value="'.$this->category->entita.'" />';                          
          echo '<input class="submitChange" type="submit" value="Změnit pořadí">';
        echo '</form>';             
      echo '</div> <!-- .method -->';
    }
  }
}
?> 