<?php
require_once '../lib/Tools/AddButton/AddButton.php';

require_once '../lib/Tools/DeleteButton/DeleteButton.php';

require_once '../lib/Tools/HideButton/HideButton.php';

require_once '../lib/Tools/CheckButton/CheckButton.php';

require_once '../lib/Tools/EditButton/EditButton.php';

require_once '../lib/Tools/ShowButton/ShowButton.php';

/**
 * Správa uživatelských účtů
 */
class AccountPerson
{
  
  public $db_connect = null;	
  
  /**
	* User id.
	*
	@var string
	*/
	public $uid = null;
				
	/**
	* @param array $db_connect
	* @param string $uid                         	 
	*/
	
  public function __construct($db_connect, $uid)
	{
		
    $this->db_connect = $db_connect;
		
    $this->uid = (string) $uid;

    $this->tb = 'registration';    

  } 
	
  public function insertButton() {    
     
    $addButton = new AddButton($this->uid);
    
    $addButton->addButtonFirst($entita = 'account', 'přidat uživatele'); // hlavni admin

  }
        
  public function registrationForm() {

    if (isset($_POST['name'])) $name = $_POST['name']; else $name = null;
    if (isset($_POST['surname'])) $surname = $_POST['surname']; else $surname = null;
    if (isset($_POST['email'])) $email = $_POST['email']; else $email = null;
    if (isset($_POST['phone'])) $phone = $_POST['phone']; else $phone = null;
    if (isset($_POST['birth'])) $birth = $_POST['birth']; else $birth = null;
  
    echo '<h3>Registrace</h3>';
    
    echo '<form method="post" action="">'; 
    
      echo '<fieldset class="formNewUser">';                      
        
        $hideButton = new HideButton ($this->uid);
        $hideButton->hideButtonHref($file = 'person.php');  
        
        echo '<input type="hidden" name="insAccount" value="1" />';
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<div class="row_reg"><div class="form_header">Jméno: </div><input type="text" name="name" value="'.$name.'" /></div>';
        echo '<div class="row_reg"><div class="form_header">Příjmení: </div><input type="text" name="surname" value="'.$surname.'" /></div>';
        echo '<div class="row_reg"><div class="form_header">E-mail: </div><input type="text" name="email" value="'.$email.'" /></div>';
        echo '<div class="row_reg"><div class="form_header">Telefon: </div><input type="text" name="phone" value="'.$phone.'" /></div>';
        echo '<div class="row_reg"><div class="form_header">Rok narození: </div><input type="text" name="birth" value="'.$birth.'" size="4"/></div>';        
        
        $selected1 = null;
        $selected2 = null;
        
        if (isset($_POST['sex'])) {
          
          switch ($_POST['sex']) {
            case 1: $selected1 = ' selected="selected"'; $selected2 = null;
            break;
            case 2: $selected2 = ' selected="selected"'; $selected1 = null;
            break;
            default: $selected2 = null; $selected1 = null;
          }                  
        
        }
        
        echo '<div class="row_reg"><div class="form_header">Žena / Muž: </div>';        
          echo '<select name="sex">';
            echo '<option value="1"'.$selected1.'>Žena</option>';
            echo '<option value="2"'.$selected2.'>Muž</option>';                                         
          echo '</select>';
        echo '</div>';
        
        echo '<div class="row_reg_nm"><div class="form_header">Kategorie:</div>';
          
          echo '<select name="category">';

            $categ = mysqli_query($this->db_connect, "select * from category_1 order by id");
                
            $selected = null;
            
            while ($ctg = mysqli_fetch_array($categ)) {           

              if (isset($_POST['category'])) {
              
                if ($_POST['category'] == $ctg['id']) $selected = ' selected="selected"'; else $selected = null;
              
              }
              
              echo '<option value="'.$ctg['id'].'"'.$selected.'>'.$ctg['category'].'</option>';
                  
            }
                                      
          echo '</select>';
                        
        echo '</div>';

        echo '<div class="form_header"><input type="submit" value="odeslat" /></div>';
        
      echo '</fieldset>';
    
    echo '</form>';
  
  }
        
  public function editForm($id) {
   
    $result = mysqli_query($this->db_connect, "select * from `{$this->tb}` where id = $id");
    
    $row = mysqli_fetch_array($result);
    
    echo '<h3>Editace uživatele</h3>';
    
    echo '<form method="post" action="">'; 
    
      echo '<fieldset class="formNewUser">';                      
        
        $hideButton = new HideButton ($this->uid);
        $hideButton->hideButtonHref($file = 'person.php');  
        
        echo '<input type="hidden" name="insEditAccount" value="1" />';
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="name" value="0" />';
        echo '<div class="row_reg"><div class="form_header">Jméno: </div><strong>'.$row['name'].'&nbsp;'.$row['surname'].'</strong></div>';
        echo '<div class="row_reg"><div class="form_header">E-mail: </div>'.$row['email'].'</div>';
        echo '<div class="row_reg"><div class="form_header">Datum registrace: </div>'.$row['registrationDate'].'</div>';
        echo '<div class="row_reg"><div class="form_header">Telefon: </div><input type="text" name="phone" value="'.$row['phone'].'" /></div>';
        echo '<div class="row_reg"><div class="form_header">Rok narození: </div><input type="text" name="birth" value="'.$row['birth'].'" size="4"/></div>';        
        switch ($row['sex']) {
          case 1: $selected1 = ' selected="selected"'; $selected2 = null;
          break;
          case 2: $selected2 = ' selected="selected"'; $selected1 = null;
          break;
          default: $selected2 = null; $selected1 = null;
        }                  
        echo '<div class="row_reg"><div class="form_header">Žena / Muž: </div>';        
          echo '<select name="sex">';
            echo '<option value="1"'.$selected1.'>Žena</option>';
            echo '<option value="2"'.$selected2.'>Muž</option>';                                         
          echo '</select>';
        echo '</div>';
        
        echo '<div class="row_reg_nm"><div class="form_header">Kategorie:</div>';
          
          echo '<select name="category">';

            $categ = mysqli_query($this->db_connect, "select * from category_1 order by id");
                
            while ($ctg = mysqli_fetch_array($categ)) {           

              if ($row['category'] == $ctg['id']) $selected = ' selected="selected"'; else $selected = null;
              
              
              echo '<option value="'.$ctg['id'].'"'.$selected.'>'.$ctg['category'].'</option>';
                  
            }
                                      
          echo '</select>';
                        
        echo '</div>';
             
        switch ($row['active']) {
          case 1: $checked1 = ' checked="checked"'; $checked2 = null;
          break;
          case 0: $checked2 = ' checked="checked"'; $checked1 = null;
          break;
          default: $checked2 = null; $checked1 = null;
        }            
        echo '<div class="row_reg"><div class="form_header">Aktivní: </div>ano:&nbsp;<input type="radio" name="active" value="1"'.$checked1.' />&nbsp;&nbsp;&nbsp;ne:&nbsp;<input type="radio" name="active" value="0"'.$checked2.' /></div>';   

        switch ($row['confirm']) {
          case 1: $checked1 = ' checked="checked"'; $checked2 = null;
          break;
          case 0: $checked2 = ' checked="checked"'; $checked1 = null;
          break;
          default: $checked2 = null; $checked1 = null;
        }            
        echo '<div class="row_reg"><div class="form_header">Potvrzeno: </div>ano:&nbsp;<input type="radio" name="confirm" value="1"'.$checked1.' />&nbsp;&nbsp;&nbsp;ne:&nbsp;<input type="radio" name="confirm" value="0"'.$checked2.' /></div>';   
        echo '<div class="form_header"><input type="submit" value="odeslat" /></div>';
        
      echo '</fieldset>';
    echo '</form>';

  }
  
  public function changePass($id) {
    
    echo '<h3>Změna hesla</h3>';
    
    echo '<form method="post" action="">'; 
    
      echo '<fieldset class="formNewUser">';                      
        
        $hideButton = new HideButton ($this->uid);
        $hideButton->hideButtonHref($file = 'person.php');  
        
        if (isset($_POST['person'])) $per =  $_POST['person']; else $per = null;
        
        echo '<input type="hidden" name="insPass" value="1" />';
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';             
        echo '<div class="row_reg"><div class="form_header">Nové heslo: </div><input type="text" name="person" value="'.$per.'" /></div>';
        echo '<div class="row_reg"><div class="form_header">Heslo znovu: </div><input type="text" name="again" value="" /></div>';                     
        echo '<div class="form_header"><input type="submit" value="odeslat" /></div>';
        
      echo '</fieldset>';
    
    echo '</form>';

  }
  
  public function insPass($id, $personaly, $again) {
    
    $personaly = trim($personaly);
    $again = trim($again);
    
    if (strlen($personaly) < 5) {
      echo '<span class="warAdmin">&nbsp;&nbsp;Heslo musí obsahovat nejméně 5 znaků!<br /><br /></span>';
      $this->changePass($id);    
    } else if ($personaly != $again) {
      echo '<span class="warAdmin">&nbsp;&nbsp;Heslo pro ověření bylo zadáno chybně!<br /><br /></span>';
      $this->changePass($id);
    } else {        
      
      $personaly = mysqli_real_escape_string($this->db_connect, $personaly);
      $personaly = md5($personaly);
      
      $update = mysqli_query($this->db_connect, "update `{$this->tb}` set personaly = '$personaly' where id = $id");
      if (!$update) {
        echo '<span class="warAdmin">Chyba! Editace hesla uživatele se nezdařila.<br /><br /></span>';
      } else {
        echo '<span class="warAdmin">Změna hesla proběhla úspěšně.<br /><br /></span>';
        //$this->insertButton();
        $this->changePass($id);
      }
    }
  
  }             
                         
  public function deleteUser($id) { 
    
    $delete = mysqli_query($this->db_connect, "delete from `{$this->tb}` where id = $id limit 1");
    
    /*
    $candidateImg = glob('../candidate_foto/'.$id.'/*');
      
      if ($candidateImg != null) {
      
        foreach ($candidateImg as $cI) {
        
          unlink($cI); 
        
        }
        
      }
      
      rmdir('../candidate_foto/'.$id);
      */
    
    
    if (!$delete) {
      
      echo '<span class="warAdmin">Chyba! Uživatele se nepodařilo smazat.</span>';
     
      $this->insertButton();   
    
    }
 
  }
          
  public function queryDeleteUser($id) {  

    $result = mysqli_query($this->db_connect, "select name, surname from `{$this->tb}` where id = $id");
    $row = mysqli_fetch_array($result);
    
    echo '<div class="formDelQuery">';
        
      $hideButton = new HideButton($this->uid);  
      echo $hideButton->hideButtonFirst();
          
      echo '<form method="post" action="">';
        echo '<span class="warAdmin">Chcete opravdu smazat účet <strong>'.$row['surname'].'&nbsp;'.$row['name'].'</strong>? </span>'; 
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="dokAccount" value="1" />';
        echo '<input type="submit" value="smazat" /><br /><br />';              
      echo '</form>';

    echo '</div>';
             
  }
          
  public function changeActive($id, $active) {

    $update = mysqli_query($this->db_connect, "update `{$this->tb}` set active = $active where id = $id");
    $this->insertButton();
    
    if (!$update) echo '<span class="warAdmin">Chyba! Nastavení aktivity se nezdařilo.</span><br /><br />';

  }
          
  public function editUser($id, $phone, $birth, $sex, $category, $active, $confirm) {
      
    $update = mysqli_query($this->db_connect, "update `{$this->tb}` set phone = '".mysqli_real_escape_string($this->db_connect, $phone)."',  birth = '".mysqli_real_escape_string($this->db_connect, $birth)."', sex = $sex, category = $category, active = $active, confirm = $confirm where id = $id");
      
    if (!$update) {
        
      echo '<span class="warAdmin">Chyba! Editace hesla uživatele se nezdařila.<br /><br /></span>';
      
    } else {
        
      echo '<span class="warAdmin">Editace účtu proběhla úspěšně.<br /><br /></span>';

      $this->editForm($id);
      
    }
  
  }                  
      
  public function insAccount($name, $surname, $email, $phone, $birth, $sex, $category) { 
    
    $emailArr = null;
      
    $result = mysqli_query($this->db_connect, "select email from registration");
      
    while ($row = mysqli_fetch_array($result)) {
        
      $emailArr[] = $row['email'];
      
    }
      
    $exist = 1;
      
    if ($emailArr != null) {
      
      if (!in_array($_POST['email'], $emailArr)) $exist = 0;
      
    } else { // tabulka je prazdna
      
      $exist = 0;
      
    }
      
    if ($exist == 0) {        
      
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = null;
      
      for ($i = 0; $i < 7; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
        
      $personaly = md5($randomString);
        
      $uid = rand(121031239, 998989798); 
        
      $insertReg = mysqli_query($this->db_connect, "insert into registration (uid, name, surname, email, phone, birth, sex, category, personaly) values ($uid, '"
      .mysqli_real_escape_string($this->db_connect, $_POST['name'])
      ."','"
      .mysqli_real_escape_string($this->db_connect, $_POST['surname'])
      ."','"
      .mysqli_real_escape_string($this->db_connect, $_POST['email'])
      ."','"
      .mysqli_real_escape_string($this->db_connect, $_POST['phone'])
      ."','"
      .mysqli_real_escape_string($this->db_connect, $_POST['birth'])
      ."', {$_POST['sex']}, {$_POST['category']}, '$personaly')"); 
                   
      if (!$insertReg) {
            
        echo 'Chyba! Přenos dat se nezdařil.';  
        
      } else {
            
        $result = mysqli_query($this->db_connect, "select id from registration order by id desc limit 0,1");
          
        $row = mysqli_fetch_array($result);
          
        $id = $row['id'];
          
        $md5Id = md5($id);
          
        $updateReg = mysqli_query($this->db_connect, "update registration set md5Id = '$md5Id' where id = $id");
          
        if (!$updateReg) {
          
          echo '<span class="warAdmin">Chyba! Přenos dat se nezdařil.<br /><br /></span>';
          
        } else {
        
          $this->insertButton(); 
        
        }
            
      }   

    } else {
    
      echo '<span class="warAdmin">Registrace na zadaný e-mail již existuje.<br /><br /></span>';
      
      $this->registrationForm();
    
    }
    
  }         
                                                          
  public function showUser() {

    echo '<div class="pager" id="pagerPerson"></div>';
    
    echo '<table id="myTable" class="tablesorter">';

      echo '<caption>Seznam uživatelů</caption>';
      
      echo '<thead>';
        
        echo '<tr><th>Jméno</th><th>Akt</th><th>Edit</th><th>Pas</th><th>Del</th></tr>';
      
      echo '</thead>';
      
      echo '<tbody>';
          
        $result = mysqli_query($this->db_connect, "select id, name, surname, active from `{$this->tb}` order by surname asc");

        while ($row = mysqli_fetch_array($result)) {

          echo '<tr class="itemPerson">';
            
            echo '<td>'.$row['surname'].'&nbsp;'.$row['name'].'</td>';
                        
            echo '<td class="tableButton">';

              $checkButton = new CheckButton($this->uid);
              
              $checkButton->checked($entita = 'account', $row['active'], $row['id']);

            echo '</td>';                           
            
            echo '<td class="tableButton">';
  
              $editButton = new EditButton($this->uid);
              
              $editButton->editButtonFirst($entita = 'account', $row['id']);
                                     
            echo '</td>'; 
            
            echo '<td class="tableButton">';
  
              echo '<form method="post" action="">';
                echo '<input type="hidden" name="changePass" value="1" />';
                echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
                echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
                echo '<input class="password" type="submit" value="" />';
              echo '</form>';
                                     
            echo '</td>'; 
                                 
            echo '<td class="tableButton">';
              
              $deleteButton = new DeleteButton ($this->uid);
              
              $deleteButton->deleteButtonFirst($entita = 'account', $row['id']);
       
            echo '</td>';
          
          echo '</tr>';
        
        }
      
      echo '</tbody>';
    
    echo '</table>';
  
  }

}
?>

