<?php

require_once (dirname(__FILE__) . '/MessageRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Message) 

class Message {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	    /**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
	
		  /**
	 * Pole e-mailových adres
	 *
	 * @var array
	 */
	public $mailArr = null;
	
		  /**
	 * e-mail pro skrytou kopii
	 *
	 * @var string
	 */
	public $addBcc = null;
	
			  /**
	 * mail server
	 *
	 * @var string
	 */
	public $mailServer = null;
	
	public function __construct($db_connect, $lang, $mailArr, $addBcc, $mailServer) {
    $this->db_connect = $db_connect;
    $this->lang = (int) $lang;
    $this->mailArr = $mailArr;
    $this->addBcc = (string) $addBcc;
    $this->mailServer = (string) $mailServer;
  }

  public function mailMessage ($mailArr, $addBcc, $mailServer)
  {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = $_POST['message'];
    $date = date('j. n. Y G:i:s');
    
    require "./phpmailer/class.phpmailer.php";
    
    if ($mailArr != null) {
      foreach ($mailArr as $k => $em) { 
        $mail = new PHPMailer();
        $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
        $mail->Host = $mailServer;  // zadáme adresu SMTP serveru
        $mail->SMTPAuth = false;               // nastavíme true v případě, že server vyžaduje SMTP autentizaci
        //$mail->Username = "";   // uživatelské jméno pro SMTP autentizaci
        //$mail->Password = "";            // heslo pro SMTP autentizaci
        $mail->From = $_POST['email'];   // adresa odesílatele skriptu
        $mail->FromName = $name; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
        $mail->AddAddress($em);  // přidáme příjemce a klidně i druhého včetně jména
        if ($k == 0) $mail->AddBcc($addBcc); 
        $mail->Subject = "Dotaz návštěvníka webu";    // nastavíme předmět e-mailu
        $mail->IsHTML(true);
        $mail->Body = "<html><body><strong>Datum dotazu:</strong> $date<br />
        <strong>E-mail:</strong> $email<br />
        <strong>Telefon:</strong> $phone<br />
        <strong>Dotaz:</strong> $message<br />  
        ";
        
        $mail->AltBody = "Datum dotazu: $date\n
        E-mail: $email\n
        Telefon: $phone\n
        Dotaz: $message\n   
        ";  // nastavíme tělo e-mailu
        $mail->WordWrap = 100;   // je vhodné taky nastavit zalomení (po 50 znacích)
        $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail
        
        if(!$mail->Send()) {  // odešleme e-mail
          echo '<div class="warrCover"><div class="warr">';
            echo 'Error. E-mail failed.';
            echo 'Notice: ' . $mail->ErrorInfo;
          echo '</div></div>';
        } else {
          unset ($mail);
          echo '<div class="warrCover"><div class="warr">E-mail sent.</div></div>';
        }
      }
    }
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new MessageRenderer($this);
		}

		return $this->renderer->render();
	}

}
?>