<?php
class NewPassword {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	    /**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
	
		  /**
	 * Pole e-mailových adres
	 *
	 * @var array
	 */
	public $mailArr = null;
	
		  /**
	 * e-mail pro skrytou kopii
	 *
	 * @var string
	 */
	public $addBcc = null;
	
			  /**
	 * mail server
	 *
	 * @var string
	 */
	public $mailServer = null;
	
		  /**
	 * path to securimage
	 *
	 * @var string
	 */
	public $path = null;
	
	public function __construct($db_connect, $lang, $mailArr, $addBcc, $mailServer, $path) {
    $this->db_connect = $db_connect;
    $this->lang = (int) $lang;
    $this->mailArr = $mailArr;
    $this->addBcc = (string) $addBcc;
    $this->mailServer = (string) $mailServer;
    $this->path = (string) $path;
  }

  public function mailPassword($personaly) // $mailArr, $addBcc, $mailServer
  {
    switch ($this->lang) {
      case 1: $phReg1 = 'Change over'; 
      $phReg3 = 'Date'; 
      $phReg4 = 'Your new password'; 
      $phReg5 = 'Klara'; 
      $phReg6 = 'Singles in Prague';  
      break;
      case 2: $phReg1 = 'Změna'; 
      $phReg3 = 'Datum'; 
      $phReg4 = 'Vaše nové heslo'; 
      $phReg5 = 'Ing. Klára Le Cornu'; 
      $phReg6 = 'Singles in Prague';
    }
    
    $date = date('j. n. Y G:i:s');
       
    require ($this->path.'phpmailer/class.phpmailer.php');
    
    if ($this->mailArr != null) {
      foreach ($this->mailArr as $k => $em) { 
        $mail = new PHPMailer();
        $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
        $mail->Host = $this->mailServer;  // zadáme adresu SMTP serveru
        $mail->SMTPAuth = true;               // nastavíme true v případě, že server vyžaduje SMTP autentizaci        
        $mail->Username = "singles@singlesinprague.cz";   // uživatelské jméno pro SMTP autentizaci
        $mail->Password = "Iamsuccesful123";            // heslo pro SMTP autentizaci 57ubyXWUXR
        $mail->From = 'singles@singlesinprague.cz';   // adresa odesílatele skriptu
        $mail->FromName = 'Singles in Prague'; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
        $mail->AddAddress($em);  // přidáme příjemce a klidně i druhého včetně jména
        if ($k == 0) $mail->AddBcc($this->addBcc); 
        $mail->Subject = "Singles in Prague: $phReg1";    // nastavíme předmět e-mailu
        $mail->IsHTML(true);
        $mail->Body = "<html>
          <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
          </head>
          <body style=\"font-family: 'Times New Roman', serif; font-size:16px; line-height:1.3;\">
            <div style=\"float:left; width:670px; height:auto; margin-bottom:15px;\">           
              <img style=\"margin:10px\" src=\"http://singlesinprague.cz/design/logo.png\" alt=\"Logo\">
              <h2 style=\"font-size:24px; color:#992f4a; line-height:1.3;\">$phReg1</h2>
              <strong>$phReg3:</strong> $date<br /><br />                   
              <strong>$phReg4:</strong> $personaly<br /><br />
              $phReg5<br />
              $phReg6<br />                           
            </div>
          </body>
        </html>
        ";       
        $mail->AltBody = "$phReg1\n\n
          $phReg3: $date\n\n                   
          $phReg4: $personaly\n\n
          $phReg5\n
          $phReg6\n 
        ";  // nastavíme tělo e-mailu
        $mail->WordWrap = 100;   // je vhodné taky nastavit zalomení (po 50 znacích)
        $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail
        
        if(!$mail->Send()) {  // odešleme e-mail
          echo '<div class="warrCover"><div class="warr">';
            echo 'Error. E-mail failed.';
            echo 'Notice: ' . $mail->ErrorInfo;
          echo '</div></div>';
        } else {
          unset ($mail);
          // echo 'E-mail sent. ';
        }
      }
    }
  }

}
?>