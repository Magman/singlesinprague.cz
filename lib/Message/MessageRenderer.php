<?php
class MessageRenderer {

  private $message;

	public function __construct(Message $message)
	{
		$this->message = $message;
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    switch ($this->message->lang) {
      case 1: $call = 'Váš dotaz'; $name = 'Jméno *'; $email = 'E-mail *'; $phone = 'Telefon'; $query = 'Váš dotaz'; $code = 'Opište kontrolní kód'; $phName = 'Nezadali jste jméno!'; $phEmail = 'E-mail nemá správný tvar!'; $phPhone = 'Nezadali jste telefon!';  $phOk = 'E-mail byl odeslán.'; $phCode = 'Kontrolní kód nebyl zadán správně.'; $phSend = 'odeslat';
      break;
      case 2: $call = 'Your question'; $name = 'Name *'; $email = 'E-mail *'; $phone = 'Phone'; $query = 'Your question'; $code = 'Rewrite the control code'; $phName = 'You have not entered your name!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your telephone number!'; $phOk = 'Your e-mail has been sent.'; $phCode = 'The control code is not correct.'; $phSend = 'send';
      break;
      case 3: $call = 'Ihre Anfrage'; $name = 'Name *'; $email = 'E-Mail *'; $phone = 'Telefon'; $query = 'Ihre Anfrage'; $code = 'Bitte Kontrollcode abschreiben'; $phName = 'Name fehlt!'; $phEmail = 'E-Mail im falschen Format!'; $phPhone = 'Telefonnummer fehlt!'; $phOk = 'Ihre E-Mail wurde versendet.'; $phCode = 'Der Kontrollcode wurde falsch eingegeben.'; $phSend = 'wegschicken';
      break;
      case 4: $call = 'Message'; $name = 'Nom *'; $email = 'E-mail *'; $phone = 'Téléphone'; $query = 'Message'; $code = 'Rewrite the control code'; $phName = 'You have not entered your name!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your telephone number!'; $phOk = 'Your e-mail has been sent.'; $phCode = 'The control code is not correct.'; $phSend = 'send';
      break;
      case 5: $call = 'Messagio'; $name = 'Nome *'; $email = 'E-mail'; $phone = 'Teléfono'; $query = 'Messagio'; $code = 'Rewrite the control code'; $phName = 'You have not entered your name!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your telephone number!'; $phOk = 'Your e-mail has been sent.'; $phCode = 'The control code is not correct.'; $phSend = 'send';
      break;
      case 6: $call = 'Mensaje'; $name = 'Nombre *'; $email = 'E-mail *'; $phone = 'Teléfono'; $query = 'Mensaje'; $code = 'Rewrite the control code'; $phName = 'You have not entered your name!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your telephone number!'; $phOk = 'Your e-mail has been sent.'; $phCode = 'The control code is not correct.'; $phSend = 'send';
      break;
      case 7: $call = 'Записка'; $name = 'Имя *'; $email = 'Э-почта *'; $phone = 'Телефон'; $query = 'Записка'; $code = 'Rewrite the control code'; $phName = 'You have not entered your name!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your telephone number!'; $phOk = 'Your e-mail has been sent.'; $phCode = 'The control code is not correct.'; $phSend = 'send';
    }
                
    echo '<form class="message" method="post" action="">';
      echo '<p class="ajaxData"></p>';
      echo '<table class="tabMess">';
        if (isset($_POST['captcha_code'])) {
        
          if (empty($_POST['name'])) { 
            echo '<div class="warrCover"><div class="warr">'.$phName.'</div></div>';
            //$again = 1;
          } else if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $_POST['email'])) {
            echo '<div class="warrCover"><div class="warr">'.$phEmail.'</div></div>';
            //$again = 1;
          } else { 
         
            require_once './securimage/securimage.php';
                
            $securimage = new Securimage();
                            
            if ($securimage->check($_POST['captcha_code']) == false) {
              echo '<div class="warrCover"><div class="warr">'.$phCode.'</div></div>';
            } else {            
              $this->message->mailMessage ($this->message->mailArr, $this->message->addBcc, $this->message->mailServer);
              //$again = 0; 
              unset ($_POST['name']); 
              unset ($_POST['email']);
              unset ($_POST['phone']);
              unset ($_POST['message']);             
            }                   
          }
        }
        if (isset($_POST['message'])) $formmessage = $_POST['message']; else $formmessage = null;
        if (isset($_POST['name'])) $formname = $_POST['name']; else $formname = null;
        if (isset($_POST['email'])) $formemail = $_POST['email']; else $formemail = null;
        if (isset($_POST['phone'])) $formphone = $_POST['phone']; else $formphone = null;    
        
        echo '<tr><td>'.$name.'</td><td><input class="required inpField" type="text" name="name" value="'.$formname.'" /></td></tr>';
        echo '<tr><td>'.$email.'</td><td><input class="email inpField" type="text" name="email" value="'.$formemail.'" /></td></tr>'; 
        echo '<tr><td>'.$phone.'</td><td><input class="phone inpField" type="text" name="phone" value="'.$formphone.'" /></td></tr>';
        echo '<tr><td colspan="2"><textarea class="textField" name="message" rows="4" cols="40">'.$formmessage.'</textarea></td></tr>';            
        echo '<tr><td colspan="2">';
            echo '<div class="control">';
              echo '<div class="pict">';
                echo '<img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" />';
                echo '<div class="refresh"><a href="#" title="Nový kód" onclick="document.getElementById(\'captcha\').src = \'./securimage/securimage_show.php?\' + Math.random(); return false"><img src="./securimage/images/refresh.png" alt="refresh" /></a></div>';
              echo '</div>';
              echo '<div class="field">';
                echo '<input type="text" name="captcha_code" value="'.$code.'" onclick="if(!this.__click) {this.__click = true;this.value = \'\'}" />';
              echo '</div>';
            echo '</div>';
         echo '</td></tr>';
        echo '<tr><td colspan="2"><input type="submit" value="'.$phSend.'" /></td></tr>'; 
      echo '</table>';            
    echo '</form>';  
  }

} 
?>  