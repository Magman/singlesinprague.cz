<?php
/**
 * Set parameters
*/
class SettingAdmin
{

  public $db_connect = null;	
  
  public $langAdmin = 0;	
	
  public function __construct($db_connect, $langAdmin)
	{
		$this->db_connect = $db_connect;
	  $this->langAdmin = $langAdmin;
	}
	
	public function updateSetting ($largeViewX, $largeViewY, $maxFileSize, $fileSize, $thumb, $limit, $width, $switcher, $sort) 
	{
    $phArr = $this->translate($this->langAdmin);
    
    if (!preg_match("/^[1-9]([0-9]){0,3}$/", $largeViewX)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[0].'</div></div>'; 
    } else if (!preg_match("/^[1-9]([0-9]){0,3}$/", $largeViewY)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[0].'</div></div>';     
    } else if (!preg_match("/^[1-9]([0-9]){0,6}$/", $maxFileSize)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[1].'</div></div>';  
    } else if (!preg_match("/^[1-9]([0-9]){0,1}$/", $fileSize)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[2].'</div></div>';  
    } else if ($fileSize > 60) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[3].'</div></div>';  
    } else if (!preg_match("/^[1-9]([0-9]){0,1}$/", $limit)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[4].'</div></div>';  
    } else if ($limit > 15) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[5].'</div></div>';  
    } else if (!preg_match("/^[1-9]([0-9]){0,3}$/", $width)) {
      echo '<div class="wACover"><div class="warrAdmin margForm">'.$phArr[7].'</div></div>'; 
    } else {
      $result = mysqli_query($this->db_connect, "update parameter set value = $largeViewX where id = 1");
      $result = mysqli_query($this->db_connect, "update parameter set value = $maxFileSize where id = 2");
      $result = mysqli_query($this->db_connect, "update parameter set value = $fileSize where id = 3");
      $result = mysqli_query($this->db_connect, "update parameter set value = $thumb where id = 4");
      $result = mysqli_query($this->db_connect, "update parameter set value = $limit where id = 5");
      //$result = mysqli_query($this->db_connect, "update parameter set value = $perPage where id = 6");
      $result = mysqli_query($this->db_connect, "update parameter set value = $width where id = 7");
      $result = mysqli_query($this->db_connect, "update parameter set value = $switcher where id = 8"); 
      $result = mysqli_query($this->db_connect, "update parameter set value = $largeViewY where id = 9");
      $result = mysqli_query($this->db_connect, "update parameter set value = $sort where id = 10");       
    }      
  }
  
  public function formSetting ()
  {
    $phArr = $this->translate($this->langAdmin);
    
    $result = mysqli_query($this->db_connect, "select id, value from parameter order by id");
    while ($row = mysqli_fetch_array($result)) {
      switch ($row['id']) {
        case 1: $largeViewX = $row['value'];
        case 2: $maxFileSize = $row['value'];
        case 3: $fileSize = $row['value'];
        case 4: $thumb = $row['value'];
        case 5: $limit = $row['value'];
        case 6: $perPage = $row['value'];
        case 7: $width = $row['value'];
        case 8: $switcher = $row['value'];
        case 9: $largeViewY = $row['value'];
        case 10: $sort = $row['value'];
      }    
    }
    
    switch ($thumb) {
      case 0: $checked0 = 'checked="checked"';  $checked1 = '';
      break;
      case 1: $checked0 = '';  $checked1 = 'checked="checked"';
    }
    
    switch ($switcher) {
      case 0: $checkedS0 = 'checked="checked"';  $checkedS1 = '';
      break;
      case 1: $checkedS0 = '';  $checkedS1 = 'checked="checked"';
    }
    
    switch ($sort) {
      case 0: $checkedSo0 = 'checked="checked"';  $checkedSo1 = '';
      break;
      case 1: $checkedSo0 = '';  $checkedSo1 = 'checked="checked"';
    }
    
    if (isset($_POST['largeViewX'])) $largeViewX = $_POST['largeViewX'];
    if (isset($_POST['largeViewY'])) $largeViewY = $_POST['largeViewY'];
    if (isset($_POST['maxFileSize'])) $maxFileSize = $_POST['maxFileSize'];
    if (isset($_POST['fileSize'])) $fileSize =  $_POST['fileSize'];
    if (isset($_POST['thumb'])) {  
      $thumb = $_POST['thumb']; 
      switch ($thumb) {
        case 0: $checked0 = 'checked="checked"';  $checked1 = '';
        break;
        case 1: $checked0 = '';  $checked1 = 'checked="checked"';
      }     
    }
    if (isset($_POST['limit'])) $limit = $_POST['limit'];
    if (isset($_POST['perPage'])) $perPage = $_POST['perPage'];
    if (isset($_POST['width'])) $width = $_POST['width'];
    if (isset($_POST['switcher'])) {  
      $switcher = $_POST['switcher']; 
      switch ($switcher) {
        case 0: $checkedS0 = 'checked="checked"';  $checkedS1 = '';
        break;
        case 1: $checkedS0 = '';  $checkedS1 = 'checked="checked"';
      }     
    }
    if (isset($_POST['sort'])) {  
      $sort = $_POST['sort']; 
      switch ($sort) {
        case 0: $checkedSo0 = 'checked="checked"';  $checkedS01 = '';
        break;
        case 1: $checkedSo0 = '';  $checkedSo1 = 'checked="checked"';
      }     
    }
        
      echo '<form method="post" action="" class="formParam">';
        echo '<table>';
          echo '<tr><td>'.$phArr[8].'&nbsp;</td><td><input type="text" name="largeViewX" value="'.$largeViewX.'" size="8" /></td><td>&nbsp;px&nbsp;<span class="italic">'.$phArr[9].'</span></td></tr>';
          echo '<tr><td>'.$phArr[34].'&nbsp;</td><td><input type="text" name="largeViewY" value="'.$largeViewY.'" size="8" /></td><td>&nbsp;px&nbsp;<span class="italic">'.$phArr[9].'</span></td></tr>';
          echo '<tr><td>'.$phArr[10].'&nbsp;</td><td><input type="text" name="maxFileSize" value="'.$maxFileSize.'" size="8" /></td><td>&nbsp;'.$phArr[11].'</td></tr>';
          echo '<tr><td>'.$phArr[12].'&nbsp;</td><td><input type="text" name="fileSize" value="'.$fileSize.'" size="8" /></td><td></td></tr>';
          //echo '<tr><td>'.$phArr[13].'&nbsp;</td><td>'.$phArr[14].'&nbsp;<input type="radio" name="thumb" value="0" '.$checked0.' /></td><td>'.$phArr[15].'&nbsp;<input type="radio" name="thumb" value="1" '.$checked1.' /></td></tr>';
          //echo '<tr><td>'.$phArr[16].'&nbsp;</td><td><input type="text" name="limit" value="'.$limit.'" size="8" /></td><td><span class="italic">('.$phArr[17].')</span></td></tr>';
          //echo '<tr><td>'.$phArr[18].'&nbsp;</td><td><input type="text" name="perPage" value="'.$perPage.'" size="8" /></td><td><span class="italic">('.$phArr[19].')</span></td></tr>';
          //echo '<tr><td>'.$phArr[20].'&nbsp;</td><td><input type="text" name="width" value="'.$width.'" size="8" /></td><td>&nbsp;px&nbsp;<span class="italic">(frontend)</span></td></tr>';
          //echo '<tr><td>'.$phArr[21].'&nbsp;</td><td>'.$phArr[31].'&nbsp;<input type="radio" name="switcher" value="0" '.$checkedS0.' /></td><td>'.$phArr[30].'&nbsp;<input type="radio" name="switcher" value="1" '.$checkedS1.' /></td></tr>';
          echo '<tr><td>'.$phArr[35].'&nbsp;</td><td>'.$phArr[36].'&nbsp;<input type="radio" name="sort" value="0" '.$checkedSo0.' />&nbsp;&nbsp;&nbsp;</td><td>'.$phArr[37].'&nbsp;<input type="radio" name="sort" value="1" '.$checkedSo1.' /></td></tr>';
        echo '</table>';
        echo '<input type="submit" value="'.$phArr[22].'" />';
      echo '</form>';     
  }
  
  public function updateLanguage ($selected, $idLanguage)
  {
  
    $result = mysqli_query($this->db_connect, "update language set active = $selected where id = $idLanguage"); 
  
  }
  
  public function changeOrderLanguage ($order)
  {
    $phArr = $this->translate($this->langAdmin);
                    
    if ($order != null) {                           
      echo '<div class="edit_block_list warAdmin">';
      // Control of unique items
      if (count($order) != count(array_unique($order))) {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[23].'</div></div>';
      } else {
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                     
          $sql = "update language set ord = '"
          .mysqli_real_escape_string($this->db_connect, $i)."' where id = $id";                        
            $esult = mysqli_query($this->db_connect, $sql);
          $i++;      
        }
        if (!$result) {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[24].'</div></div>';
        } else {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[25].'</div></div>';
        }                                                                             
      }
    }  
  
  }
  
  public function formLanguageAdmin ($langAdminArr)
  {
    
    
    $phArr = $this->translate($this->langAdmin);
 
    $checked1 = null;
    $checked2 = null;    
    switch ($this->langAdmin) {
      case 1: $checked1 = 'checked="checked"';
      break;
      case 2: $checked2 = 'checked="checked"';
    }
    
    echo '<form method="post" action="" />';
      echo '<h3>'.$phArr[33].'</h3>';
      echo '<table class="tabLangAdminAdmin"><tr>';
        echo '<td><img src="../lib/Gallery/design/en.png" alt="en" />&nbsp;<input type="radio" name="setLA" value="2" '.$checked2.' /></td>';
        echo '<td><img src="../lib/Gallery/design/cs.png" alt="cs" /><input type="radio" name="setLA" value="1" '.$checked1.' /></td>';
        echo '<td><input type="submit" value="'.$phArr[22].'" /></td>';
      echo '</tr></table>';
    echo '</form>';
    
    
  }
  
  public function formLanguage ()
  {
    $phArr = $this->translate($this->langAdmin);
  
    $result = mysqli_query($this->db_connect, "select id, lang, ord from language order by ord");
    echo '<form method="post" action="" class="formLangAdmin">';
      echo '<table class="tabLangAdmin">';
        echo '<tr><th><h3>'.$phArr[26].'</h3></th><th><h3>'.$phArr[27].'</h3></th><th><h3>'.$phArr[28].'</h3></th></tr>';
        while ($row = mysqli_fetch_array($result)) {
          echo '<tr><td><img src="../lib/SwitcherLanguage/design/'.$row['lang'].'.png" title="'.$row['lang'].'" /></td><td><input type="text" name="order['.$row['id'].']" value="'.$row['ord'].'" size="1"/></td>';
            $selected = mysqli_query($this->db_connect, "select active from language where id = {$row['id']}");
            $sel = mysqli_fetch_array($selected);
            echo '<td>';
              if ($sel['active'] == 1) {
                echo '<a href="'.$_SERVER['SCRIPT_NAME'].'?selected=0&amp;idLanguage='.$row['id'].'" title="'.$phArr[28].'"><img src="../img/checked_yes.gif" alt="'.$phArr[30].'" /></a>';
              } else {
                echo '<a href="'.$_SERVER['SCRIPT_NAME'].'?selected=1&amp;idLanguage='.$row['id'].'" title="'.$phArr[29].'"><img src="../img/checked_no.gif" alt="'.$phArr[31].'" /></a>';
              }
            echo '</td>';            
          echo '</tr>';        
        }
      echo '</table>';
      echo '<input type="submit" name="change" value="'.$phArr[32].'" />';
    echo '</form>';  
  }
  
  private function translate ($lang)
  {
    $phArr = array();
    $result = mysqli_query($this->db_connect, "select phrase from trans_admin where category = 'sett2' and lang = $lang order by ord");
    while ($row = mysqli_fetch_array($result)) {
      $phArr[] = $row['phrase'];
    }
    return $phArr;
  }

}