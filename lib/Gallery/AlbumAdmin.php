<?php
/**
 * Administration of albums
 */
class AlbumAdmin
{

  public $db_connect = null;	
    
    /**
	 * Hlavní jazyk.
	 *
	 * @var int
	 */
	public $langAdmin = 0;
	
	
	/**
	 * Položka jazyky
	 *
	 * @var array
	 */
	public $language = null;
	
		/**
	 * Položka jazyky
	 *
	 * @var array
	 */
	public $languageActive = null;
					
	/**
	 * @param array $db_connect
	 * @param int $langAdmin
	 * @param array $language
	 * @param array $languageActive                	 
	*/
	
  public function __construct($db_connect, $langAdmin, $language, $languageActive)
	{
		$this->db_connect = $db_connect;
		$this->langAdmin = (int) $langAdmin;
		$this->language = $language;
		$this->languageActive = $languageActive;
	}

	public function showAlbum()
  {
    $phArr = $this->translate($this->langAdmin);
    
    $tbAlbumAdmin = 'gallery_album_'.$this->langAdmin;
              
    // 1. LEVEL 
    echo '<div class="editBlockAlbum">';           
      //echo '<h2>1'.$phArr[5].'</h2>'; // st level
      echo '<div class="menuList">';                                 
        // Form item sequence 
        echo '<form method="post" action="">';
          if ($this->langAdmin == 1) {            
            $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.item, gallery_album_1.`order` from gallery_album_1, events where gallery_album_1.idParent = 0 and gallery_album_1.idEvent = events.id order by events.dateEvent desc");
          } else {
            $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.`order`, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin`, events where gallery_album_1.idParent = 0 and  gallery_album_1.id = `$tbAlbumAdmin`.id and gallery_album_1.idEvent = events.id order by events.dateEvent desc");
          }
            
          while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $item = $row['item'];
            $order = $row['order'];
            $orderArr[$id][$item] = $order;
          } 
          
          if (!isset($orderArr)) $orderArr = array();              
          if ($orderArr == NULL) {
            echo $phArr[0].'<br /><br />'; // The gallery does not contain any album.
          } else {                              
            foreach ($orderArr as $idm => $arrm) {
              foreach ($arrm as $it => $or) {
                echo '<div class="menuRow">';
                  echo '<strong>'.$it.'</strong>';                 
                    echo '<table class="editMenuAlbum"><tr>'; // sequence
                      echo '<td><a href = "admin_album.php?album='.$idm.'&amp;edit=1" title="'.$phArr[2].'"><img src="../lib/Gallery/design/tuzka.gif" alt="'.$phArr[2].'" /></a></td>'; // edit
                      //echo '<td><a href = "admin_album.php?album='.$idm.'&amp;del=1" title="'.$phArr[3].'"><img src="../lib/Gallery/design/delete.gif" alt="'.$phArr[3].'" /></a></td>'; // dlete
                    echo '</tr></table>';
                echo '</div>';
              }
            }            
            $orderArrTransfer = $orderArr;
            unset ($orderArr);
          }
          //echo '<input class="submitChange" type="submit" value="'.$phArr[26].'" />'; // change sequence
        echo '</form>';                    
        // Add item of 1. level
        /*
        echo '<form method="post" action="">';
          echo '<div class="input_form"><input class="inputMenu" type="text" name="item" size="25" />';
          echo '<input type="hidden" name="idParent" value="0" />';            
          echo '&nbsp;<input class="submit" type="submit" value="'.$phArr[4].'" /></div>'; // add item
        echo '</form>';                              
        */     
      echo '</div> <!-- .menu_list -->'; 	 	
    echo '</div> <!-- .edit_block_list -->';
    
    /*
    echo '<div class="editBlockAlbum">';
      echo '<h2>2'.$phArr[5].'</h2>'; // st level
      if (!isset($orderArrTransfer)) $orderArrTransfer = array();
      if ($orderArrTransfer != NULL) {
        foreach ($orderArrTransfer as $idP => $arrm) {                 
          echo '<div class="menuList">';
            if ($this->langAdmin == 1) {            
              $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $idP");
            } else {
              $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id");
            } 
            $row = mysqli_fetch_array($result);
            echo '<h3>'.$row['item'].'</h3>';
            // Form item sequence
            echo '<form method="post" action="">';
              if ($this->langAdmin == 1) {            
                $result = mysqli_query($this->db_connect, "select id, item, `order` from gallery_album_1 where idParent = $idP order by `order`");
              } else {
                $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.`order`, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id order by gallery_album_1.`order`");
              } 
              
              $orderArr2 = array();
              while ($row = mysqli_fetch_array($result)) {
                $id = $row['id'];
                $item = $row['item'];
                $order = $row['order'];
                $orderArr2[$id][$item] = $order;
              }  
            
              if ($orderArr2 == null) {
                echo $phArr[6].'<br /><br />'; // The album does not contain any sub-albums.
              } else {                              
                foreach ($orderArr2 as $idm => $arrm) {
                  foreach ($arrm as $it => $or) {
                    echo '<div class="menuRow">';
                      echo '<strong>'.$it.'</strong>';                 
                        echo '<table class="editMenuAlbum"><tr><td>'.$phArr[1].'&nbsp;<input type="text" name="order['.$idm.']" value="'.$or.'" size="1" /></td>'; // sequence
                          echo '<td><a href = "admin_album.php?album='.$idm.'&amp;edit=1" title="'.$phArr[2].'"><img src="../lib/Gallery/design/tuzka.gif" alt="'.$phArr[2].'" /></a></td>'; // edit
                          echo '<td><a href = "admin_album.php?album='.$idm.'&amp;del=1" title="'.$phArr[3].'"><img src="../lib/Gallery/design/delete.gif" alt="'.$phArr[3].'" /></a></td>'; // delete
                        echo '</tr></table>';
                    echo '</div>';
                  }
                }
                $orderArrTransfer2[] = $orderArr2;
                unset ($orderArr2);
              }  
              echo '<input class="submitChange" type="submit" value="'.$phArr[26].'" />'; // change sequence                     
            echo '</form>';
            // Add item of 2. level
            echo '<form method="post" action="">';
              echo '<div class="input_form"><input class="inputMenu" type="text" name="item" size="25" />';
              echo '<input type="hidden" name="idParent" value="'.$idP.'" />';            
              echo '&nbsp;<input class="submit" type="submit" value="'.$phArr[4].'" /></div>'; // add item
            echo '</form>';
          echo '</div> <!-- .menu_list -->';
        }
      }
    echo '</div> <!-- .edit_block_list -->';

    echo '<div class="editBlockAlbum">';
      echo '<h2>3. úroveň</h2>';
      if(!isset($orderArrTransfer2)) $orderArrTransfer2 = array();
      if ($orderArrTransfer2 != NULL) {
        foreach ($orderArrTransfer2 as $k => $v) {              
          foreach ($v as $idP => $arrm) {
            echo '<div class="menuList">';
              // idParent parent item
              $result = mysqli_query($this->db_connect, "select idParent from gallery_album_1 where id = $idP");
              $row = mysqli_fetch_array($result);
              // primary item
              if ($this->langAdmin == 1) {
                $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = {$row['idParent']}");
              } else {
                $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = {$row['idParent']} and  gallery_album_1.id = `$tbAlbumAdmin`.id");
              }                            
              $row = mysqli_fetch_array($result);
              $navigation = $row['item'];
              // name of item
              if ($this->langAdmin == 1) {            
                $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $idP");
              } else {
                $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id");
              } 
              $row = mysqli_fetch_array($result);
              echo '<h3>'.$navigation.'&nbsp;&gt;&gt;&nbsp;'.$row['item'].'</h3>';
              // Form item sequence
              echo '<form method="post" action="">';
                if ($this->langAdmin == 1) {            
                  $result = mysqli_query($this->db_connect, "select id, item, `order` from gallery_album_1 where idParent = $idP order by `order`");
                } else {
                  $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.`order`, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id order by gallery_album_1.`order`");
                } 
                
                $orderArr3 = null;
                while ($row = mysqli_fetch_array($result)) {
                  $id = $row['id'];
                  $item = $row['item'];
                  $order = $row['order'];
                  $orderArr3[$id][$item] = $order;
                }  
              
                if ($orderArr3 == NULL) {
                  echo $phArr[6].'<br /><br />'; // The album does not contain any sub-albums.
                } else {                              
                  foreach ($orderArr3 as $idm => $arrm) {
                    foreach ($arrm as $it => $or) {
                      echo '<div class="menuRow">';
                        echo '<strong>'.$it.'</strong>';                 
                          echo '<table class="editMenuAlbum"><tr><td>'.$phArr[1].'&nbsp;<input type="text" name="order['.$idm.']" value="'.$or.'" size="1" /></td>'; // sequence
                            echo '<td><a href = "admin_album.php?album='.$idm.'&amp;edit=1" title="'.$phArr[2].'"><img src="../lib/Gallery/design/tuzka.gif" alt="'.$phArr[2].'" /></a></td>'; // edit
                            echo '<td><a href = "admin_album.php?album='.$idm.'&amp;del=1" title="'.$phArr[3].'"><img src="../lib/Gallery/design/delete.gif" alt="'.$phArr[3].'" /></a></td>'; // delete
                          echo '</tr></table>';
                      echo '</div>';
                    }
                  }
                  $orderArrTransfer3[] = $orderArr3;
                  unset ($orderArr3);
                } 
                echo '<input class="submitChange" type="submit" value="'.$phArr[26].'" />'; // change sequence                   
              echo '</form>';
              // Add item of 3. level
              echo '<form method="post" action="">';
                echo '<div class="input_form"><input class="inputMenu" type="text" name="item" size="25" />';
                echo '<input type="hidden" name="idParent" value="'.$idP.'" />';            
                echo '&nbsp;<input class="submit" type="submit" value="'.$phArr[4].'" /></div>'; // add item                    
              echo '</form>';
            echo '</div> <!-- .menu_list -->';
          }
        }
      }
    echo '</div> <!-- .edit_block_list -->';    
    echo '<div class="editBlockAlbum">';
      echo '<h2>4'.$phArr[5].'</h2>'; // 1. level      
      if (!isset($orderArrTransfer3)) $orderArrTransfer3 = array();
      foreach ($orderArrTransfer3 as $k => $v) {        
        foreach ($v as $idP => $arrm) {
          echo '<div class="menuList">';            
            // idParent parent item
            $result = mysqli_query($this->db_connect, "select idParent from gallery_album_1 where id = $idP");
            $row = mysqli_fetch_array($result);
            $idParent = $row['idParent'];
            // parent item 2
            if ($this->langAdmin == 1) {
              $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $idParent");
            } else {
              $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = {$row['idParent']} and  gallery_album_1.id = `$tbAlbumAdmin`.id");
            }                            
            $row = mysqli_fetch_array($result);
            $navigation2 = $row['item'];
            $result = mysqli_query($this->db_connect, "select idParent from gallery_album_1 where id = $idParent");
            $row = mysqli_fetch_array($result);
            $idParent = $row['idParent'];
            // parent item 1
            if ($this->langAdmin == 1) {
              $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $idParent");
            } else {
              $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = {$row['idParent']} and  gallery_album_1.id = `$tbAlbumAdmin`.id");
            }  
            $row = mysqli_fetch_array($result);
            $navigation1 = $row['item'];
            // name of item                    
            if ($this->langAdmin == 1) {            
              $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $idP");
            } else {
              $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id");
            } 
            $row = mysqli_fetch_array($result);
            echo '<h3>'.$navigation1.'&nbsp;&gt;&gt;&nbsp;'.$navigation2.'&nbsp;&gt;&gt;&nbsp;'.$row['item'].'</h3>';
            // Form item sequence
            echo '<form method="post" action="">';
              if ($this->langAdmin == 1) {            
                $result = mysqli_query($this->db_connect, "select id, item, `order` from gallery_album_1 where idParent = $idP order by `order`");
              } else {
                $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.`order`, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = $idP and  gallery_album_1.id = `$tbAlbumAdmin`.id order by gallery_album_1.`order`");
              } 
                
              $orderArr4 = null;
              while ($row = mysqli_fetch_array($result)) {
                $id = $row['id'];
                $item = $row['item'];
                $order = $row['order'];
                $orderArr4[$id][$item] = $order;
              }  
              
              if ($orderArr4 == NULL) {
                echo $phArr[6].'<br /><br />'; // The album does not contain any sub-albums.
              } else {                              
                foreach ($orderArr4 as $idm => $arrm) {
                  foreach ($arrm as $it => $or) {
                    echo '<div class="menuRow">';
                      echo '<strong>'.$it.'</strong>';                 
                        echo '<table class="editMenuAlbum"><tr><td>'.$phArr[2].'&nbsp;<input type="text" name="order['.$idm.']" value="'.$or.'" size="1" /></td>'; // sequence
                          echo '<td><a href = "admin_album.php?album='.$idm.'&amp;edit=1&amp;lang='.$this->langAdmin.'" title="'.$phArr[2].'"><img src="../lib/Gallery/design/tuzka.gif" alt="'.$phArr[2].'" /></a></td>'; // edit
                          echo '<td><a href = "admin_album.php?album='.$idm.'&amp;del=1&amp;lang='.$this->langAdmin.'" title="'.$phArr[3].'"><img src="../lib/Gallery/design/delete.gif" alt="'.$phArr[3].'" /></a></td>'; // delete
                        echo '</tr></table>';
                    echo '</div>';
                  }
                }
                unset ($orderArr4);
              }
              echo '<input class="submitChange" type="submit" value="'.$phArr[26].'" />'; // change sequence                      
            echo '</form>';
            // Add item of 4. úrovně
            echo '<form method="post" action="">';
              echo '<div class="input_form"><input class="inputMenu" type="text" name="item" size="25" />';
              echo '<input type="hidden" name="idParent" value="'.$idP.'" />';            
              echo '&nbsp;<input class="submit" type="submit" value="'.$phArr[4].'" /></div>'; // add item                     
            echo '</form>';
          echo '</div> <!-- .menu_list -->';
        }
      }
    echo '</div> <!-- .edit_block_list -->';
    */
  }
  
	public function insertAlbum($item, $idParent, $idEvent)
  {              
    $phArr = $this->translate($this->langAdmin);

    if (empty($item)) {
      
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[7].'</div></div>'; // You did not enter the item name!
    
    } else {                                                       
      
      $order = 1;
                    
      $result = mysqli_query($this->db_connect, "select `order` from gallery_album_1 
      where idParent = {$idParent} order by `order` desc limit 0,1");
      $row = mysqli_fetch_assoc($result);
      $order = $row['order'] + 1;
                                                     
      // write to database for main language of administration                      
      $insertMain = mysqli_query($this->db_connect, "insert into gallery_album_1 (item, idParent, `order`, idEvent) values ('{$item}', {$idParent}, $order, $idEvent)");
      if (!$insertMain) {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[8].'</div></div>'; // Error! The album was not successfully entered into the database.
      } else {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[9].'</div></div>'; // Album was entered into the database.
      }               
      
      if ($insertMain) {                         
        
        $result = mysqli_query($this->db_connect, "select id from gallery_album_1 order by id desc limit 0,1");
        $row = mysqli_fetch_array($result);
        $id = $row['id'];

        foreach ($this->language as $lg) {
          if ($lg != 1) {                
            $tbAlbum = 'gallery_album_'.$lg;                      
            $insertLang = mysqli_query($this->db_connect, "insert into `$tbAlbum` (id, item) values ($id, '$item')");
          }
        }
        if (!$insertLang) {
        
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[8].'</div></div>'; 
      
        } else {
             
          $this->createFolder(); 

        }
      
      }
  
      return 1;                      
    }
  
  }
  
  public function createFolder ()
  {  
    $phArr = $this->translate($this->langAdmin);
    
    $result = mysqli_query($this->db_connect, "select id from gallery_album_1 order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $album = $row['id'];

    $upload = mkdir("../gallery/$album",0777);                                                         
    if (chmod("../gallery/$album",0777)) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[10].'</div></div>'; // Folder  was created on the server.
    } else {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[11].'</div></div>'; // Error! The folder was not successfully created on the server. Check the gallery folder path.
    }    
  }
  
  public function createFolderViaFtp ($ftpServer, $ftpLogin, $ftpPass, $dir)
  {
    $phArr = $this->translate($this->langAdmin);
    
    $result = mysqli_query($this->db_connect, "select id from gallery_album_1 order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $album = $row['id'];
    
    $ftp = ftp_connect($ftpServer);
    $login_result = ftp_login($ftp, $ftpLogin, $ftpPass);
                
    $path =  $dir.'/gallery/'.$album;
    ftp_mkdir($ftp, $path);
                                                          
    if (ftp_site($ftp, "CHMOD 0777 $path") != false) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[10].'</div></div>'; // Folder  was created on the server.
    } else {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[11].'</div></div>'; // Error! The folder was not successfully created on the server. Check the gallery folder path.
    }  
  }
  
  public function changeOrderAlbum ($order)
  {
    $phArr = $this->translate($this->langAdmin);
    
    // Control unique item
    if ($order != null) {
      if (count($order) != count(array_unique($order))) {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[27].'</div></div>'; // The inserted number sequence must be unique!
      } else {
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                    
          $result = mysqli_query($this->db_connect, "update gallery_album_1 set `order` = $i where id = $id");
          $i++;      
        }
        if (!$result) {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[24].'</div></div>'; // Error! The change was not saved successfully!
        } else {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[28].'</div></div>'; // The sequence has been changed successfully.
        }                                                                             
      }                                               
    } else {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[12].'</div></div>'; // There are no items at the given level.
    }  
  }

  public function deleteQueryAlbum ($album)
  {                                                             
    $phArr = $this->translate($this->langAdmin); 
      
    $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $album");
    $row = mysqli_fetch_array($result);
    $item = $row['item'];
           
    $result = mysqli_query($this->db_connect, "select id from gallery where idAlbum = $album"); 
    if (mysqli_num_rows($result) > 0) $noDel = 1; 
    $result = mysqli_query($this->db_connect, "select id from gallery_album_1 where idParent = $album");      
    if (mysqli_num_rows($result) > 0) $noDel = 2;
    if (!isset($noDel)) $noDel = 0;     
    if ($noDel == 1) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[13].'</div></div>'; // Warning! The album contains images. If you would like to delete album, you have to delete all images from the album first.
    } else  if ($noDel == 2) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[14].'</div></div>'; // Warning! The album contains a sub-album. If you would like to delete album, you have to delete the sub-albums from the album first.
    } else {  
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[15]; // Are you sure you want to delete the album?
        echo '<table><tr>';   
          echo '<td>';    
            echo '<form method="post" action="">';
              echo '<input type="hidden" name="dok" value="1" />';
              echo '<input type="hidden" name="album" value="'.$album.'" />'; 
              echo '<input type="submit" value="'.$phArr[3].'" />'; // delete
            echo '</form>';
          echo '</td>';
          echo '<td>';
            echo '<form method="get" action="">';
              echo '<input type="submit" value="'.$phArr[29].'" />'; // no
            echo '</form>';
          echo '</td>';
        echo '</tr></table>';
      echo '</div></div>';
    }    
  }

  public function deleteAlbum ($dok, $album)
  {
    if ($dok == 1) {
      $phArr = $this->translate($this->langAdmin);
                                    
      $result = mysqli_query($this->db_connect, "select item from gallery_album_1 where id = $album");
      $row = mysqli_fetch_array($result);
      $item = $row['item'];
                                                         
      $list = glob("../gallery/$album/*");
      if ($list != false) {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[16].'</div></div>'; // The album probably contains photographs or an inserted album.  You can not delete this item. If you want to delete this item, you have to delete the inserted photographs and albums first.
      } else {           
        
        $albums = glob("../gallery/*");
        if (in_array('../gallery/'.$album, $albums)) {        
          if (rmdir("../gallery/$album")) {
            echo '<div class="wACover"><div class="warrAdmin">'.$phArr[17].'</div></div>'; // The album has been successfully deleted from the server.   
          } else {
            echo '<div class="wACover"><div class="warrAdmin">'.$phArr[18].'</div></div>'; // Error! The album was not deleted from the server.
          }                    
        } else {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[19].'</div></div>'; // The item cannot be found on the server!
        }

        $result = mysqli_query($this->db_connect, "select id from language order by id"); // všechny jazyky, nikoliv aktivní
        while ($row = mysqli_fetch_array($result)) {
          $tbAlbum = 'gallery_album_'.$row['id'];
          $delete = mysqli_query($this->db_connect, "delete from `$tbAlbum` where id = $album");
        } 
        if (!$delete) {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[20].'</div></div>'; // Deletion of the album from the database was not successful!
        } else {
          echo '<div class="wACover"><div class="warrAdmin">'.$phArr[21].'</div></div>'; // Deletion of the album from the database has been successful.
        }
      }
    }
  }

  public function editFormAlbum ($album)
  {

    $phArr = $this->translate($this->langAdmin);
      
      echo '<div class="editItem">';    
        echo '<table>';
          foreach ($this->languageActive as $code => $lg) {
            $tbAlbum = 'gallery_album_'.$lg;
                            
            $result = mysqli_query($this->db_connect, "select item from `$tbAlbum` where id = $album limit 0,1");
            $row = mysqli_fetch_array($result); 
            $item = $row['item']; 
                                                                                                              
            echo '<form class="editAlbum" method="post" action="">';          
              echo '<input type="hidden" name="insedit" value="1" />';
              echo '<input type="hidden" name="album" value="'.$album.'" />';
              echo '<input type="hidden" name="lang" value="'.$lg.'" />';                    
              echo '<tr>';
                echo '<td><img src="../lib/Gallery/design/'.$code.'.png" alt="language" /></td>';
                echo '<td><input type="text" name="new" size="80" value="'.$item.'" /></td>';
                echo '<td><input class="submit" type="submit" value="'.$phArr[22].'" /></td>'; // change item
              echo '</tr>';
            echo '</form>';
          }
        echo '</table>';                                                                                    
      echo '</div>';
  }

  public function editAlbum ($album, $item, $lg)
  {
    
    
    $phArr = $this->translate($this->langAdmin);
  
    $tbAlbum = 'gallery_album_'.$lg;
                
    if (empty($item)) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[7].'</div></div>'; // The item must not be empty!
    } else {
      $sql = "update `$tbAlbum` set item = '"
      .mysqli_real_escape_string($this->db_connect, $item)."' where id = $album";                                        
                                  
      $result = mysqli_query($this->db_connect, $sql);
      if (!$result) {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[24].'</div></div>'; // Error! The change was not saved successfully!
      } else {
        echo '<div class="wACover"><div class="warrAdmin">'.$phArr[25].'</div></div>'; // Renaming the item has been successfull.
      }                                                                                                          
    }
  }
  
  private function translate ($lang)
  {
    $phArr = array();
    $result = mysqli_query($this->db_connect, "select phrase from trans_admin where category = 'album2' and lang = $lang order by ord");
    while ($row = mysqli_fetch_array($result)) {
      $phArr[] = $row['phrase'];
    }
    return $phArr;
  }

}
