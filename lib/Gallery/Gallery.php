<?php
/**
 * version 1.0.6
 * license: free  
 * 8. 9. 2015
 * new jquery.pagination1.0.5.js  
*/
class Gallery
{

  public $db_connect = null;	
    		
	 /**
	 * Parametr pro jazyk
	 *
	 * @var int
	 */
	public $lang = 0; 

	 /**
	 * Parametr pro page id
	 *
	 * @var int
	 */
	public $id = 0; 
						
	 /**
	 * @param array $db_connect
	 * @param array $language             	 
	*/
	
  public function __construct($db_connect, $lang, $id)
	{
		$this->db_connect = $db_connect;
		$this->lang = (int) $lang;
		$this->id = (int) $id;
                $this->link = $_SERVER['SCRIPT_NAME'];
	}

  public function showAlbum ()
  {  
    if ($this->lang == 1) {
      $result = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = 0 order by `order`");
    } else {
      $tbAlbum = 'gallery_album_'.$this->lang;
      $result = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.idParent = 0 and gallery_album_1.id = `$tbAlbum`.id order by `order`");
    }
    
    while ($row = mysqli_fetch_array($result)) {
      echo '<div class="itemPhotoGal itemPhotoBg">';  
        echo '<table class="tabThumbGal"><tr>';
          echo '<td><p><a href = "'.$this->link.'?album='.$row['id'].'&amp;id='.$this->id.'" title="'.$row['item'].'">'.$row['item'].'</a></p></td>';
        echo '</tr></table>';
      echo '</div>';
    } 
    
    if (mysqli_num_rows($result) == 0) echo '<h3>'.$phArr[5].'</h3>';  
    //$this->powered();          
  }
  
  public function showAlbumThumb ($limit, $sort)
  {  
    if ($this->lang == 1) {
      $result = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = 0 and id > 1 order by `order`");
    } else {
      $tbAlbum = 'gallery_album_'.$this->lang;
      $result = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.idParent = 0 and gallery_album_1.id = `$tbAlbum`.id and gallery_album_1.id > 1 order by `order`");
    }
    
    switch ($sort) {
      case 0: $s = 'asc';
      break;
      case 1: $s = 'desc';
    }
    
    while ($row = mysqli_fetch_array($result)) {
      echo '<div class="albumThumb">';
        echo '<h3><a href = "'.$this->link.'?album='.$row['id'].'&amp;id='.$this->id.'" title="'.$row['item'].'">'.$row['item'].'</a></h3>';
        $foto = mysqli_query($this->db_connect, "select gallery.nameImage from gallery, gallery_album_1 
        where gallery_album_1.id = {$row['id']} and gallery_album_1.id = gallery.idAlbum order by gallery.id {$s} limit 0, {$limit}");
        while ($f = mysqli_fetch_array($foto)) {
          echo '<div class="itemPhotoGal">';  
            echo '<table class="tabThumbGal"><tr>';
              echo '<td><a href = "'.$this->link.'?album='.$row['id'].'&amp;id='.$this->id.'" title="'.$row['item'].'"><img src="./gallery/'.$row['id'].'/thumb_'.$f['nameImage'].'" alt="thumb_'.$f['nameImage'].'" /></a></td>';    
            echo '</tr></table>';
          echo '</div>';  
        } 
      echo '</div>';
    } 
    if (mysqli_num_rows($result) == 0) echo '<h3>No album</h3>';
    //$this->powered();
  }
            
  public function showPhoto ($album, $sort)
  {                                       
    
    $phArr = $this->translation ($this->lang);
    
    $tbAlbum = 'gallery_album_'.$this->lang;
    
    if ($this->lang == 1) {
      $result = mysqli_query($this->db_connect, "select item, idParent from gallery_album_1 where id = $album");
    } else {
      $result = mysqli_query($this->db_connect, "select `$tbAlbum`.item, gallery_album_1.idParent from gallery_album_1, `$tbAlbum` where gallery_album_1.id = $album and gallery_album_1.id = `$tbAlbum`.id");
    }
        
    while ($row = mysqli_fetch_array($result)) {
      $item = $row['item']; 
      $idParent = $row['idParent'];
    }
    
    /* NAVIGATION */
    if ($idParent == 0) {
      $navigation = $item;
    } else {
      if ($this->lang == 1) {
        $result = mysqli_query($this->db_connect, "select id, item, idParent from gallery_album_1 where id = $idParent"); // parent item
      } else {
        $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.id = $idParent and gallery_album_1.id = `$tbAlbum`.id"); 
      }      
      $row = mysqli_fetch_array($result);
      $id2 = $row['id'];
      $item2 = $row['item'];
      $idParent2 = $row['idParent'];      
      if ($idParent2 == 0) {        
        $navigation = '<a href="'.$this->link.'?album='.$id2.'&amp;id='.$this->id.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;
      } else {
        if ($this->lang == 1) {
          $result = mysqli_query($this->db_connect, "select id, item, idParent from gallery_album_1 where id = $idParent2"); // parent item
        } else {
          $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.id = $idParent2 and gallery_album_1.id = `$tbAlbum`.id");
        }        
        $row = mysqli_fetch_array($result);
        $id3 = $row['id'];
        $item3 = $row['item'];
        $idParent3 = $row['idParent'];
        if ($idParent3 == 0) {        
          $navigation = '<a href="'.$this->link.'?album='.$id3.'&amp;id='.$this->id.'">'.$item3.'</a>&nbsp;&gt;&gt;&nbsp;<a href="'.$this->link.'?album='.$id2.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;
        } else {
          if ($this->lang == 1) {
            $result = mysqli_query($this->db_connect, "select id, item, idParent from gallery_album_1 where id = $idParent3"); // parent item
          } else {
            $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.id = $idParent3 and gallery_album_1.id = `$tbAlbum`.id");
          } 
          $row = mysqli_fetch_array($result);
          $id4 = $row['id'];
          $item4 = $row['item'];
          $navigation = '<a href="'.$this->link.'?album='.$id4.'&amp;id='.$this->id.'">'.$item4.'</a>&nbsp;&gt;&gt;&nbsp;<a href="'.$this->link.'?album='.$id3.'">'.$item3.'</a>&nbsp;&gt;&gt;&nbsp;<a href="'.$this->link.'?album='.$id2.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;        
        }
      }
    }
    echo '<div class="coverNavigation">';       
      echo '<h3>'.$navigation.'</h3>';
    echo '</div> <!-- .coverNavigation -->';
    /*
    echo '<a href = "'.$this->link.'?id='.$this->id.'" title="'.$phArr[0].'">';
      echo '<div class="upFront"></div>';
    echo '</a>';
    */           
    // Show subordinary albums          
    if ($this->lang == 1) {
      $result = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = $album order by `order`");              
    } else {
      $result = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbum`.item from gallery_album_1, `$tbAlbum` where gallery_album_1.idParent = $album and gallery_album_1.id = `$tbAlbum`.id order by `order`");
    }
    if (mysqli_num_rows($result) > 0) {          
      while ($row = mysqli_fetch_array($result)) {
        echo '<div class="itemPhotoGal itemPhotoBg">';  
          echo '<table class="tabThumbGal"><tr>';
            echo '<td><p><a href = "'.$this->link.'?album='.$row['id'].'&amp;id='.$this->id.'" title="'.$row['item'].'">'.$row['item'].'</a></p></td>';
          echo '</tr></table>';
        echo '</div>';
      } 
    }
                            
    switch ($sort) {
      case 0: $s = 'asc';
      break;
      case 1: $s = 'desc';
    }
    
                 
    $result = mysqli_query($this->db_connect, "select id, nameImage from gallery where idAlbum = $album order by id {$s}");
    echo '<div class="pagerCover">';
      echo '<div id="divPager"></div>';
    echo '</div>';
    echo '<div id="gallery">'; // for jquery.lightbox-0.5
      while ($row = mysqli_fetch_array($result)) {
        $nameImage = $row['nameImage'];
        $image = $row['id'];
        $path = './gallery/'.$album.'/thumb_'.$nameImage;

        echo '<div class="divItem itemPhotoGal">'; 
          echo '<table class="tabThumbGal"><tr>';
            echo '<td><a href="./gallery/'.$album.'/'.$nameImage.'" title="'.$this->comment($image).'"><img src="'.$path.'" alt="'.$nameImage.'"/></a></td>';    
          echo '</tr></table>';  
        echo '</div>';                              
      }       
    echo '</div> <!-- #gallery -->';                                                              

    //$this->powered();
  }
  
  public function comment ($idImage) {
    if ($this->lang == 1) {
      $comm = mysqli_query($this->db_connect, "select comment from gallery_comment_1 where image = $idImage");
    } else {
      $tbComment = 'gallery_comment_'.$this->lang;
      $comm = mysqli_query($this->db_connect, "select `$tbComment`.comment from `$tbComment`, gallery_comment_1 where gallery_comment_1.image = $idImage 
      and `$tbComment`.id = gallery_comment_1.id order by `$tbComment`.id");        
    }
    if (mysqli_num_rows($comm) > 0) {
      $c = mysqli_fetch_assoc($comm);
      $comment = $c['comment'];
    } else {
      $comment = '';
    }
    return $comment;
  }
  
  public function translation ($lang)
  {
    $result = mysqli_query($this->db_connect, "select phrase from trans_gal where lang = $lang and category = 'gal'");
    while ($row = mysqli_fetch_array($result)) {
      $phArr[] = $row['phrase'];
    }     
    return $phArr; 
  } 
  
  private function powered ()
  {
    echo '<div class="powered">';
      echo '<table><tr>';
      echo '<td>Powered by </td><td><a class="newTab" href="http://www.gallery603.com" title="gallery603"><img src="./img/logo.png" alt="gallery603" /></a></td>';
      echo '</tr></table>';
    echo '</div>';
  }

}