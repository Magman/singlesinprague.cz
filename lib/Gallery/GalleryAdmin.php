<?php 
/**
 * Gallery
 */
class GalleryAdmin
{

  public $db_connect = null;	
    
    /**
	 * Main language.
	 *
	 * @var int
	 */
	public $langAdmin = 0;
	
	
	/**
	 * Languages
	 *
	 * @var array
	 */
	public $language = null;
					
	/**
	 * @param array $db_connect
	 * @param int $langAdmin
	 * @param array $language             	 
	*/
	
  public function __construct($db_connect, $langAdmin, $language)
	{
		$this->db_connect = $db_connect;
		$this->langAdmin = (int) $langAdmin;
		$this->language = $language;
	}
  
  public function showGallery ()
  { 
    $phArr = $this->translate($this->langAdmin);
     
    $tbAlbumAdmin = 'gallery_album_'.$this->langAdmin;
    if ($this->langAdmin == 1) {
      $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.item from gallery_album_1, events where gallery_album_1.idParent = 0 and gallery_album_1.idEvent = events.id order by events.dateEvent desc");
    } else {                  
      $result = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin`, events where gallery_album_1.idParent = 0 and gallery_album_1.id = `$tbAlbumAdmin`.id and gallery_album_1.idEvent = events.id order by events.dateEvent desc");
    }
    
    while ($row = mysqli_fetch_array($result)) {
      $itemArr[$row['id']] = $row['item'];
    } 
    if (!isset($itemArr)) $itemArr = array();
    if ($itemArr == NULL) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[0].'</div></div>'; // The gallery does not contain any album.
    } else {                     
      foreach ($itemArr as $idm => $it) {
        echo '<div class="showAlbum">';
          // 1. level
          echo '<div class="volume1">';
            echo '<a href="admin_gallery.php?album='.$idm.'"><strong>'.$it.'</strong></a>';                                                                                        
            if ($this->langAdmin == 1) {
              $volume2 = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = $idm order by `order`");
            } else {           
              $volume2 = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = $idm and gallery_album_1.id = `$tbAlbumAdmin`.id order by `order`");
            }            
            echo '<div class="volume2">';
              while ($vol2 = mysqli_fetch_array($volume2)) {
                echo '<div class="volume2In">';
                  echo '<a href="admin_gallery.php?album='.$vol2['id'].'"><strong>'.$vol2['item'].'</strong></a>';      
                  if ($this->langAdmin == 1) {
                    $volume3 = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = {$vol2['id']} order by `order`");
                  } else {
                    $volume3 = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = {$vol2['id']} and gallery_album_1.id = `$tbAlbumAdmin`.id order by `order`");
                  }
                  while ($vol3 = mysqli_fetch_array($volume3)) {
                    echo '<div class="volume3">';
                      echo '<a href="admin_gallery.php?album='.$vol3['id'].'"><strong>'.$vol3['item'].'</strong></a>';             
                      if ($this->langAdmin == 1) {
                        $volume4 = mysqli_query($this->db_connect, "select id, item from `$tbAlbumAdmin` where idParent = {$vol3['id']} order by `order`");
                      } else {
                        $volume4 = mysqli_query($this->db_connect, "select gallery_album_1.id, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.idParent = {$vol3['id']} and gallery_album_1.id = `$tbAlbumAdmin`.id order by `order`");
                      }     
                      while ($vol4 = mysqli_fetch_array($volume4)) {
                        echo '<div class="volume4">';
                          echo '<a href="admin_gallery.php?album='.$vol4['id'].'"><strong>'.$vol4['item'].'</strong></a>';                     
                        echo '</div> <!-- .volume4 -->';
                      }
                    echo '</div> <!-- .volume3 -->';
                  }
                echo '</div> <!-- .volume2In -->';
              }  
            echo '</div> <!-- .volume2 -->';
          echo '</div> <!-- .volume1 -->';        
        echo '</div> <!-- .showAlbum -->'; 
      }
    }                                                                                                          
  }
    
  public function insertPhoto ($ins, $album, $largeViewX, $largeViewY) 
  {
    if ($ins == 1) {
      $phArr = $this->translate($this->langAdmin);
      
      // Parameters in file ../function/param.php
      $largeThumb = 260; // orig 130
             
      ini_set('memory_limit', '64M');
      
      if (isset($_FILES['uploads'])) {
        $nameArr = array();
        foreach ($_FILES['uploads']['name'] as $k => $n) {        
        
          if ($_FILES['uploads']['name'][$k] == null) {
            echo '<div class="wACover"><div class="warrAdmin">'.$phArr[1].'</div></div>'; // You have not selected any image.
          } else if ($_FILES['uploads']['size'][$k] == 0) {
            echo '<div class="wACover"><div class="warrAdmin">'.$phArr[2].':&nbsp;'.$_FILES['uploads']['name'][$k].'</div></div>'; // Error! The image is too larg and will not be uploaded.
          } else if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg' or substr($_FILES['uploads']['type'][$k], 6) === 'gif' or substr($_FILES['uploads']['type'][$k], 6) === 'png') {
            // moves space characters and diacritica (explode breaks string to arrays due to dots)
            $name = explode(".", strtr($n, array(' '=>'_', '–'=>'-', 'Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
              'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
              'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
              'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
              'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z')));
            // moves dots and Upper case
            $newName = null;
            foreach ($name as $i => $nm) {
              if ($i < count($name) - 1) $newName .= $nm;
            }
            $nameArr[$k] = strtolower($newName).'.'.substr($_FILES['uploads']['type'][$k], 6); // image/jpeg
            unset ($newName);
          } else {
            echo '<div class="wACover"><div class="warrAdmin">'.$phArr[3].':&nbsp;'.$_FILES['uploads']['name'][$k].'</div></div>'; // Error! The image is not jpeg, gif or png and will not be uploaded.
          }
        }
        // control of duplicity
        $imagesDb = array();
        $result = mysqli_query($this->db_connect, "select nameImage from gallery where idAlbum = $album");
        while ($row = mysqli_fetch_array($result)) {
          $imagesDb[] = $row['nameImage'];
        }        
        if ($nameArr != null) {  
          foreach ($nameArr as $k => $nA) {        
            if (!in_array($nA, $imagesDb)) { // control of duplicity         
              $uploadfile = '../gallery/'.$album.'/'.$nA;
                                          
              if (move_uploaded_file($_FILES['uploads']['tmp_name'][$k], $uploadfile)) {                        
                // Plná velikost
                if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {
                  $im = imagecreatefromjpeg($uploadfile);
                  $x = imagesx($im);
                  $y = imagesy($im);
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                  $im = imagecreatefromgif($uploadfile);
                  $x = imagesx($im);
                  $y = imagesy($im);
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                  $im = imagecreatefrompng($uploadfile);
                  $x = imagesx($im);
                  $y = imagesy($im);
                }
                
                if ($x > $y) {
                  if ($x > $largeViewX) {
                    $rx1 = $largeViewX;
                    $ry1 = $y * $largeViewX / $x;
                  } else {
                    $rx1 = $x;
                    $ry1 = $y;
                  }                                             
                } else if ($x == $y) {
                  if ($y > $largeViewY) {
                    $ry1 = $largeViewY;
                    $rx1 = $x * $largeViewY / $y;
                  } else {
                    $rx1 = $x;
                    $ry1 = $y;
                  }                 
                } else {
                  if ($y > $largeViewY) {
                    $ry1 = $largeViewY;
                    $rx1 = $x * $largeViewY / $y;
                  } else {
                    $rx1 = $x;
                    $ry1 = $y;
                  }                 
                }       
                
                $view = imagecreatetruecolor($rx1, $ry1);
                $imView = imagecopyresampled($view, $im, 0, 0, 0, 0, $rx1, $ry1, $x, $y);
                
                $fullname = '../gallery/'.$album.'/'.$nA;
                
                if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {        
                  $dataImage = imagejpeg($view, $fullname, 100);  
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                  $dataImage = imagegif($view, $fullname, 100);
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                  $dataImage = imagepng($view, $fullname, 9);
                }
                                 
                // Thumbnail
                if($x > $y) {            
                  $rx2 = $largeThumb;
                  $ry2 = $y * $largeThumb / $x;
                } else {
                  $ry2 = $largeThumb;
                  $rx2 = $x * $largeThumb / $y;
                }
                         
                $view = imagecreatetruecolor($rx2, $ry2);
                $imView = imagecopyresampled($view, $im, 0, 0, 0, 0, $rx2, $ry2, $x, $y);
                
                $fullname = '../gallery/'.$album.'/thumb_'.$nA;
                
                if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {        
                  $dataThumb = imagejpeg($view, $fullname, 100);  
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                  $dataThumb = imagegif($view, $fullname, 100);
                } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                  $dataThumb = imagepng($view, $fullname, 9);
                } 
                $sql = "insert into gallery (nameImage, idAlbum, dateInsert) values ('"
                .mysqli_real_escape_string($this->db_connect, $nA)
                ."','"
                .mysqli_real_escape_string($this->db_connect, $album)
                ."', now())"
                ;
                              
                mysqli_query($this->db_connect, $sql);  
                
                $result = mysqli_query($this->db_connect, "select id from gallery order by id desc limit 0,1");
                $row = mysqli_fetch_array($result);                                                  
                
                //$update = mysqli_query($this->db_connect, "update gallery set dataImage = '$obrazek' where id = {$row['id']}");                     
                
                imagedestroy($im);

                echo '<div class="wACover"><div class="warrAdmin">'.$phArr[4].':&nbsp;'.$nA.'</div></div>'; // The image has been successfully uploaded to the server.         
              } else {
                echo '<div class="wACover"><div class="warrAdmin">'.$phArr[5].':&nbsp;'.$nA.'</div></div>'; // Error! The image was not successfully uploaded to the server.
              }
            } else {
              echo '<div class="wACover"><div class="warrAdmin">'.$phArr[6].':&nbsp;'.$nA.' je již v albu načten.</div></div>'; // Error! The image with this name already exists in the album.
            }
          } 
        } 
      }
    }
  }  
  
  public function inputPhoto ($album, $maxFileSize, $fileSize)
  {  
    $phArr = $this->translate($this->langAdmin);
    
    echo '<form action="" method="post" enctype="multipart/form-data">';
      echo '<input type="hidden" name="ins" value="1" />';
      echo '<input type="hidden" name="album" value="'.$album.'" />';
      echo '<input type="hidden" name="file_size" value="'.$fileSize.'" />';
      echo '<input type="hidden" name="max_file_size" value="'.$maxFileSize.'" />';
      echo '<strong>'.$phArr[7].'</strong>&nbsp;<input type="file" name="uploads[]" multiple="multiple" />'; // Upload images:
      echo '<input type="submit" value="'.$phArr[8].'" />'; // save
    echo '</form>';
  } 
  
  public function showPhoto ($album, $maxFileSize, $fileSize)
  {
    $phArr = $this->translate($this->langAdmin);
    
    $tbAlbumAdmin = 'gallery_album_'.$this->langAdmin;

    echo '<div class="multipart">';
      echo '<div>';
        $this->inputPhoto ($album, $maxFileSize, $fileSize); 
      echo '</div>'; 
    echo '</div>';
               
    if ($this->langAdmin == 1) {
      $result = mysqli_query($this->db_connect, "select item, idParent from `$tbAlbumAdmin` where id = $album"); 
    } else {
      $result = mysqli_query($this->db_connect, "select gallery_album_1.idParent, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $album and gallery_album_1.id = `$tbAlbumAdmin`.id");
    }
    
    while ($row = mysqli_fetch_array($result)) {
      $item = $row['item'];
      $idParent = $row['idParent'];
    }
                
    if ($idParent == 0) {
      $navigation = $item;
    } else {
      if ($this->langAdmin == 1) {
        $result = mysqli_query($this->db_connect, "select id, item, idParent from `$tbAlbumAdmin` where id = $idParent"); // parent item
      } else {
        $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $idParent and gallery_album_1.id = `$tbAlbumAdmin`.id"); 
      }      
      $row = mysqli_fetch_array($result);
      $id2 = $row['id'];
      $item2 = $row['item'];
      $idParent2 = $row['idParent'];      
      if ($idParent2 == 0) {        
        $navigation = '<a href="admin_gallery.php?album='.$id2.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;
      } else {
        if ($this->langAdmin == 1) {
          $result = mysqli_query($this->db_connect, "select id, item, idParent from `$tbAlbumAdmin` where id = $idParent2"); // parent item
        } else {
          $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where gallery_album_1.id = $idParent2 and gallery_album_1.id = `$tbAlbumAdmin`.id");
        }        
        $row = mysqli_fetch_array($result);
        $id3 = $row['id'];
        $item3 = $row['item'];
        $idParent3 = $row['idParent'];
        if ($idParent3 == 0) {        
          $navigation = '<a href="admin_gallery.php?album='.$id3.'">'.$item3.'</a>&nbsp;&gt;&gt;&nbsp;<a href="admin_gallery.php?album='.$id2.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;
        } else {
          if ($this->langAdmin == 1) {
            $result = mysqli_query($this->db_connect, "select id, item, idParent from `$tbAlbumAdmin` where id = $idParent3"); // parent item
          } else {
            $result = mysqli_query($this->db_connect, "select gallery_album_1.id, gallery_album_1.idParent, `$tbAlbumAdmin`.item from gallery_album_1, `$tbAlbumAdmin` where `$tbAlbumAdmin`.id = $idParent3 and gallery_album_1.id = `$tbAlbumAdmin`.id");
          }
          $row = mysqli_fetch_array($result);
          $id4 = $row['id'];
          $item4 = $row['item'];
          $navigation = '<a href="admin_gallery.php?album='.$id4.'">'.$item4.'</a>&nbsp;&gt;&gt;&nbsp;<a href="admin_gallery.php?album='.$id3.'">'.$item3.'</a>&nbsp;&gt;&gt;&nbsp;<a href="admin_gallery.php?album='.$id2.'">'.$item2.'</a>&nbsp;&gt;&gt;&nbsp;'.$item;        
        }
      }
    }
    
    echo '<div class=coverNavigationAdmin>';
      echo '<h3>'.$navigation.'</h3>';
      echo '<a href="admin_gallery.php" title="back">
      <img class="up" src="../lib/Gallery/design/up.gif" alt="to gallery" onmouseover="this.src = \'../lib/Gallery/design/up_hover.gif\';" onmouseout="this.src = \'../lib/Gallery/design/up.gif\';" /></a>';
      // return to  gallery    
    echo '</div>';
       
    //show folder
    //echo '<div class="foldersAdmin">';   
    if ($this->langAdmin == 1) {
      $result = mysqli_query($this->db_connect, "select id, item from gallery_album_1 where idParent = $album order by id"); // parent item
    } else {
      $tbAlbumAdmin = 'gallery_album_'.$this->langAdmin;            
      $result = mysqli_query($this->db_connect, "select `$tbAlbumAdmin`.id, `$tbAlbumAdmin`.item from `$tbAlbumAdmin`, gallery_album_1 where gallery_album_1.idParent = $album and gallery_album_1.id =  `$tbAlbumAdmin`.id order by id");
    }
   
    while ($row = mysqli_fetch_array($result)) {
      echo '<div class="itemPhotoGal itemPhotoBg hFolder">';
        echo '<table class="tabThumbGal"><tr>';
          echo '<td><p><a href="./admin_gallery.php?album='.$row['id'].'" title="'.$row['item'].'">'.$row['item'].'</a></p></td>';
        echo '</td></tr></table>';
      echo '</div> <!-- .itemPhotoGal -->'; 
    }   
    //echo '</div>';
    
    //show photo                                                
    echo '<form method="post" action="" id="delChecked">';
      $result = mysqli_query($this->db_connect, "select id, nameImage from gallery where idAlbum = $album order by id desc");    
      while ($row = mysqli_fetch_array($result)) {                                           
        $path = '../gallery/'.$album.'/thumb_'.$row['nameImage'];
        
        $part = explode('.', $row['nameImage']);
        $format = $part[count($part) - 1];

        if ($format == 'jpg' or $format == 'jpeg') {
          $imagex = imagesx(imagecreatefromjpeg($path));
          $imagey = imagesy(imagecreatefromjpeg($path));
        } else if ($format == 'gif') {
          $imagex = imagesx(imagecreatefromgif($path));
          $imagey = imagesy(imagecreatefromgif($path));
        } else if ($format == 'png') {
          $imagex = imagesx(imagecreatefrompng($path));
          $imagey = imagesy(imagecreatefrompng($path));
        } 
        if ($imagex / $imagey >= 1) {
          $style= ' class="thumbImgL"';
        } else {
          $style= ' class="thumbImgP"';
        }
        
        echo '<div class="itemPhoto"><table><tr><td class="tabThumb">';
          echo '<a href="admin_photo.php?album='.$album.'&amp;image='.$row['id'].'" title="'.$row['nameImage'].'"><img'.$style.' src="'.$path.'" alt="'.$row['nameImage'].'" /></a>'; // <!-- class="'.$styl.'" -->          
        echo '</td></tr>';
        echo '<tr><td class="checkbox"><input type="checkbox" name="check['.$row['id'].']" /></td></tr>';
        echo '</table></div> <!-- .itemPhoto -->';                                  
      } 
      if (mysqli_num_rows($result) > 0) {
        echo '<table class="submitChecked"><tr><td>';
          echo '<input type="submit" name="delChecked" value="'.$phArr[9].'" />'; // delete marked
        echo '</td></tr></table>';   
      }
    echo '</form>';
  }
  
  public function deleteChecked ($checked)
  {  
    $phArr = $this->translate($this->langAdmin);
    
    if ($checked == null) {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[10].'</div></div>'; // No images have been selected.
    } else {      
      foreach ($checked as $k => $v) {
        for ($i = 0; $i < 1; $i++) {
          $result = mysqli_query($this->db_connect, "select idAlbum from gallery where id = $k");
          $row = mysqli_fetch_array($result);
          $album = $row['idAlbum']; 
        }
        $result = mysqli_query($this->db_connect, "select nameImage from gallery where id = $k");
        $row = mysqli_fetch_array($result);
        $delSer = unlink ('../gallery/'.$album.'/'.$row['nameImage']);
        $delSer = unlink ('../gallery/'.$album.'/thumb_'.$row['nameImage']);
        if ($delSer) {
          $delete = mysqli_query($this->db_connect, "delete from gallery where id = $k");
          if ($delete) echo '<div class="wACover"><div class="warrAdmin">'.$phArr[11].':&nbsp;'.$row['nameImage'].'</div></div>'; // The image was deleted.
        }              
      }   
    }  
  }
  
  private function translate ($lang)
  {
    $phArr = array();
    $result = mysqli_query($this->db_connect, "select phrase from trans_admin where category = 'gallad2' and lang = $lang order by ord");
    while ($row = mysqli_fetch_array($result)) {
      $phArr[] = $row['phrase'];
    }
    return $phArr;
  }
  
}         
?>