 <?php
/**
 * Show images
 */
class PhotoAdmin
{

  public $db_connect = null;	
    
    /**
	 * Hlavní jazyk.
	 *
	 * @var int
	 */
	public $langAdmin = 0;
	
	
	/**
	 * Languages
	 *
	 * @var array
	 */
	public $language = null;
	
		/**
	 * Active languages
	 *
	 * @var array
	 */
	public $languageActive = null;
	
	
					
	/**
	 * @param array $db_connect
	 * @param int $langAdmin
	 * @param array $language 
	 * @param array $languageActive               	 
	*/
	
  public function __construct($db_connect, $langAdmin, $language, $languageActive)
	{
		$this->db_connect = $db_connect;
		$this->langAdmin = (int) $langAdmin;
		$this->language = $language;
		$this->languageActive = $languageActive;
	}

	public function showPhotoDetail ($album, $image)
  {        
    $phArr = $this->translate($this->langAdmin);
    
    ini_set('memory_limit', '64M');

    $result = mysqli_query($this->db_connect, "select nameImage from gallery where id = $image");
    $row = mysqli_fetch_array($result);
    $nameImage = $row['nameImage'];
    
    // Control image size  
    if (strtolower(substr($nameImage, -4)) === 'jpeg') {
      $im = imagecreatefromjpeg('../gallery/'.$album.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'jpg') {
      $im = imagecreatefromjpeg('../gallery/'.$album.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'gif') {
      $im = imagecreatefromgif('../gallery/'.$album.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'png') {
      $im = imagecreatefrompng('../gallery/'.$album.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    }
    if ($x > 460) {
      $styl = 'imgGal';
    } else {
      $styl = 'imgGalPortrait';
    }
              
    echo '<div class="detailAdmin">';
      echo '<img src="../gallery/'.$album.'/'.$nameImage.'" class="'.$styl.'" alt="'.$nameImage.'" />'; 
      echo '<div class="up">';
        echo '<a href="admin_gallery.php?album='.$album.'" title="'.$phArr[0].'">
        <img src="../lib/Gallery/design/up.gif" alt="zpět do alba" onmouseover="this.src = \'../lib/Gallery/design/up_hover.gif\';" onmouseout="this.src = \'../lib/Gallery/design/up.gif\';" /></a>';
      echo '</div> <!-- .up -->'; // return to album
    echo '</div>';
               
    $tbComment1 = 'gallery_comment_1';
                
    $result = mysqli_query($this->db_connect, "select id from `$tbComment1` where image = $image");
    $row = mysqli_fetch_assoc($result);
    $id = $row['id'];
                
    $num_rows = mysqli_num_rows($result);
                 
    if ($num_rows == 0) {
                
      // Form insert comment
      echo '<form class="editComment" method="post" action="">';
        echo '<input type="hidden" name="insertComment" value="1" />';
        echo '<input type="hidden" name="image" value="'.$image.'" />';
        echo '<input type="hidden" name="album" value="'.$album.'" />';
        echo '<input class="inpComment" type="text" name="comment" value="'.$phArr[1].'" />'; // add commentary
        echo '&nbsp;&nbsp;<input type="submit" value="'.$phArr[2].'" />';
      echo '</form>';
    } else {
  
      // Form edit comment
      foreach ($this->languageActive as $code => $lg) {
                        
        $tbComment = 'gallery_comment_'.$lg;
                    
        $result = mysqli_query($this->db_connect, "select comment from `$tbComment` where id = $id");
        $row = mysqli_fetch_assoc($result);
        $comment = $row['comment'];
                        
        echo '<form class="editComment" method="post" action="">';
          echo '<table>';
            echo '<input type="hidden" name="editComment" value="1" />';
            echo '<input type="hidden" name="lang" value="'.$lg.'" />';
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            
            echo '<tr>';
              echo '<td><img src="../lib/Gallery/design/'.$code.'.png"></td>';
              echo '<td><input class="inpComment" type="text" name="comment" value="'.$comment.'" /></td>';
              echo '<td><input type="submit" value="'.$phArr[2].'" /></div></td>'; // send
            echo '</tr>'; 
          echo '</table>';
        echo '</form>';  
      }  
    }
                
  }
    
  public function insertComment ($image, $comment)
  {
    $phArr = $this->translate($this->langAdmin);
    
    $tbComment1 = 'gallery_comment_1';
                                                                
    $sql = 
    "insert into `$tbComment1` (comment, image) "
    ."values('"
    .mysqli_real_escape_string($this->db_connect, $comment)
    ."','"
    .mysqli_real_escape_string($this->db_connect, $image)
    ."')"
    ;
                
    $result = mysqli_query($this->db_connect, $sql);
                
    $result = mysqli_query($this->db_connect, "select id from `$tbComment1` order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $id = $row['id'];
                 
    // other languages
    $lng = $this->language; // for view edit form
    $lang1 = array_shift($lng);
    foreach ($lng as $lg) {
      $tbComment = 'gallery_comment_'.$lg;
                                                                  
      $sql = 
      "insert into `$tbComment` (id, comment) "
      ."values('"
      .mysqli_real_escape_string($this->db_connect, $id)
      ."','"
      .mysqli_real_escape_string($this->db_connect, $comment)
      ."')"
      ;
                  
      $result = mysqli_query($this->db_connect, $sql);
    }
                                        
    if (!$result) {
      echo mysqli_error($this->db_connect);
    } else {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[3].'</div></div>'; // The commentary has been successfully added.              
    }      
  }
  
  public function editComment ($id, $lg, $comment)
  {  
    $phArr = $this->translate($this->langAdmin);
    
    $tbComment = 'gallery_comment_'.$lg;                           
    $sql = "update `$tbComment` set comment = '"
    .mysqli_real_escape_string($this->db_connect, $comment)."' where id = $id";
              
    $result = mysqli_query($this->db_connect, $sql);
               
    if (!$result) {
      echo mysqli_error($this->db_connect);
    } else {
      echo '<div class="wACover"><div class="warrAdmin">'.$phArr[4].'</div></div>'; // The commentary has been successfully edited.               
    }   
  }
  
  private function translate ($lang)
  {
    $phArr = array();
    $result = mysqli_query($this->db_connect, "select phrase from trans_admin where category = 'photo2' and lang = $lang order by ord");
    while ($row = mysqli_fetch_array($result)) {
      $phArr[] = $row['phrase'];
    }
    return $phArr;
  }


}