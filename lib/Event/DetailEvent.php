<?php
require_once('./lib/IncreaseDate/IncreaseDate.php');

class DetailEvent {

    /**
	* Připojení k DB.
	*
	* @var array
	*/
	public $db_connect = null;
	
	  /**
	* lang.
	*
	* @var int
	*/
	public $lang = 0;
		
	public function __construct($db_connect, $lang) {
    
    $this->db_connect = $db_connect;
    
    $this->lang = (int) $lang;

  }

  public function detEvent ($uid) {
    
    if ($this->lang == 1) {
      $phEvn = 'Events';
    } else if ($this->lang == 2) {
      $phEvn = 'Seznamovací večírky';
    }
    
    echo '<h1>'.$phEvn.'</h1>'; 
            
    $now = date('Y-m-d');
    switch ($this->lang) {
      case 1: $phAttend = 'Attend: '; $phAttendF = 'Attend';
      break;
      case 2: $phAttend = 'Účastnit se: '; $phAttendF = 'Zúčastnit se';
    }

    $result = mysqli_query($this->db_connect, "select * from events where dateEvent >= '$now' order by dateEvent asc");
              
    while ($row = mysqli_fetch_array($result)) {
              
      $image = array();
                
      $idEvent = $row['id'];
      $dateEvent = $row['dateEvent'];
      $hour = $row['hour'];
      $minute = $row['minute'];
      $category = $row['category'];
      $place = $row['place'];
      $address = $row['address'];
      $attendDefault = $row['attend'];
      $map = $row['map'];

      switch ($category) {
        case 1: $lg = 'en';
        break;
        case 2: $lg = 'cs';    
      }
                
      $tb = 'category_'.$this->lang;
      $categ = mysqli_query($this->db_connect, "select category from `$tb` where id = $category");
      $ctg = mysqli_fetch_array($categ);

      $idEvt = mysqli_query($this->db_connect, "select id from event_1 where idEvent = $idEvent");              
      $idE = mysqli_fetch_array($idEvt);
              
      $idEv = $idE['id'];
                
      $tb = 'event_'.$this->lang;
      $textEvent = mysqli_query($this->db_connect, "select title, perex, event from `$tb` where id = $idEv");
      $tE = mysqli_fetch_array($textEvent);
          
      echo '<div class="event" id="event'.$idEvent.'">';
                  
        $image = mysqli_query($this->db_connect, "select gallery.nameImage, gallery_album_1.id from gallery, gallery_album_1 where gallery_album_1.idEvent = $idEvent and gallery.idAlbum = gallery_album_1.id order by gallery.dateInsert asc limit 0,1");
        $img = mysqli_fetch_array($image);
        
        echo '<div class="formOrderEv">';
                  
          echo '<div class="spacer">';
                    
          echo '</div>';

          echo '<div class="map">';
          
            echo $map;
          
          echo '</div>';
          
          echo '<img class="buttonCloseFormEv" src="./design/close_form_new.png" alt="hide" />';
                    
          echo '<form class="formOrd" method="post" action="">';
                    
            echo '<p><input type="hidden" name="idEvent" value="'.$idEvent.'" /></p>';
            
            echo '<p><input type="hidden" name="uid" value="'.$uid.'" /></p>';
                      
            echo '<table>'; 
                      
              $prc = mysqli_query($this->db_connect, "select price from price");
              while ($pc = mysqli_fetch_array($prc)) {
                $priceArr[] = $pc['price'];
              }
              
              switch ($this->lang) {
                case 1: $phName = 'Name'; 
                $phPhone = 'Phone number'; 
                $phSend = 'Send booking';
                $phPref = 'Your payment preference'; 
                $phCash = 'At the Entrance '.$priceArr[1].' CZK'; 
                $phBank = 'Transfer to account '.$priceArr[0].' CZK'; 
                $phMember = 'Membership (6 month) '.$priceArr[2].' CZK'; 
                $phVoucher = 'Voucher no.'; 
                $phKnow = '(if You know)'; 
                $phSurname = 'Surname'; 
                $phDateBirth = 'Year of Birth'; 
                $phMale = 'Male';  
                $phFemale = 'Female';  
                $phAgree = 'I agree with Terms and Conditions'; 
                $phBookingForm = 'Fill the booking form'; 
                $phAddress = 'Address';
                break;
                case 2: $phName = 'Jméno'; 
                $phPhone = 'Telefon'; 
                $phSend = 'Odeslat rezervaci'; 
                $phPref = 'Preferuji platbu';  
                $phCash = 'Hotově u vchodu '.$priceArr[1].' Kč';  
                $phBank = 'Bankovním převodem '.$priceArr[0].' Kč'; 
                $phMember = 'Členství (6 měsíců) '.$priceArr[2].' Kč'; 
                $phVoucher = 'Voucher č.'; 
                $phKnow = '(znáte-li)'; 
                $phSurname = 'Příjmení';  
                $phDateBirth = 'Rok narození'; 
                $phMale = 'Muž'; 
                $phFemale = 'Žena'; 
                $phAgree = 'Souhlasím s obchodními podmínkami'; 
                $phBookingForm = 'Vyplňte rezervační formulář'; 
                $phAddress = 'Adresa';   
              }
                          
              echo '<tr><td colspan="2"><p class="ajaxOrder"></p></td></tr>';
                        
              echo '<tr><td colspan="2"><h2 class="fillBook">'.$phBookingForm.'</h2></td></tr>';
              
              if ($uid == 0) {
             
                echo '<tr><td><strong>'.$phName.':</strong> </td><td><input class="inpReserve" type="text" name="name" value="" /></td></tr>';
                  
                echo '<tr><td><strong>'.$phSurname.':</strong> </td><td><input class="inpReserve" type="text" name="surname" value="" /></td></tr>';
                            
                echo '<tr><td><strong>E-mail:</strong> </td><td><input class="inpReserve" type="text" name="email" value="" /></td></tr>';
                            
                echo '<tr><td><strong>'.$phPhone.':</strong> </td><td><input class="inpReserve" type="text" name="phone" value="" /></td></tr>';
                  
                echo '<tr><td><strong>'.$phDateBirth.':</strong> </td><td><input class="inpYear" type="text" name="birth" value="" /></td></tr>';
                  
                echo '<tr><td><strong>'.$phFemale.'/'.$phMale.':</strong></td><td>';
                  
                  echo '<select name="sex">';
                    
                    echo '<option value="1">'.$phFemale.'</option>';
                      
                    echo '<option value="2">'.$phMale.'</option>';
                    
                  echo '</select>';
                    
                echo '</td></tr>';
                        
              }
              
              echo '<tr><td colspan="2"><h3>'.$phPref.'</h3></td></tr>';
                                                                         
              echo '<tr><td><strong>'.$phBank.':</strong> </td><td><input type="radio" name="payment" value="1" checked="checked" /></td></tr>';
                          
              echo '<tr><td><strong>'.$phCash.':</strong> </td><td><input type="radio" name="payment" value="2" /></td></tr>';
                          
              echo '<tr><td><strong>'.$phMember.':</strong> </td><td><input type="radio" name="payment" value="3" /></td></tr>';
                          
              echo '<tr><td><strong>'.$phVoucher.':</strong> </td><td><input class="inpReserve" type="text" name="voucher" value="" /></td></tr>';
                   
              echo '<tr>';
              
                echo '<td colspan="2">';
                
                  echo '<div class="conditionForm">';                   

                    echo '<img class="delWinForm" src="./design/close_form_new.png" alt="hide" />';
                    
                    $text = new Text ($this->db_connect, $id = 105, $this->lang);
                    echo $text->showText(); 
                  
                  echo '</div>';
                  
                  echo '<div class="coverPhAgree"><span class="phAgree"><strong>'.$phAgree.'</strong></span> <input type="checkbox" name="agree" /></div>';
                  
                echo '</td>';
                
              echo '</tr>';
              
              echo '<tr><td colspan="2"><input class="sbmReserve buttonSubmit" type="submit" value="'.$phSend.'" /></td></tr>';
                      
            echo '</table>';
                          
          echo '</form>';
                  
        echo '</div>';        
        
        $foto = './gallery/'.$img['id'].'/thumb_'.$img['nameImage'];
        
        echo '<img class="eventImg" src="'.$foto.'" alt="'.$img['nameImage'].'" />';              
                
        echo '<div class="ttEv">';
        
          echo '<h2>'.$tE['title'].'</h2>';
          
        echo '</div>';
                  
        echo '<p class="eventCat"><strong><span class="bannCategoryList">'.$ctg['category'].'</span></strong><br /></p>';
        
        echo '<div class="buttonOrderEv bOEv"><img src="./design/heart_small_red_white.png" alt="heart" />'.$phAttendF.'</div>';
    
        //echo '<p style="float:left; width:300px;"><strong><span class="bannCategoryList">'.$ctg['category'].'</span></strong><br /></p>';
    
        echo '<div class="actionInfoEv">';
      
          $increaseDate = new IncreaseDate($this->lang);
          
          $incDate = $increaseDate->incDate($dateEvent);
          
          echo '<table><tr><td><img src="./design/kalendar-cerny.png" alt="kalendar" /></td><td>'.$incDate.' '.$hour.':'.$minute.' - 22:00</td></tr></table>';
          
          $attends = mysqli_query($this->db_connect, "select id from orders where idEvent = $idEvent");
          $attends = mysqli_num_rows($attends);
                    
          if ($attends < 27) $attends = $attendDefault;
        
          switch ($this->lang) {
            case 1: $phAttend = 'attend: ';
            break;
            case 2: $phAttend = 'účastníků: ';      
          }
                    
          echo '<table><tr><td><img src="./design/osobny-cerne.png" alt="attend" /></td><td>'.$phAttend.' '.$attends.'</td></tr></table>'; 
                    
          echo '<table><tr><td><img src="./design/place-cerne.png" alt="place" /></td><td>'.$place.'</td></tr></table>';
                    
        echo '</div>';
        
        echo '<div class="address">';
        
          echo '<strong>'.$phAddress.':</strong> '.$address;
        
        echo '</div>';    

        echo '<div class="eventEv">';                                                                                     
                    
          echo $tE['event'];
                  
        echo '</div>';
          
      echo '</div>';
              
    }
              
  }

}
?>
