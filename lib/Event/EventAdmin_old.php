<?php
/* změna pořadí prvků v tabulce */

require_once (dirname(__FILE__) . '/EventAdminRenderer.php');

require_once ('../lib/Tools/HideButton/HideButton.php');

require_once ('../lib/Gallery/AlbumAdmin.php');

class EventAdmin
{
	    /**
	 * Připojení k db.
	 *
	 * @var array
	 */
	public $db_connect = null;
    
  public function __construct ($db_connect)
  {
    $this->db_connect = $db_connect;  
  }

  public function insertButton() 
  {
    echo '<form class="insertButton" method="post" action="">';
      echo '<input type="submit" name="input" value="nová akce" />'; 
    echo '</form>'; 
  }
  
  public function input()
  {
    
    $hide = new HideButton($uid = 0);
    $hide->hideButtonFirst();
    
    echo '<h2>Nová akce</h2>';
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="insert" value="1" />';
      echo '<table class="tabInput">';
        echo '<tr><td>Kategorie</td>';
        echo '<td>';
          echo '<select name="category">';
            $result = mysqli_query($this->db_connect, "select * from category_1 order by id asc");
            while ($row = mysqli_fetch_array($result)) {
              echo '<option value="'.$row['id'].'">'.$row['category'].'</option>';            
            }
          echo '</select>';
        echo '</td></tr>';
        echo '<tr><td>Datum</td><td><input id="datepicker" class="middle" type="text" name="dateEvent" value="" /></td></tr>';
        echo '<tr><td>Hodin</td>';
        echo '<td>';
          echo '<select name="hour">';            
            for ($i = 0; $i <= 24; $i++) {
              echo '<option value="'.$i.'">'.$i.'</option>';
            }
          echo '</select>';
        echo '</td></tr>';
        echo '<tr><td>Minut</td>';
        echo '<td>';
          echo '<select name="minute">';            
            echo '<option value="00">00</option>';
            echo '<option value="15">15</option>';
            echo '<option value="30">30</option>';
            echo '<option value="45">45</option>';
          echo '</select>';
        echo '</td></tr>';
        echo '<tr><td>Místo</td><td><input class="long" type="text" name="place" value="" /></td></tr>';
        echo '<tr><td>Adresa</td><td><input class="long" type="text" name="address" value="" /></td></tr>';
        echo '<tr><td>Zúčastněných</td><td><input type="text" name="attend" value="" size="2"/></td></tr>';
        echo '<tr><td>Mapa</td><td></td></tr>';
        echo '<tr><td colspan="2"><textarea name="map" cols="52" rows="5"></textarea></td></tr>';
        echo '<tr><td><input type="submit" value="odeslat" /></td><td></td></tr>';
      echo '</table>';
    echo '<form>';
                                                                      
  }
  
  public function insert($category, $dateEvent, $hour, $minute, $place, $address, $attend, $map)
  {
    $date = substr($dateEvent, 6, 4).'-'.substr($dateEvent, 0, 2).'-'.substr($dateEvent, 3, 2);
    $result = mysqli_query($this->db_connect, "insert into events (category, dateEvent, hour, minute, place, address, attend, map) values ($category, '$date', $hour, '$minute', '"
    .mysqli_real_escape_string($this->db_connect, $place)
    ."','"
    .mysqli_real_escape_string($this->db_connect, $address)
    ."', '$attend', '$map')"
    );
    if (!$result) echo 'Chyba! Uložení akce se nezdařilo.';
    
    $result = mysqli_query($this->db_connect, "select id from events order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $idEvent = $row['id'];
    
    $result = mysqli_query($this->db_connect, "insert into event_1 (idEvent) values ($idEvent)");
    $result = mysqli_query($this->db_connect, "select id from event_1 order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $idEv1 = $row['id'];
    $result = mysqli_query($this->db_connect, "insert into event_2 (id) values ($idEv1)");
    if (!$result) echo 'Chyba! Uložení popisu akce se nezdařilo.';
    
    $album = new AlbumAdmin ($this->db_connect, $langAdmin = 1, $language = array(1,2), $languageActive = array(1,2));
    $album->insertAlbum($item = 'Album '.$idEvent, $idParent = 0, $idEvent);
  
  }
  
  public function edit($id)
  {
    $result = mysqli_query($this->db_connect, "select * from events where id = $id");
    $row = mysqli_fetch_array($result);

    $hide = new HideButton($uid = 0);
    $hide->hideButtonFirst();
    
    echo '<h2>Editace akce</h2>';
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="insedit" value="1" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<table class="tabInput">';
        echo '<tr><td>Kategorie</td>';
        echo '<td>';
          $selected = array();
          $selected[1] = null;
          $selected[2] = null; 
          $selected[3] = null;  
          $selected[4] = null;         
          echo '<select name="category">';
            switch ($row['category']) {
              case 1: $selected[1] = ' selected="selected"'; 
              break;
              case 2: $selected[2] = ' selected="selected"';
              break;
              case 3: $selected[3] = ' selected="selected"';
              break;
              case 4: $selected[4] = ' selected="selected"';
            }             
            $categ = mysqli_query($this->db_connect, "select id, category from category_1 order by id");
            while ($ctg = mysqli_fetch_array($categ)) {
              echo '<option value="'.$ctg['id'].'"'.$selected[$ctg['id']].'>'.$ctg['category'].'</option>';
            } 
          echo '</select>';       
        echo '</td></tr>';
        $date = substr($row['dateEvent'], 5, 2).'/'.substr($row['dateEvent'], 8, 2).'/'.substr($row['dateEvent'], 0, 4); 
        echo '<tr><td>Datum</td><td><input id="datepicker" class="middle" type="text" name="dateEvent" value="'.$date.'" /></td></tr>';
        echo '<tr><td>Hodin</td>';
        echo '<td>';
          echo '<select name="hour">';            
            for ($i = 0; $i <= 24; $i++) {
              if ($i == $row['hour']) $selected = ' selected="selected"'; else $selected = null;
              echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
          echo '</select>';
        echo '</td></tr>';
        echo '<tr><td>Minut</td>';
        echo '<td>';
          echo '<select name="minute">';            
            $selected1 = null;
            $selected2 = null;
            $selected3 = null;
            $selected4 = null;
            switch ($row['minute']) {
              case '00': $selected1 = ' selected="selected"';
              break;
              case '15': $selected2 = ' selected="selected"';
              break;
              case '30': $selected3 = ' selected="selected"';
              break;
              case '45': $selected4 = ' selected="selected"';             
            }
            echo '<option value="00"'.$selected1.'>00</option>';
            echo '<option value="15"'.$selected2.'>15</option>';
            echo '<option value="30"'.$selected3.'>30</option>';
            echo '<option value="45"'.$selected4.'>45</option>';
          echo '</select>';
        echo '</td></tr>';
        echo '<tr><td>Místo</td><td><input class="long" type="text" name="place" value="'.$row['place'].'" /></td></tr>';
        echo '<tr><td>Adresa</td><td><input class="long" type="text" name="address" value="'.$row['address'].'" /></td></tr>';
        echo '<tr><td>Zúčastněných</td><td><input type="text" name="attend" value="'.$row['attend'].'" size="2"/></td></tr>';
        echo '<tr><td>Mapa</td><td></td></tr>';
        echo '<tr><td colspan="2"><textarea name="map" cols="52" rows="5">'.$row['map'].'</textarea></td></tr>';        
        echo '<tr><td><input type="submit" value="odeslat" /></td><td></td></tr>';
      echo '</table>';
    echo '</form>'; 
     
  }
  
  public function insedit($id, $category, $dateEvent, $hour, $minute, $place, $address, $attend, $map)
  {
    $date = substr($dateEvent, 6, 4).'-'.substr($dateEvent, 0, 2).'-'.substr($dateEvent, 3, 2);
    $result = mysqli_query($this->db_connect, "update events set category = $category, dateEvent = '$date', hour = $hour, minute = '$minute', place = '"
    .mysqli_real_escape_string($this->db_connect, $place)
    ."', address = '"
    .mysqli_real_escape_string($this->db_connect, $address)
    ."', attend = $attend, map = '$map' where id = $id
    ");
    if (!$result) echo 'Chyba! Editace se nezdařila.';
  }
  
  public function describe ($id, $languageActive, $lang)
  {

    $hide = new HideButton($uid = 0);
    $hide->hideButtonFirst();
    
    echo '<h2>Popis akce</h2>';
    
    //nacteni vlajek
    echo '<table><tr>';
              
      foreach ($languageActive as $co => $lg) {                        
        echo '<td>';                 
          echo '<form method="post" action="" />';
            echo '<input type="hidden" name="describe" value="1" />';
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            echo '<input type="hidden" name="lang" value="'.$lg.'" />';
            echo '<input style="width:40px; height:40px; border:none; background:url(../lib/design/'.$co.'.png) no-repeat; cursor:pointer" type="submit" value="" />'; 
          echo '</form>';
        echo '</td>'; 
        echo '<td>';                                 
      }
    
    echo '</tr></table>';
         
    foreach ($languageActive as $co => $lg) {                                                              
      if ($lg == $lang) echo '<div class="menuLang"><img src="../lib/design/'.$co.'.png" alt="'.$co.'" /></div>';
    }        
             
    $result = mysqli_query($this->db_connect, "select id from event_1 where idEvent = $id");
    $row = mysqli_fetch_array($result);
    $idEv = $row['id']; 
    
    $tb = 'event_'.$lang; 
    $result = mysqli_query($this->db_connect, "select * from `$tb` where id = $idEv");
    $row = mysqli_fetch_array($result);   
    
    echo '<form method="post" action="">';  
      echo '<input type="hidden" name="insdescribe" value="1" />';
      echo '<input type="hidden" name="id" value="'.$idEv.'" />';
      echo '<input type="hidden" name="lang" value="'.$lang.'" />';      
      echo '<table class="tabInput">';              
        echo '<tr><td>Název akce</td><td><input class="long" type="text" name="title" value="'.$row['title'].'" /></td></tr>';
        echo '<tr><td>Perex</td><td></td></tr>';
        echo '<tr><td colspan="2"><textarea rows="5" cols="61" name="perex">'.$row['perex'].'</textarea></td></tr>';
        echo '<tr><td>Popis</td><td></td></tr>';
        echo '<tr><td class="areaEvent" colspan="2"><textarea name="event">'.$row['event'].'</textarea></td></tr>';
        echo '<tr><td><input type="submit" value="odeslat" /></td><td></td></tr>';
      echo '</table>';
    echo '</form>';
    
    echo '<script type="text/javascript">';
    	echo 'CKEDITOR.replace( \'event\' );';
    echo '</script>'; 
  
  }
  
  public function insDescribe ($id, $lang, $title, $perex, $event) {
  
    $tb = 'event_'.$lang;
    $result = mysqli_query($this->db_connect, "update `$tb` set title = '"
    .mysqli_real_escape_string($this->db_connect, $title)."', perex = '"
    .mysqli_real_escape_string($this->db_connect, $perex)."', event = '"
    .mysqli_real_escape_string($this->db_connect, $event)."' where id = $id");
    
    if (!$result) echo 'Chyba! Aktualizace se nezdařila.';
    
  }
  
  public function deleteQuery ($id)
  {
    $result = mysqli_query($this->db_connect, "select dateEvent from events where id = $id");
    $row = mysqli_fetch_array($result);
    echo '<div class="warrCover"><div class="warr">';
      echo 'Opravdu chcete akci ze dne <strong>'.$row['dateEvent'].'</strong> smazat?'; 
      echo '<table><tr>';
      echo '<td>';
        echo '<form method="post" action="">';
          echo '<input type="hidden" name="dok" value="1" />';
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="smazat" />';
        echo '</form>';
      echo '</td>';
      echo '<td>';
        echo '<form method="get" action="">';
          echo '<input type="submit" value="ne" />';
        echo '</form>';
      echo '</td>';
      echo '</tr></table>';
    echo '</div></div>';
  }
  
  public function delete ($id)
  {
    $result = mysqli_query($this->db_connect, "delete from events where id = $id");
    if(!$result) echo 'Chyby! Akci se nepodařilo smazat.'; 
  }
    
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new EventAdminRenderer($this);
		}
		return $this->renderer->render();
	}

}
?>
