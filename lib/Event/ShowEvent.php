<?php
class ShowEvent {

    /**
	* Připojení k DB.
	*
	* @var array
	*/
	public $db_connect = null;
	
	public function __construct($db_connect) {
    
    $this->db_connect = $db_connect;

  }

  public function event ($limit, $lang) {
    
    $now = date('Y-m-d');
    switch ($lang) {
      case 1: $attend = 'Attend';
      break;
      case 2: $attend = 'Zúčastnit se';
    }

    $from = $limit - 1;
        
    $result = mysqli_query($this->db_connect, "select * from events where dateEvent >= '$now' order by dateEvent asc limit $from, $limit");
        
    $row = mysqli_fetch_array($result);
    $idEvent = $row['id'];
    $dateEvent = $row['dateEvent'];
    $hour = $row['hour'];
    $minute = $row['minute'];
    $place = $row['place'];
    $attendDefault = $row['attend'];
    $category = $row['category'];
    
    $lg = null;
    
    $result = mysqli_query($this->db_connect, "select lang from category_1 where id = $category");
    $row = mysqli_fetch_array($result);
    $langC = $row['lang'];
    
    switch ($langC) {
      case 1: $lg = 'en';
      break;
      case 2: $lg = 'cs'; 
      break;
      case 3: $lg = 'ce';    
    }
    
    echo '<img class="flag" src="./design/'.$lg.'_flag.png" alt="'.$lg.'" />';    
     
    $result = mysqli_query($this->db_connect, "select id from event_1 where idEvent = $idEvent");
    $row = mysqli_fetch_array($result);
    $idEv = $row['id'];
      
    $tb = 'event_'.$lang;
    $result = mysqli_query($this->db_connect, "select title, perex, event from `$tb` where id = $idEv");
    $row = mysqli_fetch_array($result);
    
    echo '<h2>'.$row['title'].'</h2>';
    
    $tb = 'category_'.$lang;
    $ctgr = mysqli_query($this->db_connect, "select category from `$tb` where id = $category");
    $cg = mysqli_fetch_array($ctgr);
    
    echo '<strong><span class="bannCategory">'.$cg['category'].'</span></strong><br />';
    
    echo '<div class="actionInfo">';
      
      $dayD = date("D", mktime(0,0,0,substr($dateEvent, 5, 2), substr($dateEvent, 8, 2), substr($dateEvent, 0, 4)));
      
      if ($lang == 2) {
        
        switch ($dayD) {
          
          case 'Mon': $dayD = 'po';
          break;
          case 'Tue': $dayD = 'út';
          break;
          case 'Wed': $dayD = 'st';
          break;
          case 'Thu': $dayD = 'čt';
          break;
          case 'Fri': $dayD = 'pá';
          break;
          case 'Sat': $dayD = 'so';
          break;
          case 'Sun': $dayD = 'ne';        
       
        }
      
      }
      
      $day = date("j", mktime(0,0,0,substr($dateEvent, 5, 2), substr($dateEvent, 8, 2), substr($dateEvent, 0, 4)));
      
      if ($lang == 2) $day = $day.'.';
      
      $month = date("M", mktime(0,0,0,substr($dateEvent, 5, 2), substr($dateEvent, 8, 2), substr($dateEvent, 0, 4)));
      
      if ($lang == 2) {
        
        switch ($month) {
          
          case 'Jan': $month = 'ledna';
          break;
          case 'Feb': $month = 'února';
          break;
          case 'Mar': $month = 'března';
          break;
          case 'Apr': $month = 'dubna';
          break;
          case 'May': $month = 'května';
          break;
          case 'Jun': $month = 'června';
          break;
          case 'Jul': $month = 'července';
          break;
          case 'Aug': $month = 'srpna'; 
          break;
          case 'Sep': $month = 'září'; 
          break;
          case 'Okt': $month = 'října'; 
          break;
          case 'Nov': $month = 'listopadu'; 
          break; 
          case 'Dec': $month = 'prosince';              
        }
      
      }
      
      echo '<table class="tbWhen"><tr><td><img src="./design/kalendar-bily.png" alt="kalendar" /></td><td>'.$dayD.' '.$day.' '.$month.' '.$hour.':'.$minute.'</td></tr></table>';
      
      $attends = mysqli_query($this->db_connect, "select id from orders where idEvent = $idEvent");
      $attends = mysqli_num_rows($attends);
      
      if ($attends < 27) $attends = $attendDefault;
      
      switch ($lang) {
        case 1: $phAttend = 'attend: '; $phMoreInfo = 'More information';
        break;
        case 2: $phAttend = 'účastníků: '; $phMoreInfo = 'Více informací';      
      }
      
      echo '<table class="tbAttend"><tr><td><img src="./design/osobny-bile.png" alt="attend" /></td><td>'.$phAttend.' '.$attends.'</td></tr></table>'; 
      
      echo '<table class="tbPrice"><tr><td><img src="./design/place-bile_new.png" alt="place" /></td><td>'.$place.'</td></tr></table>';
      
    echo '</div>';    

    //echo '<p class="perex">';                                                                                     
      
      //echo $row['perex'];
    
    //echo '</p>';
    
    echo '<div class="handler">';
              
      echo '<div class="slideButton" id="more'.$idEvent.'">'.$phMoreInfo.'</div>';
      
      echo '<div class="orderButton" id="order'.$idEvent.'"><img src="./design/heart_small_red_new.png" alt="heart" />'.$attend.'</div>';  
    
    echo '</div>';
    
    /*
    
    echo '<div class="formOrder">';
    
      echo '<div class="spacer">';
      
      echo '</div>';
      
      echo '<div class="ajaxOrder"></div>';
      
      echo '<img class="buttonCloseForm" src="./design/close_form.png" alt="hide" />';
      
      echo '<form class="formOrd" method="post" action="">';
      
        echo '<input type="hidden" name="idEvent" value="'.$idEvent.'" />';
        
        echo '<table>';
        
          switch ($lang) {
            case 1: $phName = 'Name'; $phPhone = 'Phone'; $phSend = 'Send';
            break;
            case 2: $phName = 'Jméno'; $phPhone = 'Telefon'; $phSend = 'Odeslat';          
          }
          
          echo '<tr><td>'.$phName.'*: </td><td><input class="inpReserve" type="text" name="name" value="" /></td></tr>';
          
          echo '<tr><td>E-mail*: </td><td><input class="inpReserve" type="text" name="email" value="" /></td></tr>';
          
          echo '<tr><td>'.$phPhone.': </td><td><input class="inpReserve" type="text" name="phone" value="" /></td></tr>';
          
          echo '<tr><td colspan="2"><input class="sbmReserve" type="submit" value="'.$phSend.'" /></td></tr>';
        
        echo '</table>';
            
      echo '</form>';
    
    echo '</div>';
    
    echo '<div class="detailArticl">';
    
      echo '<div class="spacer">';
      
      echo '</div>';
          
      echo '<img class="buttonCloseDetail" src="./design/close_form.png" alt="hide" />';
          
      echo $row['event']; 
                    
    echo '</div>';
  
    */
  
  }

}
?>
