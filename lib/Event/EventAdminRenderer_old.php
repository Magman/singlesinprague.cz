<?php

require_once '../lib/Help/HelpAdmin.php';

class EventAdminRenderer
{
  private $event;
  
  public function __construct (EventAdmin $event)
  {
    $this->event = $event;  
  }

  public function render()	
	{ 		
		return $this->renderDefault();
	}

  public function renderDefault()
  {
    
    $help = new HelpAdmin ('cs'); // nápověda
    
    $help->showHelp ('event', 1);
    
    echo '<table class="tabAdmin">';
    
      echo '<tr><th>Kategorie</th><th>datum</th><th>čas</th><th>místo</th><th>edit</th><th>text</th><th>del</th></tr>';
    
      $result = mysqli_query($this->event->db_connect, "select * from events order by dateEvent desc");
      
      while ($row = mysqli_fetch_array($result)) {
        
        $category = mysqli_query($this->event->db_connect, "select category from category_2 where id = {$row['category']}");
        $ctg = mysqli_fetch_array($category);       
        
        echo '<tr>';        
          
          echo '<td>'.$ctg['category'].'</td>';
          
          echo '<td>'.$row['dateEvent'].'</td>';
          
          echo '<td>'.$row['hour'].':'.$row['minute'].'</td>';
          
          echo '<td>'.$row['place'].'</td>';
          
          echo '<td>';
          
            echo '<form method="post" action="">';
            
              echo '<input type="hidden" name="edit" value="1" />';
              
              echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
              
              echo '<input class="pencil" type="submit" value="" />';
              
            echo '</form>';
                    
          echo '</td>';
          
          echo '<td>';
          
            echo '<form method="post" action="">';
            
              echo '<input type="hidden" name="describe" value="1" />';
              
              echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
              
              echo '<input class="describe" type="submit" value="" />';
              
            echo '</form>';
                    
          echo '</td>';
          
          echo '<td>';
          
            echo '<form method="post" action="">';
            
              echo '<input type="hidden" name="del" value="1" />';
              
              echo '<input type="hidden" name="id" value="'.$row['id'].'" />';
              
              echo '<input class="delete" type="submit" value="" />';
              
            echo '</form>';
                    
          echo '</td>';
           
        echo '</tr>';
      
      }
      
    echo '</table>';
     
  }

}
?>