<?php
class addButton
{
    /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**
	* @param int $uid   
	*/  
  
  public function __construct ($uid)
  {
    $this->uid = $uid;
  }
  
  public function addButtonFirst($entita, $value)
  {
    $entita = ucfirst($entita);
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="add'.$entita.'" value="1" />';
      echo '<input type="submit" value="'.$value.'" />';
    echo '</form>'; 
  }
  
  public function addButtonSecond($entita, $entitaParent, $idParent, $value)
  {    
    $entita = ucfirst($entita);
    $entitaParent = ucfirst($entitaParent);
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />';
      echo '<input type="hidden" name="add'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="submit" value="'.$value.'" />';
    echo '</form>'; 
  }
  
  public function addButtonThird($entita, $entitaParent, $idParent, $entitaParentP, $idParentP, $value)
  {    
    $entita = ucfirst($entita);
    $entitaParent = ucfirst($entitaParent);
    $entitaParentP = ucfirst($entitaParentP);
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />';
      echo '<input type="hidden" name="show'.$entitaParent.'" value="1" />';
      echo '<input type="hidden" name="add'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="id'.$entitaParentP.'" value="'.$idParentP.'" />';
      echo '<input type="submit" value="'.$value.'" />';
    echo '</form>'; 
  }
  
}
?>