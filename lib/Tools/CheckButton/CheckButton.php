<?php
class CheckButton
{
  
  /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**
	* @param int $uid  
	*/  
  
  public function __construct ($uid)
  {
    $this->uid = $uid;
  }
  
  public function checked($entita, $value, $id)
  {
    $entita = ucfirst($entita);
    
    if ($value == 1) {
      echo '<form method="post" action="">';  
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="check'.$entita.'" value="1" />';
        echo '<input type="hidden" name="checked" value="0" />';
        echo '<input class="formCheckedYes" type="submit" value="" />';
      echo '</form>';       
    } else {
      echo '<form method="post" action="">';  
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="check'.$entita.'" value="1" />';
        echo '<input type="hidden" name="checked" value="1" />';
        echo '<input class="formCheckedNo" type="submit" value="" />';
      echo '</form>';        
    }  
  }
  
  public function checkedSecond($funct, $entita, $entitaParent, $idParent, $value, $id)
  {
    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    
    if ($value == 1) {
      echo '<form method="post" action="">';  
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
        echo '<input type="hidden" name="'.$funct.$entita.'" value="1" />'; // def x active
        echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
        echo '<input type="hidden" name="checked" value="0" />';
        echo '<input class="formCheckedYes" type="submit" value="" />';
      echo '</form>';       
    } else {
      echo '<form method="post" action="">';  
        echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
        echo '<input type="hidden" name="'.$funct.$entita.'" value="1" />'; // def x active
        echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
        echo '<input type="hidden" name="checked" value="1" />';
        echo '<input class="formCheckedNo" type="submit" value="" />';
      echo '</form>';        
    }  
  }
  
}