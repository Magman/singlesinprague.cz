<?php
class DeleteButton
{
    
  /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**		
	* @param int $uid   
	*/  
	
	public function __construct($uid)
  {
    $this->uid = $uid;
  }
  
  public function deleteButtonFirst($entita, $id)
  {

    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    
    echo '<form method="post" action="">';  
      echo '<input type="hidden" name="del'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
  
  public function deleteButtonSecond($entita, $entitaParent, $idParent, $id)
  {

    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    
    echo '<form method="post" action="">'; 
      echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="del'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
  
  public function deleteButtonThird($entita, $entitaParent, $idParent, $entitaParentP, $idParentP, $id)
  {

    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    if ($entitaParentP != null) $entitaParentP = ucfirst($entitaParentP); else $entitaParentP = null;
    
    echo '<form method="post" action="">'; 
      echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="show'.$entitaParent.'" value="1" />';
      echo '<input type="hidden" name="del'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="id'.$entitaParentP.'" value="'.$idParentP.'" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
	
}
?>