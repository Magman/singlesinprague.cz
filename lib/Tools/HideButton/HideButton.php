<?php
class HideButton
{
    
  /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**	
	* @param int $uid   
	*/  
	
	public function __construct($uid)
  {
    $this->uid = $uid;

  }
  
  public function hideButtonFirst() // cancel delete 1 level, hide 2 level table
  {
    
    echo '<form method="post" action="">';  
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
  
  public function hideButtonSecond($entita, $entitaParent, $idParent) // cancel hide 2 level
  {
    
    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;    
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />';  
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
  
  public function hideButtonThird($entita, $entitaParent, $idParent, $entitaParentP, $idParentP) // cancel delete 3 level
  {
    
    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    if ($entitaParentP != null) $entitaParentP = ucfirst($entitaParentP); else $entitaParentP = null;     
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="show'.$entitaParent.'" value="1" />'; 
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="id'.$entitaParentP.'" value="'.$idParentP.'" />';
      echo '<input class="formDel" type="submit" value="" />';
    echo '</form>';  
  
  }
  
  public function hideButtonHref($file) // cancel form 1 level
  {
    echo '<a href="'.$file.'?uid='.$this->uid.'" title="back"><img class="imgDelForm" src="../lib/design/delete.gif" alt="hide" /></a>';
  }
  	
}
?>