<?php
/**
 * Elementy formulare s pameti po reloadu (select a checbox z kategorii)
 */
class FormElement
{
  
  public $db_connect = null;	
				
	/**
	* @param array $db_connect                         	 
	*/
	
  public function __construct($db_connect)
	{
		$this->db_connect = $db_connect;
  } 
  
  public function inputHidden($name, $value)
  {
    echo '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
  }
  
  public function addInput($entita, $default, $size)
  {
  
    if (isset($_POST[$entita])) $value = $_POST[$entita]; else $value = $default;
    echo '<input type="text" name="'.$entita.'" value="'.$value.'" size="'.$size.'" />';  
  
  }
  
  public function addTextarea($entita, $default, $size)
  {
  
    if (isset($_POST[$entita])) $value = $_POST[$entita]; else $value = $default;
    echo '<textarea name="'.$entita.'" cols="'.$size.'">'.$value.'</textarea>';   
  
  }
  
  public function addRadioYesNo($entita)
  {

    if (isset($_POST[$entita])) $value = $_POST[$entita]; else $value = 0;
    switch ($value) {
      case 1: $checked1 = ' checked="checked"'; $checked2 = null;
      break;
      case 0: $checked2 = ' checked="checked"'; $checked1 = null;
    } 
    echo 'Ano:&nbsp;<input type="radio" name="'.$entita.'" value="1"'.$checked1.' />&nbsp;&nbsp;&nbsp;Ne:&nbsp;<input type="radio" name="'.$entita.'" value="0"'.$checked2.' />';     
  
  }
    
  public function addSelect($entita, $prefix, $default, $class, $empty) 
  {
      
    if (isset($_POST[$entita])) $checked = $_POST[$entita]; else $checked = $default;
    echo '<select name="'.$entita.'" class="'.$class.'">';
      if ($empty == 1) echo '<option value="0"'.$selected.'>---</option>';       
      $tb = 'cat'.$prefix.ucfirst($entita).'_1';
      $result = mysqli_query($this->db_connect, "select * from `$tb` order by ord");
      if ($checked == null) {
        $selected = null;
      }          
      while ($row = mysqli_fetch_array($result)) {
        if ($checked != null) {
          if ($row['id'] == $checked) {
            $selected = ' selected="selected"';
          } else {
            $selected = null;
          }
              
        }
        echo '<option value="'.$row['id'].'"'.$selected.'>'.$row[$entita].'</option>';          
      }
    echo '</select>';
  
  }  
  
  public function addCheckbox($entita, $prefix, $class) // prefix pro odliseni tabulek s kategoriemi
  {
         
    if (isset($_POST[$entita])) $checked = $_POST[$entita]; else $checked = null;
    $tb = 'cat'.$prefix.ucfirst($entita).'_1';
    $result = mysqli_query($this->db_connect, "select id, `$entita` from `$tb`");
    echo '<table>';
      while ($row = mysqli_fetch_array($result)) {                 
        if ($checked != null) {
          $selected = null;
          foreach ($checked as $ch) {
            if ($ch == $row['id']) {
              $selected = ' checked="checked"';
            } 
          }  
          echo '<tr><td class="'.$class.'">'.$row[$entita].'</td><td><input type="checkbox" name="'.$entita.'[]" value="'.$row['id'].'"'.$selected.' /></td></tr>';
        } else {
          echo '<tr><td class="'.$class.'">'.$row[$entita].'</td><td><input type="checkbox" name="'.$entita.'[]" value="'.$row['id'].'" /></td></tr>';
        }
      }
    echo '</table>';  

  }
  
  public function editInput($entita, $tb, $where, $size)
  {
      
    $result = mysqli_query($this->db_connect, "select `$entita` from `$tb` where id = $where");
    $row = mysqli_fetch_array($result);
    if (isset($_POST[$entita])) $value = $_POST[$entita]; else $value = $row[$entita];
    echo '<input type="text" name="'.$entita.'" value="'.$value.'" size="'.$size.'" />';  
  
  }
  
  public function editTextarea($entita, $tb, $lang, $where, $size)
  {
  
    $result = mysqli_query($this->db_connect, "select id from `$tb` where idAd = $where"); // $lang = 1
    $row = mysqli_fetch_array($result);
    $tb = substr($tb, 0, strlen($tb) - 1).$lang;
    $result = mysqli_query($this->db_connect, "select `$entita` from `$tb` where id = {$row['id']}");
    $row = mysqli_fetch_array($result);
    $value = $row[$entita];

    if (isset($_POST[$entita])) $value = $_POST[$entita]; else $value = $value;
    echo '<textarea name="'.$entita.'" cols="'.$size.'">'.$value.'</textarea>';   
  }
  
  public function editRadioYesNo($entita, $tb, $where)
  {
  
    $result = mysqli_query($this->db_connect, "select `$entita` from `$tb` where id = $where");
    $row = mysqli_fetch_array($result);

    if (isset($_POST[$entita])) $checked = $_POST[$entita]; else $checked = $row[$entita];
        
    switch ($checked) {
      case 1: $selected1 = ' checked="checked"'; $selected2 = null;
      break;
      case 0: $selected2 = ' checked="checked"'; $selected1 = null;
    } 
    echo 'Ano:&nbsp;<input type="radio" name="'.$entita.'" value="1"'.$selected1.' />&nbsp;&nbsp;&nbsp;Ne:&nbsp;<input type="radio" name="'.$entita.'" value="0"'.$selected2.' />'; 
           
  }
  
  public function editSelect($entita, $prefix, $class, $tb, $where, $empty) 
  {
  
    echo '<select name="'.$entita.'" class="'.$class.'">';  
      if ($empty == 1) echo '<option value="0">---</option>';
      if (isset($_POST[$entita])) { // opakovane odeslani
        $checked = $_POST[$entita];
      } else {
        $value = 'cat'.$prefix.ucfirst($entita);
        $result = mysqli_query($this->db_connect, "select `$value` from `$tb` where id = $where");
        $row = mysqli_fetch_array($result);
        $checked = $row[$value];
      }
      if ($checked == null) {
        $selected = null;
      } 
      $tb = 'cat'.$prefix.ucfirst($entita).'_1';
      $result = mysqli_query($this->db_connect, "select * from `$tb` order by ord");         
      while ($row = mysqli_fetch_array($result)) {
        if ($checked != null) {
          if ($row['id'] == $checked) {
            $selected = ' selected="selected"';
          } else {
            $selected = null;
          }             
        }
        echo '<option value="'.$row['id'].'"'.$selected.'>'.$row[$entita].'</option>';          
      }
    echo '</select>';  
  
  }
  
  public function editCheckbox($entita, $prefix, $tb, $where, $class) // prefix pro odliseni tabulek s kategoriemi
  {
  
    if (isset($_POST[$entita])) {
      $checked = $_POST[$entita];
    } else {
      $tb = substr($tb, 0, 5).ucfirst($entita);            
      $result = mysqli_query($this->db_connect, "select `$entita` from `$tb` where idAd = $where");
      while ($row = mysqli_fetch_array($result)) {
        $checked[] = $row[$entita];
      }          
    }
    
    $tb = 'cat'.$prefix.ucfirst($entita).'_1';
    $result = mysqli_query($this->db_connect, "select id, `$entita` from `$tb` order by ord");
 
    echo '<table>';
      while ($row = mysqli_fetch_array($result)) {                 
        if ($checked != null) {
          $selected = null;
          foreach ($checked as $ch) {
            if ($ch == $row['id']) {
              $selected = ' checked="checked"';
            } 
          }  
          echo '<tr><td class="'.$class.'">'.$row[$entita].'</td><td><input type="checkbox" name="'.$entita.'[]" value="'.$row['id'].'"'.$selected.' /></td></tr>';
        } else { 
          echo '<tr><td class="'.$class.'">'.$row[$entita].'</td><td><input type="checkbox" name="'.$entita.'[]" value="'.$row['id'].'" /></td></tr>';
        }
      }
    echo '</table>';  
  }
 
}
?>