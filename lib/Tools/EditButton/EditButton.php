<?php
class EditButton
{
  
  /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**
	* @param int $uid   
	*/  
  
  public function __construct ($uid)
  {
    $this->uid = $uid;
  }
  
  
  public function editButtonFirst($entita, $id)
  {
    $entita = ucfirst($entita);
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="edit'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formLupa" type="submit" value="" />';
    echo '</form>';    
  }
  
  public function editButtonSecond($entita, $entitaParent, $idParent, $id)
  {
    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="edit'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formLupa" type="submit" value="" />';
    echo '</form>';    
  }
  
  public function editButtonSecond2($entita, $entitaParent, $idParent, $entitaIn, $id)
  {
    if ($entita != null) $entita = ucfirst($entita); else $entita = null;
    if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
    if ($entitaIn != null) $entitaIn = ucfirst($entitaIn); else $entitaIn = null;
    
    echo '<form method="post" action="">';
      echo '<input type="hidden" name="show'.$entita.'" value="1" />'; 
      echo '<input type="hidden" name="edit'.$entitaIn.'" value="1" />'; 
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id" value="'.$id.'" />';
      echo '<input class="formAcc" type="submit" value="" />';
    echo '</form>';    
  }
  
}