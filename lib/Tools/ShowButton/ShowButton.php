<?php
class ShowButton
{
  
  /**
	* User uid.
	*
	@var int
	*/
	public $uid = 0;
		
		/**
	* @param int $id     
	*/  
  
  public function __construct ($uid)
  {
    $this->uid = $uid;
  }
    
  public function showButtonFirst($entita, $entitaParent, $idParent)
  {
    echo '<form method="post" action="">';
      
      switch ($entita) {
        case 'account': $style = 'class="formAcc"'; // pero
        break;
        case 'contact': $style = 'class="formPerson"'; // kontakt
        break;
        //case 'test': $style = 'class="formTest"'; // test
        //break;
        case 'profil': $style = 'class="formArchiv"'; // text
        break;
        case 'adEmployment': $style = 'class="formI"'; // advertisment Employment
        break;
        case 'adBrigade': $style = 'class="formI"'; // advertisment Employment
        break;
        case 'adUser': $style = 'class="formI"'; // advertisment Employment
        break;
        default: $style = 'class="formI"';      
      }
      
      if ($entita != null) $entita = ucfirst($entita); else $entita = null;
      if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
      
      echo '<input type="hidden" name="show'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input '.$style.' type="submit" value="" />';
    echo '</form>';    
  }
  
  public function showButtonSecond($entita, $entitaParent, $idParent, $entitaParentP, $idParentP)
  {
    echo '<form method="post" action="">';
      
      switch ($entita) {
        case 'account': $style = 'class="formAcc"'; // pero
        break;
        case 'contact': $style = 'class="formPerson"'; // kontakt
        break;
        case 'test': $style = 'class="formTest"'; // test
        break;
        case 'archiv': $style = 'class="formArchiv"'; // kontakt
        break;
        default: $style = 'class="formI"';      
      }
      
      if ($entita != null) $entita = ucfirst($entita); else $entita = null;
      if ($entitaParent != null) $entitaParent = ucfirst($entitaParent); else $entitaParent = null;
      if ($entitaParentP != null) $entitaParentP = ucfirst($entitaParentP); else $entitaParentP = null;
      
      echo '<input type="hidden" name="show'.$entita.'" value="1" />';
      echo '<input type="hidden" name="uid" value="'.$this->uid.'" />';
      echo '<input type="hidden" name="show'.$entitaParent.'" value="1" />';
      echo '<input type="hidden" name="id'.$entitaParent.'" value="'.$idParent.'" />';
      echo '<input type="hidden" name="id'.$entitaParentP.'" value="'.$idParentP.'" />';
      echo '<input '.$style.' type="submit" value="" />';
    echo '</form>';    
  }
  
}
?>