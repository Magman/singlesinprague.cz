<?php

class MenuTopVerRenderer
{

	private $menuTop;
	
	public function __construct(MenuTopVer $menuTop)
	{
		$this->menuTop = $menuTop;
	}
	
	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
   //echo '<table>';
    echo '<ul>'; 
      if ($this->menuTop->itemFirstArr() != null) {
        
        foreach ($this->menuTop->itemFirstArr() as $iMf => $iFA) {                        
                                  
          if ($this->menuTop->lang == 1) {
            $result = mysqli_query($this->menuTop->db_connect, "select id, item from {$this->menuTop->tbMenu()} where idParent = $iMf order by ord");
          } else {
            $result = mysqli_query($this->menuTop->db_connect, "select {$this->menuTop->tbMenu()}.id, {$this->menuTop->tbMenu()}.item from {$this->menuTop->tbMenu()}, menu_top_1 where menu_top_1.id = {$this->menuTop->tbMenu()}.id and menu_top_1.idParent = $iMf order by menu_top_1.ord");
          }                                        
                                        
          $itemVoArr = array();                
          
          while ($row = mysqli_fetch_array($result)) {

            $item = $row['item']; 

            $idmvo = $row['id'];
            $itemVoArr[$idmvo] = $item; 
          
          }
                    
          if ($itemVoArr != null) {
            $endLi = '';
            $endUl = '</li>';
          } else {
            $endLi = '</li>';
            $endUl = '';
          }
                          
          $c = $this->diacritica($iFA);
                                          
          //echo '<td>';
            //echo '<ul>';
              
              if ($this->menuTop->id != 0) { 
                if ($this->menuTop->id == $iMf) {
                  $style = ' style="color:#9b2724;"';
                } else {                            
                  $style = '';
                }
              } else { // kdyz neprijde id (homepage)
                $itemFirstArr = array_keys($this->menuTop->itemFirstArr());
                if ($itemFirstArr[0] == $iMf) {
                  $style = ' style="color:#9b2724; "';
                } else {
                  $style = '';
                }
              }
              
              $result = mysqli_query($this->menuTop->db_connect, "select active from menu_top_1 where id = $iMf");
              $row = mysqli_fetch_assoc($result);
              $active = $row['active'];
              /*
              if ($iMf == 101) {
                $styleCookie = ' class="itemCookie101"';
              } else {
                $styleCookie = null;
              }
              */
              
              //$styleCookie = null;
              /*
              if ($active != 0) {
                if ($iMf == $this->menuTop->idHome()) { // Home
                  echo '<li><a href="'.$this->menuTop->path.'index.php?id='.$iMf.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; //  '.$style.'
                } else {  
                  echo '<li'.$styleCookie.'><a href="'.$this->menuTop->path.'index.php?id='.$iMf.'" title=""'.$style.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; //  '.$style.'
                }
              }              
              */
              
              if ($active != 0) {
                echo '<li><a href="'.$this->menuTop->path.'index.php?id=1&amp;it='.$iMf.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; //  '.$style.'
              }               
              
              if ($itemVoArr != null) {
                $numItem = count($itemVoArr);
                echo '<ul class="item'.$iMf.'">';                
                  foreach ($itemVoArr as $iMv => $iVoA) {                                    
                                 
                    $c = $this->diacritica($iVoA);                           
                
                    if ($this->menuTop->id == $iMv) {
                      $styleVo = ' style="color:#9b2724;"';
                    } else {
                      $styleVo = '';
                    }                           
              
                    $result = mysqli_query($this->menuTop->db_connect, "select active from menu_top_1 where id = $iMv");
                    $row = mysqli_fetch_array($result);
                    $active = $row['active'];
                    if ($active != 0) {                   
                      echo '<li><a href="'.$this->menuTop->path.$c.'.html?id='.$iMv.'" title="'.$iVoA.'"'.$styleVo.'>'.$iVoA.'</a></li>'; //  '.$styleVo.'                                               
                    }
                  
                  }
                
                echo '</ul>';
              
              }
              
              echo $endUl;
            //echo '</ul>';
          //echo '</td>';
          $itemVoArr = array();        
        }
      }
    
    echo '</ul>';  
	 //echo '</table>';
	
	}
	
	public function diacritica ($item)
  {
    $c = strtolower(strtr($item, array('Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
    'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
    'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
    'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
    'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z','/'=>'a','"'=>'-',' '=>'-',
    '_'=>'-','!'=>'-','?'=>'-', '&'=>'-', "'"=>'-', "`"=>'-','('=>'-',')'=>'-','['=>'-',']'=>'-',
    '{'=>'-','}'=>'-','\\'=>'-','*'=>'-','<'=>'-','>'=>'-',','=>'-','.'=>'','%'=>'-', '´'=>'-')));
                      
    return $c; 
  }  

}