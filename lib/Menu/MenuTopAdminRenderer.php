<?php
// menu položek sekce OBSAH
class MenuTopAdminRenderer
{

	private $menuTopAdmin;
	
	public function __construct(MenuTop $menuTopAdmin)
	{
		$this->menuTopAdmin = $menuTopAdmin;
	}
	
	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    echo '<ul>';
      if ($this->menuTopAdmin->itemFirstArrCont() != null) {
        
        foreach ($this->menuTopAdmin->itemFirstArrCont() as $iMf => $iFA) {                        
          $result = mysqli_query($this->menuTopAdmin->db_connect, "select id, item from menu_top_1 where idParent = $iMf order by ord");
                                     
          $itemVoArr = array();                
          while ($row = mysqli_fetch_array($result)) {
            $idmvo = $row['id'];
            $itemVoArr[$idmvo] = $row['item']; 
          }
                        
          if ($itemVoArr != null) {
            $endLi = '';
            $endUl = '</li>';
          } else {
            $endLi = '</li>';
            $endUl = '';
          }
                          
          $c = $this->diacritica($iFA);
                                                                        
          if ($this->menuTopAdmin->id != 0) { 
            if ($this->menuTopAdmin->id == $iMf) {
              $style = 'id="actualAdmin"';
            } else {                            
              $style = '';
            }
          } else {
            $itemFirstArr = array_keys($this->menuTopAdmin->itemFirstArr());
            if ($itemFirstArr[0] == $iMf) {
              $style = 'id="actualAdmin"';
            } else {
              $style = '';
            }
          }
               
          echo '<li><a href="admin_text.php?id='.$iMf.'" title="'.$iFA.'" '.$style.'>'.$iFA.'</a>'.$endLi.'';

              
          if ($itemVoArr != null) {
            echo '<ul>';
              foreach ($itemVoArr as $iMv => $iVoA) {                                    
                                 
                $c = $this->diacritica($iVoA);                           
                                 
                if ($this->menuTopAdmin->id == $iMv) {
                  $styleVo = 'id="actualAdmin"';
                } else {
                  $styleVo = '';
                }                           
                
                $result = mysqli_query($this->menuTopAdmin->db_connect, "select active from menu_top_1 where id = $iMv");
                $row = mysqli_fetch_array($result);
                $active = $row['active'];
                if ($active != 0) {
                  echo '<li><a href="admin_text.php?id='.$iMv.'" title="'.$iVoA.'" '.$styleVo.'>'.$iVoA.'</a></li>';                            
                }
              }
            echo '</ul>';
          }
          echo $endUl;
          $itemVoArr = array();
        }
      }
    echo '</ul>';
	
	}
	
	public function diacritica ($item)
  {
    $c = strtolower(strtr($item, array('Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
    'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
    'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
    'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
    'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z','/'=>'a','"'=>'-',' '=>'-',
    '_'=>'-','!'=>'-','?'=>'-', '&'=>'-', "'"=>'-', "`"=>'-','('=>'-',')'=>'-','['=>'-',']'=>'-',
    '{'=>'-','}'=>'-','\\'=>'-','*'=>'-','<'=>'-','>'=>'-',','=>'-','.'=>'','%'=>'-', '´'=>'-')));
                      
    return $c; 
  }  

}