<?php

class MenuTopRenderer
{

	private $menuTop;
	
	public function __construct(MenuTop $menuTop)
	{
		$this->menuTop = $menuTop;
	}
	
	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    echo '<table>'; 
      echo '<tr>';
        if ($this->menuTop->itemFirstArr() != null) {
          foreach ($this->menuTop->itemFirstArr() as $iMf => $iFA) {                        

            echo '<td>';
              echo '<ul>';

          
                if ($this->menuTop->lang == 1) {
                  $result = mysqli_query($this->menuTop->db_connect, "select id, item from {$this->menuTop->tbMenu()} where idParent = $iMf order by ord");
                } else {
                  $result = mysqli_query($this->menuTop->db_connect, "select {$this->menuTop->tbMenu()}.id, {$this->menuTop->tbMenu()}.item from {$this->menuTop->tbMenu()}, menu_top_1 where menu_top_1.id = {$this->menuTop->tbMenu()}.id and menu_top_1.idParent = $iMf order by menu_top_1.ord");
                }                                        
                    
                $itemVoArr = array();                
                    
                while ($row = mysqli_fetch_array($result)) {
                  $idmvo = $row['id'];
                  $itemVoArr[$idmvo] = $row['item']; 
                }
                                  
                if ($itemVoArr != null) {
                  $endLi = '';
                  $endUl = '</li>';
                } else {
                  $endLi = '</li>';
                  $endUl = '';
                }
                                    
                $c = $this->diacritica($iFA);
                                                             
                if ($this->menuTop->id != 0) { 
                  if ($this->menuTop->id == $iMf) {
                    $style = ' id="actual"';
                  } else {                            
                    $style = '';
                  }
                } else {
                  $itemFirstArr = array_keys($this->menuTop->itemFirstArr());
                  if ($itemFirstArr[0] == $iMf) {
                    $style = ' id="actual"';
                  } else {
                    $style = '';
                  }
                }
          
                $result = mysqli_query($this->menuTop->db_connect, "select active from menu_top_1 where id = $iMf");
                $row = mysqli_fetch_assoc($result);
                $active = $row['active'];
                        
                if ($active != 0) {
                  echo '<li><a href="'.$this->menuTop->path.'index.php?id=1&amp;it='.$iMf.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; // '.$style.' 
                }
              
                if ($itemVoArr != null) {
                  echo '<ul>';
                    foreach ($itemVoArr as $iMv => $iVoA) {                                    
                                           
                      $c = $this->diacritica($iVoA);                           
                         
                      if ($this->menuTop->id == $iMv) {
                        $styleVo = ' id="actual"';
                        $styleVoCss = ' id="actualCss"';
                      } else {
                        $styleVo = '';
                        $styleVoCss = '';
                      }                           

                      $result = mysqli_query($this->menuTop->db_connect, "select active from menu_top_1 where id = $iMv");
                      $row = mysqli_fetch_array($result);
                      $active = $row['active'];
                      if ($active != 0) {                  
                        echo '<li'.$styleVo.'><a href="'.$this->menuTop->path.$c.'.html?id='.$iMv.'" title=""'.$styleVoCss.'>'.$iVoA.'</a></li>';  // <li '.$styleVo.'>  '.$styleVoCss.'                                        
                      }
                    }
                  echo '</ul>';
                }
                echo $endUl;          
              echo '</ul>';
            echo '</td>';          
                    
            $itemVoArr = array();
          }
        }       
      echo '</tr>';
    echo '</table>';          	
	}
	
	public function diacritica ($item)
  {
    $c = strtolower(strtr($item, array('Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
    'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
    'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
    'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
    'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z','/'=>'a','"'=>'-',' '=>'-',
    '_'=>'-','!'=>'-','?'=>'-', '&'=>'-', "'"=>'-', "`"=>'-','('=>'-',')'=>'-','['=>'-',']'=>'-',
    '{'=>'-','}'=>'-','\\'=>'-','*'=>'-','<'=>'-','>'=>'-',','=>'-','.'=>'','%'=>'-', '´'=>'-')));
                      
    return $c; 
  }  

}