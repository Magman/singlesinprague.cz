<?php

require_once (dirname(__FILE__) . '/MenuTopRenderer.php');
require_once (dirname(__FILE__) . '/MenuTopAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako MenuTop) 

class MenuTop
{
    	/**
	 * Pripojeni k DB.
	 *
	 * @var null
	 */
	public $db_connect = null;
	
	   /**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
	
		/**
	 * id Menu.
	 *
	 * @var int
	 */
	public $id = 0;
	
		/**
	 * xmlLang
	 *
	 * @var string
	 */
	public $xmlLang = null;
	
		/**
	 * parHotel
	 *
	 * @var int
	 */
	public $parHotel = 0;
	
			/**
	 * $path ../ nebo ./ kvůli mutaci
	 *
	 * @var string
	 */
	public $path = null;
	
  public function __construct($db_connect, $lang, $id, $xmlLang, $parHotel, $path) // $xmlLang, $parHotel kvůli booking, $path ../ or ./ menu mutace
	{
		$this->db_connect = $db_connect;
		$this->lang = (int) $lang;
		$this->id = (int) $id;
		$this->xmlLang = (string) $xmlLang;
		$this->parHotel = (int) $parHotel;
		$this->path = (string) $path;
  }

  public function tbMenu ()
  {
    $tbMenu = 'menu_top_'.$this->lang;
    return $tbMenu;
  }
  
  public function idHome ()
  {
    $result = mysqli_query($this->db_connect, "select id from menu_top_1 where idParent = 0 and active != 0 order by ord limit 0,1");
    $row = mysqli_fetch_array($result);
    $idHome = $row['id'];
    return $idHome;
  }
  
  public function itemFirstArr ()
  {
    if ($this->lang == 1) {
      $result = mysqli_query($this->db_connect, "select id, item from {$this->tbMenu()} where idParent = 0 and active != 0 order by ord");
    } else {
      $result = mysqli_query($this->db_connect, "select {$this->tbMenu()}.id, {$this->tbMenu()}.item from {$this->tbMenu()}, menu_top_1 where menu_top_1.id = {$this->tbMenu()}.id and menu_top_1.idParent = 0 and menu_top_1.active != 0 order by menu_top_1.ord");
    }  
    while ($row = mysqli_fetch_array($result)) {
      $idmi = $row['id'];
      $itemFirstArr[$idmi] = $row['item']; 
    }
    return $itemFirstArr;
  }
  
  public function itemFirstArrCont ()
  {
    $result = mysqli_query($this->db_connect, "select id, item from menu_top_1 where idParent = 0 and active != 0 order by ord");

    while ($row = mysqli_fetch_array($result)) {
      $idmi = $row['id'];
      $itemFirstArr[$idmi] = $row['item']; 
    }
    return $itemFirstArr;
  }
  
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new MenuTopRenderer($this);
		}

		return $this->renderer->render();
	}
	
	public function renderAdmin()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new MenuTopAdminRenderer($this);
		}

		return $this->renderer->render();
	}
	
}
?>