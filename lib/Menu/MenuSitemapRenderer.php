<?php

class MenuSitemapRenderer
{

	private $MenuSitemap;
	
	public function __construct(MenuSitemap $MenuSitemap)
	{
		$this->MenuSitemap = $MenuSitemap;
	}
	
	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    echo '<ul>'; 
      if ($this->MenuSitemap->itemFirstArr() != null) {
        foreach ($this->MenuSitemap->itemFirstArr() as $iMf => $iFA) {                        
          
          if ($this->MenuSitemap->lang == 1) {
            $result = mysqli_query($this->MenuSitemap->db_connect, "select id, item from {$this->MenuSitemap->tbMenu()} where idParent = $iMf order by ord");
          } else {
            $result = mysqli_query($this->MenuSitemap->db_connect, "select {$this->MenuSitemap->tbMenu()}.id, {$this->MenuSitemap->tbMenu()}.item from {$this->MenuSitemap->tbMenu()}, menu_top_1 where menu_top_1.id = {$this->MenuSitemap->tbMenu()}.id and menu_top_1.idParent = $iMf order by menu_top_1.ord");
          }                                        
          
          $itemVoArr = array();                
          
          while ($row = mysqli_fetch_array($result)) {
            $idmvo = $row['id'];
            $itemVoArr[$idmvo] = $row['item']; 
          }
                        
          if ($itemVoArr != null) {
            $endLi = '';
            $endUl = '</li>';
          } else {
            $endLi = '</li>';
            $endUl = '';
          }
                          
          $c = $this->diacritica($iFA);
                                                             
          if ($this->MenuSitemap->id != 0) { 
            if ($this->MenuSitemap->id == $iMf) {
              $style = ' id="actual"';
            } else {                            
              $style = '';
            }
          } else {
            $itemFirstArr = array_keys($this->MenuSitemap->itemFirstArr());
            if ($itemFirstArr[0] == $iMf) {
              $style = ' id="actual"';
            } else {
              $style = '';
            }
          }

          $result = mysqli_query($this->MenuSitemap->db_connect, "select active from menu_top_1 where id = $iMf");
          $row = mysqli_fetch_assoc($result);
          $active = $row['active'];
              
          if ($active != 0) {
            if ($iMf == $this->MenuSitemap->idHome()) { // Home
              echo '<li><a href="'.$this->MenuSitemap->path.'index.php?id='.$iMf.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; // '.$style.'
            } else if ($iMf == 4) {  
              echo '<li><a href="'.$this->MenuSitemap->path.'booking.php?action=reviews&amp;id='.$iMf.'&amp;user_language='.$this->MenuSitemap->xmlLang.'&amp;hotel_id='.$this->MenuSitemap->parHotel.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.'';  // '.$style.'
            } else if ($iMf == 9) {  
              echo '<li><a href="'.$this->MenuSitemap->path.'virtual/virtual.php?id='.$iMf.'&amp;user_language='.$this->MenuSitemap->xmlLang.'&amp;hotel_id='.$this->MenuSitemap->parHotel.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; // '.$style.' 
            } else {  
              echo '<li><a href="'.$this->MenuSitemap->path.$c.'.html?id='.$iMf.'" title=""'.$style.'>'.$iFA.'</a>'.$endLi.''; // '.$style.' 
            }
          }
              
          if ($itemVoArr != null) {
            echo '<ul>';
              foreach ($itemVoArr as $iMv => $iVoA) {                                    
                                 
                $c = $this->diacritica($iVoA);                           
               
                if ($this->MenuSitemap->id == $iMv) {
                  $styleVo = ' id="actual"';
                  $styleVoCss = ' id="actualCss"';
                } else {
                  $styleVo = '';
                  $styleVoCss = '';
                }                           

                $result = mysqli_query($this->MenuSitemap->db_connect, "select active from menu_top_1 where id = $iMv");
                $row = mysqli_fetch_array($result);
                $active = $row['active'];
                if ($active != 0) {
                  if ($iMv == 9) {
                    echo '<li><a href="'.$this->MenuSitemap->path.'./virtual/virtual.php?id='.$iMv.'" title="" '.$styleVoCss.'>'.$iVoA.'</a></li>';
                  } else {                   
                    echo '<li'.$styleVo.'><a href="'.$this->MenuSitemap->path.$c.'.html?id='.$iMv.'" title=""'.$styleVoCss.'>'.$iVoA.'</a></li>';  // <li '.$styleVo.'>  '.$styleVoCss.'                         
                  }
                }
              }
              echo '</ul>';
            }
          echo $endUl;
          $itemVoArr = array();
        }
       }    
    echo '</ul>';  	
	}
	
	public function diacritica ($item)
  {
    $c = strtolower(strtr($item, array('Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
    'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
    'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
    'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
    'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z','/'=>'a','"'=>'-',' '=>'-',
    '_'=>'-','!'=>'-','?'=>'-', '&'=>'-', "'"=>'-', "`"=>'-','('=>'-',')'=>'-','['=>'-',']'=>'-',
    '{'=>'-','}'=>'-','\\'=>'-','*'=>'-','<'=>'-','>'=>'-',','=>'-','.'=>'','%'=>'-', '´'=>'-')));
                      
    return $c; 
  }  

}