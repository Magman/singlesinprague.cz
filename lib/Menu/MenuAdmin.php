<?php

require_once (dirname(__FILE__) . '/MenuAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako MenuAdmin) 

class MenuAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	public function __construct($db_connect) {
    $this->db_connect = $db_connect;
  }


  public function lang()
  {
    $result = mysqli_query($this->db_connect, "select lang from language where id = 1 limit 0,1");
    $row = mysqli_fetch_array($result);
    $codeLang = $row['lang'];
    return $codeLang;  
  }
  
  public function orderArr()
  {
    $result = mysqli_query($this->db_connect, "select id, item, ord from menu_top_1 where idParent = 0 order by ord");
    while ($row = mysqli_fetch_array($result)) {
      $orderArr[$row['id']][$row['item']] = $row['ord'];
    }
    return $orderArr;
  }
  
  public function itemArr()
  {
    $result = mysqli_query($this->db_connect, "select id, item from menu_top_1 where idParent = 0 order by `ord`");
    while ($row = mysqli_fetch_array($result)) {
      $itemArr[$row['id']] = $row['item'];  
    }
    return $itemArr;
  } 
  
  public function activity($id, $checked)
  {
    $result = mysqli_query($this->db_connect, "update menu_top_1 set active = $checked where id = $id");        
  }
  
  public function insertItem($item, $idParent, $language, $ftpServer, $ftpLogin, $ftpPass, $root)
  {
    $item = trim($item);

    if ($item == null) {
      echo '<div class="warrCover"><div class="warr">Nezadali jste název položky!</div></div>';
    } else {                                                                                                                                                                                       
      //Main Menu               
      $order = 1;
      $result = mysqli_query($this->db_connect, "select ord from menu_top_1 where idParent = $idParent order by ord desc limit 0,1");
      $row = mysqli_fetch_array($result);
      $order = $row['ord'] + 1;

      $item = strtr($item, array('"'=>'',"'"=>""));
                              
      // zapis do databaze pro hlavni jazyk administrace                                                          
      $sql = "insert into menu_top_1 (item, idParent, ord) values ('"
      .mysqli_real_escape_string($this->db_connect, $item)."', $idParent, $order)";
                                                
      $result = mysqli_query($this->db_connect, $sql);
                                                                                                     
      if (!$result) {
        echo '<div class="warrCover"><div class="warr">Chyba. Položku menu <strong>'.$item.'</strong> se nepodařilo zapsat do databáze pro hlavní jazyk.</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Položka menu <strong>'.$item.'</strong> byla úspěšně zapsána do databáze pro hlavní jazyk.</div></div>';
      }              
                            
      $result = mysqli_query($this->db_connect, "select id from menu_top_1 order by id desc limit 0,1");
      $row = mysqli_fetch_array($result);
      $id = $row['id'];

      $sql = "insert into text_1 (idMenu, `text`) values ('"
      .mysqli_real_escape_string($this->db_connect, $id)."','"
      .mysqli_real_escape_string($this->db_connect, $item)."')";
                            
      $result = mysqli_query($this->db_connect, $sql);
                            
      if (!$result) {
        echo '<div class="warrCover"><div class="warr">Chyba! Prázdný text pro položku <strong>'.$item.'</strong> se nepodařilo vložit do databáze pro hlavní jazyk.</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Prázdný text pro položku <strong>'.$item.'</strong> byl vložen do databáze pro hlavní jazyk.</div></div>';
      }  
                              
      $result = mysqli_query($this->db_connect, "select id from text_1 where idMenu = $id order by id desc limit 0,1");
      $row = mysqli_fetch_array($result);
      $idText = $row['id'];
                            
      $lng = array_shift($language); 
      
      // menu ostatní jazyky                
      foreach ($language as $lg) {
        $tbMenu = 'menu_top_'.$lg; 
                                                                              
        $sql = "insert into `$tbMenu` (id, item) values ('"
        .mysqli_real_escape_string($this->db_connect, $id)."','"
        .mysqli_real_escape_string($this->db_connect, $item)."')";
                              
        $insertMenu = mysqli_query($this->db_connect, $sql);
                                                                                
        // text ostatní jazyky
        $tbText = 'text_'.$lg;
        $sql = "insert into `$tbText` (id, `text`) values ($idText, '"
        .mysqli_real_escape_string($this->db_connect, $item)."')";
                            
        $insertText = mysqli_query($this->db_connect, $sql);
                      
      } 
      if (!$insertMenu) {
        echo '<div class="warrCover"><div class="warr">Chyba! Položku menu <strong>'.$item.'</strong> se nepodařilo zapsat do databáze pro vedlejší jazyky.</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Položka menu <strong>'.$item.'</strong> byla úspěšně zapsána do databáze pro vedlejší jazyky.</div></div>';
      }
      if (!$insertText) {
        echo '<div class="warrCover"><div class="warr">Chyba! Prázdný text pro položku <strong>'.$item.'</strong> se nepodařilo vložit do databáze pro vedlejší jazyky.</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Prázdný text pro položku <strong>'.$item.'</strong> byl vložen do databáze pro vedlejší jazyky.</div></div>';
      }                                                                      
            
      //$this->createSliderFolder ($id, $ftpServer, $ftpLogin, $ftpPass, $root);    

    } 
  }
  
  public function createSliderFolder ($id, $ftpServer, $ftpLogin, $ftpPass, $root) // id menu
  {
    //Vložení folderu pro slider
    $ftp = ftp_connect($ftpServer);
    $loginResult = ftp_login($ftp, $ftpLogin, $ftpPass);
    $path = $root.'/slider/'.$id;
    $dir = ftp_mkdir($ftp, $path);
    $access = ftp_site($ftp, "CHMOD 0777 $path");
    if (!$dir) echo 'Chyba! Folder pro slider se nepodařilo vytvořit.<br />';  
  }
  
  public function changeOrder($order)
	{                
    if ($order != null) {                           
      //Kontrola zda jsou prvky poradi unikátní
      if (count($order) != count(array_unique($order))) {
        echo '<div class="warrCover"><div class="warr">Vložená čísla pořadí musí být unikátní!</div></div>';
      } else {
        // ZMENY
        $result = aSort($order);
        $i = 1;                
        foreach ($order as $id => $or) {                     
          $sql = "update menu_top_1 set ord = $i where id = $id";                        
          $result = mysqli_query($this->db_connect, $sql);
          $i++;      
        }
        
        if (!$result) {
          echo '<div class="warrCover"><div class="warr">Chyba! Změna pořadí se nezdařila!</div></div>';
        } else {          
          echo '<div class="warrCover"><div class="warr">Změna pořadí proběhla úspěšně.</div></div>';
        }                                                                                  
      }
    } 	
	}
	
	public function deleteQuery($del, $id)
	{
    $result = mysqli_query($this->db_connect, "select item from menu_top_1 where id = $id");
    $row = mysqli_fetch_array($result);
    if ($del != 0) {
      echo '<div class="warrCover"><div class="warr">';
      echo 'Opravdu chcete položku <strong>'.$row['item'].'</strong> smazat?'; 
      echo '<table><tr>';
      echo '<td>';
        echo '<form method="post" action="">';
          echo '<input type="hidden" name="dok" value="1" />';
          echo '<input type="hidden" name="del" value="0" />';
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="smazat" />';
        echo '</form>';
      echo '</td>';
      echo '<td>';
        echo '<form method="get" action="">';
          echo '<input type="hidden" name="del" value="0" />';
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="ne" />';
        echo '</form>';
      echo '</td>';
      echo '</tr></table>';
      echo '</div></div>';
    }
	}
	
	public function deleteOk($id)
	{
    $result = mysqli_query($this->db_connect, "select item from menu_top_1 where id = $id");
    $row = mysqli_fetch_array($result);
    $item = $row['item']; 
                  
    $result = mysqli_query($this->db_connect, "select id from menu_top_1 where idParent = $id");                  
                                                                        
    if (mysqli_num_rows($result) == 0) {
                                     
      $delete = mysqli_query($this->db_connect, "delete from menu_top_1 where id = $id"); // smazani polozky menu
      if (!$delete) {
        echo '<div class="warrCover"><div class="warr">Smazání položky menu <strong>'.$item.'</strong> z databáze se nezdařilo!</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Smazání položky menu <strong>'.$item.'</strong> bylo úspěšné.</div></div>';
      } 
      // menu v mutacich a texty pomoci cizich klicu                                            
    
      //$this->deleteSliderFolder ($id); //Smazání podřízeného SLIDERU
    
    } else {
      echo '<div class="warrCover"><div class="warr">Položka menu (složka) <strong>'.$item.'</strong> obsahuje podřízené položky. 
      Jestliže tuto položku chcete smazat, musíte nejprve smazat podřízené položky!</div></div>';
    }  
  }
  
  private function deleteSliderFolder ($id)
  {
    $result = mysqli_query($this->db_connect, "select id, image from slider where idAlbum = $id");      
    if (mysqli_num_rows($result) > 0) {
      while ($row = mysqli_fetch_array($result)) {
        $idImage = $row['id'];    
        $filename = '../slider/'.$id.'/'.$row['image'];
        if (!unlink($filename)) echo '<span class="warAdmin">Chyba! Smazání fotografie ze slideru se nezdařilo.</span><br />';
        $delSlider = mysqli_query($this->db_connect, "delete from slider where id = $idImage");
        if (!$delSlider) echo '<span class="warAdmin">Chyba! Smazání fotografie ze slideru se nezdařilo.</span><br />';
      }
    }
    $folname = '../slider/'.$id;
    if (!rmdir($folname)) echo '<span class="warAdmin">Chyba! Smazání folderu slideru se nezdařilo.</span><br />';  
  }
  
  public function insedit($id, $item, $lg)
	{
  	$item = trim($item);  
                  
    if (empty($item)) {
      echo '<div class="warrCover"><div class="warr">Vyplňte název položky! Položka musí obsahovat alespoň 1 znak.</div></div>';
    } else { 
                
      $tbMenu = 'menu_top_'.$lg;  

      $sql = "update `$tbMenu` set item = '".mysqli_real_escape_string($this->db_connect, $item)."' where id = $id";                                    
                                  
      $result = mysqli_query($this->db_connect, $sql);
      if (!$result) {
        echo '<div class="warrCover"><div class="warr">Chyba! Uložení změny se nezdařilo.</div></div>';
      } else {
        echo '<div class="warrCover"><div class="warr">Přejmenování položky <strong>'.$item.'</strong> bylo úspěšné pro jazyk <strong>'.$lg.'</strong>.</div></div>';
      }
    }
	}
	
	public function edit($id, $languageActive)
	{    
    foreach ($languageActive as $lg) {
    
      $result = mysqli_query($this->db_connect, "select lang from language where id = $lg");
      $row = mysqli_fetch_array($result);
      $lang = $row['lang'];                                              
      
      $tbMenu = 'menu_top_'.$lg;
                    
      $result = mysqli_query($this->db_connect, "select item from `$tbMenu` where id = $id limit 0,1");  // idParent
      $row = mysqli_fetch_array($result);                                                         
      echo '<form method ="post" action="">';          
        echo '<input type="hidden" name="insedit" value="1">';
        echo '<input type="hidden" name="edit" value="1">';
        echo '<input type="hidden" name="del" value="0" />';
        echo '<input type="hidden" name="id" value="'.$id.'">';
        echo '<input type="hidden" name="lg" value="'.$lg.'">';                    
        echo '<table class="editItem">';       
          echo '<tr><td><img src="../lib/design/'.$lang.'.png" alt="'.$lang.'" /></td> 
          <td><input type="text" name="change" size="60" value="'.$row['item'].'"></td>
          <td><input type="submit" value="OK"></td></tr>';
         echo '</table>';     
      echo '</form>';                                                          
    } 
  }	

	
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new MenuAdminRenderer($this);
		}

		return $this->renderer->render();
	}


}

?>