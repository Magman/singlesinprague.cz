<?php

require_once '../lib/Help/HelpAdmin.php';

class MenuAdminRenderer {

  private $menuAdmin;

	public function __construct(MenuAdmin $menuAdmin)
	{
		$this->menuAdmin = $menuAdmin;
	}

	public function render()
	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{
    $help = new HelpAdmin ('cs'); // nápověda
    
    echo '<div class="menuLang"><img src="../lib/design/'.$this->menuAdmin->lang().'.png" alt="'.$this->menuAdmin->lang().'" /></div>';
    echo '<h2>Navigační menu</h2>';
      
    $help->showHelp ('menu', 1);
      
    echo '<div class="menuList">'; 
      // Formulář pro editaci pořadí
      echo '<form method="post" action="">';
        if ($this->menuAdmin->orderArr() == NULL) {
          echo "Žádná položka<br /><br />";
        } else {
          foreach ($this->menuAdmin->orderArr() as $idm => $arrm) {
            foreach ($arrm as $it => $or) { 
              echo '<div class="menuRow">';
                echo '<strong>'.$it.'</strong>';                                         
                echo '<table class="editMenuIt">';
                  echo '<tr>';
                  echo '<td class="order">';
                  echo 'pořadí <input type="text" name="order['.$idm.']" value="'.$or.'" size="1">';                                                            
                  echo '</td>';
                  echo '<td class="pencil">';
                  if ($idm > 99 or $idm == 6 or $idm == 1 or $idm == 3 or $idm == 5 or $idm == 7 or $idm == 8) echo '<a href = "admin_menu.php?id='.$idm.'&edit=1"><img src="../lib/design/tuzka.gif" /></a>';
                  echo '</td>';
                  echo '<td class="del">';                    
                    if ($idm > 99) echo '<a href = "admin_menu.php?id='.$idm.'&del=1"><img src="../lib/design/delete.gif" /></a>';
                  echo '</td>';                    
                  echo '<td class="checked">';
                    $result = mysqli_query($this->menuAdmin->db_connect, "select active from menu_top_1 where id = $idm");
                    $row = mysqli_fetch_array($result);
                    if ($row['active'] > 0) {
                      echo '<strong>aktivní</strong>:&nbsp;&nbsp;<a href="admin_menu.php?checked=0&amp;id='.$idm.'" title="aktivní"><img src="../lib/design/checked_yes.gif" alt="yes" /></a>';
                    } else {
                      echo '<strong>aktivní</strong>:&nbsp;&nbsp;<a href="admin_menu.php?checked='.$idm.'&amp;id='.$idm.'" title="neaktivní"><img src="../lib/design/checked_no.gif" alt="no" /></a>';
                    }
                  echo '</td>';                      
                  echo '</tr>';
                echo '</table>';
              echo '</div>';
            }
          }
          $this->menuAdmin->orderArr = array();
        }
        echo '<input type="hidden" name="sequence" value="1" />';
        echo '<input type="hidden" name="edit" value="0" />';
        echo '<input type="hidden" name="del" value="0" />';
        echo '<input type="submit" value="Změnit pořadí">';
      echo '</form>';
      echo '<form method="post" action="">';
        echo '<div><strong>Název</strong> <input class="inputMenu" type="text" name="item">';
        echo '<input type="hidden" name="insert" value="1" />';
        echo '<input type="hidden" name="edit" value="0" />';
        echo '<input type="hidden" name="del" value="0" />';
        echo '<input type="hidden" name="idParent" value="0" />';            
        echo '&nbsp;<input type="submit" value="Přidat položku" /></div>';
      echo '</form>';                                            
    echo '</div> <!-- .menuList -->';
        
    // 2. ÚROVEŇ                          
    /*
    echo '<div class="secLevel">';
      echo '<h2>2. úroveň</h2>';
    echo '</div>'; 
    if ($this->menuAdmin->itemArr() == null) {
      echo "Žádná položka<br /><br />";
    } else {            
      $itemArr = array_unique($this->menuAdmin->itemArr());            
      foreach ($itemArr as $iM => $ita) {                      
        echo '<div class="menuList">';           
        echo "<h3>Podřízené položky pro $ita</h3>";
          echo '<form method="post" action="">';
            
            $orderArr = array();                                    
            $result = mysqli_query($this->menuAdmin->db_connect, "select id, item, ord from menu_top_1 where idParent = $iM order by ord");
            while ($row = mysqli_fetch_array($result)) {
              $orderArr[$row['id']][$row['item']] = $row['ord'];
            }
              
            if ($orderArr != null) {                              
              foreach ($orderArr as $idm => $arrm) {                
                foreach ($arrm as $vo => $or) {        
                  echo '<div class="navigationMenu">'.$ita.' &gt;&gt;</div>'; // $vo
                  echo '<div class="menuRow">';
                    echo '<strong>'.$vo.'</strong>';                      
                    echo '<table class="editMenuIt">';
                      echo '<tr>';  
                        echo '<td class="order">pořadí <input type="text" name="order['.$idm.']" value="'.$or.'" size="1"></td>';
                        echo '<td class="pencil">';
                          if ($idm > 99 or $idm == 5 or $idm == 7 or $idm == 8) echo '<a href = "admin_menu.php?id='.$idm.'&edit=1"><img src="../lib/design/tuzka.gif" /></a>';
                        echo '</td>';
                        echo '<td class="del">';
                          if ($idm > 99) echo '<a href = "admin_menu.php?id='.$idm.'&item='.$vo.'&del=1"><img src="../lib/design/delete.gif" /></a>';
                        echo '</td>';
                        echo '<td class="checked">';
                          $result = mysqli_query($this->menuAdmin->db_connect, "select active from menu_top_1 where id = $idm");
                          $row = mysqli_fetch_array($result);
                          if ($row['active'] > 0) {
                            echo '<strong>aktivní</strong>:&nbsp;&nbsp;<a href="admin_menu.php?checked=0&amp;id='.$idm.'" title="aktivní"><img src="../lib/design/checked_yes.gif" alt="yes" /></a>';
                          } else {
                            echo '<strong>aktivní</strong>:&nbsp;&nbsp;<a href="admin_menu.php?checked='.$idm.'&amp;id='.$idm.'" title="neaktivní"><img src="../lib/design/checked_no.gif" alt="no" /></a>';
                          }
                        echo '</td>';
                      echo '</tr>';
                    echo '</table>';
                  echo '</div>';
                }
              }
              $orderArr = array();
            }
            
            echo '<input type="hidden" name="sequence" value="1" />';
            echo '<input type="hidden" name="edit" value="0" />';
            echo '<input type="hidden" name="del" value="0" />';                          
            echo '<div class="orderRow"><input type="submit" value="Změnit pořadí"></div>';
          echo '</form>';
          echo '<form method="post" action="">';
            echo '<div><strong>Název</strong> <input class="inputMenu" type="text" name="item">';
            echo '<input type="hidden" name="insert" value="1" />';
            echo '<input type="hidden" name="edit" value="0">';
            echo '<input type="hidden" name="del" value="0" />';
            echo '<input type="hidden" name="idParent" value="'.$iM.'" />';            
            echo '&nbsp;<input type="submit" value="Přidat položku" /></div>';
          echo '</form>';            
        echo '</div> <!-- .menuList -->';                                       
      }
    }                               
    */
  }

}

?>