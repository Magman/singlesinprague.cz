<?php
class OrderAdminRenderer {

  private $orderAdmin;

	public function __construct(OrderAdmin $orderAdmin)
	{
		$this->orderAdmin = $orderAdmin;
	}

	public function render()	
	{ 		
		return $this->renderDefault();
	}

	private function renderDefault()
	{  
          
    //require_once '../lib/Help/HelpAdmin.php'; 
    
    //$help = new HelpAdmin ('cs'); // nápověda
      
    //$help->showHelp ('order', 1); 
    
    echo '<h2>Rezervace</h2>';
                 
    $result = mysqli_query($this->orderAdmin->db_connect, "select * from orders where del = 0 order by id desc");       
 
    $this->showTable ($result);                   
            
  }
  
  private function showTable ($result)
  {
    
    echo '<script type="text/javascript">
      // pagination setting
      $(document).ready(function(){
        $.pagMag.startPagination(
          perPage = 10, // items per page
          reload = 1, // default 0, value 1 keeps actual page when webpage is reloaded (need modul cookies.js) 
          item = \'itemOrderAdmin\', // css class of item
          pager = \'pagerOrderAdmin\' // css id of pagination    
        );      
      }); 
    </script>';
    
    echo '<div class="pager" id="pagerOrderAdmin"></div>'; 
    
    echo '<table class="tabAdmin tablesorter" id="tabOrder">';
      
      echo '<thead>';
     
        echo '<tr><th>číslo</th><th>rezervováno</th><th>kategorie</th><th>kdy</th><th>jméno</th><th>telefon</th><th>email</th><th></th><th></th></tr>';
      
      echo '</thead>';
      
      echo '<tbody>';
      
        while ($row = mysqli_fetch_array($result)) {
          
          $product = mysqli_query($this->orderAdmin->db_connect, "select category_1.category from category_1, events where events.id = {$row['idEvent']} and events.category = category_1.id");
          $pdt = mysqli_fetch_array($product);
          
          $evnt = mysqli_query($this->orderAdmin->db_connect, "select dateEvent from events where id = {$row['idEvent']}");
          $evt = mysqli_fetch_array($evnt);
          
          echo '<tr class="item itemOrderAdmin"><td><strong>'.$row['orderNo'].'</strong></td><td>'.$row['dateOrder'].'</td><td>'.$pdt['category'].'</td><td>'.$evt['dateEvent'].'</td><td>'.$row['surname'].' '.$row['name'].'</td><td>'.$row['phone'].'</td><td><a href="mailto:'.$row['email'].'" title="'.$row['email'].'">'.$row['email'].'</a></td>';
            
            echo '<td>';
              echo '<form method="post" action="">';
                echo '<input type="hidden" name="detail" value="1" />';
                echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                echo '<input type="image" src="../lib/design/tuzka.gif" value="" />';
              echo '</form>';         
            echo '</td>';               
            
            echo '<td>';
              echo '<form method="post" action="">';
                echo '<input type="hidden" name="del" value="1" />';
                echo '<input type="hidden" name="id" value="'.$row['id'].'" />'; 
                echo '<input type="image" src="../lib/design/delete.gif" value="" />';
              echo '</form>'; 
            echo '</td>';
          
          echo '</tr>';
        
      }
      
      echo '</tbody>';
    
    echo '</table>';  
  
  }

}

?>