<?php
// NEREGISTROVANI
require_once('./lib/IncreaseDate/IncreaseDate.php');

class Order {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	    /**
	 * Jazyk.
	 *
	 * @var int
	 */
	public $lang = 0;
	
		  /**
	 * Pole e-mailových adres
	 *
	 * @var array
	 */
	public $mailArr = null;
	
		  /**
	 * e-mail pro skrytou kopii
	 *
	 * @var string
	 */
	public $addBcc = null;
	
			  /**
	 * mail server
	 *
	 * @var string
	 */
	public $mailServer = null;
	
		  /**
	 * path to securimage
	 *
	 * @var string
	 */
	public $path = null;
	
	public function __construct($db_connect, $lang, $mailArr, $addBcc, $mailServer, $path) {
    $this->db_connect = $db_connect;
    $this->lang = (int) $lang;
    $this->mailArr = $mailArr;
    $this->addBcc = (string) $addBcc;
    $this->mailServer = (string) $mailServer;
    $this->path = (string) $path;
  }

  public function mailOrder() // $mailArr, $addBcc, $mailServer
  {
    switch ($this->lang) {
      case 1: $phOrderNo = 'Reservation no.'; 
      $phPar1 = 'Hello and welcome to Singles in Prague,'; 
      $phPar2 = 'Thank you for your interest in attending our upcoming event.'; 
      $phPar3 = 'Information about event'; 
      $phDateOrder = 'Reservation date'; 
      $phDateEvent = 'When'; 
      $phPlace = 'Where'; 
      $phName = 'Name'; 
      $phPhone = 'Phone'; 
      $phPar4 = 'Information about the event:';  
      $phPar5 = 'Information to your online payment:'; 
      $phPrice = 'Amount:'; 
      $phPar6 = 'Account number:  1234212'; 
      $phPar7 = 'Bank code: 5500 Raiffeisen Bank'; 
      $phPar8 = 'Variable symbol:'; 
      $phPar9 = 'All you need to bring is a good mood and a smile!'; 
      $phPar10 = 'We look forward to seeing you.'; 
      $phPar11 = 'Klara'; 
      $phPar12 = 'Singles in Prague'; 
      $phPar13 = 'Payment'; 
      $phPar14 = 'at the entrance.'; 
      $phVoucher = 'Bring your voucher with your voucher number, please.';
      break;
      case 2: $phOrderNo = 'Rezervace č.'; 
      $phPar1 = 'Dobrý den,'; 
      $phPar2 = 'Děkujeme za Váš zájem zúčastnit se večeru pro nezadané pořádaným Singles in Prague.';
      $phPar3 = 'Informace o večeru'; 
      $phDateOrder = 'Datum rezervace'; 
      $phDateEvent = 'Kdy'; 
      $phPlace = 'Kde'; 
      $phName = 'Jméno'; 
      $phPhone = 'Telefon';  
      $phPar4 = 'Informace o večeru:';  
      $phPar5 = 'Informace k platbě:'; 
      $phPrice = 'Částka:'; 
      $phPar6 = 'Číslo účtu: 1234212'; 
      $phPar7 = 'Kód banky: 5500'; 
      $phPar8 = 'Variabilní symbol:'; 
      $phPar9 = 'Přijďte s dobrou náladou a úsměvem. Těšíme se na Vás.'; 
      $phPar10 = 'S přátelským pozdravem,'; 
      $phPar11 = 'Ing. Klára Le Cornu'; 
      $phPar12 = 'Singles in Prague';  
      $phPar13 = 'Platba'; 
      $phPar14 = 'u vchodu.'; 
      $phVoucher = 'Přineste prosím svůj voucher s Vaším číslem.';
    }
        
    $result = mysqli_query($this->db_connect, "select * from orders order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $id = $row['id'];
    $orderNo = $row['orderNo'];
    $dateOrder = $row['dateOrder'];
    $dateEvent = $row['dateEvent'];
    $hour = $row['hour'];
    $minute = $row['minute'];
    $place = $row['place'];
    $address = $row['address'];
    $name = $row['name'];
    $surname = $row['surname'];
    $email = $row['email'];
    $phone = $row['phone'];
    $payment = $row['payment'];
    $voucher = $row['voucher'];
    
    $increaseDate = new IncreaseDate($this->lang);
    $incDate = $increaseDate->incDate($dateEvent);
    
    $result = mysqli_query($this->db_connect, "select price from price where id = $payment");
    $row = mysqli_fetch_array($result);
    $price = $row['price'];
    
    if ($voucher != 0) { 
      $showPayment = null;
      $showPayment = '<strong>'.$phVoucher.'</strong><br /><br />'; 
    } else if ($payment == 1) {     
      $showPayment = null;
      $showPayment .= '<strong>'.$phPar5.'</strong><br />';
      $showPayment .= $phPrice.' '.$price.' CZK<br />';
      $showPayment .= $phPar6.'<br />';
      $showPayment .= $phPar7.'<br />';
      $showPayment .= $phPar8.' '.$orderNo.'<br /><br />';    
    } else if ($payment == 3) {
      switch ($this->lang) {
        case 1: $product = 'Membership 6 month';
        break;
        case 2: $product = 'Členství 6 měsíců';
      }
      $showPayment = null;
      $showPayment .= '<strong>'.$phPar5.'</strong><br />';
      $showPayment .= $product.'<br />';
      $showPayment .= $phPrice.' '.$price.' CZK<br />';
      $showPayment .= $phPar6.'<br />';
      $showPayment .= $phPar7.'<br />';
      $showPayment .= $phPar8.' '.$orderNo.'<br /><br />';        
    } else if ($payment == 2) {
      $showPayment = null;
      $showPayment .= '<strong>'.$phPar13.':</strong> '.$price.' CZK '.$phPar14.'<br /><br />';
    }
       
    require ($this->path.'phpmailer/class.phpmailer.php');
    
    if ($this->mailArr != null) {
      foreach ($this->mailArr as $k => $em) { 
        $mail = new PHPMailer();
        $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
        $mail->Host = $this->mailServer;  // zadáme adresu SMTP serveru
        $mail->SMTPAuth = true;               // nastavíme true v případě, že server vyžaduje SMTP autentizaci        
        $mail->Username = "singles@singlesinprague.cz";   // uživatelské jméno pro SMTP autentizaci
        $mail->Password = "Iamsuccesful123";            // heslo pro SMTP autentizaci 57ubyXWUXR
        $mail->From = 'singles@singlesinprague.cz';   // adresa odesílatele skriptu
        $mail->FromName = 'Singles in Prague'; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
        $mail->AddAddress($em);  // přidáme příjemce a klidně i druhého včetně jména
        if ($k == 0) $mail->AddBcc($this->addBcc); 
        $mail->Subject = "Singles in Prague: $phOrderNo $orderNo";    // nastavíme předmět e-mailu
        $mail->IsHTML(true);
        $mail->Body = "<html>
          <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
          </head>
          <body style=\"font-family: 'Times New Roman', serif; font-size:16px; line-height:1.3;\">
            <div style=\"float:left; width:670px; height:auto; margin-bottom:15px;\">           
              <img style=\"margin:10px\" src=\"http://singlesinprague.cz/design/logo.png\" alt=\"Logo\">
              <h2 style=\"font-size:24px; color:#992f4a; line-height:1.3;\">$phOrderNo:</strong> $orderNo</h2>
              $phPar1<br /><br />
              $phPar2<br /><br />
              <strong>$phDateOrder:</strong> $dateOrder<br />      
              <strong>$phName:</strong> $name $surname<br />
              <strong>E-mail:</strong> $email<br />
              <strong>$phPhone:</strong> $phone<br /><br /> 
              <strong>$phPar3</strong><br />
              <strong>$phDateEvent:</strong> $incDate $hour:$minute<br />
              <strong>$phPlace:</strong> $place, $address<br /><br />
              $showPayment            
              $phPar9<br /> 
              $phPar10<br /><br />
              $phPar11, $phPar12<br />                               
            </div>
          </body>
        </html>
        ";       
        $mail->AltBody = "$phOrderNo: $orderNo\n
        $phPar1\n\n
        $phPar2\n\n
        $phPar3\n\n
        $phDateOrder: $dateOrder\n      
        $phName: $name $surname\n
        E-mail: $email\n
        $phPhone: $phone\n\n
        $phPar4\n
        $phDateEvent: $dateEvent\n
        $phPlace: $place\n\n
        $phPar9\n 
        $phPar10\n
        $phPar11\n
        $phPar12\n 
        ";  // nastavíme tělo e-mailu
        $mail->WordWrap = 100;   // je vhodné taky nastavit zalomení (po 50 znacích)
        $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail
        
        if(!$mail->Send()) {  // odešleme e-mail
          echo '<div class="warrCover"><div class="warr">';
            echo 'Error. E-mail failed.';
            echo 'Notice: ' . $mail->ErrorInfo;
          echo '</div></div>';
        } else {
          unset ($mail);
          // echo 'E-mail sent. ';
        }
      }
    }
  }

}
?>