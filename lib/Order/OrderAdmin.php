<?php

require_once (dirname(__FILE__) . '/OrderAdminRenderer.php');
// absolutní cesta vztažená k umístění volajícího souboru (na stejné úrovni jako Pager) 

require_once ('../lib/Tools/HideButton/HideButton.php');

class OrderAdmin {

      /**
	 * Připojení k DB.
	 *
	 * @var array
	 */
	public $db_connect = null;
	
	      /**
	 * Jazyky.
	 *
	 * @var array
	 */
	public $language = null;

	      /**
	 * Aktivní jazyky.
	 *
	 * @var array
	 */
	public $languageActive = null;
		
	public function __construct($db_connect, $language, $languageActive) {
    $this->db_connect = $db_connect;
    $this->language = $language;
    $this->languageActive = $languageActive;
  }
  
  public function dokOrder ($id)
  {
    
    $result = mysqli_query($this->db_connect, "update orders set del = 1 where id = $id");
    if ($result) {
      echo '<div class="warrCover"><div class="warr">Objednávka byl úpěšně smazána.</div></div>';
    } else {
      echo '<div class="warrCover"><div class="warr">Chyba! Objednávku se nepodařilo smazat.</div></div>';
    }
  
  }

  public function deleteOrder ($id)
  {
    $result = mysqli_query($this->db_connect, "select orderNo from orders where id = $id");
    $row = mysqli_fetch_array($result);
    echo '<div class="warrCover"><div class="warr">';
      echo 'Opravdu chcete objednávku <strong>'.$row['orderNo'].'</strong> smazat?'; 
      echo '<table><tr>';
      echo '<td>';
        echo '<form method="post" action="">';
          echo '<input type="hidden" name="dok" value="1" />';
          echo '<input type="hidden" name="id" value="'.$id.'" />';
          echo '<input type="submit" value="smazat" />';
        echo '</form>';
      echo '</td>';
      echo '<td>';
        echo '<form method="get" action="">';
          echo '<input type="submit" value="ne" />';
        echo '</form>';
      echo '</td>';
      echo '</tr></table>';
    echo '</div></div>';
  }
 
  public function detailOrder ($id)
  {
    
    $result = mysqli_query($this->db_connect, "select * from orders where id = $id");
    $row = mysqli_fetch_array($result);
    
    if ($row['uid'] == 0) $phReg = 'ne'; else $phReg = 'ano';
    
    $product = mysqli_query($this->db_connect, "select category_1.category from category_1, events where events.id = {$row['idEvent']} and events.category = category_1.id");
    $pdt = mysqli_fetch_array($product);
          
    $evnt = mysqli_query($this->db_connect, "select dateEvent from events where id = {$row['idEvent']}");
    $evt = mysqli_fetch_array($evnt);
    
    echo '<div class="editBlockList">';
      echo '<form class="backOrder" method="post" action="">';
        echo '<input type="image" src="../lib/design/delete.gif" value="" />';
      echo '</form>';      
      
      echo '<h2>'.$row['orderNo'].'</h2>';
      echo '<h3>'.$pdt['category'].'</h3>';
       echo '<table>';
        echo '<tr><td><strong>Datum rezervace:</strong></td><td>'.$row['dateOrder'].'</td></tr>';
        echo '<tr><td><strong>Jméno:</strong></td><td>'.$row['surname'].' '.$row['name'].'</td></tr>';
        echo '<tr><td><strong>E-mail:</strong></td><td><a href="mailto:'.$row['email'].'" title="'.$row['email'].'">'.$row['email'].'</a></td></tr>';
        echo '<tr><td><strong>Telefon:</strong></td><td>'.$row['phone'].'</td></tr>';
        echo '<tr><td><strong>Rok narození:</strong></td><td>'.$row['birth'].'</td></tr>';
        switch ($row['sex']) {
          case 1: $sex = 'žena';
          break;
          case 2: $sex = 'muž';
        }
        echo '<tr><td><strong>Pohlaví:</strong></td><td>'.$sex.'</td></tr>';
        echo '<tr><td><strong>Registrace:</strong></td><td>'.$phReg.'</td></tr>';
        echo '<tr><td><strong>Datum akce:</strong></td><td>'.$row['dateEvent'].'</td></tr>';
        echo '<tr><td><strong>Čas:</strong></td><td>'.$row['hour'].':'.$row['minute'].'</td></tr>';
        echo '<tr><td><strong>Místo:</strong></td><td>'.$row['place'].'</td></tr>';
        echo '<tr><td><strong>Adresa:</strong></td><td>'.$row['address'].'</td></tr>';
        switch ($row['payment']) {
          case 1: $payment = 'převodem z účtu';
          break;
          case 2: $payment = 'na místě';
          break;
          case 3: $payment = 'členství';
        }
        echo '<tr><td><strong>Způsob platby:</strong></td><td>'.$payment.'</td></tr>';       
        echo '<tr><td><strong>Voucher:</strong></td><td>'.$row['voucher'].'</td></tr>';     
      echo '</table>';
    
    echo '</div> <!-- .edit_block_list -->';  
  }
  
  public function updateProduct($id, $lang, $idParent, $title, $resort, $price, $describe)
  {
    $title = strtr($title, array("'"=>""));
    
    $describe = strtr($describe, array("'"=>""));
        
    $result = mysqli_query($this->db_connect, "update product_1 set idParent = $idParent, price = $price where id = $id");                                                    
    
    $tb = 'product_'.$lang;          
    $sql = "update `$tb` set `title` = '"
    .mysqli_real_escape_string($this->db_connect, $title)."', `resort` = '"
    .mysqli_real_escape_string($this->db_connect, $resort)."',`describe` = '"
    .mysqli_real_escape_string($this->db_connect, $describe)."' where id = $id";
              
    $result = mysqli_query($this->db_connect, $sql);
                          
    if (!$result) {
      echo '<div class="warrCover"><div class="warr">Chyba! Aktualizace se nezdařila.</div></div>';
    } else {
      echo '<div class="warrCover"><div class="warr">Aktualizace akce proběhla úspěšně.</div></div>';
    }   
  }
  
  public function showPhoto ($folder, $maxFileSize, $fileSize)
  {
    
    echo '<div class="newsGallery">';
    
      echo '<h3>'.$this->titleProduct($folder).'</h3>'; 
      
      $hideButton = new HideButton($uid = 0);
      $hideButton->hideButtonHref($file = 'admin_product.php');
      
      echo '<div class="multipart">';
        $this->inputPhoto ($folder, $maxFileSize, $fileSize);  
      echo '</div>';
      
      //show photo                                                
      echo '<form method="post" action="" id="delChecked">';
        echo '<input type="hidden" name="folder" value="'.$folder.'" />'; // kvuli navratu do folderu
        $result = mysqli_query($this->db_connect, "select id, nameImage from product_gallery where idNews = $folder order by id");    
        while ($row = mysqli_fetch_array($result)) {                                           
          
          $uploadfile = '../product_foto/'.$folder.'/'.$row['nameImage'];
          
          if (substr($row['nameImage'], -3) == 'png') {
            $im = imagecreatefrompng($uploadfile);
            $x = imagesx($im);
            $y = imagesy($im);          
          } else if (substr($row['nameImage'], -3) == 'gif') {
            $im = imagecreatefromgif($uploadfile);
            $x = imagesx($im);
            $y = imagesy($im);          
          } else {
            $im = imagecreatefromjpeg($uploadfile);
            $x = imagesx($im);
            $y = imagesy($im);          
          } 
          
          if ($x >= $y) {
            $style = ' class="newsLandscape"';
          } else {
            $style = ' class="newsPortrait"';
          }
          
          echo '<div class="itemPhoto"><table><tr><td class="tabThumb">';
            echo '<a href="admin_product.php?detail=1&amp;folder='.$folder.'&amp;image='.$row['id'].'" title="'.$row['nameImage'].'"><img'.$style.' src="../product_foto/'.$folder.'/thumb_'.$row['nameImage'].'" alt="'.$row['nameImage'].'" /></a>'; // <!-- class="'.$styl.'" -->          
          echo '</td></tr>';
          echo '<tr><td class="checkbox"><input type="checkbox" name="check['.$row['id'].']" /></td></tr>';
          echo '</table></div> <!-- .itemPhoto -->';                                  
        } 
        if (mysqli_num_rows($result) > 0) {
          echo '<div class="newsGallery" id="newsGallery">';
            echo '<table class="submitChecked"><tr><td>';
              echo '<input type="submit" name="delChecked" value="smazat označené" />'; // delete marked
            echo '</td></tr></table>';
          echo '</div>';   
        }
      echo '</form>';
    
    echo '</div>';
      
  }  
  
  public function inputPhoto ($folder, $maxFileSize, $fileSize)
  {  
    
    echo '<form action="" method="post" enctype="multipart/form-data">';
      echo '<input type="hidden" name="insImg" value="1" />';
      echo '<input type="hidden" name="folder" value="'.$folder.'" />';
      echo '<input type="hidden" name="file_size" value="'.$fileSize.'" />';
      echo '<input type="hidden" name="max_file_size" value="'.$maxFileSize.'" />';
      echo '<strong>Vyber obrázky</strong>&nbsp;<input type="file" name="uploads[]" multiple="multiple" />'; // Upload images:
      echo '<input type="submit" value="načíst" />'; // save
    echo '</form>';
  } 
  
  public function insertPhoto ($folder, $largeViewX, $largeViewY) 
  {
      
    // Parametry v souboru ../function/param.php
    $largeThumb = 100;
             
    ini_set('memory_limit', '128M');
      
    if (isset($_FILES['uploads'])) {
      $nameArr = array();
      foreach ($_FILES['uploads']['name'] as $k => $n) { 
        if ($_FILES['uploads']['name'][$k] == null) {
          echo '<div class="warrCover"><div class="warr">Nevzybrali jste žádné foto!</div></div>'; // You have not selected any image.
        } else if ($_FILES['uploads']['size'][$k] == 0) {
          echo '<div class="warrCover"><div class="warr">Chyba! Obrázek je příliš velký a nebude načten:&nbsp;'.$_FILES['uploads']['name'][$k].'</div></div>'; // Error! The image is too larg and will not be uploaded.
        } else if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg' or substr($_FILES['uploads']['type'][$k], 6) === 'gif' or substr($_FILES['uploads']['type'][$k], 6) === 'png') {
          // odstrani diakritiku a mezery (explode rozdělí řetězec do pole podle teček)
          $name = explode(".", strtr($n, array(' '=>'_', '–'=>'-', 'Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
            'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
            'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
            'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
            'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z')));
          // odstrani tecky a velka pismena
          $newName = null;
          foreach ($name as $i => $nm) {
            if ($i < count($name) - 1) $newName .= $nm;
          }
          $nameArr[$k] = strtolower($newName).'.'.substr($_FILES['uploads']['type'][$k], 6); // image/jpeg
          unset ($newName);
        } else {
          echo '<div class="warrCover"><div class="warr">Obrázek není formátu JPG, GIF ani PNG:&nbsp;'.$_FILES['uploads']['name'][$k].'</div></div>'; // Error! The image is not jpeg, gif or png and will not be uploaded.
        }
      }
      // pro kontrolu duplicity foto
      $imagesDb = array();
      $result = mysqli_query($this->db_connect, "select nameImage from product_gallery where idNews = $folder");
      while ($row = mysqli_fetch_array($result)) {
        $imagesDb[] = $row['nameImage'];
      }        
      if ($nameArr != null) {  
        foreach ($nameArr as $k => $nA) {        
          if (!in_array($nA, $imagesDb)) { // pro kontrolu duplicity foto          
            $uploadfile = '../product_foto/'.$folder.'/'.$nA;
            if (move_uploaded_file($_FILES['uploads']['tmp_name'][$k], $uploadfile)) {                        
              // Plná velikost
              if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {
                $im = imagecreatefromjpeg($uploadfile);
                $x = imagesx($im);
                $y = imagesy($im);
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                $im = imagecreatefromgif($uploadfile);
                $x = imagesx($im);
                $y = imagesy($im);
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                $im = imagecreatefrompng($uploadfile);
                $x = imagesx($im);
                $y = imagesy($im);
              }
                
              if ($x > $y) {
                if ($x > $largeViewX) {
                  $rx1 = $largeViewX;
                  $ry1 = $y * $largeViewX / $x;
                } else {
                  $rx1 = $x;
                  $ry1 = $y;
                }                                             
              } else if ($x == $y) {
                if ($y > $largeViewY) {
                  $ry1 = $largeViewY;
                  $rx1 = $x * $largeViewY / $y;
                } else {
                  $rx1 = $x;
                  $ry1 = $y;
                }                 
              } else {
                if ($y > $largeViewY) {
                  $ry1 = $largeViewY;
                  $rx1 = $x * $largeViewY / $y;
                } else {
                  $rx1 = $x;
                  $ry1 = $y;
                }                 
              }       
                
              $view = imagecreatetruecolor($rx1, $ry1);
              $imView = imagecopyresampled($view, $im, 0, 0, 0, 0, $rx1, $ry1, $x, $y);
                
              $fullname = '../product_foto/'.$folder.'/'.$nA;
                
              if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {        
                imagejpeg($view, $fullname, 100);  
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                imagegif($view, $fullname, 100);
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                imagepng($view, $fullname, 9);
              } 
              // Thumbnail
              if($x > $y) { //podmínka zajišťující správné zmenšení fotografie i v případě orientace navýšku            
                $rx2 = $x * $largeThumb / $y;
                $ry2 = $largeThumb;
              } else {
                $ry2 = $y * $largeThumb / $x; 
                $rx2 = $largeThumb;
              }
                         
              $view = imagecreatetruecolor($rx2, $ry2);
              $imView = imagecopyresampled($view, $im, 0, 0, 0, 0, $rx2, $ry2, $x, $y);
                
              $fullname = '../product_foto/'.$folder.'/thumb_'.$nA;
                
              if (substr($_FILES['uploads']['type'][$k], 6) === 'jpeg') {        
                imagejpeg($view, $fullname, 100);  
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'gif') {
                imagegif($view, $fullname, 100);
              } else if (substr($_FILES['uploads']['type'][$k], 6) === 'png') {
                imagepng($view, $fullname, 9);
              } 
              $sql = "insert into product_gallery (idNews, nameImage, dateInsert) values ('"
              .mysqli_real_escape_string($this->db_connect, $folder)
              ."','"
              .mysqli_real_escape_string($this->db_connect, $nA)
              ."', now())"
              ;
                              
              mysqli_query($this->db_connect, $sql);                       
                
              echo '<div class="warrCover"><div class="warr">Obrázek byl úspěšně načten na server:&nbsp;'.$nA.'</div></div>'; // The image has been successfully uploaded to the server.         
            } else {
              echo '<div class="warrCover"><div class="warr">Chyba! Obrázek se nepodařilo načíst:&nbsp;'.$nA.'</div></div>'; // Error! The image was not successfully uploaded to the server.
            }
          } else {
            echo '<div class="warrCover"><div class="warr">Obrázek stejného jména je již načten:&nbsp;'.$nA.' je již v albu načten.</div></div>'; // Error! The image with this name already exists in the album.
          }
        } 
      } 
    }

  } 
  
	public function showPhotoDetail ($folder, $image)
  {           
    
    echo '<div class="newsGallery">';
    
    echo '<h3>'.$this->titleProduct($folder).'</h3>'; 
    
    ini_set('memory_limit', '64M');

    $result = mysqli_query($this->db_connect, "select nameImage from product_gallery where id = $image");
    $row = mysqli_fetch_array($result);
    $nameImage = $row['nameImage'];
    
    //Kontrola velikosti foto   
    if (strtolower(substr($nameImage, -4)) === 'jpeg') {
      $im = imagecreatefromjpeg('../product_foto/'.$folder.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'jpg') {
      $im = imagecreatefromjpeg('../product_foto/'.$folder.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'gif') {
      $im = imagecreatefromgif('../product_foto/'.$folder.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    } else if (strtolower(substr($nameImage, -3)) === 'png') {
      $im = imagecreatefrompng('../product_foto/'.$folder.'/'.$nameImage);
      $x = imagesx($im);
      $y = imagesy($im);
    }
    if ($x > 460) {
      $styl = 'imgGal';
    } else {
      $styl = 'imgGalPortrait';
    }
              
    echo '<div class="detailAdmin">';
      echo '<img src="../product_foto/'.$folder.'/'.$nameImage.'" class="'.$styl.'" alt="'.$nameImage.'" />'; 
      echo '<div class="up" id="newsUp">';
        echo '<a href="admin_product.php?img=1&amp;folder='.$folder.'" title="Zpět do alba">
        <img src="../lib/Gallery/design/up.gif" alt="zpět do alba" onmouseover="this.src = \'../lib/Gallery/design/up_hover.gif\';" onmouseout="this.src = \'../lib/Gallery/design/up.gif\';" /></a>';
      echo '</div> <!-- .up -->'; // return to album
    echo '</div>';
               
    $tbComment1 = 'product_comment_1';
                
    $result = mysqli_query($this->db_connect, "select id from `$tbComment1` where image = $image");
    $row = mysqli_fetch_assoc($result);
    $id = $row['id'];
                
    $num_rows = mysqli_num_rows($result);
                 
    if ($num_rows == 0) {
                
      // Formulář pro VLOŽENÍ popisku
      echo '<form class="editComment" method="post" action="">';
        echo '<input type="hidden" name="insertComment" value="1" />';
        echo '<input type="hidden" name="image" value="'.$image.'" />';
        echo '<input type="hidden" name="folder" value="'.$folder.'" />';
        echo '<input class="inpComment" type="text" name="comment" value="Vložit komentář" />'; // add commentary
        echo '&nbsp;&nbsp;<input type="submit" value="odeslat" />';
      echo '</form>';
    } else {
  
      // Formulář pro EDITACI popisku
      foreach ($this->languageActive as $code => $lg) {
                        
        $tbComment = 'product_comment_'.$lg;
                    
        $result = mysqli_query($this->db_connect, "select comment from `$tbComment` where id = $id");
        $row = mysqli_fetch_assoc($result);
        $comment = $row['comment'];
                        
        echo '<form class="editComment" method="post" action="">';
          echo '<table>';
            echo '<input type="hidden" name="editComment" value="1" />';
            echo '<input type="hidden" name="lang" value="'.$lg.'" />';
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            echo '<input type="hidden" name="folder" value="'.$folder.'" />'; // kvuli navratu do showDetail
            echo '<input type="hidden" name="image" value="'.$image.'" />';   // kvuli navratu do showDetail        
            echo '<tr>';
              echo '<td><img src="../lib/design/'.$code.'.png"></td>';
              echo '<td><input class="inpComment" type="text" name="comment" value="'.$comment.'" /></td>';
              echo '<td><input type="submit" value="odeslat" /></div></td>'; // send
            echo '</tr>'; 
          echo '</table>';
        echo '</form>';  
      }  
    }
    
    echo '</div> <!-- newsGallery -->';
                
  }  
  
  public function insertComment($image, $comment)
  {
 
    $tbComment1 = 'product_comment_1';
                                                                
    $sql = 
    "insert into `$tbComment1` (comment, image) "
    ."values('"
    .mysqli_real_escape_string($this->db_connect, $comment)
    ."','"
    .mysqli_real_escape_string($this->db_connect, $image)
    ."')"
    ;
                
    $result = mysqli_query($this->db_connect, $sql);
                
    $result = mysqli_query($this->db_connect, "select id from `$tbComment1` order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
    $id = $row['id'];
                 
    //další jazyky
    $lng = $this->language; // kvůli zobrazení editačního formuláře 
    $lang1 = array_shift($lng);
    foreach ($lng as $lg) {
      $tbComment = 'product_comment_'.$lg;
                                                                  
      $sql = 
      "insert into `$tbComment` (id, comment) "
      ."values('"
      .mysqli_real_escape_string($this->db_connect, $id)
      ."','"
      .mysqli_real_escape_string($this->db_connect, $comment)
      ."')"
      ;
                  
      $result = mysqli_query($this->db_connect, $sql);
    }
                                        
    if (!$result) {
      echo mysqli_error($this->db_connect);
    } else {
      echo '<div class="warrCover"><div class="warr">Komentář byl úspěšně vložen.</div></div>'; // The commentary has been successfully added.              
    }      
  }  
  
  public function editComment ($id, $lg, $comment)
  {  
    
    $tbComment = 'product_comment_'.$lg;                           
    $sql = "update `$tbComment` set comment = '"
    .mysqli_real_escape_string($this->db_connect, $comment)."' where id = $id";
              
    // Zaslání SQL příkazu do databáze.
    $result = mysqli_query($this->db_connect, $sql);
               
    if (!$result) {
      echo mysqli_error($this->db_connect);
    } else {
      echo '<div class="warrCover"><div class="warr">Komentář byl úspěšně editován.</div></div>'; // The commentary has been successfully edited.               
    }   
  } 
  
  public function deleteChecked($checked)
  {  
    
    if ($checked == null) {
      echo '<div class="warrCover"><div class="warr">Nebylo vybráno žádné foto!</div></div>'; // No images have been selected.
    } else {      
      foreach ($checked as $k => $v) {
        for ($i = 0; $i < 1; $i++) { // vykona pouze jednou
          $result = mysqli_query($this->db_connect, "select idNews from product_gallery where id = $k");
          $row = mysqli_fetch_array($result);
          $folder = $row['idNews']; 
        }
        $result = mysqli_query($this->db_connect, "select nameImage from product_gallery where id = $k");
        $row = mysqli_fetch_array($result);
        $delSer = unlink ('../product_foto/'.$folder.'/'.$row['nameImage']);
        $delSer = unlink ('../product_foto/'.$folder.'/thumb_'.$row['nameImage']);
        if ($delSer) {
          $delete = mysqli_query($this->db_connect, "delete from product_gallery where id = $k");
          if ($delete) echo '<div class="warrCover"><div class="warr">Obrázek byl smazán:&nbsp;'.$row['nameImage'].'</div></div>'; // The image was deleted.
        }              
      }   
    }  
  } 
  
  public function showFile ($folder, $maxFileSize, $fileSize)
  {

    echo '<div class="newsGallery">';
    
      echo '<h3>'.$this->titleProduct($folder).'</h3>'; 
      
      $hideButton = new HideButton($uid = 0);
      $hideButton->hideButtonHref($file = 'admin_product.php');
      
      echo '<div class="multipart">';
        $this->inputFile ($folder, $maxFileSize, $fileSize);  
      echo '</div>';
                                                          
      $files = glob('../product_download/'.$folder.'/*');  
        
      if ($files != null) {
          
        echo '<form method="post" action="" id="delChecked">';
        echo '<input type="hidden" name="folder" value="'.$folder.'" />'; // kvuli navratu do folderu
          
        foreach ($files as $f) {                                           
            
          $name = substr($f, 21 + strlen($folder));
            
          if (substr($f, -3) === 'doc') {
            $style = ' class="fileW"';
          } else if (substr($f, -4) === 'docx') {
            $style = ' class="fileW"';
          } else if (substr($f, -3) === 'xls') {
            $style = ' class="fileE"';
          } else if (substr($f, -4) === 'xlsx') {
            $style = ' class="fileE"';
          } else if (substr($f, -3) === 'pdf') {
            $style = ' class="fileP"';
          }            
        
          echo '<div class="itemPhoto"><table'.$style.'><tr><td class="tabThumb">';
            //echo '<img src="../news_foto/'.$folder.'/thumb_'.$row['nameImage'].'" alt="'.$row['nameImage'].'" />'; // <!-- class="'.$styl.'" -->          
            echo '<p>'.$name.'</p>';
          echo '</td></tr>';
          echo '<tr><td class="checkbox">';
            echo '<a href="admin_product.php?folder='.$folder.'&amp;alias='.$f.'" title="editovat alias"><img src="../lib/design/tuzka.gif"></a>';
            echo '<input type="checkbox" name="check['.$f.']" /></td></tr></table>';
          echo '</div> <!-- .itemPhoto -->';
        
        } 
          
        
        echo '<div class="newsGallery" id="newsGallery">';
          echo '<table class="submitChecked"><tr><td>';
            echo '<input type="submit" name="delCheckedFile" value="smazat označené" />'; // delete marked
          echo '</td></tr></table>';
        echo '</div>';   
        
        echo '</form>'; 
        
      }
      
    echo '</div>';  
  
  }
  
  public function inputFile ($folder, $maxFileSize, $fileSize)
  {  
    
    echo '<form action="" method="post" enctype="multipart/form-data">';
      echo '<input type="hidden" name="insFile" value="1" />';
      echo '<input type="hidden" name="folder" value="'.$folder.'" />';
      echo '<input type="hidden" name="file_size" value="'.$fileSize.'" />';
      echo '<input type="hidden" name="max_file_size" value="'.$maxFileSize.'" />';
      echo '<strong>Vyber soubory</strong>&nbsp;<input type="file" name="download[]" multiple="multiple" />'; // Upload images:
      echo '<input type="submit" value="načíst" />'; // save
    echo '</form>';
  } 
  
  public function insertFile($folder) 
  {
             
    ini_set('memory_limit', '128M');
      
    if (isset($_FILES['download'])) {
      
      //var_dump($_FILES['download']);
      
      $nameArr = array();
      foreach ($_FILES['download']['name'] as $k => $n) { 
        if ($_FILES['download']['name'][$k] == null) {
          echo '<div class="warrCover"><div class="warr">Nevybrali jste žádný soubor!</div></div>'; // You have not selected any image.
        } else if ($_FILES['download']['size'][$k] == 0) {
          echo '<div class="warrCover"><div class="warr">Chyba! Soubor je příliš velký a nebude načten:&nbsp;'.$_FILES['download']['name'][$k].'</div></div>'; // Error! The image is too larg and will not be uploaded.
        } else if (substr($_FILES['download']['name'][$k], -3) === 'doc' or substr($_FILES['download']['name'][$k], -4) === 'docx' or substr($_FILES['download']['name'][$k], -3) === 'xls' or substr($_FILES['download']['name'][$k], -4) === 'xlsx' or substr($_FILES['download']['name'][$k], -3) === 'pdf') {
          // odstrani diakritiku a mezery (explode rozdělí řetězec do pole podle teček)
          $name = explode(".", strtr($n, array(' '=>'_', '–'=>'-', 'Á'=>'A','Ä'=>'A','Č'=>'C','Ç'=>'C','Ď'=>'D','É'=>'E','Ě'=>'E',
            'Ë'=>'E','Í'=>'I','Ň'=>'N','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O','Ó'=>'O',
            'Ó'=>'O','Ö'=>'O','Ř'=>'R','Š'=>'S','Ť'=>'T','Ú'=>'U','Ů'=>'U','Ü'=>'U','Ý'=>'Y','Ž'=>'Z','á'=>'a',
            'ä'=>'a','č'=>'c','ç'=>'c','ď'=>'d','é'=>'e','ě'=>'e','ë'=>'e','í'=>'i','ň'=>'n','ó'=>'o','ö'=>'o',
            'ř'=>'r','š'=>'s','ť'=>'t','ú'=>'u','ů'=>'u','ü'=>'u','ý'=>'y','ž'=>'z')));
          // odstrani tecky a velka pismena
          $newName = null;
          foreach ($name as $i => $nm) {
            if ($i < count($name) - 1) $newName .= $nm;
          }
          
          if (substr($_FILES['download']['name'][$k], -3) === 'doc') {
            $appendix = 'doc';
          } else if (substr($_FILES['download']['name'][$k], -4) === 'docx') {
            $appendix = 'docx';
          } else if (substr($_FILES['download']['name'][$k], -3) === 'xls') {
            $appendix = 'xls';
          } else if (substr($_FILES['download']['name'][$k], -4) === 'xlsx') {
            $appendix = 'xlsx';
          } else if (substr($_FILES['download']['name'][$k], -3) === 'pdf') {
            $appendix = 'pdf';
          }
                    
          $nameArr[$k] = strtolower($newName).'.'.$appendix; // image/jpeg
          
          unset ($newName);
        } else {
          echo '<div class="warrCover"><div class="warr">Soubor není formátu DOC, DOCX, XLS, XLSX ani PDF:&nbsp;'.$_FILES['download']['name'][$k].'</div></div>'; // Error! The image is not jpeg, gif or png and will not be uploaded.
        }
      }
        
      if ($nameArr != null) {  
        foreach ($nameArr as $k => $nA) {        
         
          $uploadfile = '../product_download/'.$folder.'/'.$nA;
            
          // move_uploaded_file
            
          if (move_uploaded_file($_FILES['download']['tmp_name'][$k], $uploadfile)) {                        
            echo '<div class="warrCover"><div class="warr">Soubor byl úspěšně načten na server:&nbsp;'.$nA.'</div></div>'; // The image has been successfully uploaded to the server.         
            $insert = mysqli_query($this->db_connect, "insert into product_download (folder, name, `alias`) values ($folder, '$nA', '$nA')");
          } else {
            echo '<div class="warrCover"><div class="warr">Chyba! Soubor se nepodařilo načíst:&nbsp;'.$nA.'</div></div>'; // Error! The image was not successfully uploaded to the server.
          }
        } 
      } 
    }

  } 
  
  public function deleteCheckedFile($checked)
  {  
    
    if ($checked == null) {
      echo '<div class="warrCover"><div class="warr">Nebyl vybrán žádný soubor!</div></div>'; // No images have been selected.
    } else {      
      foreach ($checked as $k => $v) {

        $delSer = unlink ($k);
        
        $name = explode('/', $k);
        
        $name = $name[3];
        
        $delete = mysqli_query($this->db_connect, "delete from product_download where name = '$name'");
             
      }   
    }  
  }  
  
  public function editAlias($folder, $alias)
  {
    
    $part = explode('/', $alias);
    
    $entita = $part[1];
    
    $folder = $part[2];
    
    $name = $part[3];
    
    $result = mysqli_query($this->db_connect, "select id, alias from `$entita` where folder = $folder and name = '$name'");
    $row = mysqli_fetch_array($result);

    echo '<div class="coverFormAlias">';
      echo '<form action="" method="post">';
        echo '<input type="hidden" name="insEditAlias" value="1" />';
        echo '<input type="hidden" name="folder" value="'.$folder.'" />';
        echo '<input type="hidden" name="idAlias" value="'.$row['id'].'" />'; 
        echo '<strong>Alias souboru:</strong> <input type="text" name="alias" value="'.$row['alias'].'" size="50" />&nbsp;';
        echo '<input type="submit" value="editovat" />';
      echo '</form>'; 
      echo '<a href="admin_product.php?back=1&amp;folder='.$part[2].'" title="zpět"><img class="imgDelAlias" src="../lib/design/delete.gif" alt="zpět" /></a>'; 
    echo '</div>';  
  
  }
  
  public function insEditAlias($idAlias, $alias)
  {
    
    $update = mysqli_query($this->db_connect, "update product_download set alias = '"
    .mysqli_real_escape_string($this->db_connect, $alias)."' where id = $idAlias");  
  
  }
  
  public function titleProduct($id) 
  {
  
    $result = mysqli_query($this->db_connect, "select title from product_1 where id = $id");
    $row = mysqli_fetch_array($result);
    return $row['title'];
  
  }
           
  public function render()
	{
		if (!isset($this->renderer)) {
			$this->renderer = new OrderAdminRenderer($this);
		}
		return $this->renderer->render();
	}


}

?>