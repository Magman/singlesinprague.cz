<?php
/**
 * Správa fotogalerie
 */
class SwitcherLanguage
{

  public $db_connect = null;	
    		
	/**
	 * Parametr pro jazyk
	 *
	 * @var int
	 */
	public $lang = 0;
	
	/**
	* Parametr pro page id 
	*
	* @var int
	*/
	public $id = 0;
	
	/**
	* Parametr pro page link 
	*
	* @var string
	*/
	public $link = null;
	
	/**
	* Parametr pro idAlbum
	*
	* @var int
	*/
	public $idAlbum = 0;
	
		/**
	* Parametr pro path
	*
	* @var string
	*/
	public $path = null;
						
  public function __construct($db_connect, $lang, $id, $link, $idAlbum, $path)
	{
		$this->db_connect = $db_connect;
		$this->lang = (int) $lang;
		$this->id = (int) $id;
		$this->link = (string) $link;
    $this->idAlbum = (int) $idAlbum;
    $this->path = (string) $path;	
	}

  public function showLanguages ($parHotel)
  {
    // Pro přepínání jazyků uvnitř alba
    if ($this->idAlbum != 0) { 
      $al = '&amp;idA='.intval($this->idAlbum);
    } else {
      $al = '';
    }           
        
    $result = mysqli_query($this->db_connect, "select id, lang from language where active = 1 order by `ord` desc");
    while ($row = mysqli_fetch_array($result)) {
    
      if (!empty($parHotel)) {
        $par = 'hotel_id='.$parHotel.'&amp;guide_id=802&amp;user_language='.$row['lang'].'&amp;';
      } else {
        $par = '';
      }
    
      if ($row['id'] == $this->lang) $actual = ' class="langActual"'; else $actual = null;
     /* if ($this->id == 1) { // pro zaindexování mutací
        echo '<a href="'.$row['lang'].'" 
        title="'.$row['lang'].'"><img src="./lib/SwitcherLanguage/design/'.$actual.'" alt="'.$row['lang'].'" /></a>';
      } else { */
        //echo '<a href="'.$this->link.'?'.$par.'lang='.$row['id'].'&amp;id='.$this->id.$al.'"
        echo '<a href="'.$this->link.'?'.$par.'lang='.$row['id'].'&amp;id='.$this->id.'" 
        title="'.$row['lang'].'"><span'.$actual.'>' . strtoupper($row['lang']) . '</span>&nbsp;&nbsp;</a>';
      //}                  
    }   
  }
}