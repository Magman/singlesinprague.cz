<?php
function languages ($connect) {    
  global $language; // all languages
  global $languageActive; // active languages
  global $langAdmin; // language for administration
 
  $language = array();
  $result = mysqli_query($connect, "select id from language order by id"); 
  while ($row = mysqli_fetch_array($result)) {
    $language[] = $row['id'];
  } 
  $languageActive = array();
  $result = mysqli_query($connect, "select id, lang from language where active = 1 order by `ord`");
  while ($row = mysqli_fetch_array($result)) {
    $languageActive[$row['lang']] = $row['id'];
  }
  
  // pouze pro popisky galerie, jinak natvrdo 1 (hlavní tabulka pro entitu např. menu_top_1)
  $langAdmin = 1;
  $result = mysqli_query($connect, "select id from language where admin = 1");
  $row = mysqli_fetch_array($result);
  $langAdmin = $row['id'];
  
  if ($languageActive == null) {
    $languageActive[] = $langAdmin;
  }
}
?>