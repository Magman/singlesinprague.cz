<?php
function language ($connect) {    
  global $lang;
  global $xmlLang;
  if (isset($_GET['lang'])) { // $_GET['lang'] - parametr pro jazyk, $_GET['vas_parametr'] - můžete nahradit vaším parametrem pro přenos jazyků
    $lang = intval($_GET['lang']);
    $_SESSION['lang'] = $lang; 
  } else {
    $result = mysqli_query($connect, "select id from language where active = 1 order by ord asc limit 0,1");
    if (mysqli_num_rows($result) == 0) {
      $result = mysqli_query($connect, "select id from language where admin = 1 limit 0,1");
    }
    $row = mysqli_fetch_array($result);
    $lang = $row['id'];
  }
  if (isset($_SESSION['lang'])) $lang = intval($_SESSION['lang']);
  $result = mysqli_query($connect, "select lang from language where id = $lang");
  $row = mysqli_fetch_array($result);
  $xmlLang = $row['lang'];
  
  $lang = intval($lang);
  
  if ($lang == 0) $lang = 1;
}
?>