$(document).ready(function() {
  // default
  //$('.switcher p').eq(0).css('backgroundImage', 'url(design/point_active.png)');
  // show first page
  $('.references p').hide();
  $('.references p').eq(0).show('slow');  
	var pages = $('.references p').length;
	var numPg = 1;
  // switcher
  $('.next').click(function() {
    numPg += 1;
    if (numPg > pages) {
      numPg = pages;
    }
    // content
    $('.references p').css({display: 'none'});
    $('.references p').eq(numPg - 1).fadeIn('slow'); 
  });
	$('.prev').click(function() {
    numPg -= 1;
    if (numPg < 2) {
      numPg = 1;
    }
    // content
    $('.references p').css({display: 'none'});
    $('.references p').eq(numPg - 1).fadeIn('slow'); 
  });	
});    
