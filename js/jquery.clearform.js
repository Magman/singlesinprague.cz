$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form') {
      if (type != 'hidden') {
        return $(':input',this).clearForm();
      }
    }
    if (type == 'text' || type == 'password' || tag == 'textarea') {
      this.value = '';
    } else if (type == 'checkbox') {
      this.checked = false;
    } else if (tag == 'select') {
      this.selectedIndex = 0;    
    }       
    $('.conditionForm').fadeOut('slow');
  });
};