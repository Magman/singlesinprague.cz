$(document).ready(function() {
  // default
  $('.contact').css('width', '59px');
  $('.message').css('width', '0px');
  // contact 
  $('div.messageButton').click(function() {  
    var widthMess = parseFloat($('.message').css('width'));
    if (widthMess == 0) {
      $('.contact').animate({width: '324px'}, 'slow');
      $('.message').animate({width: '265px'}, 'slow');
    } else {
      $('.contact').animate({width: '59px'}, 'slow');
      $('.message').animate({width: '0px'}, 'slow');    
    }
  });   
});    
