function start()
{
  $(".message").find("input[type='submit']").click(function(event)
  {
    event.preventDefault();  // zabranit odeslani
    var dataString = $(".message").serialize();  // prevede odesilana data do retezce (form class="message")
    $.ajax({
      type: "POST",
      url: "ajaxdata.php",
      data: dataString,
      dataType: "text",
      success: function(serverData)
      {
        $('.ajaxData').html(serverData);
      }
    });
  });
}
      
$(document).ready(start);
