$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 1000;  // How many characters are shown by default
    var ellipsestext = "...";
    //var moretext = "Show more >"; v index kvuli lang
    //var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            // puvodne misto divu spany (pro neformatovany text) + css (span)
            var html = c + '<div class="moreellipses"><strong>' + ellipsestext + '</strong>&nbsp;</div><div class="morecontent"><div>' + h + '</div>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></div>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
