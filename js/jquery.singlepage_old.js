$(document).ready(function(){      
  // first page
  $.browser.chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()); 
  if($.browser.chrome) {
    $chromeDiff = parseInt(55); 
    $limit = parseInt(845); 
  } else {
    $chromeDiff = parseInt(0); 
    $limit = parseInt(900); 
  }  
  $widthWin = parseInt(window.outerWidth);
  $heightWin = parseInt(window.outerHeight);
  $heightImg = parseInt($widthWin * 455 / 1000);
  $length = $('.slogan').css('height').length - 2;
  $heightSlogan = parseInt($('.slogan').css('height').substr(0, $length));
  $length = $('.forEn').css('height').length - 2;
  $heightFor = parseInt($('.forEn').css('height').substr(0, $length));
  if ($widthWin > 860) { // prepina blok forEn na 100 % (bloky pod sebou)
    if ($heightWin < $limit) { // 760
      $top = parseInt($heightWin - 60 - 40 - 90 - $heightSlogan - $heightFor + $chromeDiff);
      //alert($top + ' ' + $heightWin + ' ' + $heightSlogan + ' ' + $heightFor + ' ' + ' ' + $chromeDiff + ' ' + $('.coverContent').css('marginTop'));
      $('.coverContent').css('marginTop', $top + 'px'); 
      if ($heightWin < 620) {  // aby neslo do pisma 
        $('.coverContent').css('marginTop', '130px');
      }

    }
  } 
  // menu
  $('.menuJs2 a').click(function(event){
    event.preventDefault();
    $item = $('.menuJs2 a').index(this);
    $hH = parseInt($('.content').eq($item).position().top); 
    window.scrollTo(0,$hH);
    $('.menuJs2 a').css('color','black');
    $(this).css('color', '#992f4a');
  });
  $('#menuVer a').click(function(e){
    e.preventDefault();
    $itemV = $('#menuVer a').index(this);
    $hV = parseInt($('.content').eq($itemV).position().top); 
    window.scrollTo(0,$hV);
    $('#menuVer a').css('color','black');
    $(this).css('color', '#992f4a');
  });
});
