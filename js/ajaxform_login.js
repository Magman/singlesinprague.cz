function start()
{
  $(".formOrdLog").find("input.sbmReserveLog").click(function(event)
  {
    event.preventDefault();  // zabranit odeslani
    var dataString = $(".formOrdLog").serialize();  // prevede odesilana data do retezce  
    $.ajax({
      type: "POST",
      url: "ajaxlogin.php",
      data: dataString,
      dataType: "text",
      success: function(serverData)
      {        
        $('.ajaxLog').html(serverData); // vypisuje do form
        if (serverData.substr(0,4) == 'OK::') { // odfiltruje text clearForm()
          var thanksAlert = serverData.split('::');
          var thanks =  thanksAlert[1];
        }                
        if (serverData.substr(0,4) != 'OK::') {
           alert(serverData);
        } else {
          alert(thanks);
        }
      }
    });
  });
}
      
$(document).ready(start);