$(document).ready(function() {
  $('.formOrder').hide();
  // button order showEvent
  $('.buttonOrder').click(function() {
    $('.formOrder form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $('.formOrder').slideUp('slow'); // skryje vsechny otevrene 
    $block = $(this).parent().next();
    $block.children().addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this
    if ($block.is(':visible')) {
      $block.slideUp('slow');
    } else {
      $block.slideDown('slow'); 
    }
  });  
  // close button
  $('.buttonCloseForm').click(function() {
    $('.formOrder').slideUp('slow');
  });
  // clear form
  $('.buttonCloseForm').click(function(){
    $('.formOrd').clearForm();
    $('.ajaxOrder').html('');
  });
  $('.buttonOrder').click(function(){
    $('.formOrd').clearForm();
    $('.ajaxOrder').html('');
  });
  $('.formOrderEv').hide();
  // button order detailEvent
  $('.buttonOrderEv').click(function() {
    $('.formOrderEv form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $('.formOrderEv').slideUp('slow'); // skryje vsechny otevrene 
    // $blockEv = $(this).next().next().next();
    $blockEv = $(this).prev().prev().prev().prev();  
    $blockEv.children().addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this
    if ($blockEv.is(':visible')) {
      $blockEv.slideUp('slow');
    } else {
      $blockEv.slideDown('slow');
    }
  });  
  // close button
  $('.buttonCloseFormEv').click(function() {
    $('.formOrderEv').slideUp('slow');
  });
  // clear form
  $('.buttonCloseFormEv').click(function(){
    $('.formOrd').clearForm();
    $('.ajaxOrder').html('');
  });
  $('.buttonOrderEv').click(function(){
    $('.formOrd').clearForm();
    $('.ajaxOrder').html('');
  });  
}); 

