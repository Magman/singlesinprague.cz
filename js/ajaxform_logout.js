function start()
{
  $(".formOrdLogout").find("input.sbmReserveLogout").click(function(event)
  {
    event.preventDefault();  // zabranit odeslani
    var dataString = $(".formOrdLogout").serialize();  // prevede odesilana data do retezce  
    $.ajax({
      type: "POST",
      url: "ajaxlogout.php",
      data: dataString,
      dataType: "text",
      success: function(serverData)
      {        
        $('.ajaxForgot').html(serverData); // vypisuje do form
        if (serverData.substr(0,4) == 'OK::') { // odfiltruje text clearForm()
          var thanksAlert = serverData.split('::');
          var thanks =  thanksAlert[1];
        }                
        if (serverData.substr(0,4) != 'OK::') {
           alert(serverData);
        } else {
          alert(thanks);
        }
      }
    });
  });
}
      
$(document).ready(start);