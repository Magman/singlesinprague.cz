$(document).ready(function() {
  // default
  $('.detailArticl').hide();
  // slide button
  $('.buttonSlide').click(function(){
    $('.detailArticl').slideUp('slow');
    $('.buttonSlide').css('background-image', 'url(./design/button_down.png)');
    //$slideBlock = $(this).parent().parent().next().next().next();
    $slideBlock = $(this).parent().next().next();
    if ($slideBlock.is(':visible')) {
      $slideBlock.slideUp('slow');
      $(this).css('background-image', 'url(./design/button_down.png)');
    } else {
      $slideBlock.slideDown('slow');
      $(this).css('background-image', 'url(./design/button_up.png)');         
    }
  }); 
  // close button
  $('.buttonCloseDetail').click(function(){
    $('.detailArticl').slideUp('slow');
    $('.buttonSlide').css('background-image', 'url(./design/button_down.png)');   
  });
  // Events
  $('.detailArticlEv').hide();
  // slide button
  $('.buttonSlideEv').click(function(){
    $('.detailArticlEv').slideUp('slow');
    $('.buttonSlideEv').css('background-image', 'url(./design/more_info_ev.png)');
    $slideBlockEv = $(this).parent().next().next();
    if ($slideBlockEv.is(':visible')) {
      $slideBlockEv.slideUp('slow');
      $(this).css('background-image', 'url(./design/more_info_ev.png)');
    } else {
      $slideBlockEv.slideDown('slow');
      $(this).css('background-image', 'url(./design/close_info_ev.png)');         
    }
  }); 
  // close button
  $('.buttonCloseDetailEv').click(function(){
    $('.detailArticlEv').slideUp('slow');
    $('.buttonSlideEv').css('background-image', 'url(./design/more_info_ev.png)');   
  });  
});
