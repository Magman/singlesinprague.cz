// JavaScript Document
$(document).ready(function() {          
  // slideDown, slideUp menuJs
  $('.menuJs2 ul ul').hide();
  $('.menuJs2 ul li').on("mouseenter", function() {
    $(this).children("ul").slideDown("slow");
  });
  $('.menuJs2 ul li').on("mouseleave", function() {
    $(this).children("ul").stop(true, true).slideUp();
  });    
  // slide menuVer
  var widthMenu = $('#navigation').outerWidth();
  var startPos = '-' + widthMenu + 'px'; 
  $('#navigation').css({left:startPos});    
  $('#navButton').click(function() {  
    if ($('#navigation').css('left') == startPos) {
      $('#navigation').animate({left:'0px'}, 'slow');
      $.cookie("statusmenu", "1");         
    } else {
      $('#navigation').animate({left:startPos}, 'slow'); 
      $.cookie("statusmenu", "0");
    };           
  });
  if ($.cookie("statusmenu") == '1') {
    $('#navigation').css({left:'0px'});     
  };                  
});
