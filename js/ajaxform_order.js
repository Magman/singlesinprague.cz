function start()
{
  $(".formOrd").find("input.sbmReserve").click(function(event)
  {
    event.preventDefault();  // zabranit odeslani
    var dataString = $(".formOrd").serialize();  // prevede odesilana data do retezce (form class="formOrd")    
    $.ajax({
      type: "POST",
      url: "ajaxorder.php",
      data: dataString,
      dataType: "text",
      success: function(serverData)
      {        
        $('.ajaxOrder').html(serverData); // vypisuje do form
        if (serverData.substr(0,4) == 'OK::') { // odfiltruje text clearForm()
          var thanksAlert = serverData.split('::');
          var thanks =  thanksAlert[1];
        }                
        if (serverData.substr(0,4) != 'OK::') {
           alert(serverData);
        } else {
          alert(thanks);
        }
      }
    });
  });
}
      
$(document).ready(start);
