$(document).ready(function(){
  $idEvent1 = $('.forEn div.slideButton').attr('id');
  $('.forEn div.slideButton').click(function(){
    $('.formOrderEv form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $length = $idEvent1.length;         
    $block = '#event' + $idEvent1.substr(4, $length - 4); // #event celý blok event    
    $form = $block + ' .formOrderEv form'; 
    $($form).addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this    
    $scroll = parseInt($($block).position().top);
    window.scrollTo(0, $scroll); 
    $formOrder = $block + ' .formOrderEv';
    $($formOrder).slideDown('slow');
  }); 
  $idEvent2 = $('.forCs div.slideButton').attr('id');
  $('.forCs div.slideButton').click(function(){
    $('.formOrderEv form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $length = $idEvent2.length;         
    $block = '#event' + $idEvent2.substr(4, $length - 4); // #event celý blok event    
    $form = $block + ' .formOrderEv form'; 
    $($form).addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this        
    $scroll = parseInt($($block).position().top);
    window.scrollTo(0, $scroll); 
    $formOrder = $block + ' .formOrderEv';
    $($formOrder).slideDown('slow');
  }); 
  // order button
  $idEventOr1 = $('.forEn div.orderButton').attr('id');
  $('.forEn div.orderButton').click(function(){
    $('.formOrderEv form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $length = $idEventOr1.length;         
    $block = '#event' + $idEventOr1.substr(5, $length - 5);    
    $form = $block + ' .formOrderEv form'; 
    $($form).addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this        
    $scroll = parseInt($($block).position().top);
    window.scrollTo(0, $scroll); 
    $formOrder = $block + ' .formOrderEv';
    $($formOrder).slideDown('slow');
    $(".formOrd input[name=payment][value='some value']").prop("checked",true);
  }); 
  $idEventOr2 = $('.forCs div.orderButton').attr('id');
  $('.forCs div.orderButton').click(function(){
    $('.formOrderEv form').removeClass('formOrd'); // kvuli ajaxu - pouze aktivni form
    $length = $idEventOr2.length;         
    $block = '#event' + $idEventOr2.substr(5, $length - 5);    
    $form = $block + ' .formOrderEv form'; 
    $($form).addClass('formOrd'); // kvuli ajaxu - odchozi param pouze z this         
    $scroll = parseInt($($block).position().top);
    window.scrollTo(0, $scroll); 
    $formOrder = $block + ' .formOrderEv';
    $($formOrder).slideDown('slow');
    $(".formOrd input[name=payment][value='some value']").prop("checked",true);
  }); 
}); 
