$(document).ready(function() {
  // default
  $('.formRegistration').css('width', '0px');
  // contact 
  $('div.user').click(function() {  
    var width = parseFloat($('.formRegistration').css('width'));
    if (width == 0) {
      $('.formRegistration').animate({width: '370px'}, 'slow');
    } else {
      $('.formRegistration').animate({width: '0px'}, 'slow');  
    }
  }); 
  $('.closeFormReg').click(function() {
    $('.formRegistration').animate({width: '0px'}, 'slow');
  });  
});    
