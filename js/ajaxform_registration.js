function start()
{
  $(".formOrdReg").find("input.sbmReserveReg").click(function(event)
  {
    event.preventDefault();  // zabranit odeslani
    var dataString = $(".formOrdReg").serialize();  // prevede odesilana data do retezce  
    $.ajax({
      type: "POST",
      url: "ajaxreg.php",
      data: dataString,
      dataType: "text",
      success: function(serverData)
      {        
        $('.ajaxReg').html(serverData); // vypisuje do form
        if (serverData.substr(0,4) == 'OK::') { // odfiltruje text clearForm()
          var thanksAlert = serverData.split('::');
          var thanks =  thanksAlert[1];
        }                
        if (serverData.substr(0,4) != 'OK::') {
           alert(serverData);
        } else {
          alert(thanks);
        }
      }
    });
  });
}
      
$(document).ready(start);
