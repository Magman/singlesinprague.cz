$(document).ready(function() {
  // default
  $('.formRegistration').css('width', '0px');
  // contact 
  $('div.userReg').click(function() {  
    var width = parseFloat($('.formRegistration').css('width'));
    if (width == 0) {
      $('.formRegistration').animate({width: '370px'}, 'slow');
      $('.formLogin').animate({width: '0px'}, 'slow');
    } else {
      $('.formRegistration').animate({width: '0px'}, 'slow');  
    }
  }); 
  $('.closeFormReg').click(function() {
    $('.formRegistration').animate({width: '0px'}, 'slow');
  }); 
  // default
  $('.formLogin').css('width', '0px'); 
  // login
  if ($.cookie('login') == '1') {
    $('.formLogin').css({width: '370px'});
    $('.formRegistration').css({width: '0px'});   
  }
  $('div.userLog').click(function() {  
    $('.formOrdLog').show();
    $('.formForgot').hide();     
    var width = parseFloat($('.formLogin').css('width'));
    if (width == 0) {
      $('.formLogin').animate({width: '370px'}, 'slow');
      $('.formRegistration').animate({width: '0px'}, 'slow'); 
      $.cookie('login', '1');
    } else {
      $('.formLogin').animate({width: '0px'}, 'slow'); 
      $.cookie('login', '0'); 
    }
  }); 
  $('.closeFormLog').click(function() {
    $('.formLogin').animate({width: '0px'}, 'slow');
    $.cookie('login', '0'); 
  });  
  $('#changeForm').click(function() { 
    $('.formRegistration').animate({width: '370px'}, 'slow');
    $('.formLogin').animate({width: '0px'}, 'slow'); 
    $.cookie('login', '0'); 
  }); 
  $('.formForgot').hide();
  $('#changeForgot').click(function() { 
    $('.formForgot').fadeIn('slow');
    $('.formOrdLog').hide();
  }); 
  $('.formOwnPass').hide();
  $('.pChangePass').click(function() { 
    $('.formOwnPass').slideToggle('slow');
  }); 
});    
