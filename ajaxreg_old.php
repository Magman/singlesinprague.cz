<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

$sent = 0;

switch ($lang) {
  case 1: $phName = 'You have not entered your name!'; $phSurname = 'You have not entered your surname!'; $phEmail = 'The format of the e-mail is incorrect!'; $phPhone = 'You have not entered your phone!'; $phBirth = 'You have not entered your year of birth!'; $phOrder = 'OK::Thanks for your registration.::';  $phFailData = 'Error! Data transfer failed.'; $phAgree = 'Check your agreement width terms and conditions.';  $phCode = 'The control code is not correct.'; $phExist = 'Registration on this email exist.';
  break;
  case 2: $phName = 'Nezadali jste jméno!'; $phSurname = 'Nezadali jste příjmení!'; $phEmail = 'E-mail není ve správném tvaru!'; $phPhone = 'Nezadali jste telefon!'; $phBirth = 'Nezadali jste rok narození!'; $phOrder = 'OK::Děkujeme Vám za Vaší registraci.::'; $phFailData = 'Chyba! Přenos dat se nezdařil.'; $phAgree = 'Potvrďte váš souhlas s obchodními podmínkami.'; $phCode = 'Kontrolní kód nebyl zadán správně.'; $phExist = 'Registrace na tento email již existuje';
}  

if (isset($_POST['captcha_code'])) {

  if (empty($_POST['name'])) { 
      
    echo $phName;
    
  } else if (empty($_POST['surname'])) { 
      
    echo $phSurname;
    
  } else if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $_POST['email'])) {
      
    echo $phEmail;
         
  } else if (empty($_POST['phone'])) { 
      
    echo $phPhone;
    
  } else if (empty($_POST['birth'])) { 
      
    echo $phBirth;
    
  } else if (!isset($_POST['agree'])) { 
      
    echo $phAgree;
    
  } else {            
            
    require_once './securimage/securimage.php';
                
    $securimage = new Securimage();
    
    if ($securimage->check($_POST['captcha_code']) == false) {
              
      echo $phCode;
            
    } else { 
    
      $emailArr = null;
      
      $result = mysqli_query($db_connect, "select email from registration");
      
      while ($row = mysqli_fetch_array($result)) {
        
        $emailArr[] = $row['email'];
      
      }
      
      $exist = 1;
      
      if ($emailArr != null) {
      
        if (!in_array($_POST['email'], $emailArr)) $exist = 0;
      
      } else { // tabulka je prazdna
      
        $exist = 0;
      
      }
      
      if ($exist == 0) {
        
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = null;
        for ($i = 0; $i < 7; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        $personaly = md5($randomString);
        
        $uid = rand(121031239, 998989798);
        
        $insert = mysqli_query($db_connect, "insert into registration (uid, name, surname, email, phone, birth, sex, category, personaly) values ($uid, '"
        .mysqli_real_escape_string($db_connect, $_POST['name'])
        ."','"
        .mysqli_real_escape_string($db_connect, $_POST['surname'])
        ."','"
        .mysqli_real_escape_string($db_connect, $_POST['email'])
        ."','"
        .mysqli_real_escape_string($db_connect, $_POST['phone'])
        ."','"
        .mysqli_real_escape_string($db_connect, $_POST['birth'])
        ."', {$_POST['sex']}, {$_POST['category']}, '$personaly')"); 
                   
        if (!$insert) {
            
          echo $phFailData;  
        
        } else {  
          
          $result = mysqli_query($db_connect, "select id from registration order by id desc limit 0,1");
          
          $row = mysqli_fetch_array($result);
          
          $id = $row['id'];
          
          $md5Id = md5($id);
          
          $update = mysqli_query($db_connect, "update registration set md5Id = '$md5Id' where id = {$row['id']}");
          
          if (!$update) {
          
            echo $phFailData;
          
          } else {
            
            require_once './lib/Message/Registration.php';
              
            $registration = new Registration ($db_connect, $lang, $mailArr = array('lecornu.klara@gmail.com', 'singles@singlesinprague.cz', $_POST['email']), $addBcc = 'mailing@prima-e-shop.com', $mailServer = 'out.smtp.cz', $path = './'); 
            
            $registration->mailRegistration ($row['id']);     
               
            $sent = 1; 
          
            echo $phOrder; 
            
          }
            
        } 
      
      } else {
      
        echo $phExist;
      
      }
          
    }
    
  }    
                             
}

if ($sent == 1) { ?>
  <script type="text/javascript">
    $('.formOrdReg').clearForm();
  </script>
<?php } ?>
