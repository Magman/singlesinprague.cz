<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

require_once './include/parameter.php';

if (isset($_GET['id'])) { // id menu
  $id = $_GET['id']; 
  $defAct = 1; 
} else {
  $id = 1; 
  $defAct = 0;
}
require_once './lib/Text/Text.php';
$text = new Text ($db_connect, $id, $lang);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $xmlLang; ?>" lang="<?php echo $xmlLang; ?>">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo $text->showDescription(); ?>" />
    <meta name="keywords" content="<?php echo $text->showKeywords(); ?>" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/main.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/color.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/css/lib.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/SwitcherLanguage/css/switcher.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Menu/css/menu.css" />
    <!-- <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Gallery/css/gallery.css" /> -->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Message/css/message.css" />
    <link rel="stylesheet" type="text/css" href="./css/jquery.lightbox-0.5.css" />
    <title><?php echo $text->showTitle(); ?></title>  
    <script type="text/javascript">
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script> 
    <script type="text/javascript" src="./js/jquery1.10.2.js"></script>
    <script type="text/javascript" src="./js/cookies.js"></script>
    <script type="text/javascript" src="./js/ajaxform.js"></script>
    <script type="text/javascript" src="./js/ajaxform_order.js"></script>
    <script type="text/javascript" src="./js/ajaxform_registration.js"></script>
    <script type="text/javascript" src="./js/jquery.clearform.js"></script>
    <script type="text/javascript" src="./js/jquery.accordion.js"></script>
    <script type="text/javascript" src="./js/jquery.eventlink.js"></script>
    <script type="text/javascript" src="./js/jquery.formorder.js"></script>
    <script type="text/javascript" src="./js/jquery.formreg.js"></script>
    <script type="text/javascript" src="./js/jquery.adver.js"></script>
    <script type="text/javascript" src="./js/jquery.newtab.js"></script>
    <script type="text/javascript" src="./js/jquery.window.js"></script>
    <script type="text/javascript" src="./js/menu.js"></script>
    <script type="text/javascript" src="./js/jquery.browser.js"></script> 
    <script type="text/javascript" src="./js/jquery.singlepage.js"></script> 
    <script type="text/javascript" src="./js/jquery.lightbox-0.5.js"></script>
    <script type="text/javascript">
      $(function() {
      	$('#gallery a').lightBox({fixedNavigation:true});
      });
    </script> 
    <?php 
    if (isset($_GET['it'])) { 
      $item = $_GET['it']; ?>
      <script type="text/javascript">
        $(document).ready(function(){ // from galleries
          $blockFrom = '#item' + <?php echo $item; ?>;
          $hId = parseInt($($blockFrom).position().top);
          window.scrollTo(0,$hId);
        });
      </script>
    <?php } ?>
    <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');    
      ga('create', 'UA-75996534-1', 'auto');
      ga('send', 'pageview');    
    </script>    
  </head>
  <body>       
    <div class="centerer">              
      <div class="heaDer">          
        <a href="index.php" title="home"><img class="logo" src="./design/logo.png" alt="logo" /></a>                                        
        <div class="coverMenuJs">  
          <div class="menuJs2">
            <?php 
            require_once './lib/Menu/MenuTop.php';                          
            $menu = new MenuTop ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu; $xmlLang, $parHotel kvůli booking, $path ../ or ./ menu mutace 
            echo $menu->render (); 
            ?>      
          </div> <!-- .menu --> 
        </div>       
        <div id="navButton"><img src="./design/nav_button.gif" alt="menu" title="menu" /></div>
        <div id="navigation">        
          <div id="menuVer">
            <?php
            require_once './lib/Menu/MenuTopVer.php'; 
            $menu = new MenuTopVer ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu 
            echo $menu->render(); 
            ?>
          </div>  
        </div>
        <div class="switcher">
            <?php         
            //$parHotelBu = $parHotel;
            require_once './lib/SwitcherLanguage/SwitcherLanguage.php';
            if (isset($_GET['album'])) $idA = $_GET['album']; else $idA = 0;
            $switcher = new SwitcherLanguage ($db_connect, $lang, $id, $link = 'index.php', $idA, $path = './');
            $switcher->showLanguages ($parHotel = null);
            //$parHotel = $parHotelBu;
            ?> 
        </div> <!-- .switcher -->       
      </div>
      <!-- <div class="user"> -->
        <?php
        /* switch ($lang) {
          case 1: $phReg = 'Sign up';
          break;
          case 2: $phReg = 'Registrace'; 
        }
        echo $phReg; */
        ?>
      <!-- </div> -->                     
      <div class="slogan">
        <?php
        switch ($lang) {
          case 1: $sloganMain = 'Ladies and gentlemen,<br />welcome to Singles in Prague!';
          $sloganSub = '<br />Singles in Prague is a website for singles events in Prague oriented towards singles who are 30 and older. We have events for both Czech and English speaking singles, 30+ and 45+.';
          break;
          case 2: $sloganMain = 'Dámy a pánové,<br />vítáme Vás na Singles in Prague!';
          $sloganSub = '<br />Tato webová stránka je určena pro nezadané nad 30 let. Organizujeme společenské akce v českém a anglickém jazyce, věk 30+ a věk 45+.';
        }
        ?>       
        <h1><?php echo $sloganMain; ?></h1>
        <h2 id="sloganH2"><?php echo $sloganSub; ?></h2>
      </div>
      <div class="coverContent">      
        <?php        
        if ($id == 1) {  
          // banners     
          require_once('./lib/Event/ShowEvent.php');
          $event = new ShowEvent($db_connect);

          echo '<div class="forEn">';
                
            $event->event($limit = 1, $lang);          
              
          echo '</div>';
              
          echo '<div class="forCs">'; 
      
            $event->event($limit = 2, $lang);
      
          echo '</div>'; 
        } 
  
        switch ($lang) {          
          case 1: $phBack = 'back'; $phFb = 'You can receive a 10% discount on your entry if you follow us on the social media.';
          break;
          case 2: $phBack = 'zpět'; $phFb = 'Získejte 10% slevu na Vašem vstupu, když nás následujete na sociálních médiích.';         
        }
              
        echo '<div class="content" id="item1">';  
                                    
          $text = new Text ($db_connect, $id = 1, $lang);
          echo $text->showText(); 

          echo '<p class="back"><a href="index.php" title="back">'.$phBack.'</a></p>';
                   
          //echo '<div class="coverFb"><div class="fb-like" data-href="https://www.facebook.com/www.SinglesInPrague.cz" data-send="false" data-layout="button_count" data-width="70" data-show-faces="false"></div>'.$phFb.'</div>';
                                        
          echo '<h3>'.$phFb.'</h3>'; 
          
          echo '<table class="socMedia">';

            echo '<tr>';
              
              echo '<td>';
              
                echo '<div class="fb-like" data-href="https://www.facebook.com/singlesinpraguecz" data-send="false" data-layout="button_count" data-width="70" data-show-faces="false"></div>';  
              
              echo '</td>';
              
              echo '<td>';
              
                echo '<a class="newTab" href="https://www.instagram.com/singlesinprague/" title="Instagram"><img src="./design/instagram.png" alt="Instagram" /></a>';
              
              echo '</td>';
              
              echo '<td>';
              
                echo '<a class="newTab" href="https://twitter.com/SinglesInPrague" title="Twitter"><img src="./design/twitter.png" alt="Twitter" /></a>';
              
              echo '</td>';
              
              echo '<td>';
              
                echo '<a class="newTab" href="https://www.linkedin.com/company/9188560?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9188560%2Cidx%3A2-1-2%2CtarId%3A1468259458960%2Ctas%3Asingles%20in%20prague" title="LinkedIn"><img src="./design/linkedin.png" alt="LinkedIn" /></a>';
              
              echo '</td>';
              
            echo '</tr>';
            
          echo '</table>';        
        
        
        echo '</div>';
        
        // Events        
        echo '<div class="content" id="item3">';  
                  
          require_once('./lib/Event/DetailEvent.php');
          $detailEvent = new DetailEvent($db_connect, $lang);
          
          $detailEvent->detEvent();
          
          //echo '<p class="back"><a href="index.php?id=1&amp;lang='.$lang.'" title="back">'.$phBack.'</a></p>';
          echo '<p class="back"><a href="index.php" title="back">'.$phBack.'</a></p>';
              
        echo '</div>';
        
        $result = mysqli_query($db_connect, "select distinct text_1.idMenu from text_1, menu_top_1 where menu_top_1.active != 0 and text_1.idMenu = menu_top_1.id and (menu_top_1.id > 1 and menu_top_1.id != 3) order by menu_top_1.ord asc");
        
        while ($row = mysqli_fetch_array($result)) {
        
          echo '<div class="content" id="item'.$row['idMenu'].'">';  
            
            if ($row['idMenu'] == 2) {
            
              switch ($lang) {
                case 1: $phGallery = 'Galleries';
                break;
                case 2: $phGallery = 'Galerie';
              }
              
              echo '<h1>'.$phGallery.'</h1>';
              
              echo '<div class="albums">';
              
                $albums = mysqli_query($db_connect, "select gallery_album_1.id, gallery_album_1.idEvent from gallery_album_1, events where gallery_album_1.idEvent = events.id order by events.dateEvent desc");
                
                while ($abs = mysqli_fetch_array($albums)) {
                
                  $images = mysqli_query($db_connect, "select nameImage from gallery where idAlbum = {$abs['id']} order by dateInsert desc limit 0,1");
                  $img = mysqli_fetch_array($images);
                  
                  echo '<div class="album">';
                                      
                    $tb = 'gallery_album_'.$lang;
                    
                    $title = mysqli_query($db_connect, "select item from `$tb` where id = {$abs['id']}");
                    $tt = mysqli_fetch_array($title);
                    
                    echo '<a href="gallery.php?album='.$abs['id'].'&amp;id=2" title="album"><img src="./gallery/'.$abs['id'].'/thumb_'.$img['nameImage'].'" alt="'.$tt['item'].'" /></a>';

                    echo '<div class="albumName">'; 
                    
                      echo  $tt['item'];
                    
                    echo '</div>'; 
                  
                  echo '</div>';
                                
                }
              
              echo '</div>';
            
            } else {
            
              $text = new Text ($db_connect, $id = $row['idMenu'], $lang);
              echo $text->showText(); 
            
            }            
            //echo '<p class="back"><a href="index.php?id=1&amp;lang='.$lang.'" title="back">'.$phBack.'</a></p>';
            echo '<p class="back"><a href="index.php" title="back">'.$phBack.'</a></p>';
              
          echo '</div>';
        
        }      
        ?>
      </div>
      <div class="footer smallFont">
        <div class="footerIn">        
          <div class="copy">
            <?php 
            switch ($lang) {
              case 1: $phDiscount = '<h2>50%off if you bring a friend</h2>'; $cond = 'Terms and conditions';
              break;
              case 2: $phDiscount = '<h2>50% sleva, když přivedete kamaráda</h2>'; $cond = 'Obchodní podmínky';
            }
            ?>
            &copy; Singles in Prague 2016 | powered by <a class="newTab" href="http://www.easyweb4u.cz" title="easyWeb4U">easyWeb4U.cz</a> | <span class="cond"><?php echo $cond ?></span>
          </div>  
        </div>
      </div> 
      <div class="conditionFoo">
        <img id="delWin" src="./design/delete.png" alt="hide" />
        <?php 
        $text = new Text ($db_connect, $id = 105, $lang);
        echo $text->showText(); 
        ?>
      </div>  
      <div class="formRegistration">
        
        <div class="coverClose"><?php echo $phReg; ?><img class="closeFormReg" src="./design/close_form.png" alt="close" /></div>
        
        <form class="formOrdReg" method="post" action="">
      
          <table> 
                            
            <?php
            switch ($lang) {
              case 1: $phName = 'Name'; $phPhone = 'Phone'; $phSurname = 'Surname'; $phDateBirth = 'Year of Birth'; $phMale = 'Male';  $phFemale = 'Female';  $phAgree = 'I agree with terms and conditions'; $phCat = 'Category'; $phCode = 'Rewrite the control code'; $phSend = 'Send';
              break;
              case 2: $phName = 'Jméno'; $phPhone = 'Telefon'; $phSurname = 'Příjmení';  $phDateBirth = 'Rok narození'; $phMale = 'Muž'; $phFemale = 'Žena'; $phAgree = 'Souhlasím s obchodními podmínkami'; $phCat = 'Kategorie';  $phCode = 'Opište kontrolní kód'; $phSend = 'Odeslat';   
            }      
            ?>            
                    
            <tr><td colspan="2"><p class="ajaxReg"></p></td></tr>
                              
            <tr><td class="tdFormReg"><strong><?php echo $phName; ?>:</strong> </td><td><input class="inpReserve" type="text" name="name" value="" /></td></tr>
                      
            <tr><td><strong><?php echo $phSurname; ?>:</strong> </td><td><input class="inpReserve" type="text" name="surname" value="" /></td></tr>
                                
            <tr><td><strong>E-mail:</strong> </td><td><input class="inpReserve" type="text" name="email" value="" /></td></tr>
                                
            <tr><td><strong><?php echo $phPhone; ?>:</strong> </td><td><input class="inpReserve" type="text" name="phone" value="" /></td></tr>
                      
            <tr><td><strong><?php echo $phDateBirth; ?>:</strong> </td><td><input class="inpYear" type="text" name="birth" value="" /></td></tr>
                      
            <tr><td><strong><?php echo $phFemale.'/'.$phMale; ?>:</strong></td><td>
                      
              <select name="sex">
                          
                <option value="1"><?php echo $phFemale; ?></option>
                            
                <option value="2"><?php echo $phMale; ?></option>
                          
              </select>
                        
            </td></tr>
            
            <tr><td><strong><?php echo $phCat; ?>:</strong></td><td>
                      
              <select name="category">
                          
                <?php
                $result = mysqli_query($db_connect, "select * from category_1 order by id");
                
                while ($row = mysqli_fetch_array($result)) {
                          
                  $tb = 'category_'.$lang;
                  
                  $ctg = mysqli_query($db_connect, "select category from `$tb` where id = {$row['id']}");
                  
                  $cg = mysqli_fetch_array($ctg);
                  
                  if ($row['id'] == 4) {
                  
                    switch ($lang) { // moc dlouhé
                    
                      case 1: $category = 'Lesbian singles event';
                      break;
                      case 2: $category = 'Seznamovací večer pro lesbičky';             
                    }
                  
                  } else {
                  
                    $category = $cg['category'];           
                  
                  }
                  
                  echo '<option value="'.$row['id'].'">'.$category.'</option>';
                  
                }
                
                ?>
                                      
              </select>
                        
            </td></tr>
                                   
            <tr>
                    
              <td colspan="2">
                      
                <div class="conditionForm" id="cfReg">                   
      
                  <img class="delWinForm" src="./design/delete.png" alt="hide" />
                          
                  <?php
                  $text = new Text ($db_connect, $id = 105, $lang);
                  echo $text->showText(); 
                  ?>
                        
                </div>
                        
                <div class="coverPhAgree"><span class="phAgree"><strong><?php echo $phAgree; ?></strong></span> <input type="checkbox" name="agree" /></div>
                        
              </td>
                      
            </tr>
                    
            
            <tr>
            
              <td colspan="2">
              
                <div class="control">
                    
                  <div class="pict">
                  
                    <img id="captcha" src="./securimage/securimage_show.php" alt="CAPTCHA Image" />
                      
                    <div class="refresh"><a href="#" title="Nový kód" onclick="document.getElementById('captcha').src = './securimage/securimage_show.php?' + Math.random(); return false"><img src="./securimage/images/refresh.png" alt="refresh" /></a></div>
                    
                  </div>
                    
                  <div class="field">
                  
                    <input type="text" name="captcha_code" value="<?php echo $phCode ?>" onclick="if(!this.__click) {this.__click = true;this.value = ''}" />
                  
                  </div>
                  
                </div>
               
              </td>
              
            </tr>     
            
            <tr><td colspan="2"><input class="sbmReserveReg" type="submit" value="<?php echo $phSend; ?>" /></td></tr>
                            
         </table>
                                
        </form>      
      
      </div>       
    </div> 
    <div class="adver"> <!-- hide() v jquery.adver.js -->
      <img id="delAdv" src="./design/button_close_banner.png" alt="hide" />
      <?php echo $phDiscount; ?>
    </div>     
  </body>
</html>