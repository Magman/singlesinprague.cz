<?php
require_once './function/function_connect.php';  
connect();

$success = 0;

if (isset($_GET['confirm'])) {

  $result = mysqli_query($db_connect, "update registration set `confirm` = 1 where md5Id = '".mysqli_real_escape_string($db_connect, $_GET['confirm'])."'");
  
  if ($result) $success = 1;

}


if (isset($_GET['lang'])) $lang = intval($_GET['lang']); else $lang = 1;

if ($lang == 1) {
  $xmlLang = 'en';
  $description = 'Confirm of registration';
  $title = 'registration | Singles in Prague';
  $confirm = 'Thank you for confirming your registration to our website <a href="http://www.singlesinprague.cz/index.php?lang=1" title="SinglesInPrague">SinglesInPrague.cz</a>.';
  $noConfirm = 'Error! Confirmation failed. <a href="http://www.singlesinprague.cz/index.php?lang=1" title="back">Back</a>.';
} else {
  $xmlLang = 'cs';
  $description = 'Potvrzení registrace';
  $title = 'registrace | Singles in Prague';
  $confirm = 'Potvrdili jste Vaší registraci do webu <a href="http://www.singlesinprague.cz/index.php?lang=2" title="SinglesInPrague">SinglesInPrague.cz</a>.';
  $noConfirm = 'Chyba! Ověření se nazdařilo. <a href="http://www.singlesinprague.cz/index.php?lang=2" title="zpět">Zpět</a>.';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $xmlLang; ?>" lang="<?php echo $xmlLang; ?>">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo $description; ?>" />
    <meta name="keywords" content="Singles in Prague" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/main.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/color.css" />
    <title><?php echo $title; ?></title>
    <style type="text/css">
      body {background:none !important; }
    </style>      
  </head>
  <body>       
    <div class="centerer">              
      <div class="heaDer">          
        <a href="index.php" title="home"><img class="logo" src="./design/logo.png" alt="logo" /></a>                                              
      </div>
      <div class="confirm">
        <?php 
        if ($success == 1) {
        
          echo $confirm; 
          
        } else {
                
          echo $noConfirm.' '.$_GET['confirm'];
        
        }
        ?>
      </div>
      <div class="footer smallFont">
        <div class="footerIn">        
          <div class="copy">
            &copy; Singles in Prague 2016 | powered by <a class="newTab" href="http://www.easyweb4u.cz" title="easyWeb4U">easyWeb4U.cz</a>
          </div>  
        </div>
      </div>       
    </div>     
  </body>
</html>