<?php
session_start();

require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

require_once './include/parameter.php';

$sent = 0;

if (isset($_POST['logout'])) {

  switch ($lang) {
    case 1: $phSuccess = 'OK::Your log out was succesfully completed.::';
    break;
    case 2: $phSuccess = 'OK::Vaše odhlášení bylo úspěšné.::';   
  }
    
  if ($_POST['logout'] == 1) {
  
    unset ($_SESSION['user']);
    
    $sent = 1;
    
    echo $phSuccess;
  
  }

}

if ($sent == 1) { ?>
  <script type="text/javascript"> 
    $('.formOrdLogout').clearForm();  
    location.reload(); 
  </script>
<?php } ?>