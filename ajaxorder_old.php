<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

$sent = 0;

switch ($lang) {
  case 1: $phName = 'You have not entered your name!'; $phSurname = 'You have not entered your surname!'; $phEmail = 'The format of the e-mail is incorrect!'; $phBirth = 'You have not entered your year of birth!'; $phOrder = 'OK::Thanks for your reservation.::'; $phFailData = 'Error! Data transfer failed.'; $phVoucher = 'Voucher has incorrect format.';  $phAgree = 'Check your agreement width terms and conditions.';
  break;
  case 2: $phName = 'Nezadali jste jméno!'; $phSurname = 'Nezadali jste příjmení!'; $phEmail = 'E-mail není ve správném tvaru!'; $phBirth = 'Nezadali jste rok narození!'; $phOrder = 'OK::Děkujeme Vám za Vaší rezervaci.::'; $phFailData = 'Chyba! Přenos dat se nezdařil.'; $phVoucher = 'Voucher nemá správný tvar.'; $phAgree = 'Potvrďte váš souhlas s obchodními podmínkami.';
}  
   
$exist = 0;

if (isset($_POST['voucher'])) {

  $voucher = $_POST['voucher'];
  
  if (!empty($voucher)) {
    
    $exist = 1;
            
  } else {
  
    $voucher = '0';
  
  }

} else {

  $voucher = '0';

}

if (empty($_POST['name'])) { 
    
  echo $phName;
  
} else if (empty($_POST['surname'])) { 
    
  echo $phSurname;
  
} else if (!preg_match("/^[^@]+@[^@]+[.][a-zA-Z]+$/", $_POST['email'])) {
    
  echo $phEmail;
       
} else if (!preg_match("/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/", $voucher) and $exist == 1) {
    
  echo $phVoucher;
       
} else if (!isset($_POST['agree'])) { 
    
  echo $phAgree;
  
} else {            
        
  if (isset($_POST['idEvent'])) {      
                 
    $result = mysqli_query($db_connect, "select dateEvent, hour, minute, place, address from events where id = {$_POST['idEvent']}");
    $row = mysqli_fetch_array($result);
    $dateEvent = $row['dateEvent'];
    $hour = $row['hour'];
    $minute = $row['minute'];
    $place = $row['place'];
    $address = $row['address'];
       
    $insert = mysqli_query($db_connect, "insert into orders (idEvent, dateEvent, hour, minute, place, address, name, surname, email, phone, birth, sex, payment, voucher) values ({$_POST['idEvent']}, '$dateEvent', $hour, '$minute', '$place', '$address', '"
    .mysqli_real_escape_string($db_connect, $_POST['name'])
    ."','"
    .mysqli_real_escape_string($db_connect, $_POST['surname'])
    ."','"
    .mysqli_real_escape_string($db_connect, $_POST['email'])
    ."','"
    .mysqli_real_escape_string($db_connect, $_POST['phone'])
    ."','"
    .mysqli_real_escape_string($db_connect, $_POST['birth'])
    ."', {$_POST['sex']}, {$_POST['payment']}, '$voucher')"); 
    
    $result = mysqli_query($db_connect, "select id from orders order by id desc limit 0,1");
    $row = mysqli_fetch_array($result);
     
    $insert = mysqli_query($db_connect, "update orders set orderNo = {$row['id']} where id = {$row['id']}"); 
        
    if (!$insert) {
        
      echo 'Error! Insert of order failed.';
        
    } else {
      
      require_once './lib/Order/Order.php';
      $order = new Order ($db_connect, $lang, $mailArr = array('lecornu.klara@gmail.com', 'singles@singlesinprague.cz', $_POST['email']), $addBcc = 'mailing@prima-e-shop.com', $mailServer = 'out.smtp.cz', $path = './');        
      $order->mailOrder ();     
      
      $sent = 1; 

      echo $phOrder;      
        
    }     
      
  } else {
        
    echo $phFailData;
      
  }                      

}

if ($sent == 1) { ?>
  <script type="text/javascript">
    $('.formOrd').clearForm();
  </script>
<?php } ?>
