<?php 
session_start(); 
require_once '../include/protect_sess.php';
require_once '../function/function_connect.php';
connect();
require_once '../include/parameter.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../favicon.ico" />
    <meta name="description" content="administrace systému SinglesInPrague" />
    <meta name="keywords" content="SinglesInPrague" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/admin.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/color_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/pagination.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/account.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/css/lib.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/Help/css/help.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/Setting/css/setting.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/Menu/css/menu_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/Gallery/css/gallery.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/ui-lightness/jquery-ui-1.10.1.custom.min.css" />   
    <script type="text/javascript" src="../js/jquery1.10.2.js"></script>
    <script type="text/javascript" src="../js/cookies.js"></script>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../js/del_checked.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
    <script type="text/javascript" src="../js/jquery.pagination1.0.5.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.min.js"></script>
    <script type="text/javascript">
      $(function() {
        $( "#datepicker" ).datepicker();
      });
    </script>  
    <script type="text/javascript">
      $(document).ready(function() 
        { 
          $("#tabOrder").tablesorter(); 
          $("#tabNewsletter").tablesorter({sortList: [[0,1]]});
          $("#tabShowEmail").tablesorter({sortList: [[0,0]]});
          $("#tabEmail").tablesorter({sortList: [[0,0]]});
          $("#tabEventEmail").tablesorter({sortList: [[0,0]]});  
        } 
      ); 
	  </script>                    
    <title>administrace | Singles In Prague</title>    
  </head>
  <body>
    <div class="centererAdmin">
      <div class="languageAdmin">
        <a href="./index_admin.php" title="Home"><img src="../design/logo2.png" alt="Logo" /></a>
      </div> <!-- .language -->
      <?php require_once '../include/menuadmin.php'; ?>