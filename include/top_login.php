<?php require_once '../include/parameter.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="../favicon.ico" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/main.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/admin.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../css/color_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="../lib/Menu/css/menu_new.css" />                       
    <title>administrace | Singles in Prague></title>    
  </head>
  <body>
    <div class="centererAdmin">
      <div class="languageAdmin">
        <a href="./index_admin.php" title="Home"><img src="../design/logo2.png" alt="Logo" /></a>
      </div> <!-- .language -->