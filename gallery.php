<?php
session_start();
require_once './function/function_connect.php';  
connect();

require_once './function/function_language.php';
language($db_connect);

require_once './include/parameter.php';

if (isset($_GET['id'])) { // id menu
  $id = intval($_GET['id']); 
} else {
  $id = 1; 
}

require_once './lib/Text/Text.php';
$text = new Text ($db_connect, $id, $lang);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $xmlLang; ?>" lang="<?php echo $xmlLang; ?>">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="./favicon.ico" />
    <meta name="description" content="<?php echo $text->showDescription(); ?>" />
    <meta name="keywords" content="<?php echo $text->showKeywords(); ?>" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/main_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./css/color_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/css/lib.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/SwitcherLanguage/css/switcher_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Menu/css/menu_new.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="./lib/Gallery/css/gallery_new.css" />
    <link rel="stylesheet" type="text/css" href="./css/jquery.lightbox-0.5.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin-ext" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script&amp;subset=latin-ext" rel="stylesheet" />
    <title><?php echo $text->showTitle(); ?></title>  
    <script type="text/javascript">
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script> 
    <script type="text/javascript" src="./js/jquery1.10.2.js"></script>
    <script type="text/javascript" src="./js/cookies.js"></script>
    <script type="text/javascript" src="./js/menu.js"></script>
    <script type="text/javascript" src="./js/jquery.lightbox-0.5.js"></script>
    <script type="text/javascript">
      $(function() {
      	$('#gallery a').lightBox({fixedNavigation:true});
      });
    </script>     
  </head>
  <body>       
    <div class="centerer">              
      <div class="heaDer">          
        <a href="index.php" title="home"><img class="logo" src="./design/logo_sip.png" alt="logo" /></a>                                        
        <div class="coverMenuJs">  
          <div class="menuJs2">
            <?php 
            require_once './lib/Menu/MenuTop.php';                          
            $menu = new MenuTop ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu; $xmlLang, $parHotel kvůli booking, $path ../ or ./ menu mutace 
            echo $menu->render (); 
            ?>      
          </div> <!-- .menu --> 
        </div>       
        <div id="navButton"><img src="./design/nav_button.gif" alt="menu" title="menu" /></div>
        <div id="navigation">        
          <div id="menuVer">
            <?php
            require_once './lib/Menu/MenuTopVer.php'; 
            $menu = new MenuTopVer ($db_connect, $lang, $id, $xmlLang, $parHotel, $path = './'); // id menu 
            echo $menu->render(); 
            ?>
          </div>  
        </div>
        <div class="switcher">
            <?php         
            //$parHotelBu = $parHotel;
            require_once './lib/SwitcherLanguage/SwitcherLanguage.php';
            if (isset($_GET['album'])) $idA = $_GET['album']; else $idA = 0;
            $switcher = new SwitcherLanguage ($db_connect, $lang, $id, $link = 'index.php', $idA, $path = './');
            $switcher->showLanguages ($parHotel = null);
            //$parHotel = $parHotelBu;
            ?> 
        </div> <!-- .switcher -->       
      </div>               
      <div class="slogan">
        <?php
        switch ($lang) {
          case 1: $sloganMain = 'Ladies and gentlemen,<br />welcome to Singles in Prague!';
          $sloganSub = '<br />Singles in Prague is a website for singles events in Prague oriented towards singles who are 30 and older. We have events for both English and Czech speaking singles.';
          break;
          case 2: $sloganMain = 'Dámy a pánové,<br />vítáme Vás na Singles in Prague!';
          $sloganSub = '<br />Tato webová stránka je určena pro nezadané nad 30 let. Organizujeme společenské akce v českém a anglickém jazyce.';
        }
        ?>       
        <h1><?php echo $sloganMain; ?></h1>
        <h2 style="line-height:1.5"><?php echo $sloganSub; ?><h2>
      </div>
      <img class="slider" src="./design/slider1.gif" alt="slider" />
      <div class="coverContent">      
        <?php          
        switch ($lang) {          
          case 1: $phBack = 'back';
          break;
          case 2: $phBack = 'zpět';          
        }
              
        echo '<div class="content">';  
            
          require_once('./lib/Gallery/Gallery.php');
          $gallery = new Gallery($db_connect, $lang, $id);
          
          $gallery->showPhoto($album = intval($_GET['album']), $sort = 1);
          
          //echo '<p class="back"><a href="index.php?id=1&lang='.$lang.'" title="back">'.$phBack.'</a></p>';
              
        echo '</div>';
                     
        ?>
      </div>
      <div class="footer smallFont">
        <div class="footerIn">        
          <div class="copy">
            &copy; Singles in Prague 2016 | powered by <a href="http://www.easyweb4u.cz" title="easyWeb4U">easyWeb4U.cz</a>
          </div>  
        </div>
      </div> 
    </div> 
  </body>
</html>